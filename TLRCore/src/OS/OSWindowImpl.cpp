#include <Core.h>
#include <OS/OSWindow.h>
#include <OS/OSWindowImpl.h>

namespace OS {

IOSWindowImpl::IOSWindowImpl( IOSWindow *window )
 : pWindow(window)
{ Core::pWindow = window; }

IOSWindowImpl::~IOSWindowImpl( )
{}


} // end of namespace