#pragma once
#ifndef _OSWINDOWIMPL_H_
#define _OSWINDOWIMPL_H_

#include <lib.h>

namespace OS {

class IOSWindow;

class TLR_API IOSWindowImpl
{
  public:
	IOSWindowImpl( IOSWindow *window );
	virtual ~IOSWindowImpl( );

	virtual void initialize( ) = 0;
	virtual void deinitialize( ) = 0;
	virtual bool create( ) = 0;
	virtual void toggleFullscreen( ) = 0;
	virtual void swapBuffers( ) = 0;

  protected:
	IOSWindow *pWindow;
};




} // end of namespace
#endif