#include "OSKeys.h"

namespace OS {

IOSKeyMapper::IOSKeyMapper( )
{
	for( unsigned int i = 0; i < MAX_NUMBER_OF_KEYS; i++ )
		m_KeyMappingTable[ i ] = static_cast<LogicalKey>(0);


	m_LogicalKeyTable[ LK_ESCAPE ] = '\0';
	m_LogicalKeyTable[ LK_F1 ] = '\0';
	m_LogicalKeyTable[ LK_F2 ] = '\0';
	m_LogicalKeyTable[ LK_F3 ] = '\0';
	m_LogicalKeyTable[ LK_F4 ] = '\0';
	m_LogicalKeyTable[ LK_F5 ] = '\0';
	m_LogicalKeyTable[ LK_F6 ] = '\0';
	m_LogicalKeyTable[ LK_F7 ] = '\0';
	m_LogicalKeyTable[ LK_F8 ] = '\0';
	m_LogicalKeyTable[ LK_F9 ] = '\0';
	m_LogicalKeyTable[ LK_F10 ] = '\0';
	m_LogicalKeyTable[ LK_F11 ] = '\0';
	m_LogicalKeyTable[ LK_F12 ] = '\0';
	m_LogicalKeyTable[ LK_TILDA ] = '\0'; /* ~ */
	m_LogicalKeyTable[ LK_TAB ] = '\0';
	m_LogicalKeyTable[ LK_CAPSLOCK ] = '\0';
	m_LogicalKeyTable[ LK_LEFTSHIFT ] = '\0';
	m_LogicalKeyTable[ LK_LEFTCTRL ] = '\0';
	m_LogicalKeyTable[ LK_LEFTWINKEY ] = '\0';
	m_LogicalKeyTable[ LK_LEFTALT ] = '\0';
	m_LogicalKeyTable[ LK_RIGHTSHIFT ] = '\0';
	m_LogicalKeyTable[ LK_RIGHTCTRL ] = '\0';
	m_LogicalKeyTable[ LK_RIGHTWINKEY ] = '\0';
	m_LogicalKeyTable[ LK_RIGHTALT ] = '\0';
	m_LogicalKeyTable[ LK_A ] = 'A';
	m_LogicalKeyTable[ LK_B ] = 'B';
	m_LogicalKeyTable[ LK_C ] = 'C';
	m_LogicalKeyTable[ LK_D ] = 'D';
	m_LogicalKeyTable[ LK_E ] = 'E';
	m_LogicalKeyTable[ LK_F ] = 'F';
	m_LogicalKeyTable[ LK_G ] = 'G';
	m_LogicalKeyTable[ LK_H ] = 'H';
	m_LogicalKeyTable[ LK_I ] = 'I';
	m_LogicalKeyTable[ LK_J ] = 'J';
	m_LogicalKeyTable[ LK_K ] = 'K';
	m_LogicalKeyTable[ LK_L ] = 'L';
	m_LogicalKeyTable[ LK_M ] = 'M';
	m_LogicalKeyTable[ LK_N ] = 'N';
	m_LogicalKeyTable[ LK_O ] = 'O';
	m_LogicalKeyTable[ LK_P ] = 'P';
	m_LogicalKeyTable[ LK_Q ] = 'Q';
	m_LogicalKeyTable[ LK_R ] = 'R';
	m_LogicalKeyTable[ LK_S ] = 'S';
	m_LogicalKeyTable[ LK_T ] = 'T';
	m_LogicalKeyTable[ LK_U ] = 'U';
	m_LogicalKeyTable[ LK_V ] = 'V';
	m_LogicalKeyTable[ LK_W ] = 'W';
	m_LogicalKeyTable[ LK_X ] = 'X';
	m_LogicalKeyTable[ LK_Y ] = 'Y';
	m_LogicalKeyTable[ LK_Z ] = 'Z';
	m_LogicalKeyTable[ LK_EXCLAMATIONMARK ] = '!';
	m_LogicalKeyTable[ LK_AT ] = '@';
	m_LogicalKeyTable[ LK_POUND ] = '#';
	m_LogicalKeyTable[ LK_DOLLAR ] = '$';
	m_LogicalKeyTable[ LK_PERCENT ] = '%';
	m_LogicalKeyTable[ LK_CARET ] = '^';
	m_LogicalKeyTable[ LK_AMPLESAND ] = '&';
	m_LogicalKeyTable[ LK_ASTERISK ] = '*';
	m_LogicalKeyTable[ LK_LEFTPAREN ] = '(';
	m_LogicalKeyTable[ LK_RIGHTPAREN ] = ')';
	m_LogicalKeyTable[ LK_1 ] = '1';
	m_LogicalKeyTable[ LK_2 ] = '2';
	m_LogicalKeyTable[ LK_3 ] = '3';
	m_LogicalKeyTable[ LK_4 ] = '4';
	m_LogicalKeyTable[ LK_5 ] = '5';
	m_LogicalKeyTable[ LK_6 ] = '6';
	m_LogicalKeyTable[ LK_7 ] = '7';
	m_LogicalKeyTable[ LK_8 ] = '8';
	m_LogicalKeyTable[ LK_9 ] = '9';
	m_LogicalKeyTable[ LK_0 ] = '0';
	m_LogicalKeyTable[ LK_MINUS ] = '\0';
	m_LogicalKeyTable[ LK_EQUAL ] = '\0';
	m_LogicalKeyTable[ LK_BACKSPACE ] = '\0';
	m_LogicalKeyTable[ LK_ENTER ] = '\0';
	m_LogicalKeyTable[ LK_COMMA ] = '\0';
	m_LogicalKeyTable[ LK_PERIOD ] = '\0';
	m_LogicalKeyTable[ LK_FORWARDSLASH ] = '\0';
	m_LogicalKeyTable[ LK_SEMICOLON ] = '\0';
	m_LogicalKeyTable[ LK_APOSTROPHE ] = '\0';
	m_LogicalKeyTable[ LK_LEFTBRACKET ] = '\0';
	m_LogicalKeyTable[ LK_RIGHTBRACKET ] = '\0';
	m_LogicalKeyTable[ LK_BACKSLASH ] = '\0';
	m_LogicalKeyTable[ LK_SPACE ] = '\0';
	m_LogicalKeyTable[ LK_PRINT ] = '\0';
	m_LogicalKeyTable[ LK_INSERT ] = '\0';
	m_LogicalKeyTable[ LK_DELETE ] = '\0';
	m_LogicalKeyTable[ LK_HOME ] = '\0';
	m_LogicalKeyTable[ LK_END ] = '\0';
	m_LogicalKeyTable[ LK_PAGEUP ] = '\0';
	m_LogicalKeyTable[ LK_PAGEDOWN ] = '\0';
	m_LogicalKeyTable[ LK_LEFT ] = '\0';
	m_LogicalKeyTable[ LK_RIGHT ] = '\0';
	m_LogicalKeyTable[ LK_UP ] = '\0';
	m_LogicalKeyTable[ LK_DOWN ] = '\0';
	m_LogicalKeyTable[ LK_NUMLOCK ] = '\0';
	m_LogicalKeyTable[ LK_DIVIDE ] = '\0';
	m_LogicalKeyTable[ LK_MULTIPLY ] = '\0';
	m_LogicalKeyTable[ LK_SUBTRACT ] = '\0';
	m_LogicalKeyTable[ LK_SEPERATOR ] = '\0';
	m_LogicalKeyTable[ LK_ADDITION ] = '\0';
	m_LogicalKeyTable[ LK_DECIMAL ] = '\0';
	m_LogicalKeyTable[ LK_NUMPAD0 ] = '\0';
	m_LogicalKeyTable[ LK_NUMPAD1 ] = '\0';
	m_LogicalKeyTable[ LK_NUMPAD2 ] = '\0';
	m_LogicalKeyTable[ LK_NUMPAD3 ] = '\0';
	m_LogicalKeyTable[ LK_NUMPAD4 ] = '\0';
	m_LogicalKeyTable[ LK_NUMPAD5 ] = '\0';
	m_LogicalKeyTable[ LK_NUMPAD6 ] = '\0';
	m_LogicalKeyTable[ LK_NUMPAD7 ] = '\0';
	m_LogicalKeyTable[ LK_NUMPAD8 ] = '\0';
	m_LogicalKeyTable[ LK_NUMPAD9 ] = '9';

}

IOSKeyMapper::~IOSKeyMapper( )
{}

void IOSKeyMapper::mapKey( unsigned int physicalKey, LogicalKey logicalKey )
{
	m_KeyMappingTable[ physicalKey ] = logicalKey;
}

char IOSKeyMapper::translateLogicalKeyToChar( LogicalKey logicalKey )
{
	return m_LogicalKeyTable[ logicalKey ];
}

} // end of namespace