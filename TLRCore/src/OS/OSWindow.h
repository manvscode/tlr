#pragma once
#ifndef _OSWINDOW_H_
#define _OSWINDOW_H_
/*
 *		OSWindow.h
 *
 *		An abstract window interface using the bridge pattern.
 *		Coded by Joseph A. Marrero
 */
#include <lib.h>
#include <OS/OSWindowImpl.h>
#include <OS/OSKeys.h>

namespace OS {

class IOSWindowImpl;
class IOSKeyMapper;

class TLR_API IOSWindow
{
  public:
	explicit IOSWindow( );
	explicit IOSWindow( IOSWindowImpl *implementation, IOSKeyMapper *keyMapper );
	virtual ~IOSWindow( );

	virtual bool initialize( ) = 0;
	virtual bool deinitialize( ) = 0;
	virtual bool create( );
	virtual void toggleFullscreen( );
	virtual void swapBuffers( );
	virtual int mainLoop( ) = 0;

	void setWidth( unsigned int width );
	void setHeight( unsigned int height );
	unsigned int width( ) const;
	unsigned int height( ) const;
	
	IOSWindowImpl *getImplementation( );
	IOSKeyMapper *getKeyMapper( ) const;

  protected:
	IOSWindowImpl *m_pImplementation;
	IOSKeyMapper *m_pKeyMapper;
	unsigned int m_uiWidth, m_uiHeight;
	
};

inline unsigned int IOSWindow::width( ) const
{ return m_uiWidth; }

inline unsigned int IOSWindow::height( ) const
{ return m_uiHeight; }

inline IOSWindowImpl *IOSWindow::getImplementation( )
{ return m_pImplementation; }

inline IOSKeyMapper *IOSWindow::getKeyMapper( ) const
{ return m_pKeyMapper; }

} // end of namespace
#endif