#ifndef WIN32
/*
 *	X11KeyMapper.cpp
 *
 *	A class that maps X11 keys to the LK_ logical
 *	keys.
 *		
 *	Coded by Joseph A. Marrero
 */
#include <X11/keysym.h>
#include "X11KeyMapper.h"

namespace OS {

CX11KeyMapper::CX11KeyMapper( )
 : IOSKeyMapper( )
{
	buildMappingTable( );
}

CX11KeyMapper::~CX11KeyMapper( )
{}
	
LogicalKey CX11KeyMapper::translateKey( unsigned int physicalKey )
{
	return m_KeyMappingTable[ physicalKey ];
}

void CX11KeyMapper::buildMappingTable( )
{
	m_KeyMappingTable[ 0x09 ] = LK_ESCAPE;
	m_KeyMappingTable[ 0x43 ] = LK_F1;
	m_KeyMappingTable[ 0x44 ] = LK_F2;
	m_KeyMappingTable[ 0x45 ] = LK_F3;
	m_KeyMappingTable[ 0x46 ] = LK_F4;
	m_KeyMappingTable[ 0x47 ] = LK_F5;
	m_KeyMappingTable[ 0x48 ] = LK_F6;
	m_KeyMappingTable[ 0x49 ] = LK_F7;
	m_KeyMappingTable[ 0x4a ] = LK_F8;
	m_KeyMappingTable[ 0x4b ] = LK_F9;
	m_KeyMappingTable[ 0x4c ] = LK_F10;
	m_KeyMappingTable[ 0x5f ] = LK_F11;
	m_KeyMappingTable[ 0x60 ] = LK_F12;
	m_KeyMappingTable[ 0x31 ] = LK_TILDA; /* ~ */
	m_KeyMappingTable[ 0x17 ] = LK_TAB;
	m_KeyMappingTable[ 0x42 ] = LK_CAPSLOCK;
	m_KeyMappingTable[ 0x32 ] = LK_LEFTSHIFT;
	m_KeyMappingTable[ 0x25 ] = LK_LEFTCTRL;
	m_KeyMappingTable[ 0x73 ] = LK_LEFTWINKEY;
	m_KeyMappingTable[ 0x40 ] = LK_LEFTALT;
	m_KeyMappingTable[ 0x3e ] = LK_RIGHTSHIFT;
	m_KeyMappingTable[ 0x60 ] = LK_RIGHTCTRL;
	//m_KeyMappingTable[ VK_RWIN ] = LK_RIGHTWINKEY;
	m_KeyMappingTable[ 0x71 ] = LK_RIGHTALT;
	m_KeyMappingTable[ 0x26 ] = LK_A;
	m_KeyMappingTable[ 0x38 ] = LK_B;
	m_KeyMappingTable[ 0x36 ] = LK_C;
	m_KeyMappingTable[ 0x28 ] = LK_D;
	m_KeyMappingTable[ 0x1a ] = LK_E;
	m_KeyMappingTable[ 0x29 ] = LK_F;
	m_KeyMappingTable[ 0x2a ] = LK_G;
	m_KeyMappingTable[ 0x2b ] = LK_H;
	m_KeyMappingTable[ 0x1f ] = LK_I;
	m_KeyMappingTable[ 0x2c ] = LK_J;
	m_KeyMappingTable[ 0x2d ] = LK_K;
	m_KeyMappingTable[ 0x2e ] = LK_L;
	m_KeyMappingTable[ 0x3a ] = LK_M;
	m_KeyMappingTable[ 0x39 ] = LK_N;
	m_KeyMappingTable[ 0x20 ] = LK_O;
	m_KeyMappingTable[ 0x21 ] = LK_P;
	m_KeyMappingTable[ 0x18 ] = LK_Q;
	m_KeyMappingTable[ 0x1b ] = LK_R;
	m_KeyMappingTable[ 0x27 ] = LK_S;
	m_KeyMappingTable[ 0x1c ] = LK_T;
	m_KeyMappingTable[ 0x1e ] = LK_U;
	m_KeyMappingTable[ 0x37 ] = LK_V;
	m_KeyMappingTable[ 0x19 ] = LK_W;
	m_KeyMappingTable[ 0x35 ] = LK_X;
	m_KeyMappingTable[ 0x1d ] = LK_Y;
	m_KeyMappingTable[ 0x34 ] = LK_Z;
	//m_KeyMappingTable[ XK_exclam  ] = LK_EXCLAMATIONMARK;
	//m_KeyMappingTable[ i ] = LK_AT;
	//m_KeyMappingTable[ i ] = LK_POUND;
	//m_KeyMappingTable[ XK_dollar ] = LK_DOLLAR;
	//m_KeyMappingTable[ XK_percent ] = LK_PERCENT;
	//m_KeyMappingTable[ i ] = LK_CARET;
	//m_KeyMappingTable[ XK_ampersand  ] = LK_AMPLESAND;
	//m_KeyMappingTable[ XK_asterisk ] = LK_ASTERISK;
	//m_KeyMappingTable[ XK_parenleft ] = LK_LEFTPAREN;
	//m_KeyMappingTable[ XK_parenright ] = LK_RIGHTPAREN;
	m_KeyMappingTable[ 0x0a ] = LK_1;
	m_KeyMappingTable[ 0x0b ] = LK_2;
	m_KeyMappingTable[ 0x0c ] = LK_3;
	m_KeyMappingTable[ 0x0d ] = LK_4;
	m_KeyMappingTable[ 0x0e ] = LK_5;
	m_KeyMappingTable[ 0x0f ] = LK_6;
	m_KeyMappingTable[ 0x10 ] = LK_7;
	m_KeyMappingTable[ 0x11 ] = LK_8;
	m_KeyMappingTable[ 0x12 ] = LK_9;
	m_KeyMappingTable[ 0x13 ] = LK_0;
	m_KeyMappingTable[ 0x14 ] = LK_MINUS;
	m_KeyMappingTable[ 0x15 ] = LK_EQUAL;
	m_KeyMappingTable[ 0x16 ] = LK_BACKSPACE;
	
	m_KeyMappingTable[ 0x24 ] = LK_ENTER;

	m_KeyMappingTable[ 0x3b ] = LK_COMMA;
	m_KeyMappingTable[ 0x3c ] = LK_PERIOD;
	m_KeyMappingTable[ 0x3d ] = LK_FORWARDSLASH;
	m_KeyMappingTable[ 0x2f ] = LK_SEMICOLON;
	m_KeyMappingTable[ 0x30 ] = LK_APOSTROPHE;
	m_KeyMappingTable[ 0x22 ] = LK_LEFTBRACKET;
	m_KeyMappingTable[ 0x23 ] = LK_RIGHTBRACKET;
	
	m_KeyMappingTable[ 0x33 ] = LK_BACKSLASH;
	m_KeyMappingTable[ 0x41 ] = LK_SPACE; 
	//m_KeyMappingTable[ XK_Print ] = LK_PRINT;
	m_KeyMappingTable[ 0x6a ] = LK_INSERT;
	m_KeyMappingTable[ 0x6b ] = LK_DELETE;
	m_KeyMappingTable[ 0x61 ] = LK_HOME;
	m_KeyMappingTable[ 0x67 ] = LK_END;
	m_KeyMappingTable[ 0x63 ] = LK_PAGEUP;
	m_KeyMappingTable[ 0x69 ] = LK_PAGEDOWN;
	m_KeyMappingTable[ 0x64 ] = LK_LEFT;
	m_KeyMappingTable[ 0x66 ] = LK_RIGHT;
	m_KeyMappingTable[ 0x62 ] = LK_UP;
	m_KeyMappingTable[ 0x68 ] = LK_DOWN;
	
	/* Number pad keys */
	m_KeyMappingTable[ 77 ] = LK_NUMLOCK; 
	m_KeyMappingTable[ 112 ] = LK_DIVIDE;
	m_KeyMappingTable[ 63 ] = LK_MULTIPLY;
	m_KeyMappingTable[ 82 ] = LK_SUBTRACT;
	//m_KeyMappingTable[ XK_KP_Separator ] = LK_SEPERATOR;
	m_KeyMappingTable[ 86 ] = LK_ADDITION;
	m_KeyMappingTable[ 91 ] = LK_DECIMAL;
	m_KeyMappingTable[ 90 ] = LK_NUMPAD0;
	m_KeyMappingTable[ 87 ] = LK_NUMPAD1;
	m_KeyMappingTable[ 88 ] = LK_NUMPAD2;
	m_KeyMappingTable[ 89 ] = LK_NUMPAD3;
	m_KeyMappingTable[ 83 ] = LK_NUMPAD4;
	m_KeyMappingTable[ 84 ] = LK_NUMPAD5;
	m_KeyMappingTable[ 85 ] = LK_NUMPAD6;
	m_KeyMappingTable[ 79 ] = LK_NUMPAD7;
	m_KeyMappingTable[ 80 ] = LK_NUMPAD8;
	m_KeyMappingTable[ 81 ] = LK_NUMPAD9;
}


} // end of namespace

#endif
