#ifndef WIN32
/*
 *		X11WindowImpl.cpp
 *
 *		Coded by Joseph A. Marrero
 */
#include <stdlib.h>
#include <stddef.h>
#include <iostream>
#include <iomanip>
#include <Core.h>
#include <utility/debug.h>
#include <OS/X11/X11Window.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/Xmu/StdCmap.h>
#include "X11WindowImpl.h"
using namespace std;

namespace OS {

CX11WindowImpl::CX11WindowImpl( )
 : IOSWindowImpl( ), m_Display(NULL), m_bIsVisible(true), m_bIsMapped(false)
{
	
}

CX11WindowImpl::~CX11WindowImpl( )
{
	deinitialize( );
}
	
void CX11WindowImpl::initialize( )
{
	Core::initialize( );
}

void CX11WindowImpl::deinitialize( )
{
	if( pWindow->isFullscreen( ) )
	{
		restoreResolution( );
		XUndefineCursor( m_Display, m_Window );
		XFreeCursor( m_Display, m_blank_cursor );
		XFreePixmap( m_Display, m_cursor_pixmap );
	}
	Core::deinitialize( );
	glXDestroyContext( m_Display, m_Context );
	glXDestroyWindow( m_Display, m_glxWindow );
	XCloseDisplay( m_Display );
}

bool CX11WindowImpl::getColormap( XVisualInfo &vi, Colormap *cmap )
{
	if( cmap == NULL ) return false;
	Status status;
	XStandardColormap *standardColormaps;
	int i, numCMaps;
	
	if( vi.c_class != TrueColor )
		return false;
	
	status = XmuLookupStandardColormap( m_Display, vi.screen, vi.visualid,
			 vi.depth, XA_RGB_DEFAULT_MAP, /* replace*/ False, /* retain */ True );
	
	if( status == 1 )
	{
		status = XGetRGBColormaps( m_Display, RootWindow( m_Display, vi.screen),
								   &standardColormaps, &numCMaps, XA_RGB_DEFAULT_MAP );
		if( status == 1 )
			for( int i = 0; i < numCMaps; i++ )
				if( standardColormaps[ i ].visualid == vi.visualid )
				{
					*cmap = standardColormaps[ i ].colormap;
					XFree( standardColormaps );
					return true;
				}
	}
	
	*cmap = XCreateColormap( m_Display, 
								   RootWindow(m_Display, vi.screen),
								   vi.visual,
								   AllocNone );

	return true;		
}

bool CX11WindowImpl::changeResolution( int width, int height )
{
	XF86VidModeModeInfo **modes = NULL;
	int vidModeMajorVersion = 0, vidModeMinorVersion = 0;
	int bestMode = 0;
	int modeNum = 0;
	
	
	
    XF86VidModeQueryVersion( m_Display, &vidModeMajorVersion, &vidModeMinorVersion );
    printf( "XF86VidModeExtension-Version %d.%d\n", vidModeMajorVersion, vidModeMinorVersion );

	XF86VidModeGetAllModeLines( m_Display, m_Screen, &modeNum, &modes );
    /* save desktop-resolution before switching modes */
    deskMode = *modes[ 0 ];
    /* look for mode with requested resolution */
    for( int i = 0; i < modeNum; i++ )
    {
        if( (modes[ i ]->hdisplay == width) && (modes[ i ]->vdisplay == height) )
        {
            bestMode = i;
        }
    }

	XF86VidModeSwitchToMode( m_Display, m_Screen, modes[ bestMode ] );
	XF86VidModeSetViewPort( m_Display, m_Screen, 0, 0 );
	dpyWidth = modes[ bestMode ]->hdisplay;
	dpyHeight = modes[ bestMode ]->vdisplay;
	printf( "Resolution %dx%d\n", dpyWidth, dpyHeight );
	XFree( modes );
    


	return true;
}

bool CX11WindowImpl::restoreResolution( )
{
	XF86VidModeSwitchToMode( m_Display, m_Screen, &deskMode );
	XF86VidModeSetViewPort( m_Display, m_Screen, 0, 0);
	return true;
}

bool CX11WindowImpl::create( )
{
	connect( ); // connect to X server
	
//#ifdef _DEBUG
//	XSynchronize(m_Display, True);
//#endif

	char *windowTitle = "[Threat Level Red]";
	m_Screen = DefaultScreen( m_Display );
	Atom wmDeleteWindow;
	int attributes[] =
	{
    	GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
    	GLX_RENDER_TYPE, GLX_RGBA_BIT,
    	GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
    	GLX_X_RENDERABLE, True,
    	//GLX_TRANSPARENT_TYPE, GLX_TRANSPARENT_RGB,
    	GLX_RED_SIZE, 4,
    	GLX_GREEN_SIZE, 4,
    	GLX_BLUE_SIZE, 4,
    	GLX_ALPHA_SIZE, 4,
        GLX_BUFFER_SIZE, 16,
        GLX_DEPTH_SIZE, 16,
        GLX_STENCIL_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		None
    };

	
	int numConfigs = 0;
    GLXFBConfig *configs = glXChooseFBConfig( m_Display, m_Screen, attributes, &numConfigs );
	
	if( configs == NULL )
	{
		cerr << "ERROR: No OpenGL frame buffer configurations(" << numConfigs << ") available. :(" << endl;
		return false;
	}
	
	XVisualInfo *vi = glXGetVisualFromFBConfig( m_Display, configs[ 0 ] );

	m_Context = glXCreateNewContext( m_Display, configs[ 0 ], GLX_RGBA_TYPE, NULL, True );
	
	
	
	ASSERT( vi != NULL );
	
	// Get a color map...
	Colormap cmap;
	if( !getColormap( *vi, &cmap ) )
	{
		cerr << "ERROR: Unable to get a valid colormap. :(" << endl;
		return false;
	}
	
	m_WindowAttributes.colormap = cmap;
	m_WindowAttributes.border_pixel = 0;
	
	cout << "DEBUG: Creating window..." << endl;

	if( pWindow->isFullscreen( ) )
	{
		char cursor_data[ 32 ]; // empty bitmap
		memset( cursor_data, 0, sizeof(char) * 32 );
		XColor any_color;
		Colormap def_colormap = DefaultColormap( m_Display, m_Screen );
		
		changeResolution( );
        /* create a fullscreen window */
        m_WindowAttributes.override_redirect = True;
        m_WindowAttributes.event_mask = ExposureMask | MapNotify |  UnmapNotify | StructureNotifyMask |
        								ButtonPressMask | ButtonReleaseMask | PointerMotionMask | 
										KeyPressMask | KeyReleaseMask;
        m_Window = XCreateWindow( m_Display, RootWindow( m_Display, vi->screen ),
            					  0, 0, dpyWidth, dpyHeight, 0, vi->depth, InputOutput, vi->visual,
            					  CWBorderPixel | CWColormap | CWEventMask | CWOverrideRedirect,
            					  &m_WindowAttributes );
        XWarpPointer( m_Display, None, m_Window, 0, 0, 0, 0, 0, 0 );
        XMapRaised( m_Display, m_Window );
        XGrabKeyboard( m_Display, m_Window, True, GrabModeAsync, GrabModeAsync, CurrentTime );
        XGrabPointer( m_Display, m_Window, True, ButtonPressMask | PointerMotionMask,
            		  GrabModeAsync, GrabModeAsync, m_Window, None, CurrentTime );
        m_cursor_pixmap = XCreatePixmapFromBitmapData( m_Display, m_Window, 
        											   cursor_data, 16, 16, 1, 0, 1 ); // make our pixmap
        XParseColor( m_Display, def_colormap, "red", &any_color );											   
		m_blank_cursor = XCreatePixmapCursor( m_Display, m_cursor_pixmap, m_cursor_pixmap, &any_color, &any_color, 8, 8 ); // make the blank cursor
		XDefineCursor( m_Display, m_Window, m_blank_cursor );   		  
        setupViewport( dpyWidth, dpyHeight );
    }
    else
    {
        /* create a window in window mode*/
        m_WindowAttributes.event_mask = ExposureMask | MapNotify |  UnmapNotify | 
        							VisibilityChangeMask | StructureNotifyMask |
									ButtonPressMask | ButtonReleaseMask | PointerMotionMask | 
									KeyPressMask | KeyReleaseMask;
        m_Window = XCreateWindow( m_Display, RootWindow(m_Display, vi->screen),
            					  0, 0, 1024, 768, 0, vi->depth, InputOutput, vi->visual,
            					  CWBorderPixel | CWColormap | CWEventMask, &m_WindowAttributes );
        /* only set window title and handle wm_delete_events if in windowed mode */
		wmDeleteWindow = XInternAtom( m_Display, "WM_DELETE_WINDOW", False );
		XSetWMProtocols( m_Display, m_Window, &wmDeleteWindow, 1 );
        XSetStandardProperties( m_Display, m_Window, windowTitle, windowTitle, None, NULL, 0, NULL);
        
		XMapRaised( m_Display, m_Window );
    }

	m_glxWindow = glXCreateWindow( m_Display, configs[ 0 ], m_Window, NULL );
	
	/* Connect the context to the window for read and write */
	glXMakeContextCurrent( m_Display, m_glxWindow, m_glxWindow, m_Context );

	XFree( configs );
	
	XMapWindow( m_Display, m_Window );
	
	if( pWindow->isFullscreen( ) )
		setupViewport( dpyWidth, dpyHeight );

	// Initialize everything...
	initialize( );
	
	return true;
}

void CX11WindowImpl::toggleFullscreen( )
{}

void CX11WindowImpl::swapBuffers( )
{ 
	glXSwapBuffers( m_Display, m_glxWindow );
}

void CX11WindowImpl::quit( int returnCode )
{
	cout << "quiting..." << endl;
	deinitialize( );
	exit( returnCode );
}

void CX11WindowImpl::connect( )
{
	char *displayName = getenv( "DISPLAY" );
	
	/* open the connection to the display "simey:0". */
	if( displayName == NULL )
		m_Display = XOpenDisplay( ":0" );
	else
		m_Display = XOpenDisplay( displayName );
		
		
	if( m_Display == NULL )
	{
    	cerr << "Cannot connect to X server :0" << endl;
    	exit( -1 );
    }
}

void CX11WindowImpl::mainLoop( )
{
	Core::pTimer->resetTimer( );
	
	float to = 0.0f;
	size_t frameCount = 0;
	/* this structure will contain the event's data, once received. */
	XEvent event;

	/* enter an "endless" loop of handling events. */
	while( true )
	{
	
		do {
    		XNextEvent( m_Display, &event);
    	
    		switch( event.type )
    		{
    			case MapNotify:
    				m_bIsMapped = true;
    				break;
    			case UnmapNotify:
    				m_bIsMapped = false;
    				break;
      			case Expose:
					swapBuffers( );
        			break;
       	 		case ConfigureNotify:
        			resize( (XConfigureEvent &) event );
        			break;
        		case DestroyNotify:
        			quit( 0 );
        			break;
        		case VisibilityNotify:
        		{
        			XVisibilityEvent *e = (XVisibilityEvent *) &event;
        			if( e->state & VisibilityFullyObscured )
        			{
        				cout << "Window not visible..." << endl;
						m_bIsVisible = false;
					}
					else {
						cout << "Window visible..." << endl;
						m_bIsVisible = true;
					}
        			break;
        		}
        		case KeyPress:
        			onKeyDown( (XKeyEvent &) event );
					break;
        		case KeyRelease:
        			onKeyUp( (XKeyEvent &) event );
        			break;
        		case MotionNotify:
        			onMouseMove( (XPointerMovedEvent &) event );
        			break;
        		case ButtonPress:
        			onMouseButtonDown( (XButtonPressedEvent &) event );
        			break;
        		case ButtonRelease:
        			onMouseButtonUp( (XButtonReleasedEvent &) event );
        			break;
      			default: /* unknown event type - ignore it. */
        			break;
    		}
    	} while( XPending( m_Display ) );
    	
    	if( !m_bIsMapped ) continue; // not mapped don't do anything...
    	if( !m_bIsVisible ) continue; //Not visible then don't render anything...
    	
		to = Core::pTimer->getTime( );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); // Clear Screen And Depth Buffer
		glClearColor( 0.2f, 0.2f, 0.2f, 0.0f );
		glLoadIdentity( );
			
		UI::timerEventNotifier.isExpired( );
		UI::pManager->render( );
		
	
		swapBuffers( );
		frameCount++;

		Core::TimeStep = (Core::pTimer->getTime( ) - to);
		
				
		if( Core::TimeStep < SECS_PER_FRAME_AT_60FPS )
			usleep( static_cast<unsigned int>(1000000.0*(SECS_PER_FRAME_AT_60FPS - Core::TimeStep)) );

		ASSERT( glGetError( ) == GL_NO_ERROR ); 	
    }
}


void CX11WindowImpl::resize( XConfigureEvent &evt )
{
#ifdef _DEBUG
	cout << "resizing... " << endl;
#endif
	setupViewport( evt.width, evt.height );
}

void CX11WindowImpl::onKeyDown( XKeyEvent &evt )
{
/*
#ifdef _DEBUG
	cout.setf( ios::hex );
	cout << "Button Down... state = 0x" <<  evt.state << ", key = 0x" << evt.keycode;
	cout.unsetf( ios::hex );
	cout.setf( ios::dec );
   	cout << "  " << evt.keycode << endl;
   	cout.unsetf( ios::dec );
#endif
*/
	ASSERT( pWindow != NULL );
	CX11KeyMapper *keyMapper = static_cast<CX11KeyMapper *>( pWindow->getKeyMapper( ) );
	ASSERT( keyMapper != NULL );
	LogicalKey k = keyMapper->translateKey( evt.keycode );
	UI::keyPressEventNotifier.setKeyState( (unsigned int) k, true );

	// check if escape key was struck...
	if( UI::keyPressEventNotifier.getKeyState( keyMapper->translateKey(0x09) ) == true )
		quit( 0 );

}

void CX11WindowImpl::onKeyUp( XKeyEvent &evt )
{
/*
#ifdef _DEBUG
   	cout << "Button Up... " << endl;
#endif
*/
	ASSERT( pWindow != NULL );
	CX11KeyMapper *keyMapper = static_cast<CX11KeyMapper *>( pWindow->getKeyMapper( ) );
	ASSERT( keyMapper != NULL );
	LogicalKey k = keyMapper->translateKey( evt.keycode );
	UI::keyPressEventNotifier.setKeyState( (unsigned int) k, false );
}

void CX11WindowImpl::setupViewport( int width, int height )
{
	ASSERT( pWindow != NULL );
	UI::windowHeight = height;
	UI::windowWidth = width;
	pWindow->setWidth( width );
	pWindow->setHeight( height );
			
	if( UI::windowHeight == 0 )
		UI::windowHeight = 1;

	glViewport( 0, 0, UI::windowWidth, UI::windowHeight );
			
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );

	glOrtho( 0.0f, UI::windowWidth - 1.0, 0.0, UI::windowHeight - 1.0, -10.0, 10.0 );
	ASSERT( glGetError( ) == GL_NO_ERROR );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity( );
}

void CX11WindowImpl::onMouseMove( XPointerMovedEvent &evt )
{
	UI::mouseEventNotifier.setButtonState( (bool) (evt.state & Button1Mask),
		 								   (bool) (evt.state & Button2Mask),
		 								   (bool) (evt.state & Button3Mask),
		 								   evt.x, evt.y );
	UI::mouseEventNotifier.setMousePosition( evt.x, evt.y );
}

void CX11WindowImpl::onMouseButtonDown( XButtonPressedEvent &evt )
{
	UI::mouseEventNotifier.setButtonState( (bool) (evt.state & Button1Mask),
		 								   (bool) (evt.state & Button2Mask),
		 								   (bool) (evt.state & Button3Mask),
		 								   evt.x, evt.y );
}

void CX11WindowImpl::onMouseButtonUp( XButtonReleasedEvent &evt )
{
	UI::mouseEventNotifier.setButtonState( (bool) (evt.state & Button1Mask),
		 								   (bool) (evt.state & Button2Mask),
		 								   (bool) (evt.state & Button3Mask),
		 								   evt.x, evt.y );
}

} // end of namespace
#endif
