#pragma once
#ifndef WIN32
#ifndef _X11WINDOW_H_
#define _X11WINDOW_H_
/*
 *		X11Window.h
 *
 *		Coded by Joseph A. Marrero
 */
#include <lib.h>
#include <OS/OSWindow.h>
#include <OS/X11/X11WindowImpl.h>
#include <OS/X11/X11KeyMapper.h>

namespace OS {

class TLR_API CX11Window : public IOSWindow
{
  public:
	CX11Window( );
	virtual ~CX11Window( );

	bool initialize( );
	bool deinitialize( );
	
	int mainLoop( );

};


} // end of namespace
#endif
#endif
