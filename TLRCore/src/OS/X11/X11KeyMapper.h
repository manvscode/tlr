#pragma once
#ifndef _X11KEYMAPPER_H_
#define _X11KEYMAPPER_H_
/*
 *	X11KeyMapper.h
 *
 *	A class that maps X11 keys to the LK_ logical
 *	keys.
 *		
 *	Coded by Joseph A. Marrero
 */
#include <lib.h>
#include <OS/OSKeys.h>
#include <map>

namespace OS {


class TLR_API CX11KeyMapper : public IOSKeyMapper
{
  public:
	CX11KeyMapper( );
	virtual ~CX11KeyMapper( );
	
	LogicalKey translateKey( unsigned int physicalKey );

  protected:
	void buildMappingTable( );
	
	//std::map<int, LogicalKey> m_KeyMappingTable;
	
};



} // end of namespace
#endif
