#pragma once
#ifndef WIN32
#ifndef _X11WINDOWIMPL_H_
#define _X11WINDOWIMPL_H_
/*
 *		X11WindowImpl.h
 *
 *		Coded by Joseph A. Marrero
 */
#include <lib.h>
#include <OS/OSWindowImpl.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
//#include<X11/extensions/Xrandr.h>
#include <X11/extensions/xf86vmode.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

namespace OS {

class TLR_API CX11WindowImpl : public IOSWindowImpl
{
  public:
	CX11WindowImpl( );
	virtual ~CX11WindowImpl( );
	
	void initialize( );
	void deinitialize( );
	bool create( );
	void toggleFullscreen( );
	void swapBuffers( );
	void quit( int returnCode );
	void mainLoop( );
	
	/*
	 *	XEvent Handlers
	 */
	void resize( XConfigureEvent &evt );
	void onKeyDown( XKeyEvent &evt );
	void onKeyUp( XKeyEvent &evt );
	void onMouseMove( XPointerMovedEvent &evt );
	void onMouseButtonDown( XButtonPressedEvent &evt );
	void onMouseButtonUp( XButtonReleasedEvent &evt );
	
	
	
  private:
	void connect( );

  protected:
  	bool getColormap( XVisualInfo &vi, Colormap *cmap );
  	bool changeResolution( int width = 1024, int height = 768 );
  	bool restoreResolution( );
  	void setupViewport( int width, int height );
  
	Display *m_Display;
	GLXFBConfig m_FBConfig;
	
	Window m_Window;
	GLXWindow m_glxWindow;
	XSetWindowAttributes m_WindowAttributes;
	GLXContext m_Context;
	int m_Screen;
	bool m_bIsVisible;
	bool m_bIsMapped;
	
	XF86VidModeModeInfo deskMode;
	int dpyWidth, dpyHeight;
	
	Pixmap m_cursor_pixmap;
	Cursor m_blank_cursor;
};




} // end of namespace
#endif
#endif
