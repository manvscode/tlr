#ifndef WIN32
#include "X11Window.h"
/*
 *		X11Window.cpp
 *
 *		Coded by Joseph A. Marrero
 */
#include <X11/Xlib.h>


namespace OS {

CX11Window::CX11Window( )
  : IOSWindow( )
{
	m_pImplementation = new CX11WindowImpl( );
	m_pKeyMapper = new CX11KeyMapper( );
	m_bIsFullscreen = false;
}

CX11Window::~CX11Window( )
{
	delete m_pImplementation;
	delete m_pKeyMapper;
}

bool CX11Window::initialize( )
{
	return true;
}

bool CX11Window::deinitialize( )
{
	return true;
}

int CX11Window::mainLoop( )
{
	CX11WindowImpl *impl = static_cast<CX11WindowImpl *>( m_pImplementation ); 
	impl->mainLoop( );
	return 0;
}


} // end of namespace
#endif
