#pragma once
#ifndef _MSKEYMAPPER_H_
#define _MSKEYMAPPER_H_
/*
 *	MSKeyMapper.h
 *
 *	A class that maps MS Window's keys to the LK_ logical
 *	keys.
 *		
 *	Coded by Joseph A. Marrero
 */
#include <lib.h>
#include <OS/OSKeys.h>


namespace OS {


class TLR_API CMSKeyMapper : public IOSKeyMapper
{
  public:
	CMSKeyMapper( );
	~CMSKeyMapper( );

	LogicalKey translateKey( unsigned int osDefinedKey );
	int translateLogicalKey( unsigned int logicalKey );

  protected:
	void buildMappingTable( );
	
	//LogicalKey m_KeyMappingTable[ MAX_NUMBER_OF_KEYS ];
};



} // end of namespace
#endif
