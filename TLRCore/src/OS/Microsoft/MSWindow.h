#pragma once
#ifndef _MSWINDOW_H_
#define _MSWINDOW_H_
/*
 *		MSWindow.h
 */
#include <lib.h>
#include <OS/OSWindow.h>
#include <OS/Microsoft/MSWindowImpl.h>
#include <OS/Microsoft/MSKeyMapper.h>

namespace OS {


class TLR_API CMSWindow : public IOSWindow
{
  public:
	CMSWindow( HINSTANCE hInstance, unsigned int width = 1024, unsigned int height = 768 );
	virtual ~CMSWindow( );

	virtual bool initialize( );
	virtual bool deinitialize( );
	virtual int mainLoop( );

	
};



} // end of namespace
#endif