#pragma once
#ifndef _MSWINDOWIMPL_H_
#define _MSWINDOWIMPL_H_

#include <lib.h>
#include <windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <OS/OSWindowImpl.h>
#include <OS/Microsoft/MSWindow.h>

#define RESOLUTION_X	1024
#define RESOLUTION_Y	768

namespace OS {

class CMSWindow;

LRESULT CALLBACK WndProc( HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam );

class TLR_API CMSWindowImpl : public IOSWindowImpl
{
	friend LRESULT CALLBACK WndProc( HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam );

  public:
	CMSWindowImpl( CMSWindow *pWin, HINSTANCE hInstance );
	~CMSWindowImpl( );


	void initialize( );
	void deinitialize( );
	bool create( );
	void toggleFullscreen( );
	bool goFullscreen( );
	void swapBuffers( );
	bool isFullscreen( ) const;

	void setHandleDC( HDC hdc );

	HWND getWindowHandle( ) const;
	HDC getHandleDC( ) const;
	HGLRC getHandleRC( ) const;

  protected:

	void setupPixelDescriptor( );

	HINSTANCE m_hInstance;
	HWND m_hWnd;
	RECT m_windowRect;
	DWORD m_windowStyle, m_extendedWindowStyle;
	HDC m_hDC;
	HGLRC m_hRC;
	bool m_bFullscreen;
};


inline bool CMSWindowImpl::isFullscreen( ) const
{ return m_bFullscreen; }

inline HWND CMSWindowImpl::getWindowHandle( ) const
{ return m_hWnd; }

inline HDC CMSWindowImpl::getHandleDC( ) const
{ return m_hDC; }

inline HGLRC CMSWindowImpl::getHandleRC( ) const
{ return m_hRC; }

} // end of namespace
#endif