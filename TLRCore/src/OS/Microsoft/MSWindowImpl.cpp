#include <Core.h>
#include <windows.h>
#include <GL/glew.h>
#include <GL/gl.h>

#include <OS/OSWindow.h>
#include <OS/Microsoft/MSWindow.h>
#include <OS/Microsoft/MSWindowImpl.h>
#include <UI/UI.h>
#include <utility/debug.h>
#include <resource.h>

namespace OS {

CMSWindowImpl::CMSWindowImpl( CMSWindow *pWin, HINSTANCE hInstance )
: IOSWindowImpl(pWin), m_hInstance(hInstance), m_hWnd(), m_windowRect(), m_windowStyle(), m_extendedWindowStyle(), m_hDC(), m_hRC()
{
#ifdef _DEBUG
	m_bFullscreen = false;
#else
	m_bFullscreen = true;
#endif
}

CMSWindowImpl::~CMSWindowImpl( )
{
	deinitialize( );
}

void CMSWindowImpl::initialize( )
{
	
	setupPixelDescriptor( );
	m_hRC = wglCreateContext( m_hDC );
	wglMakeCurrent( m_hDC, m_hRC );

	Core::initialize( Core::CO_GAME_MODE );


	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glClearColor( 0.5f, 0.5f, 0.5f, 0.5f );
	

	glEnable( GL_ALPHA_TEST );
	
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	//glEnable( GL_POLYGON_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	//glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	glShadeModel( GL_SMOOTH );
	glClearDepth( 1.0f );							// Depth Buffer Setup
	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_ALWAYS );
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_CULL_FACE );
	glFrontFace( GL_CCW );
	
	//glDepthMask(GL_TRUE);
	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );



	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );



	//glEnable( GL_LIGHTING );
	//glLightfv( GL_LIGHT0, GL_SPECULAR, WorldLight );
	//glLightfv( GL_LIGHT0, GL_POSITION, WorldLightPosition );
	//glLightfv( GL_LIGHT0, GL_SPOT_DIRECTION, WorldLightDirection );
	//glEnable( GL_LIGHT0 );
	glColorMaterial( GL_FRONT_AND_BACK, GL_EMISSION );
	glEnable( GL_COLOR_MATERIAL );
	
	glEnableClientState(GL_VERTEX_ARRAY);



	assert( glGetError( ) == GL_NO_ERROR );
}

void CMSWindowImpl::deinitialize( )
{
	Core::deinitialize( );
}

bool CMSWindowImpl::create( )
{
	WNDCLASSEX	wndClass;

	m_windowRect.top = 0;
	m_windowRect.left = 0;
	m_windowRect.bottom = pWindow->height( );
	m_windowRect.right = pWindow->width( );


	wndClass.cbSize = sizeof( WNDCLASSEX );
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = WndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = m_hInstance;
	wndClass.hIcon = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_ICON) );
	wndClass.hCursor = LoadCursor( NULL, IDC_ARROW );
	wndClass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = "ThreatLevelRed";
	wndClass.hIconSm = LoadIcon( m_hInstance, MAKEINTRESOURCE(IDI_ICON) );

	if( !RegisterClassEx(&wndClass) ){
		MessageBox( NULL, "Unable to register window class!", "Error", MB_OK | MB_ICONERROR );
		return false;
	}

	m_hWnd = CreateWindowEx( NULL, "ThreatLevelRed", "Threat Level Red", WS_POPUP | WS_EX_TOPMOST, 0, 0, RESOLUTION_X, RESOLUTION_Y, NULL, NULL, m_hInstance, NULL);


	if( !m_hWnd )
	{
		MessageBox( m_hWnd, "Unable to create window!", "Error", MB_OK | MB_ICONERROR );
		return false;
	}

	if( m_bFullscreen )
	{
		goFullscreen( );

		m_extendedWindowStyle = WS_EX_APPWINDOW;
		m_windowStyle = WS_POPUP;
		ShowCursor( FALSE );
	}
	else {
		m_extendedWindowStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		m_windowStyle = WS_OVERLAPPEDWINDOW;
	}

	AdjustWindowRectEx( &m_windowRect, m_windowStyle, FALSE, m_extendedWindowStyle );
	UpdateWindow( m_hWnd );

	
			
	//initialize( );
	ShowWindow( m_hWnd, SW_SHOW );
	return true;
}

void CMSWindowImpl::toggleFullscreen( )
{
	m_bFullscreen = !m_bFullscreen;
	
	if( m_bFullscreen )
		goFullscreen( );
}

bool CMSWindowImpl::goFullscreen( )
{
	DEVMODE devModeScreen;
	memset( &devModeScreen, 0, sizeof( devModeScreen ) );
	devModeScreen.dmSize = sizeof( devModeScreen );
	devModeScreen.dmPelsWidth = pWindow->width( );
	devModeScreen.dmPelsHeight = pWindow->height( );
	devModeScreen.dmBitsPerPel = 32/*ScreenBpp*/;
	devModeScreen.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

	if( ChangeDisplaySettings( &devModeScreen, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL )
	{
		m_bFullscreen = false;
		return false;
	}

	return true;
}

void CMSWindowImpl::swapBuffers( )
{ SwapBuffers( m_hDC ); }



void CMSWindowImpl::setHandleDC( HDC hdc )
{ m_hDC = hdc; }


void CMSWindowImpl::setupPixelDescriptor( )
{
	int PixelFormat = 0;

	static PIXELFORMATDESCRIPTOR pfd = {
		sizeof( PIXELFORMATDESCRIPTOR ),
			1,
			PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_GENERIC_ACCELERATED |
			PFD_DOUBLEBUFFER,
			PFD_TYPE_RGBA,
			32,
			0, 0, 0, 0, 0, 0,
			8, // Alpha Buffer, REQUIRED!!!!
			0,
			0,
			0, 0, 0, 0,
			16, // Z Buffer
			0,
			0,
			PFD_MAIN_PLANE,
			0,
			0, 0, 0
	};

    if( (PixelFormat = ChoosePixelFormat( m_hDC, &pfd )) == 0 ) 
    { 
        MessageBox( NULL, "ChoosePixelFormat failed", "Error", MB_OK ); 
        return; 
    } 
 
    if( SetPixelFormat( m_hDC, PixelFormat, &pfd ) == FALSE ) 
    { 
		MessageBox( NULL, "SetPixelFormat failed", "Error", MB_OK | MB_ICONERROR ); 
        return; 
    } 

}

LRESULT CALLBACK WndProc( HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	PAINTSTRUCT		ps;
	CMSWindowImpl *impl = static_cast<CMSWindowImpl *>( Core::pWindow->getImplementation( ) );
	CMSKeyMapper *keyMapper = static_cast<CMSKeyMapper *>( Core::pWindow->getKeyMapper( ) );

	switch( message )
	{
		case WM_CREATE:
			impl->setHandleDC( GetDC( hwnd ) );
			Core::pWindow->initialize( );
			break;
		case WM_PAINT:
			BeginPaint( hwnd, &ps );
			EndPaint( hwnd, &ps );
			return 0;
			break;
		case WM_QUIT:
		case WM_DESTROY:
		case WM_CLOSE:
			ReleaseDC( hwnd, impl->m_hDC );
			
			wglMakeCurrent( impl->m_hDC, NULL );
			wglDeleteContext(  impl->m_hRC );
			PostQuitMessage( 0 );
			return 0;
			break;

		case WM_SIZE:
			{
			UI::windowHeight = HIWORD( lParam );
			UI::windowWidth = LOWORD( lParam );
			Core::pWindow->setWidth( LOWORD( lParam ) );
			Core::pWindow->setHeight( HIWORD( lParam ) );
			
			if( UI::windowHeight == 0 )
				UI::windowHeight = 1;

			glViewport( 0, 0, UI::windowWidth, UI::windowHeight );
			
			glMatrixMode( GL_PROJECTION );
			glLoadIdentity( );

			glOrtho( 0.0f, UI::windowWidth - 1.0, 0.0, UI::windowHeight - 1.0, -10.0, 10.0 );
			assert( glGetError( ) == GL_NO_ERROR );
			glMatrixMode( GL_MODELVIEW );
			glLoadIdentity( );
			break;
			}
		case WM_KEYUP:						
		{
			unsigned int uiKey = static_cast<unsigned int>( keyMapper->translateKey(wParam) );
			UI::keyPressEventNotifier.setKeyState( uiKey , false );
			return 0;
			break;
		}
		case WM_KEYDOWN:					
		{
			unsigned int uiKey = static_cast<unsigned int>( keyMapper->translateKey(wParam) );
			UI::keyPressEventNotifier.setKeyState( uiKey, true );

			if( UI::keyPressEventNotifier.getKeyState( keyMapper->translateKey(VK_ESCAPE) ) == true )
			{	
				ReleaseDC( hwnd, impl->m_hDC );
				wglMakeCurrent( impl->m_hDC, NULL );
				wglDeleteContext( impl->m_hRC );
				PostQuitMessage( 0 );
				return 0;
			}
				
			break;
		}
		
		case WM_MOUSEMOVE:
			UI::mouseEventNotifier.setButtonState( (wParam & MK_LBUTTON) > 0 ? true : false, 
												   (wParam & MK_MBUTTON) > 0  ? true : false, 
												   (wParam & MK_RBUTTON) > 0  ? true : false, 
												   LOWORD(lParam), 
												   HIWORD(lParam) );
			UI::mouseEventNotifier.setMousePosition( LOWORD(lParam), HIWORD(lParam) );
			break;
		case WM_LBUTTONDOWN:
			UI::mouseEventNotifier.setButtonState( true, false, false, LOWORD(lParam), HIWORD(lParam) );
			//UI::mouseEventNotifier.setMousePosition( LOWORD(lParam), HIWORD(lParam) );
			break;
		case WM_SYSCOMMAND:						
		{
			switch( wParam )						
			{
				case SC_SCREENSAVE:				
				case SC_MONITORPOWER:
					break;
				//return 0;					
			}
			break;							
		}

		default:
			break;
	}

	return DefWindowProc( hwnd, message, wParam, lParam );
}


} // end of namespace