#include <Core.h>
#include <windows.h>
#include <OS/Microsoft/MSWindow.h>
#include <OS/Microsoft/MSWindowImpl.h>
#include <OS/Microsoft/MSKeyMapper.h>

#include <utility/debug.h>
#include <UI/UI.h>


namespace OS {

CMSWindow::CMSWindow( HINSTANCE hInstance, unsigned int width, unsigned int height )
: IOSWindow( )
{
	setWidth( width );
	setHeight( height );
	m_pImplementation = new CMSWindowImpl( this, hInstance );
	m_pKeyMapper = new CMSKeyMapper( );
}

CMSWindow::~CMSWindow( )
{
	delete m_pImplementation;
	delete m_pKeyMapper;
}

bool CMSWindow::initialize( )
{
	m_pImplementation->initialize( );

	return true;
}

bool CMSWindow::deinitialize( )
{
	m_pImplementation->deinitialize( );
	//Core::deinitialize( );
	return true;
}

int CMSWindow::mainLoop( )
{
	MSG msg;
	Core::pTimer->resetTimer( );
	
	float to = 0.0f;
	size_t frameCount = 0;

	while( true )
	{
	
		if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
		{
			if( msg.message == WM_QUIT )
				break;
			
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		else {
			to = Core::pTimer->getTime( );
			Core::TimeStep = (Core::pTimer->getTime( ) - to);
			glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); // Clear Screen And Depth Buffer
			glClearColor( 0.2f, 0.2f, 0.2f, 0.0f );
			glLoadIdentity( );
			
			UI::timerEventNotifier.isExpired( );
			UI::pManager->render( );
			
			swapBuffers( );
			frameCount++;

			Core::TimeStep = (Core::pTimer->getTime( ) - to);
			to = Core::pTimer->getTime( );

			if( Core::TimeStep < Core::SECS_PER_FRAME_AT_60FPS )
				Sleep( static_cast<DWORD>(Core::SECS_PER_FRAME_AT_60FPS - Core::TimeStep) );
			
			assert( glGetError( ) == GL_NO_ERROR );
		}
	}

	
	return 0;
}

} // end of namespace