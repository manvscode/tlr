/*
 *	MSKeyMapper.cpp
 *
 *	A class that maps MS Window's keys to the LK_ logical
 *	keys.
 *		
 *	Coded by Joseph A. Marrero
 */
#include <windows.h>
#include "MSKeyMapper.h"

namespace OS {

CMSKeyMapper::CMSKeyMapper( )
  : IOSKeyMapper( )
{
	buildMappingTable( );
}

CMSKeyMapper::~CMSKeyMapper( )
{}


void CMSKeyMapper::buildMappingTable( )
{
	m_KeyMappingTable[ VK_ESCAPE ] = LK_ESCAPE;
	m_KeyMappingTable[ VK_F1 ] = LK_F1;
	m_KeyMappingTable[ VK_F2 ] = LK_F2;
	m_KeyMappingTable[ VK_F3 ] = LK_F3;
	m_KeyMappingTable[ VK_F4 ] = LK_F4;
	m_KeyMappingTable[ VK_F5 ] = LK_F5;
	m_KeyMappingTable[ VK_F6 ] = LK_F6;
	m_KeyMappingTable[ VK_F7 ] = LK_F7;
	m_KeyMappingTable[ VK_F8 ] = LK_F8;
	m_KeyMappingTable[ VK_F9 ] = LK_F9;
	m_KeyMappingTable[ VK_F10 ] = LK_F10;
	m_KeyMappingTable[ VK_F11 ] = LK_F11;
	m_KeyMappingTable[ VK_F12 ] = LK_F12;
	m_KeyMappingTable[ VK_OEM_3 ] = LK_TILDA; /* ~ */
	m_KeyMappingTable[ VK_TAB ] = LK_TAB;
	m_KeyMappingTable[ 0x14 ] = LK_CAPSLOCK;
	m_KeyMappingTable[ 0x10 ] = LK_LEFTSHIFT;
	m_KeyMappingTable[ 0x11 ] = LK_LEFTCTRL;
	m_KeyMappingTable[ VK_LWIN ] = LK_LEFTWINKEY;
	//m_KeyMappingTable[ i ] = LK_LEFTALT;
	m_KeyMappingTable[ VK_RSHIFT ] = LK_RIGHTSHIFT;
	m_KeyMappingTable[ VK_RCONTROL ] = LK_RIGHTCTRL;
	m_KeyMappingTable[ VK_RWIN ] = LK_RIGHTWINKEY;
	//m_KeyMappingTable[ i ] = LK_RIGHTALT;
	m_KeyMappingTable[ 0X41 ] = LK_A;
	m_KeyMappingTable[ 0X42 ] = LK_B;
	m_KeyMappingTable[ 0X43 ] = LK_C;
	m_KeyMappingTable[ 0X44 ] = LK_D;
	m_KeyMappingTable[ 0X45 ] = LK_E;
	m_KeyMappingTable[ 0X46 ] = LK_F;
	m_KeyMappingTable[ 0X47 ] = LK_G;
	m_KeyMappingTable[ 0X48 ] = LK_H;
	m_KeyMappingTable[ 0X49 ] = LK_I;
	m_KeyMappingTable[ 0X4A ] = LK_J;
	m_KeyMappingTable[ 0X4B ] = LK_K;
	m_KeyMappingTable[ 0X4C ] = LK_L;
	m_KeyMappingTable[ 0X4D ] = LK_M;
	m_KeyMappingTable[ 0X4E ] = LK_N;
	m_KeyMappingTable[ 0X4F ] = LK_O;
	m_KeyMappingTable[ 0X50 ] = LK_P;
	m_KeyMappingTable[ 0X51 ] = LK_Q;
	m_KeyMappingTable[ 0X52 ] = LK_R;
	m_KeyMappingTable[ 0X53 ] = LK_S;
	m_KeyMappingTable[ 0X54 ] = LK_T;
	m_KeyMappingTable[ 0X55 ] = LK_U;
	m_KeyMappingTable[ 0X56 ] = LK_V;
	m_KeyMappingTable[ 0X57 ] = LK_W;
	m_KeyMappingTable[ 0X58 ] = LK_X;
	m_KeyMappingTable[ 0X59 ] = LK_Y;
	m_KeyMappingTable[ 0X5A ] = LK_Z;
	//m_KeyMappingTable[ i ] = LK_EXCLAMATIONMARK;
	//m_KeyMappingTable[ i ] = LK_AT;
	//m_KeyMappingTable[ i ] = LK_POUND;
	//m_KeyMappingTable[ i ] = LK_DOLLAR;
	//m_KeyMappingTable[ i ] = LK_PERCENT;
	//m_KeyMappingTable[ i ] = LK_CARET;
	//m_KeyMappingTable[ i ] = LK_AMPLESAND;
	//m_KeyMappingTable[ i ] = LK_ASTERISK;
	//m_KeyMappingTable[ i ] = LK_LEFTPAREN;
	//m_KeyMappingTable[ i ] = LK_RIGHTPAREN;
	m_KeyMappingTable[ 0x31 ] = LK_1;
	m_KeyMappingTable[ 0x32 ] = LK_2;
	m_KeyMappingTable[ 0x33 ] = LK_3;
	m_KeyMappingTable[ 0x34 ] = LK_4;
	m_KeyMappingTable[ 0x35 ] = LK_5;
	m_KeyMappingTable[ 0x36 ] = LK_6;
	m_KeyMappingTable[ 0x37 ] = LK_7;
	m_KeyMappingTable[ 0x38 ] = LK_8;
	m_KeyMappingTable[ 0x39 ] = LK_9;
	m_KeyMappingTable[ 0x30 ] = LK_0;
	m_KeyMappingTable[ 0xbd ] = LK_MINUS;
	m_KeyMappingTable[ 0xbb ] = LK_EQUAL;
	m_KeyMappingTable[ VK_BACK ] = LK_BACKSPACE;
	
	m_KeyMappingTable[ VK_RETURN ] = LK_ENTER;

	m_KeyMappingTable[ 0xbc ] = LK_COMMA;
	m_KeyMappingTable[ 0xbe ] = LK_PERIOD;
	m_KeyMappingTable[ 0xbf ] = LK_FORWARDSLASH;
	m_KeyMappingTable[ 0xba ] = LK_SEMICOLON;
	m_KeyMappingTable[ 0xde ] = LK_APOSTROPHE;
	m_KeyMappingTable[ 0xdd ] = LK_LEFTBRACKET;
	m_KeyMappingTable[ VK_RETURN ] = LK_ENTER;
	
	m_KeyMappingTable[ 0xdc ] = LK_BACKSLASH;
	m_KeyMappingTable[ VK_SPACE ] = LK_SPACE;
	m_KeyMappingTable[ VK_PRINT ] = LK_PRINT;
	m_KeyMappingTable[ VK_INSERT ] = LK_INSERT;
	m_KeyMappingTable[ VK_DELETE ] = LK_DELETE;
	m_KeyMappingTable[ VK_HOME ] = LK_HOME;
	m_KeyMappingTable[ VK_END ] = LK_END;
	m_KeyMappingTable[ VK_PRIOR ] = LK_PAGEUP;
	m_KeyMappingTable[ VK_NEXT ] = LK_PAGEDOWN;
	m_KeyMappingTable[ VK_LEFT ] = LK_LEFT;
	m_KeyMappingTable[ VK_RIGHT ] = LK_RIGHT;
	m_KeyMappingTable[ VK_UP ] = LK_UP;
	m_KeyMappingTable[ VK_DOWN ] = LK_DOWN;
	/* Number pad keys */
	m_KeyMappingTable[ VK_NUMLOCK ] = LK_NUMLOCK;
	m_KeyMappingTable[ VK_DIVIDE ] = LK_DIVIDE;
	m_KeyMappingTable[ VK_MULTIPLY ] = LK_MULTIPLY;
	m_KeyMappingTable[ VK_SUBTRACT ] = LK_SUBTRACT;
	m_KeyMappingTable[ VK_SEPARATOR ] = LK_SEPERATOR;
	m_KeyMappingTable[ VK_ADD ] = LK_ADDITION;
	m_KeyMappingTable[ VK_DECIMAL ] = LK_DECIMAL;
	m_KeyMappingTable[ VK_NUMPAD0 ] = LK_NUMPAD0;
	m_KeyMappingTable[ VK_NUMPAD1 ] = LK_NUMPAD1;
	m_KeyMappingTable[ VK_NUMPAD2 ] = LK_NUMPAD2;
	m_KeyMappingTable[ VK_NUMPAD3 ] = LK_NUMPAD3;
	m_KeyMappingTable[ VK_NUMPAD4 ] = LK_NUMPAD4;
	m_KeyMappingTable[ VK_NUMPAD5 ] = LK_NUMPAD5;
	m_KeyMappingTable[ VK_NUMPAD6 ] = LK_NUMPAD6;
	m_KeyMappingTable[ VK_NUMPAD7 ] = LK_NUMPAD7;
	m_KeyMappingTable[ VK_NUMPAD8 ] = LK_NUMPAD8;
	m_KeyMappingTable[ VK_NUMPAD9 ] = LK_NUMPAD9;
}

LogicalKey CMSKeyMapper::translateKey( unsigned int osDefinedKey )
{
	return m_KeyMappingTable[ osDefinedKey ];
}


} // end of namespace
