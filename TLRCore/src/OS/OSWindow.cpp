#include "OSWindow.h"
/*
 *		OSWindow.cpp
 *
 *		An abstract window interface using the bridge pattern.
 *		Coded by Joseph A. Marrero
 */
namespace OS {

IOSWindow::IOSWindow( )
  : m_pImplementation(0), m_pKeyMapper(0), m_uiWidth(0), m_uiHeight(0)
{}

IOSWindow::IOSWindow( IOSWindowImpl *implementation, IOSKeyMapper *keyMapper )
 : m_pImplementation(implementation), m_pKeyMapper(keyMapper), m_uiWidth(0), m_uiHeight(0)
{
}

IOSWindow::~IOSWindow( )
{
}


bool IOSWindow::create( )
{
	m_pImplementation->create( );
	return true;
}

void IOSWindow::toggleFullscreen( )
{
	m_pImplementation->toggleFullscreen( );
}

void IOSWindow::swapBuffers( )
{
	m_pImplementation->swapBuffers( );
}


void IOSWindow::setWidth( unsigned int width )
{ m_uiWidth = width; }

void IOSWindow::setHeight( unsigned int height )
{ m_uiHeight = height; }

} // end of namespace