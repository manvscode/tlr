#pragma once 
#ifndef _CORE_H_
#define _CORE_H_
/*
 *	Core.h
 */
#include <lib.h>
#include <GL/glew.h>
#include <GL/gl.h>

#include <OS/OSWindow.h>
#include <ResourceManagement/TextureManager/TextureManager.h>
#ifdef NO_TERRAIN_SPLATTING
	#include <Game/Map/MapCell.h>
	#include <Game/Map/MapCellTexturing.h>
	#include <Game/Map/Map.h>
#else
	#include <Game/Map/SplatMapCellTexturing.h>
	#include <Game/Map/SplatMap.h>
#endif
#include <Game/Map/MapSelectionManager.h>

#ifdef _DEBUG
#include <utility/memleakcheck.h>
#endif




typedef unsigned int  uint;
typedef unsigned char byte;
typedef unsigned char CoreOptions; // two bytes






// forward declarations...
class OS::IOSWindow;
//class Game::CSplatMap;
//template <>
//class Game::CMapSelectionManager<128>;
class ResourceManagement::CTextureManager;
class CTimer;


namespace Core
{
	// common frame rate constants...
	static const float SECS_PER_FRAME_AT_80FPS = 1.0f / 80.0f;
	static const float SECS_PER_FRAME_AT_60FPS = 1.0f / 60.0f;
	static const float SECS_PER_FRAME_AT_30FPS = 1.0f / 30.0f;
	static const float SECS_PER_FRAME_AT_24FPS = 1.0f / 24.0f;



	// Core Option Flags
	static const byte CO_BAREBONES	= 0x00;
	static const byte CO_UI			= 0x01;
	static const byte CO_TIMER		= 0x02;

	// Core Common Options
	static const byte CO_NORMAL		= ( CO_TIMER | CO_UI );
	static const byte CO_GAME_MODE	= ( CO_NORMAL );
	static const byte CO_TOOL_MODE	= ( CO_BAREBONES );

	/*
	 *	Core Options bits
	 */
	#ifdef BUILD_TLR_CORE // THIS IS NOT EXPORTED
	extern CoreOptions coreOptions;
	#endif

	/*
	 *	OS Window
	 */
	#ifdef BUILD_TLR_CORE // THIS IS NOT EXPORTED
	extern OS::IOSWindow *pWindow;
	#endif


	/*
	 *	Resource Management
	 */
	extern TLR_API ResourceManagement::CTextureManager *pTextureManager;


	/*
	 *	Game Subsystem
	 */
	//#ifdef NO_TERRAIN_SPLATTING
	//extern TLR_API Game::CMap<Game::CDefaultMapTexturingPolicy> map;
	//#else
	extern TLR_API Game::CSplatMap *pMap;
	//#endif
	extern TLR_API Game::CMapSelectionManager<128> selections;

	/* Timing */
	extern TLR_API CTimer *pTimer;
	extern TLR_API double TimeStep;


	/*
	 *	Core Functionality
	 */
	TLR_API bool initialize( const CoreOptions options = CO_NORMAL );
	TLR_API bool deinitialize( );



	/* Debug */
	extern void checkGLError( );

} //end of namespace
#endif
