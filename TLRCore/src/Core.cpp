//#include <vld.h>
//#pragma comment(lib, "vld.lib")
//#pragma comment(lib, "vldmtdll.lib")
/*
 *	Core.cpp
 */
#include <OS/OSWindow.h>
//#ifdef NO_TERRAIN_SPLATTING
//	#include <Game/Map/MapCell.h>
//	#include <Game/Map/MapCellTexturing.h>
//	#include <Game/Map/Map.h>
//#else
//	#include <Game/Map/SplatMapCellTexturing.h>
//	#include <Game/Map/SplatMap.h>
//#endif
#include <Game/Map/MapSelectionManager.h>
#include <UI/UI.h>
#include <Timer/Timer.h>
#include <ResourceManagement/TextureManager/TextureManager.h>

#ifdef _DEBUG
	#include <utility/debug.h>
	#include <utility/memleakcheck.h>
	#include <cassert>
#endif
#include <Core.h>

namespace Core 
{
	/*
	 *	Core Options bits
	 */
	CoreOptions coreOptions;
	
	/*
	 *	OS Window
	 */	
	OS::IOSWindow *pWindow = NULL;

	/*
	 *	Resource Management
	 */
	ResourceManagement::CTextureManager *pTextureManager = NULL;

	/*
	 *	Game Subsystem
	 */
	//#ifdef NO_TERRAIN_SPLATTING
	//Game::CMap<Game::CDefaultMapTexturingPolicy> map;
	//#else
	Game::CSplatMap *pMap;
	//#endif
	Game::CMapSelectionManager<128> selections;

	/* Timing */	
	CTimer *pTimer = NULL;
	double TimeStep = 0.0;


	/*
	 *	Core Functionality
	 */
	bool initialize( const CoreOptions options )
	{
		coreOptions = options; // save the core options

		#if defined(_DEBUG) && defined(WIN32)
			//_CrtSetReportMode( _CRT_ASSERT, _CRTDBG_MODE_DEBUG ); // display assertions in debug window
			//_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG ); // display errors in debug window
			//MEMLEAK_CHECK;
			Utility::DEBUG_TRACE( "initializing core services...\n" );
		#endif
		assert( glGetError( ) == GL_NO_ERROR );


		GLenum glewError = glewInit( );
		if( GLEW_OK != glewError )
		{
			cerr << "ERROR: There was a problem probing the graphics capabilities of your video card." << endl;
			return false;
		}

		pMap = new Game::CSplatMap( );
		if( coreOptions & CO_TIMER ) pTimer = new CTimer( );

		checkGLError( );
		glEnable( GL_LIGHTING );
		glEnable( GL_TEXTURE_2D );
		glEnable( GL_LIGHT0 );
		glEnable( GL_BLEND );
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
		glEnable( GL_COLOR_MATERIAL );
		glColorMaterial( GL_FRONT, GL_DIFFUSE );

		//assert( glGetError( ) == GL_NO_ERROR );	
		checkGLError( );

		pTextureManager = ResourceManagement::CTextureManager::getInstance( );

		#ifdef _TERRAIN_EXPERIMENT
		terrainBaseTexture = pTextureManager->addTexture( ResourceManagement::TX_TARGA, "img/terrain01_texture.tga" );
		pTextureManager->setupTexture( terrainBaseTexture, true );
		#endif


		// initialize subsystems...
		if( coreOptions & CO_UI )
			UI::initialize( );

		return true;
	}

	bool deinitialize( )
	{
		#ifdef _DEBUG
		Utility::DEBUG_TRACE( "deinitializing core services...\n" );
		#endif


		// deninitialize evreything...
		delete pMap;
		if( coreOptions & CO_TIMER ) delete pTimer;
		
		if( coreOptions & CO_UI )
			UI::deinitialize( );

		delete pTextureManager;

		#ifdef _DEBUG
			pTimer = NULL;
			//pMap = NULL;
			//pSelections = NULL;
			pTextureManager = NULL;
		#endif

		return true;
	}

	inline void checkGLError( )
	{
		GLenum error = glGetError( );
		if( error != GL_NO_ERROR )
		{
			const GLubyte *pErrorString = gluErrorString( error );
			assert( false );
		}
	}

} //end of namespace
