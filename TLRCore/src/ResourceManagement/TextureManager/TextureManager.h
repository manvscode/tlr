#pragma once
#ifndef __TEXTUREMANAGER_H__
#define __TEXTUREMANAGER_H__
/*
 *		TextureManager.h
 *
 *		A manager of textures.
 *		Coded by Joseph A. Marrero
 *		11/11/05
 */
#include <lib.h>
#include <string>
#include <map>
#include <ResourceManagement/TextureManager/Texture2D.h>

namespace ResourceManagement {

TLR_API typedef enum tagTextureFormat {
	TX_TARGA,
	TX_BITMAP
} TextureFormat;

class TLR_API CTextureManager
{
	typedef std::pair<int, CTexture2D *> TextureMapping;
	typedef std::map<int, CTexture2D *, std::less<int>, std::allocator< TextureMapping > > TextureCollection;

  public:
	virtual ~CTextureManager( );
	static CTextureManager *getInstance( );

	/*
	 *	addTexture( ) : Adds a textures texels to RAM but does not initialize
	 *	the texture to be used with OpenGL.
	 *
	 *	Return Value: -1 on error, and the texture id on success ( >= 1 )
	 */
	int addTexture( TextureFormat format, const char *filename );
	int addTexture( TextureFormat format, std::string &filename );
	/*
	 *	removeTexture( ) : Removes a previously added texture.
	 *
	 *	Return Value: None.
	 */
	void removeTexture( unsigned int textureId );

	CTexture2D *find( unsigned int textureId );
	const CTexture2D *find( unsigned int textureId ) const;

	void setupTexture( unsigned int textureId, bool buildMipmaps = false, int textureMagFilter = GL_LINEAR, int textureMinFilter = GL_LINEAR );

	void clear( );

  private:
	CTextureManager( );
	static CTextureManager *m_pInstance;
	TextureCollection m_TextureMapping;
	unsigned int m_TextureCount;
};

} // end of namespace
#endif