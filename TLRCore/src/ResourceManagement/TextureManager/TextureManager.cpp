/*
 *		TextureManager.cpp
 *
 *		A manager of textures.
 *		Coded by Joseph A. Marrero
 *		11/11/05
 */
#ifdef WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <cassert>
#include <ImageIO/ImageIO.h>
#include <ResourceManagement/TextureManager/TextureManager.h>
#include <ResourceManagement/TextureManager/Texture2D.h>

namespace ResourceManagement {

CTextureManager::CTextureManager( )
: m_TextureMapping(), m_TextureCount(0)
{
	ImageIO::initialize( );
}

CTextureManager::~CTextureManager( )
{
	clear( );
	ImageIO::deinitialize( );
}

CTextureManager *CTextureManager::m_pInstance = 0;

CTextureManager *CTextureManager::getInstance( )
{
	if( !m_pInstance )
		m_pInstance = new CTextureManager( );
	
	return m_pInstance;
}
/*
 *	Add a texture.
 *
 *	Returns >= 1 on success and -1 on failure.
 */
int CTextureManager::addTexture( TextureFormat format, const char *filename )
{
	unsigned int textureName = 0;

	switch( format )
	{
		case TX_TARGA:
		{
			ImageIO::Image imgFile;
			ImageIO::Result result = ImageIO::loadImage( &imgFile, filename, ImageIO::TARGA );
			assert( result == ImageIO::SUCCESS );

			// create texture object...
			glGenTextures( 1, &textureName );
			CTexture2D *texture = new CTexture2D( imgFile.width, imgFile.height, imgFile.bitsPerPixel >> 3, imgFile.pixelData, textureName, true );
			m_TextureMapping[ textureName ] = texture;

			// free malloc'd memory
			ImageIO::destroyImage( &imgFile );

			m_TextureCount++;
			return textureName;
			break;
		}
		case TX_BITMAP:
		{
			ImageIO::Image imgFile;
			ImageIO::Result result = ImageIO::loadImage( &imgFile, filename, ImageIO::BITMAP );
			assert( result == ImageIO::SUCCESS );

			// create texture object...
			glGenTextures( 1, &textureName );
			CTexture2D *texture = new CTexture2D( imgFile.width, imgFile.height, imgFile.bitsPerPixel >> 3, imgFile.pixelData, textureName, true );
			m_TextureMapping[ textureName ] = texture;

			// free malloc'd memory
			ImageIO::destroyImage( &imgFile );

			m_TextureCount++;
			return textureName;
			break;
		}
		default:
			assert( format != TX_BITMAP && format != TX_TARGA ); // unsupported format...
			break;
	}

	return -1;
}
/*
 *	Add a texture.
 *
 *	Returns >= 1 on success and -1 on failure.
 */
int CTextureManager::addTexture( TextureFormat format, std::string &filename )
{
	unsigned int textureName = 0;

	switch( format )
	{
		case TX_TARGA:
		{
			ImageIO::Image imgFile;
			ImageIO::Result result = ImageIO::loadImage( &imgFile, filename.c_str( ), ImageIO::TARGA );
			assert( result == ImageIO::SUCCESS );
			
			// create texture object...
			glGenTextures( 1, &textureName );			
			CTexture2D *texture = new CTexture2D( imgFile.width, imgFile.height, imgFile.bitsPerPixel >> 3, imgFile.pixelData, textureName, true );
			m_TextureMapping[ textureName ] = texture;

			// free malloc'd memory
			ImageIO::destroyImage( &imgFile );

			m_TextureCount++;
			return textureName;
			break;
		}
		case TX_BITMAP:
		{
			ImageIO::Image imgFile;
			ImageIO::Result result = ImageIO::loadImage( &imgFile, filename.c_str( ), ImageIO::BITMAP );
			assert( result == ImageIO::SUCCESS );
			
			// create texture object...
			glGenTextures( 1, &textureName );
			CTexture2D *texture = new CTexture2D( imgFile.width, imgFile.height, imgFile.bitsPerPixel >> 3, imgFile.pixelData, textureName, true );
			m_TextureMapping[ textureName ] = texture;

			// free malloc'd memory
			ImageIO::destroyImage( &imgFile );

			m_TextureCount++;
			return textureName;
			break;
		}
		default:
			assert( format != TX_BITMAP && format != TX_TARGA ); // unsupported format...
			break;
	}

	return -1;
}
/*
*	removeTexture( ) : Removes a previously added texture.
*
*	Return Value: None.
*/
void CTextureManager::removeTexture( unsigned int textureId )
{
	TextureCollection::iterator itr = m_TextureMapping.find( textureId );

	if( itr != m_TextureMapping.end( ) )
	{
		glDeleteTextures( 1, &textureId );
		delete itr->second;
		m_TextureMapping.erase( itr );
	}
}

CTexture2D *CTextureManager::find( unsigned int textureId )
{
	TextureCollection::iterator itr = m_TextureMapping.find( textureId );

	if( itr != m_TextureMapping.end( ) )
		return itr->second;
	else
		return NULL;
}

const CTexture2D *CTextureManager::find( unsigned int textureId ) const
{
	TextureCollection::const_iterator itr = m_TextureMapping.find( textureId );

	if( itr != m_TextureMapping.end( ) )
		return itr->second;
	else
		return NULL;
}


void CTextureManager::clear( )
{
	TextureCollection::iterator itr;
	for( itr = m_TextureMapping.begin( ); itr != m_TextureMapping.end( ); ++itr )
	{
		glDeleteTextures( 1, (GLuint *) &itr->first );
		delete itr->second;
	}
}


void CTextureManager::setupTexture( unsigned int textureId, bool buildMipmaps, int textureMagFilter, int textureMinFilter)
{
	CTexture2D *texture = find( textureId );
	GLfloat envColor[] = { 0.8f, 0.8f, 0.8f, 1.0f };

	if( !texture ) return;
	/*
	 *		Set Texture up
	 */
	glBindTexture( GL_TEXTURE_2D, textureId );
	
	GLenum err1 = glGetError( );
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glTexEnvfv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, envColor );

	if( buildMipmaps )
	{
		unsigned int format = texture->format( );
		gluBuild2DMipmaps( GL_TEXTURE_2D, (format == GL_RGB ? 3 : 4 ), texture->width( ), texture->height( ), format, GL_UNSIGNED_BYTE, texture->bitmap( ) );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (GLint) textureMagFilter );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (GLint) textureMagFilter );
	}
	else {
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (GLint) textureMagFilter );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (GLint) textureMinFilter );
		glTexImage2D( GL_TEXTURE_2D,
				  0,
				  texture->format( ),
				  texture->width( ),
				  texture->height( ),
				  0, //border
				  texture->format( ),
				  GL_UNSIGNED_BYTE,
				  texture->bitmap( ) );
	}



	assert( glGetError( ) == GL_NO_ERROR );
}




} // end of namespace