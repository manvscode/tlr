#include <ResourceManagement/TextureManager/Texture2D.h>
#include <cstring>


namespace ResourceManagement {


CTexture2D::CTexture2D( )
{
}

CTexture2D::CTexture2D( unsigned int width, unsigned int height, unsigned int byteCount, unsigned char *bitmap, unsigned int textureID, bool makeCopy )
  : m_Width(width), m_Height(height), m_ByteCount(byteCount), m_Bitmap(bitmap), m_TextureID(textureID), m_bMemoryAllocated(false)
{
	if( makeCopy == true )
	{
		m_Bitmap = new unsigned char[ width * height * byteCount ];
		std::memcpy( m_Bitmap, bitmap, sizeof(unsigned char) * width * height * byteCount );
		m_bMemoryAllocated = true;
	}
}

CTexture2D::CTexture2D( const CTexture2D &t )
: m_Width(t.m_Width), m_Height(t.m_Height), m_ByteCount(t.m_ByteCount), m_TextureID(t.m_TextureID)
{
	m_Bitmap = new unsigned char[ m_Width * m_Height * m_ByteCount ];
	std::memcpy( m_Bitmap, t.m_Bitmap, sizeof(unsigned char) * m_Width * m_Height * m_ByteCount );
	m_bMemoryAllocated = true;
}

CTexture2D::~CTexture2D( )
{ cleanUp( ); }

unsigned char *CTexture2D::bitmap( )
{
	return m_Bitmap;
}

const unsigned char *CTexture2D::bitmap( ) const
{
	return m_Bitmap;
}

unsigned char &CTexture2D::operator[]( unsigned int i )
{
	return m_Bitmap[ i ];
}

const unsigned char &CTexture2D::operator[]( unsigned int i ) const
{
	return m_Bitmap[ i ];
}

const CTexture2D &CTexture2D::operator =( const CTexture2D &right )
{
	if( this != &right )
	{
		m_Width = right.m_Width; 
		m_Height = right.m_Height;
		m_ByteCount = right.m_ByteCount;


		cleanUp( );
		m_Bitmap = new unsigned char[ m_Width * m_Height * m_ByteCount ];
		m_bMemoryAllocated = true;
		std::memcpy( m_Bitmap, right.m_Bitmap, sizeof(unsigned char) * m_Width * m_Height * m_ByteCount );

		m_TextureID = right.m_TextureID;
	}

	return *this;
}

void CTexture2D::cleanUp( )
{
	if( m_bMemoryAllocated )
	{
		delete [] m_Bitmap;
		m_bMemoryAllocated = false;
	}
}


} // end of namespace