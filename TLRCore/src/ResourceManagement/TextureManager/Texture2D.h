#pragma once
#ifndef _TEXTURE2D_H_
#define _TEXTURE2D_H_
#include <lib.h>
#ifdef WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>


namespace ResourceManagement {



class TLR_API CTexture2D
{
  public:
	CTexture2D( );
	CTexture2D( unsigned int width, unsigned int height, unsigned int byteCount, unsigned char *bitmap, unsigned int textureID, bool makeCopy = false );
	CTexture2D( const CTexture2D &t );
	virtual ~CTexture2D( );
	
	unsigned int width( ) const;
	unsigned int height( ) const;
	unsigned int byteCount( ) const;
	unsigned int bitCount( ) const;
	unsigned int format( ) const;
	unsigned int getTextureID( ) const;
	
	unsigned char *bitmap( );
	const unsigned char *bitmap( ) const;

	unsigned char &operator[]( unsigned int i );
	const unsigned char &operator[]( unsigned int i ) const;

	const CTexture2D &operator =( const CTexture2D &right );

  protected:
	unsigned int m_Width, m_Height;
	unsigned int m_ByteCount;
	unsigned char *m_Bitmap;
	unsigned int m_TextureID;
	bool m_bMemoryAllocated;

	void cleanUp( );
};

inline unsigned int CTexture2D::width() const
{ return m_Width; }

inline unsigned int CTexture2D::height() const
{ return m_Height; }

inline unsigned int CTexture2D::byteCount( ) const
{ return m_ByteCount; }

inline unsigned int CTexture2D::bitCount( ) const
{ return m_ByteCount * 8; }

inline unsigned int CTexture2D::format( ) const
{ return (m_ByteCount == 4 ? GL_RGBA : GL_RGB ); }

inline unsigned int CTexture2D::getTextureID( ) const
{ return m_TextureID; }



} // end of namespace
#endif