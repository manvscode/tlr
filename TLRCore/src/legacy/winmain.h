#pragma 
#ifndef __WINMAIN_H__
#define __WINMAIN_H__
/*
 *	winmain.h
 *	
 *	Coded by Joseph A. Marrero
 */

#include <Math/Vector3.h>

HWND createWindow( HINSTANCE hInstance );
void GLInitialize( );
void SetupPixelDescriptor(HDC hDC);
bool GoFullscreen(int ScreenWidth, int ScreenHeight, int ScreenBpp);
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
int mainLoop( );

int extensionInit( );

#define RESOLUTION_X		1024
#define RESOLUTION_Y		768

#ifdef _DEBUG
	bool fullscreen = FALSE;
#else
	bool fullscreen = TRUE;
#endif


HDC		hDC;





GLfloat WorldLight[ 4 ] = { 0.2f, 0.2f, 0.2f, 0.0f };
GLfloat WorldLightDirection[ 4 ] = { -2.5f, -1.0f, -2.01f, 0.0f };
GLfloat WorldLightPosition[ 4 ] = { 50.0f, 60.0f, 50.0f, 1.0f };

typedef void (APIENTRY * PFNGLFOGCOORDFEXTPROC) (GLfloat coord);		// Declare Function Prototype
PFNGLFOGCOORDFEXTPROC glFogCoordfEXT = NULL;		

GLfloat fogColor[ 4 ] = { 0.2f, 0.2f, 0.5f, 1.0f };

#endif