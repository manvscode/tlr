/*
 *		winmain.cpp
 *
 *		Coded by Joseph A. Marrero
 */


#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <cassert>
#include <GL/gl.h>


#include <core.h>
#include "winmain.h"
#include "Resource.h"
using namespace std;



int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd )
{
	HWND hWnd = createWindow( hInstance );
	RECT windowRect;
	DWORD windowStyle, extendedWindowStyle;

	if(!hWnd){
		MessageBox(NULL, "Unable to create window!", "Error", MB_OK |MB_ICONERROR);
		return -1;
	}
	ShowWindow(hWnd, SW_SHOW);
	
	if(fullscreen){
		GoFullscreen(1024, 768, 32);

		extendedWindowStyle = WS_EX_APPWINDOW;
		windowStyle = WS_POPUP;
		ShowCursor(FALSE);
	}
	else {
		extendedWindowStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		windowStyle = WS_OVERLAPPEDWINDOW;
	}
	
	AdjustWindowRectEx(&windowRect, windowStyle, FALSE, extendedWindowStyle);
	UpdateWindow(hWnd);

	
	return mainLoop( );
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT		ps;
	static HGLRC	hRC;
	int				width,
					height;

	switch( message )
	{
		case WM_CREATE:
		
			hDC = GetDC( hwnd );
			SetupPixelDescriptor( hDC );
			hRC = wglCreateContext( hDC );
			wglMakeCurrent( hDC, hRC );
			
			
			extensionInit( );
			GLInitialize( );
			Core::initialize( );
			assert( glGetError( ) == GL_NO_ERROR );
			//return 0;
			break;
		case WM_PAINT:
			BeginPaint( hwnd, &ps );
			EndPaint( hwnd, &ps );
			return 0;
			break;
		case WM_QUIT:
		case WM_DESTROY:
		case WM_CLOSE:
			//Core::deinitialize( );
			ReleaseDC( hwnd, hDC );
			
			wglMakeCurrent( hDC, NULL );
			wglDeleteContext( hRC );
			PostQuitMessage( 0 );
			return 0;
			break;

		case WM_SIZE:
			{
			UI::windowHeight = HIWORD( lParam );
			UI::windowWidth = LOWORD( lParam );
			
			if( UI::windowHeight == 0 )
				UI::windowHeight = 1;

			glViewport( 0, 0, UI::windowWidth, UI::windowHeight );
			
			glMatrixMode( GL_PROJECTION );
			glLoadIdentity( );

			//gluPerspective( 45.0f, (GLfloat) width / (GLfloat) height, 0.1f, 1000.0f );
			glOrtho(0.0f, UI::windowWidth - 1.0, 0.0, UI::windowHeight - 1.0, -10.0, 10.0);
			assert( glGetError( ) == GL_NO_ERROR );
			glMatrixMode( GL_MODELVIEW );
			glLoadIdentity( );
			//return 0;
			break;
			}
		case WM_KEYUP:						
		{
			UI::keyPressEventNotifier.setKeyState( wParam, false );
			return 0;
			break;
		}
		case WM_KEYDOWN:					
		{
			UI::keyPressEventNotifier.setKeyState( wParam, true );

			if( UI::keyPressEventNotifier.getKeyState( VK_ESCAPE ) == true )
			{	
				//Core::deinitialize( );
				ReleaseDC( hwnd, hDC );
				wglMakeCurrent( hDC, NULL );
				wglDeleteContext( hRC );
				PostQuitMessage( 0 );
				return 0;
			}
				
			//return 0;
			break;
		}
		
		case WM_MOUSEMOVE:
			UI::mouseEventNotifier.setButtonState( wParam & MK_LBUTTON, wParam & MK_MBUTTON, wParam & MK_RBUTTON, LOWORD(lParam), HIWORD(lParam) );
			UI::mouseEventNotifier.setMousePosition( LOWORD(lParam), HIWORD(lParam) );
			break;
		case WM_LBUTTONDOWN:
			UI::mouseEventNotifier.setButtonState( true, false, false, LOWORD(lParam), HIWORD(lParam) );
			//UI::mouseEventNotifier.setMousePosition( LOWORD(lParam), HIWORD(lParam) );
			break;
		case WM_SYSCOMMAND:						
		{
			switch( wParam )						
			{
				case SC_SCREENSAVE:				
				case SC_MONITORPOWER:
					break;
				//return 0;					
			}
			break;							
		}

		default:
			break;
	}
	return DefWindowProc(hwnd,message, wParam,lParam);
}


HWND createWindow( HINSTANCE hInstance )
{	
	WNDCLASSEX	wndClass;
	RECT windowRect;

	windowRect.top = 0;
	windowRect.left = 0;
	windowRect.bottom = RESOLUTION_Y;
	windowRect.right = RESOLUTION_X;


	wndClass.cbSize = sizeof( WNDCLASSEX );
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = WndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON));
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = "ThreatLevelRed";
	wndClass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON));

	if( !RegisterClassEx(&wndClass) ){
		MessageBox(NULL, "Unable to register window class!", "Error", MB_OK |MB_ICONERROR);
		return NULL;
	}

	return CreateWindowEx(NULL, "ThreatLevelRed", "Threat Level Red", WS_POPUP | WS_EX_TOPMOST, 0, 0, RESOLUTION_X, RESOLUTION_Y, NULL, NULL, hInstance, NULL);
}

void GLInitialize( )
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glClearColor( 0.5f, 0.5f, 0.5f, 0.5f );
	

	glEnable( GL_ALPHA_TEST );
	
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	glEnable( GL_POLYGON_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	glShadeModel( GL_SMOOTH );
	glClearDepth( 1.0f );							// Depth Buffer Setup
	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_ALWAYS );
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_CULL_FACE );
	glFrontFace( GL_CCW );
	
	//glDepthMask(GL_TRUE);
	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );


	// texturing
	//glEnable( GL_TEXTURE_2D );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	assert( !glGetError( ) );
	//glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, RainTextureTGAFile.imageWidth, RainTextureTGAFile.imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, RainTextureTGAFile.imageData );


	glEnable( GL_LIGHTING );
	glLightfv( GL_LIGHT0, GL_SPECULAR, WorldLight );
	glLightfv( GL_LIGHT0, GL_POSITION, WorldLightPosition );
	glLightfv( GL_LIGHT0, GL_SPOT_DIRECTION, WorldLightDirection );
	glEnable( GL_LIGHT0 );
	glColorMaterial( GL_FRONT_AND_BACK, GL_EMISSION );
	glEnable( GL_COLOR_MATERIAL );
	
	glEnableClientState(GL_VERTEX_ARRAY);

	assert( !glGetError( ) );
}

void SetupPixelDescriptor( HDC hDC )
{
	int PixelFormat = 0;

	static PIXELFORMATDESCRIPTOR pfd = {
		sizeof( PIXELFORMATDESCRIPTOR ),
			1,
			PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_GENERIC_ACCELERATED|
			PFD_DOUBLEBUFFER, PFD_TYPE_RGBA,
			32,
			0, 0, 0, 0, 0, 0,
			0,
			0,
			0,
			0, 0, 0, 0,
			16,
			0,
			0,
			PFD_MAIN_PLANE,
			0,
			0, 0, 0
	};

	PixelFormat = ChoosePixelFormat( hDC, &pfd );
	SetPixelFormat( hDC, PixelFormat, &pfd );
}

bool GoFullscreen( int ScreenWidth, int ScreenHeight, int ScreenBpp )
{
	DEVMODE devModeScreen;
	memset( &devModeScreen, 0, sizeof( devModeScreen ) );
	devModeScreen.dmSize = sizeof( devModeScreen );
	devModeScreen.dmPelsWidth = ScreenWidth;
	devModeScreen.dmPelsHeight = ScreenHeight;
	devModeScreen.dmBitsPerPel = ScreenBpp;
	devModeScreen.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

	if( ChangeDisplaySettings( &devModeScreen, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL )
	{
		fullscreen = false;
		return false;
	}

	return true;
}



int extensionInit( )
{
	char Extension_Name[ ] = "EXT_fog_coord";

	// Allocate Memory For Our Extension String
	char* glextstring=(char *)malloc( strlen( (char *) glGetString(GL_EXTENSIONS) ) + 1 );
	strcpy ( glextstring, (char *) glGetString( GL_EXTENSIONS ) );		// Grab The Extension List, Store In glextstring

	if (!strstr(glextstring,Extension_Name))				// Check To See If The Extension Is Supported
		return FALSE;							// If Not, Return FALSE

	free( glextstring );							// Free Allocated Memory
	// Setup And Enable glFogCoordEXT
	glFogCoordfEXT = (PFNGLFOGCOORDFEXTPROC) wglGetProcAddress( "glFogCoordfEXT" );

	return TRUE;
}

int mainLoop( )
{
	MSG msg;
	Core::timer.resetTimer( );
	
	float to = 0.0f;
	size_t frameCount = 0;

	while( true )
	{
	
		if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
		{
			if( msg.message == WM_QUIT )
				break;
			
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		else {
			to = Core::timer.getTime( );
			Core::TimeStep = (Core::timer.getTime( ) - to);
			glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); // Clear Screen And Depth Buffer
			glClearColor( 0.2f, 0.2f, 0.2f, 0.0f );
			glLoadIdentity( );
			
			UI::timerEventNotifier.isExpired( );
			UI::pManager->render( );
			
			SwapBuffers( hDC );
			frameCount++;

			Core::TimeStep = (Core::timer.getTime( ) - to);
			to = Core::timer.getTime( );

			if( Core::TimeStep < SIXTY_FPS )
				Sleep( (DWORD) SIXTY_FPS - Core::TimeStep );
			
			assert( glGetError( ) == GL_NO_ERROR );
		}
	}

	Core::deinitialize( );
	return 0;
}
