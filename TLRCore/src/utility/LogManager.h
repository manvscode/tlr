#pragma once
#ifndef __LOGMANAGER_H__
#define __LOGMANAGER_H__
/*
 *	LogManager.h
 *
 *	A class that manages the log files.
 *	Coded by Joseph A. Marrero
 */
#include <map>
#include <cstddef>
#include "LogFile.h" 
using namespace std;

namespace Utility {

typedef unsigned short int LogID;

typedef enum tagDefaultLogID {
	LOG_MAIN = 0,
	LOG_EMPTY = 1
} DefaultLogID;


template <class EnumLogID = DefaultLogID,
		  size_t Max = 1>
class CLogManager
{
  public:
	static CLogManager<EnumLogID, Max> *getInstance( );
	~CLogManager( );
	
 	void addLogFileMapping( const EnumLogID logid, const string &logFilename );

	void newLine( const EnumLogID logid );
	void write( const EnumLogID logid, const string &logText );
	void writef( const EnumLogID logid, const char *logTextFormat, ... );
	//void writef( const EnumLogID logid, const string &logText );
	void writeLine( const EnumLogID logid, const string &logText );
	void writeLinef( const EnumLogID logid, const char *logTextFormat, ... );
  	//void writeLinef( const EnumLogID logid, const string &logText );
  
  	//void writeToAll( const EnumLogID logid, const char *logText );
	//void writeLineToAll( const EnumLogID logid, const char *logText );
	
	bool isEmpty( ) { return m_LogFiles.size( ) == 0; }
	// These are not synchronized yet
  	void setLog( const EnumLogID logid );
  	
	ostream &operator<< ( bool val );
	ostream &operator<< ( short val );
	ostream &operator<< ( unsigned short val );
	ostream &operator<< ( int val );
	ostream &operator<< ( unsigned int val );
	ostream &operator<< ( long val );
	ostream &operator<< ( unsigned long val );
	ostream &operator<< ( float val );
	ostream &operator<< ( double val );
	ostream &operator<< ( long double val );
	ostream &operator<< ( const char *val );
	ostream &operator<< ( string &val );
	
  private:
	CLogManager( );
	static CLogManager<EnumLogID, Max> *m_pInstance;
	map<const LogID, CLogFile *> m_LogFiles;
	const size_t m_MaxNumberOfLogs;
	size_t m_NumberOfLogs;
	EnumLogID m_CurrentLog;
  	EnumLogID m_tagLodID;
  	

};


} //end of namespace
#include "LogManager.inl"
#endif
