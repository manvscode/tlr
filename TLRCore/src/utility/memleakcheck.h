#pragma once
#ifndef _MEMLEAKCHECK_H_
#define _MEMLEAKCHECK_H_


/*
 *	Win32 memory debug stuff
 */
#ifdef WIN32

#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define MEMLEAK_CHECK \
	int flag =_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF | _CRTDBG_REPORT_FLAG; \
	_CrtSetDbgFlag(flag); \
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );



// For MFC
#define MEM_LEAK_SNAPSHOT_VARS \
	_CrtMemState  oldMemState, newMemState, diffMemState;
	//CMemoryState oldMemState, newMemState, diffMemState; 

#define BEGIN_MEM_LEAK_SNAPSHOT \
	_CrtMemCheckpoint( &oldMemState );
	//oldMemState.Checkpoint(); 

#define END_MEM_LEAK_SNAPSHOT \
	_CrtMemCheckpoint( &newMemState ); \
	if( _CrtMemDifference( diffMemState, oldMemState, newMemState ) ) == TRUE ) \
	{ \
		TRACE0( "Opps. Memory leaked!\n", /*__FILE__*//*, __LINE__ */); \
		_CrtDumpMemoryLeaks(); \
	}
	//newMemState.Checkpoint(); \
	//if( diffMemState.Difference( oldMemState, newMemState ) ) \
	//{ \
	//	TRACE0( "Opps. Memory leaked!\n", /*__FILE__*//*, __LINE__ */); \
	//	diffMemState.DumpStatistics(); \
	//} 

/*
 *	Linux memory debug stuff
 */

// vagrind stuff here...
#endif

#endif // end of _MEMLEAKCHECK_H_