#pragma once
#ifndef __OBJECTPOOL_H__
#define __OBJECTPOOL_H__
/*
 *		ObjectPool.h
 *
 *		An object pool template. Allows for faster allocation by 
 *		avoiding calling the new operator.
 *
 *		Coded Joseph A. Marrero
 *		5/11/06
 */
#ifdef DEBUG_OBJECTPOOL
#include <iostream>
#endif
#include <list>
#include "debug.h"
namespace Utility {

template <class AnyType, unsigned int Size = 256>
class CObjectPool
{
  public:
	static CObjectPool *getInstance( )
	{
		if( m_Instance == 0 )
			m_Instance = new CObjectPool( );
		return m_Instance;
	}

	virtual ~CObjectPool( )
	{
		m_Instance = 0;
		assert( m_NumberOfAllocations >= 0 ); // there are leaked objects

		for( register unsigned int i = 0; i < m_PoolSize; i++ )
		{
			delete m_ObjectPool.front( );
			m_ObjectPool.pop_front( );
		}
		#ifdef DEBUG_OBJECTPOOL
		std::cout << "CObjectPool::~CObjectPool()" << endl;
		#endif
	}

	AnyType *allocate( )
	{
		if( m_NumberOfAllocations++ == m_PoolSize )
			doubleSize( );
		
		AnyType *object = m_ObjectPool.front( );
		m_ObjectPool.pop_front( );
		return object;
	}

	void release( const AnyType *object )
	{
		#ifdef DEBUG_OBJECTPOOL
		//std::cout << "release( ): type = " <<  typeid(*object).name( ) << ", " << typeid(AnyType).name( ) << std::endl;
		//assert( !strcmp(typeid(AnyType).name( ), typeid(*object).name( )) );
			//memset( (void *) object, 0, sizeof(AnyType) );
		#endif
		m_ObjectPool.push_back( const_cast<AnyType *>( object ) );
		m_NumberOfAllocations--;
	}

	inline unsigned int getNumberOfAllocations( ) const
	{
		return m_NumberOfAllocations;
	}

	void doubleSize( )
	{
		unsigned int newPoolSize = ( m_PoolSize << 1) + 1; // 2 * p + 1
		for( register unsigned int i = m_PoolSize; i < newPoolSize; i++ )
			m_ObjectPool.push_back( new AnyType( ) );
		m_PoolSize = newPoolSize;
		#ifdef DEBUG_OBJECTPOOL
		std::cout << "doubleSize( ): \tPool Size = " << m_PoolSize << ", Allocations = "<< m_NumberOfAllocations << endl;
		#endif
	}

	void halveSize( )
	{
		unsigned int doubleAllocations =  m_NumberOfAllocations << 1;
		if( doubleAllocations > m_PoolSize ) return; // can't halve.
		unsigned int newPoolSize = (unsigned int) ( (m_PoolSize - doubleAllocations) >> 1 ) + m_NumberOfAllocations + 1;
		unsigned int removeAmount = m_PoolSize - newPoolSize;
		
		for( register unsigned int i = 0; i < removeAmount; i++ )
		{
			AnyType *t =  m_ObjectPool.front( );
			delete t;
			m_ObjectPool.pop_front( );
		}
		m_PoolSize = newPoolSize;
		#ifdef DEBUG_OBJECTPOOL	
		std::cout << "halveSize( ): \tPool Size = " << m_PoolSize << ", Allocations = "<< m_NumberOfAllocations << endl;
		#endif
	}

  protected:
	CObjectPool( )
		: m_ObjectPool(), m_PoolSize(Size), m_NumberOfAllocations(0)
	{
		for( register unsigned int i = 0; i < m_PoolSize; i++ )
			m_ObjectPool.push_back( new AnyType( ) );
	}


	std::list<AnyType *> m_ObjectPool;
	unsigned int m_PoolSize, m_NumberOfAllocations;
	static CObjectPool *m_Instance;

};

template <class AnyType, unsigned int Size>
CObjectPool<AnyType, Size> *CObjectPool<AnyType, Size>::m_Instance = 0;

} //end of namespace
#endif
