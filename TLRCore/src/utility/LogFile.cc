/*
 *	LogFile.cc
 *	Coded by Joseph A. Marrero
 */
#include <sstream>
#include <time.h>
#include <errno.h>
#include <stdarg.h>
#include "LogFile.h"

namespace Utility {

CLogFile::CLogFile( const char *logFile )
	: m_FileStream(), m_LogFilename(logFile), m_bLogTime(true), m_localTime("")
{
	m_FileStream.open( logFile, ios::out | ios::app );
}

CLogFile::CLogFile( const string &logFile )
	: m_FileStream(), m_LogFilename(logFile), m_bLogTime(true), m_localTime("")
{
	m_FileStream.open( logFile.c_str( ), ios::out | ios::app );
}

CLogFile::CLogFile( const char *logFile, bool bLogTime )
	: m_FileStream(), m_LogFilename(logFile), m_bLogTime(bLogTime), m_localTime("")
{
	m_FileStream.open( logFile, ios::out | ios::app );
}

CLogFile::CLogFile( const string &logFile, bool bLogTime )
	: m_FileStream(), m_LogFilename(logFile), m_bLogTime(bLogTime), m_localTime("")
{
	m_FileStream.open( logFile.c_str( ), ios::out | ios::app );
}

CLogFile::~CLogFile( )
{
	m_FileStream.close( );
}

bool CLogFile::initialize( )
{
	if( !m_FileStream )
	{
		perror( "LOG ERROR: " );
		return false;
	}
	
	return true;
}

inline const string &CLogFile::getLogFilename( ) const
{
	return m_LogFilename;
}

void CLogFile::newLine( )
{
	m_FileStream << endl;
}

/*
 * 	Unformatted Log Output
 */
void CLogFile::write( const char *logText )
{
	m_FileStream << logText;
}

void CLogFile::write( const string &logText )
{
	m_FileStream << logText;
}

void CLogFile::writeLine( const string &logText )
{
	m_FileStream << logText << endl;
}

void CLogFile::writeLine( const char *logText )
{
	m_FileStream << logText << endl;
}

/*
 * 	Formatted Log Output
 */
//void CLogFile::writef( const char *logText )
//{
//	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << logText;
//}

void CLogFile::writef( const string &logText )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << logText;
}

void CLogFile::writef( const char *logTextFormat, ... )
{
	va_list body;
	//string output;

	va_start( body, logTextFormat );
	
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "");
	int old_precision = m_FileStream.precision( );
	m_FileStream.setf( ios_base::fixed | ios_base::showpoint );

	m_FileStream.precision( 2 );

	for( int i = 0; logTextFormat[ i ] != '\0'; ++i )
	{
		if( logTextFormat[ i ] == '%' )
		{
			switch( logTextFormat[ ++i ] )
			{
				case 'c':
					m_FileStream << ( (char) va_arg( body, int ) );
					break;
				case 'i':
				case 'd':
					m_FileStream << va_arg( body, int );
					break;
				case 'u':
					m_FileStream << va_arg( body, unsigned int );
					break;
				case 's':
					m_FileStream << va_arg( body, char * );
					break;
				case 'f':
					m_FileStream << va_arg( body, double );
					break;
				case 'p':
					m_FileStream << va_arg( body, void * );
					break;
				case '%':
					m_FileStream << '%';
				case 'l':
					switch( logTextFormat[ ++i ] )
					{
						case 'd':
							m_FileStream << va_arg( body, long int );
							break;
						default:
							break;
					}
				default:
					m_FileStream << logTextFormat[ i ];
					break;
			}
		}
		else if( logTextFormat[ i ] == '\\' )
		{
			switch( logTextFormat[ ++i ] )
			{
				case 'n':
					m_FileStream << endl;
					break;
				case 't':
					m_FileStream << "\t";
					break;
				case 'v':
					m_FileStream << "\v";
					break;
				//case 'c':
				//	m_FileStream << "\c";
				//	break;
				case '\\':
					m_FileStream << '\\';
				default:
					break;
			}
		}
		else
			m_FileStream << logTextFormat[ i ];
	}

	m_FileStream.precision( old_precision );
	
	
	va_end( body );
}

//void CLogFile::writeLinef( const char *logText )
//{
//	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << logText << endl;
//	
//}

void CLogFile::writeLinef( const string &logText )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << logText << endl;
}

void CLogFile::writeLinef( const char *logTextFormat, ... )
{
	va_list body;
	//string output;

	va_start( body, logTextFormat );
	
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "");
	int old_precision = m_FileStream.precision( );
	m_FileStream.setf( ios_base::fixed | ios_base::showpoint );

	m_FileStream.precision( 2 );

	for( int i = 0; logTextFormat[ i ] != '\0'; ++i )
	{
		if( logTextFormat[ i ] == '%' )
		{
			switch( logTextFormat[ ++i ] )
			{
				case 'c':
					m_FileStream << ( (char) va_arg( body, int ) );
					break;
				case 'i':
				case 'd':
					m_FileStream << va_arg( body, int );
					break;
				case 'u':
					m_FileStream << va_arg( body, unsigned int );
					break;
				case 's':
					m_FileStream << va_arg( body, char * );
					break;
				case 'f':
					m_FileStream << va_arg( body, double );
					break;
				case 'p':
					m_FileStream << va_arg( body, void * );
					break;
				case '%':
					m_FileStream << '%';
				case 'l':
					switch( logTextFormat[ ++i ] )
					{
						case 'd':
							m_FileStream << va_arg( body, long int );
							break;
						default:
							break;
					}
				default:
					m_FileStream << logTextFormat[ i ];
					break;
			}
		}
		else if( logTextFormat[ i ] == '\\' )
		{
			switch( logTextFormat[ ++i ] )
			{
				case 'n':
					m_FileStream << endl;
					break;
				case 't':
					m_FileStream << "\t";
					break;
				case 'v':
					m_FileStream << "\v";
					break;
				//case 'c':
				//	m_FileStream << "\c";
				//	break;
				case '\\':
					m_FileStream << '\\';
				default:
					break;
			}
		}
		else
			m_FileStream << logTextFormat[ i ];
	}

	m_FileStream << endl;

	m_FileStream.precision( old_precision );
	va_end( body );
}

void CLogFile::operator<< ( ostream &os )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << os << endl;
}

ostream &CLogFile::operator<< ( bool val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( short val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( unsigned short val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( int val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( unsigned int val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( long val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( unsigned long val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( float val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( double val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( long double val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( char val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( const char *val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

ostream &CLogFile::operator<< ( const string &val )
{
	m_FileStream << (m_bLogTime ? getTimeString( ) + " - " : "") << val;
	return m_FileStream;
}

string &CLogFile::getTimeString( )
{
	time_t now;
	time( &now );
	const struct tm *localTimeNow = localtime( &now );
	char timeString[ 32 ];
	strftime( timeString, sizeof(char) * 32, "[%x, %X %p]", localTimeNow );
	m_localTime = timeString;
	return m_localTime;
}

ostream &CLogFile::getStream( )
{
	return m_FileStream;
}

} //end of namespace
