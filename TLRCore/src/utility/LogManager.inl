/*
 *	LogManager.inl
 *
 *	A class that manages the log files.
 *
 *	I had to remove the typename's in order to get this to compile
 *	with g++. grrrr....
 *	Coded by Joseph A. Marrero
 */
#include <ostream>
#include <iostream>
#include <sstream>
#include <cstdarg>
//#include "debug.h"


namespace Utility {

template <class EnumLogID,
		  size_t Max>
CLogManager<EnumLogID, Max> *CLogManager<EnumLogID, Max>::m_pInstance = 0;

template <class EnumLogID,
		  size_t Max>
CLogManager<EnumLogID, Max> *CLogManager<EnumLogID, Max>::getInstance( )
{
	if( !m_pInstance )
		m_pInstance = new CLogManager( );

	return m_pInstance;
}

template <class EnumLogID,
		  size_t Max>
CLogManager<EnumLogID, Max>::CLogManager( )
	: m_LogFiles( ), m_MaxNumberOfLogs(Max), m_NumberOfLogs(0), m_CurrentLog(), m_tagLodID()
{
}

template <class EnumLogID,
		  size_t Max>
CLogManager<EnumLogID, Max>::~CLogManager( )
{
	map<const LogID, CLogFile *>::iterator itr;
	
	for( itr = m_LogFiles.begin( ); itr != m_LogFiles.end( ); ++itr )
		delete itr->second;
}

template <class EnumLogID,
		  size_t Max>
void CLogManager<EnumLogID, Max>::addLogFileMapping( const EnumLogID logid, const string &logFilename )
{
	//assert( m_NumberOfLogs < m_MaxNumberOfLogs );
	m_LogFiles[ logid ] = new CLogFile( logFilename );
	//m_NumberOfLogs++;
}

template <class EnumLogID,
		  size_t Max>
void CLogManager<EnumLogID, Max>::newLine( const EnumLogID logid )
{
	if( isEmpty( ) )
		return;
	m_LogFiles[ logid ]->newLine( );
}

template <class EnumLogID,
		  size_t Max>
void CLogManager<EnumLogID, Max>::write( const EnumLogID logid, const string &logText )
{
	if( isEmpty( ) )
		return;
	m_LogFiles[ logid ]->write( logText );
}


template <class EnumLogID,
		  size_t Max>
void CLogManager<EnumLogID, Max>::writef( const EnumLogID logid, const char *logTextFormat, ... )
{
	if( isEmpty( ) )
		return;
	va_list body;
	va_start( body, logTextFormat );
	ostringstream oss;

	for( int i = 0; logTextFormat[ i ] != '\0'; ++i )
	{
		if( logTextFormat[ i ] == '%' )
		{
			switch( logTextFormat[ ++i ] )
			{
				case 'c':
					oss << ( (char) va_arg( body, int ) );
					break;
				case 'i':
				case 'd':
					oss << va_arg( body, int );
					break;
				case 'u':
					oss << va_arg( body, unsigned int );
					break;
				case 's':
					oss << va_arg( body, char * );
					break;
				case 'f':
					oss << va_arg( body, double );
					break;
				case 'p':
					oss << va_arg( body, void * );
					break;
				case '%':
					oss << '%';
				case 'l':
					switch( logTextFormat[ ++i ] )
					{
						case 'd':
							oss << va_arg( body, long int );
							break;
						default:
							break;
					}
				default:
					oss << logTextFormat[ i ];
					break;
			}
		}
		else if( logTextFormat[ i ] == '\\' )
		{
			switch( logTextFormat[ ++i ] )
			{
				case 'n':
					oss << endl;
					break;
				case 't':
					oss << "\t";
					break;
				case 'v':
					oss << "\v";
					break;
				//case 'c':
				//	oss << "\c";
				//	break;
				case '\\':
					oss << '\\';
				default:
					break;
			}
		}
		else 
			oss << logTextFormat[ i ];
	}
	
	*m_LogFiles[ logid ] << oss.str( ).c_str( );

	va_end( body );
}

//template <class EnumLogID,
//		  size_t Max>
//void CLogManager<EnumLogID, Max>::writef( const EnumLogID logid, const string &logText )
//{
//	m_LogFiles[ logid ]->writef( logText );
//}

template <class EnumLogID,
		  size_t Max>
void CLogManager<EnumLogID, Max>::writeLine( const EnumLogID logid, const string &logText )
{
	if( isEmpty( ) )
		return;
	m_LogFiles[ logid ]->writeLine( logText );
}

template <class EnumLogID,
		  size_t Max>
void CLogManager<EnumLogID, Max>::writeLinef( const EnumLogID logid, const char *logTextFormat, ... )
{
  	if( isEmpty( ) )
		return;
	va_list body;
	va_start( body, logTextFormat );
	ostringstream oss;

	for( int i = 0; logTextFormat[ i ] != '\0'; ++i )
	{
		if( logTextFormat[ i ] == '%' )
		{
			switch( logTextFormat[ ++i ] )
			{
				case 'c':
					oss << ( (char) va_arg( body, int ) );
					break;
				case 'i':
				case 'd':
					oss << va_arg( body, int );
					break;
				case 'u':
					oss << va_arg( body, unsigned int );
					break;
				case 's':
					oss << va_arg( body, char * );
					break;
				case 'f':
					oss << va_arg( body, double );
					break;
				case 'p':
					oss << va_arg( body, void * );
					break;
				case '%':
					oss << '%';
				case 'l':
					switch( logTextFormat[ ++i ] )
					{
						case 'd':
							oss << va_arg( body, long int );
							break;
						default:
							break;
					}
				default:
					oss << logTextFormat[ i ];
					break;
			}
		}
		else if( logTextFormat[ i ] == '\\' )
		{
			switch( logTextFormat[ ++i ] )
			{
				case 'n':
					oss << endl;
					break;
				case 't':
					oss << "\t";
					break;
				case 'v':
					oss << "\v";
					break;
				//case 'c':
				//	oss << "\c";
				//	break;
				case '\\':
					oss << '\\';
				default:
					break;
			}
		}
		else 
			oss << logTextFormat[ i ];
	}
	
	
	*m_LogFiles[ logid ] << oss.str( ) << endl;
	va_end( body );
}



//template <class EnumLogID,
//		  size_t Max>
//void CLogManager<EnumLogID, Max>::writeLinef( const EnumLogID logid, const string &logText )
//{
//	m_LogFiles[ logid ]->writeLinef( logText );
//}


template <class EnumLogID,
		  size_t Max>
inline void CLogManager<EnumLogID, Max>::setLog( const EnumLogID logid )
{
	m_CurrentLog = logid;
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( bool val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( short val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( unsigned short val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( int val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( unsigned int val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( long val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( unsigned long val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( float val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( double val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( long double val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( const char *val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}

template <class EnumLogID,
		  size_t Max>
ostream &CLogManager<EnumLogID, Max>::operator<< ( string &val )
{
	*m_LogFiles[ m_CurrentLog ] << val;
	return m_LogFiles[ m_CurrentLog ]->getStream( );
}


} //end of namespace
