/*
 *	Huffman.cpp
 *
 *	Huffman encoder/decoder.
 *	Coded by Joseph A. Marrero
 *	http://www.L33Tprogrammer.com/
 *	December 16, 2007 - Merry Christmass!
 */
#include <stack>
#include <bitset>
#include <algorithm>
#include <climits>
#include <cassert>
#include "Huffman.h"
using namespace std;

namespace Encoding {


Huffman::Huffman( )
 : m_File(), m_FrequencyTable(), m_InBuffer(), m_OutBuffer(), m_Root(NULL)
{
}

Huffman::~Huffman( )
{
}

bool Huffman::open( const string &filename )
{ m_File.open( filename.c_str(), ios::in | ios::binary ); return !m_File.fail( ); }

bool Huffman::save( const string &outFile )
{
	m_File.open( outFile.c_str( ), ios::out | ios::binary );
	if( m_File.fail( ) )
		return false;

	for( int i = 0; i < m_OutBuffer.size( ); i++ )
		m_File.write( (char *) &m_OutBuffer[ i ], sizeof(byte) );
	//m_File.write( &m_OutBuffer[ 0 ], m_OutBuffer.size( ) );
	close( );
	return true;
}

void Huffman::close( )
{ m_File.close( ); m_File.clear( ); }

bool Huffman::encode( const string &filename )
{
	if( !open( filename ) )
		return false;
	m_InBuffer.clear( );

	// build frequency table...
	while( !m_File.eof( ) )
	{
		byte currentByte = 0x00;
		m_File.read( (char *) &currentByte, sizeof(byte) );

		if( m_File.fail( ) ) break;

		m_InBuffer.push_back( currentByte );
		m_FrequencyTable[ currentByte ]++;
	}

	unsigned int nFrequencyTableSize = m_FrequencyTable.size( ) * (sizeof(byte) + sizeof(unsigned int)) + sizeof(unsigned int);
	
#ifndef _DEBUG // ignore this check during debugging
	if( nFrequencyTableSize > m_InBuffer.size( ) )
		return false;
#endif

	stdext::hash_map<byte, HuffNode *> leafNodeTable;
	
	if( !buildHuffmanTree( m_Root, m_FrequencyTable, &leafNodeTable ) )
		return false;

	stack<bool> bitSequenceStack;
	bitset<CHAR_BIT> partialByte;
	unsigned int currentBit = 0;
	

	m_OutBuffer.clear( );

	// save file identifier...
	byte *pFileId = (byte *) &FILE_ID;
	for( int i = 0; i < 4; i++ )
		m_OutBuffer.push_back( (byte) *(pFileId + i) );

	
	unsigned long nNumberOfBytesEncoded = m_InBuffer.size( );
	byte *p_nNumberOfBytesEncoded = (byte *) &nNumberOfBytesEncoded;

	// write the number of bytes compressed...
	for( int i = 0; i < sizeof(unsigned long); i++ )
		m_OutBuffer.push_back( p_nNumberOfBytesEncoded[ i ] );

	saveFrequencyTable( m_OutBuffer, m_FrequencyTable );

	for( unsigned int i = 0; i < m_InBuffer.size( ); i++ )
	{
		HuffNode *pLeafNode = leafNodeTable[ m_InBuffer[ i ] ];
		assert( pLeafNode != NULL );
		
		while( pLeafNode != m_Root )
		{
			bitSequenceStack.push( pLeafNode->parent->left == pLeafNode ? false : true );
			pLeafNode = pLeafNode->parent;
		}
		
		while( bitSequenceStack.size( ) > 0 )
		{
			partialByte.set( CHAR_BIT - (++currentBit), bitSequenceStack.top( ) );
			bitSequenceStack.pop( );

			if( currentBit >= 8 ) // write this full byte out 
			{
				unsigned long ulByte = partialByte.to_ulong( );
				byte *p_ulByte = (byte *) &ulByte;
				m_OutBuffer.push_back( p_ulByte[ 0 ] );
				currentBit = 0;
				partialByte.reset( );
			}
		}
	}

	if( currentBit < 8 ) // write this full byte out 
	{
		unsigned long ulByte = partialByte.to_ulong( );
		byte *p_ulByte = (byte *) &ulByte;
		m_OutBuffer.push_back( p_ulByte[ 0 ] );
		currentBit = 0;
	}
	
	destroyTree( m_Root );
	close( );
	return true;
}

bool Huffman::decode( const string &filename )
{
	if( !open( filename ) )
		return false;


	m_InBuffer.clear( );

	while( !m_File.eof( ) )
	{
		byte currentByte = 0x00;
		m_File.read( (char *) &currentByte, sizeof(byte) );
		
		if( m_File.fail( ) ) break;
		m_InBuffer.push_back( currentByte );
	}

	// verify the file identifier...
	byte *pFileId = (byte *) &FILE_ID;
	if( m_InBuffer[ 0 ] != pFileId[ 0 ] || 
		m_InBuffer[ 1 ] != pFileId[ 1 ] || 
		m_InBuffer[ 2 ] != pFileId[ 2 ] || 
		m_InBuffer[ 3 ] != pFileId[ 3 ] )
		return false; // not compressed with this code...

	unsigned long nNumberOfBytesToBeDecoded = 0;
	// get the number of bytes to be decoded...
	memcpy( &nNumberOfBytesToBeDecoded, &m_InBuffer[ 4 ], sizeof(unsigned long) );

	if( m_InBuffer.size( ) <= 0 )
		return false;
	if( !loadFrequencyTable( m_InBuffer, m_FrequencyTable, 4 + sizeof(unsigned long) ) )
		return false;

	// build Huffman tree...
	if( !buildHuffmanTree( m_Root, m_FrequencyTable ) )
		return false;

	unsigned int nStartPosition = 4 + sizeof(unsigned long) + m_FrequencyTable.size( ) * (sizeof(byte) + sizeof(unsigned int)) + sizeof(unsigned int);
	bitset<CHAR_BIT> *pByteBits = new bitset<CHAR_BIT>( (unsigned long) m_InBuffer[ nStartPosition ] );
	m_OutBuffer.clear( );
	unsigned int nBit = 0;

	for( unsigned int i = nStartPosition; i < m_InBuffer.size( ) && nNumberOfBytesToBeDecoded > 0; )
	{
		HuffNode *pNode = m_Root;
		
		while( !isLeaf( pNode ) )
		{
			if( nBit >= CHAR_BIT )
			{
				delete pByteBits;
				pByteBits =  new bitset<CHAR_BIT>( (unsigned long) m_InBuffer[ ++i ] );
				nBit = 0;
			}

			if( !pByteBits->test( CHAR_BIT - (++nBit) ) )
				pNode = pNode->left;
			else
				pNode = pNode->right;
		}

		assert( pNode != NULL );
		m_OutBuffer.push_back( pNode->theByte );
		nNumberOfBytesToBeDecoded--;
	}

	delete pByteBits;
	destroyTree( m_Root );
	close( );
	return true;
}



stdext::hash_map<Huffman::byte, unsigned int> Huffman::buildFrequencyTable( const byte *buffer, unsigned int length )
{
	stdext::hash_map<byte, unsigned int> frequencyTable;

	for( unsigned int i = 0; i < length; i++ )
		frequencyTable[ buffer[ i ] ]++;
	return frequencyTable;
}

bool Huffman::buildHuffmanTree( HuffNode *&treeRoot, 
								 stdext::hash_map<byte, unsigned int> &frequencyTable,
								 stdext::hash_map<byte, HuffNode *> *leafNodeTable )
{
	if( frequencyTable.size( ) <= 0 ) return false;

	HuffCompare compare;
	vector<frequencyAndHuffNode> frequencies;
	stdext::hash_map<byte, unsigned int>::const_iterator itr;


	// bof build min heap...
	for( itr = frequencyTable.begin(); itr != frequencyTable.end(); ++itr )
		frequencies.push_back( frequencyAndHuffNode( itr->second, 
							   new HuffNode( itr->first, itr->second, NULL, NULL, NULL, true ) ) );

	make_heap(frequencies.begin( ), frequencies.end( ), compare );
	// eof build min heap...

	// bof build huffman tree...
	HuffNode *pNode = NULL;

	while( frequencies.size( ) > 1 )
	{
		pop_heap( frequencies.begin( ), frequencies.end( ), compare );
		frequencyAndHuffNode &freqAndHuffNode1 = frequencies[ frequencies.size( ) - 1 ];
		frequencies.pop_back( );

		pop_heap( frequencies.begin( ), frequencies.end( ), compare /* Make it a min heap */ );
		frequencyAndHuffNode &freqAndHuffNode2 = frequencies[ frequencies.size( ) - 1 ];
		frequencies.pop_back( );

		if( leafNodeTable != NULL )
		{
			// add leaf nodes...
			if( freqAndHuffNode1.second->bContainsData )
				(*leafNodeTable)[ freqAndHuffNode1.second->theByte ] = freqAndHuffNode1.second;
			
			if( freqAndHuffNode2.second->bContainsData )
				(*leafNodeTable)[ freqAndHuffNode2.second->theByte ] = freqAndHuffNode2.second;
		}

		
		pNode = addTwoNodesToTree( freqAndHuffNode1, freqAndHuffNode2 );

		frequencies.push_back( frequencyAndHuffNode( freqAndHuffNode1.first + freqAndHuffNode2.first, pNode ) );
		push_heap( frequencies.begin( ), frequencies.end(), compare );
	}
	
	treeRoot = pNode;
	// eof build huffman tree...
	return true;
}

Huffman::HuffNode *Huffman::addTwoNodesToTree( frequencyAndHuffNode &f1, frequencyAndHuffNode &f2 )
{
	// set up frequecy node 1
	//HuffNode *node1 = new HuffNode( f1.second->theByte, f1.first, NULL, NULL, NULL );
	HuffNode *node1 = f1.second;

	// set up frequecy node 2
	//HuffNode *node2 = new HuffNode( f2.second->theByte , f2.first, NULL, NULL, NULL );
	HuffNode *node2 = f2.second;

	// set up parent...
	HuffNode *parent = new HuffNode( NULL, node1->count + node2->count, node1, node2, NULL, false );
	node1->parent = parent;
	node2->parent = parent;


	return parent;
}




bool Huffman::encode( const vector<byte> &inBuffer,
					   vector<byte> &outBuffer )
{
	if( inBuffer.size( ) <= 0 ) return false;

	stdext::hash_map<byte, unsigned int> &frequencyTable = buildFrequencyTable( &inBuffer[ 0 ], inBuffer.size( ) );
	unsigned int nFrequencyTableSize = frequencyTable.size( ) * (sizeof(byte) + sizeof(unsigned int)) + sizeof(unsigned int);
	
#ifndef _DEBUG
	if( nFrequencyTableSize > inBuffer.size( ) )
		return false;
#endif
	
	outBuffer.clear( );

	unsigned long nNumberOfBytesEncoded = inBuffer.size( );
	byte *p_nNumberOfBytesEncoded = (byte *) &nNumberOfBytesEncoded;
	// write the number of bytes compressed...
	for( int i = 0; i < sizeof(unsigned long); i++ )
		outBuffer.push_back( p_nNumberOfBytesEncoded[ i ] );

	stdext::hash_map<byte, HuffNode *> leafNodeTable;
	HuffNode *root = NULL;
	
	if( !buildHuffmanTree( root, frequencyTable, &leafNodeTable ) )
		return false;

	stack<bool> bitSequenceStack;
	bitset<CHAR_BIT> partialByte;
	unsigned int currentBit = 0;


	saveFrequencyTable( outBuffer, frequencyTable );
	
	for( unsigned int i = 0; i < inBuffer.size( ); i++ )
	{
		HuffNode *pLeafNode = leafNodeTable[ inBuffer[ i ] ];
		assert( pLeafNode != NULL );
		
		while( pLeafNode != root )
		{
			bitSequenceStack.push( pLeafNode->parent->left == pLeafNode ? false : true );
			pLeafNode = pLeafNode->parent;
		}
		
		while( bitSequenceStack.size( ) > 0 )
		{
			partialByte.set( CHAR_BIT - (++currentBit), bitSequenceStack.top( ) );
			bitSequenceStack.pop( );

			if( currentBit >= 8 ) // write this full byte out 
			{
				unsigned long ulByte = partialByte.to_ulong( );
				byte *p_ulByte = (byte *) &ulByte;
				outBuffer.push_back( p_ulByte[ 0 ] );
				currentBit = 0;
				partialByte.reset( );
			}
		}
	}


	if( currentBit < 8 ) // write this full byte out 
	{
		unsigned long ulByte = partialByte.to_ulong( );
		byte *p_ulByte = (byte *) &ulByte;
		outBuffer.push_back( p_ulByte[ 0 ] );
		currentBit = 0;
	}
	
	destroyTree( root );
	return true;
}


bool Huffman::decode( const vector<byte> &inBuffer,
					   vector<byte> &outBuffer )
{
	if( inBuffer.size( ) <= 0 ) return false;


	unsigned long nNumberOfBytesToBeDecoded = 0;

	stdext::hash_map<byte, unsigned int> frequencyTable;
	stdext::hash_map<byte, HuffNode *> leafNodeTable;

	// get the number of bytes to be decoded...
	memcpy( &nNumberOfBytesToBeDecoded, &inBuffer[ 0 ], sizeof(unsigned long) );

	// load frequency table...
	if( !loadFrequencyTable( inBuffer, frequencyTable, sizeof(unsigned long) ) )
		return false; // loading frequency table failed...

	// build Huffman tree...
	HuffNode *tree = NULL;
	if( !buildHuffmanTree( tree, frequencyTable, NULL ) )
		return false;

	unsigned int nStartPosition = sizeof(unsigned long) + frequencyTable.size( ) * (sizeof(byte) + sizeof(unsigned int)) + sizeof(unsigned int);
	bitset<CHAR_BIT> *pByteBits = new bitset<CHAR_BIT>( (unsigned long) inBuffer[ nStartPosition ] );
	outBuffer.clear( );
	unsigned int nBit = 0;

	for( unsigned int i = nStartPosition; i < inBuffer.size( ) && nNumberOfBytesToBeDecoded > 0; )
	{
		HuffNode *pNode = tree;
		
		while( !isLeaf( pNode ) )
		{
			if( nBit >= CHAR_BIT )
			{
				delete pByteBits;
				pByteBits =  new bitset<CHAR_BIT>( (unsigned long) inBuffer[ ++i ] );
				nBit = 0;
			}

			if( !pByteBits->test( CHAR_BIT - (++nBit) ) )
				pNode = pNode->left;
			else
				pNode = pNode->right;
		}

		assert( pNode != NULL );
		outBuffer.push_back( pNode->theByte );
		nNumberOfBytesToBeDecoded--;
	}

	delete pByteBits;
	destroyTree( tree );
	return true;
}


void Huffman::saveFrequencyTable( vector<byte> &outBuffer, const stdext::hash_map<byte, unsigned int> &frequencyTable )
{
	unsigned int nSize = frequencyTable.size( );
	stdext::hash_map<byte, unsigned int>::const_iterator itr;

	byte *p_nSize = (byte *) &nSize;
	
	// save the size...
	for( unsigned int i = 0; i < sizeof(unsigned int); i++ )
		outBuffer.push_back( p_nSize[ i ] );

	// save the table...
	for( itr = frequencyTable.begin( ); itr != frequencyTable.end( ); ++itr )
	{
		// save the byte...
		outBuffer.push_back( itr->first );

		// save the byte's frequency...
		byte *pFrequency = (byte *) &itr->second;
		for( unsigned int i = 0; i < sizeof(unsigned int); i++ )
			outBuffer.push_back( pFrequency[ i ] );
	}
}

bool Huffman::loadFrequencyTable( const vector<byte> &inBuffer, stdext::hash_map<byte, unsigned int> &frequencyTable, unsigned int nPositionOfFrequencyTable )
{
	unsigned int nSize = 0;
	frequencyTable.clear( );

	if( inBuffer.size( ) < sizeof(unsigned int) ) return false;

	unsigned int totalTableSize = nSize * (sizeof(byte) + sizeof(unsigned int));

	if( inBuffer.size( ) < totalTableSize ) return false;

	memcpy( &nSize, &inBuffer[ nPositionOfFrequencyTable ], sizeof(unsigned int) );
	unsigned int nPosition = nPositionOfFrequencyTable + sizeof(unsigned int); // skip over the size we just read in...

	for( unsigned int i = 0; i < nSize; i++ )
	{
		byte theByte = inBuffer[ nPosition++ ];
		unsigned int frequency = 0;

		memcpy( &frequency, &inBuffer[ nPosition ], sizeof(unsigned int) );
		nPosition += sizeof(unsigned int);

		frequencyTable[ theByte ] = frequency;		
	}

	return true;
}

void Huffman::destroyTree( HuffNode *&tree )
{
	if( isLeaf( tree ) )
		delete tree;
	else 
	{
		destroyTree( tree->left );
		destroyTree( tree->right );
	}
}

} // end of namespace