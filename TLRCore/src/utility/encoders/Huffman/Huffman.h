#pragma once
#ifndef _HUFFMAN_H_
#define _HUFFMAN_H_
/*
 *	Huffman.h
 *
 *	Huffman encoder/decoder.
 *	Coded by Joseph A. Marrero
 *	http://www.L33Tprogrammer.com/
 *	December 16, 2007 - Merry Christmass!
 */
#include <fstream>
#include <vector>
#include <hash_map>
using namespace std;

namespace Encoding {

class Huffman
{
  public:
	typedef unsigned char byte;

  protected:
	static const int FILE_ID = 0xBEEFC0DE;
	struct HuffNode
	{
		byte theByte;
		unsigned int count;
		HuffNode *left, *right, *parent;
		bool bContainsData;

		HuffNode( byte b, unsigned int c, HuffNode *l, HuffNode *r, HuffNode *p, bool containsData = false )
		{ theByte = b; count = c; left = l; right = r; parent = p; bContainsData = containsData; }
	};

	typedef std::pair<unsigned int, Huffman::HuffNode *> frequencyAndHuffNode;

	struct HuffCompare
	{
		bool operator()( const frequencyAndHuffNode &n1, const frequencyAndHuffNode &n2 ) const
		{ return n1.first > n2.first; }
	};



	fstream m_File;
	stdext::hash_map<byte, unsigned int> m_FrequencyTable;
	//stdext::hash_map<byte, byte> m_ByteToCodeTable;
	vector<byte> m_InBuffer;
	vector<byte> m_OutBuffer;
	HuffNode *m_Root;

	/* Hidden Methods */
	static stdext::hash_map<byte, unsigned int> buildFrequencyTable( const byte *buffer, unsigned int length );
	static bool buildHuffmanTree( HuffNode *&treeRoot, 
								 stdext::hash_map<byte, unsigned int> &frequencyTable,
								 stdext::hash_map<byte, HuffNode *> *leafNodeTable = NULL );
	static HuffNode *addTwoNodesToTree( frequencyAndHuffNode &f1, frequencyAndHuffNode &f2 );
	static bool isLeaf( HuffNode *pNode ) { return pNode->right == NULL && pNode->left == NULL; }
	static void saveFrequencyTable( vector<byte> &outBuffer, const stdext::hash_map<byte, unsigned int> &frequencyTable );
	static bool loadFrequencyTable( const vector<byte> &inBuffer, stdext::hash_map<byte, unsigned int> &frequencyTable, unsigned int nPositionOfFrequencyTable = 0 );
	static void destroyTree( HuffNode *&tree );

	bool open( const string &filename );
	void close( );

  public:
	Huffman( );
	virtual ~Huffman( );

	bool save( const string &outFile );

	vector<byte> &getInBuffer( ) { return m_InBuffer; }
	vector<byte> &getOutBuffer( ) { return m_OutBuffer; }

	bool encode( const string &filename );
	bool decode( const string &filename );
	static bool encode( const vector<byte> &inBuffer,
					   vector<byte> &outBuffer );
	static bool decode( const vector<byte> &inBuffer,
					    vector<byte> &outBuffer );
};

} // end of namespace
#endif