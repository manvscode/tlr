#pragma once
#ifndef __SINGLETON_H__
#define __SINGLETON_H__

namespace Utility {

// Singleton Template
template<class AnyType>
class CSingleton
{
	public:	
		static AnyType* getInstance() 
		{
			if(m_instance == 0)
				m_instance = new AnyType();
			return m_instance;	
		}

		virtual ~CSingleton(){}

	protected:
		CSingleton(){}
	private:
		static AnyType *m_instance;
		
		
};

template <class AnyType>
AnyType * CSingleton<AnyType>::m_instance = 0;

} // end of namespace


#endif
