#pragma once
#ifndef _SUBJECT_H_
#define _SUBJECT_H_
/*
 *	Observer Pattern
 */
#include <map>
#include <set>



namespace Utility {

template <class Observer, class Event>
class ISubject
{
  private:
	typedef std::set<Observer *> ObserverCollection;

  protected:
	ObserverCollection m_Observers;

  public:
	CUISubject( ) : m_Observers()
	{}
	virtual ~CUISubject( ) { m_Observers.clear( ); }
	
	void attach( Observer &o )
	{ m_Observers.insert( &o ); }

	void detach( const Observer &o )
	{
		ObserverCollection::iterator itr;

		for( itr = m_Observers.begin( ); itr != m_Observers.end( ); ++itr )
			if( *itr == &o )
			{
				m_Observers.erase( itr );
				break;
			}
	}

	void notify( Event &e )
	{
		ObserverCollection::iterator itr;

		for( itr = m_Observers.begin( ); itr != m_Observers.end( ); ++itr )
			(*itr)->update( e );
	}

};

} // end of namespace
#endif