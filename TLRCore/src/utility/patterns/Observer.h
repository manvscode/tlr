#pragma once
#ifndef _OBSERVER_H_
#define _OBSERVER_H_
/*
 *	Observer Pattern
 */
namespace Utility {

template <class Event>
class IObserver
{
  public:
	IObserver( ){}
	virtual ~IObserver( ){}

	virtual void update( Event &e ) = 0;
};


} // end of namespace

#endif