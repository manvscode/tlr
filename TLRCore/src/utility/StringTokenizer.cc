/*
 *		CStringTokenizer.cc
 *
 *		Breaks a string into tokens that are seperated by delimiter
 *		strings.
 *
 *		Coded by Joseph A. Marrero, May 5, 2006
 */
#include <string>
#include <iostream>
#include "StringTokenizer.h"


namespace Utility {

/* requires a call to setString( ) and setDelimiter( ) */
CStringTokenizer::CStringTokenizer( )
	: m_szTokenizedString(), m_cDelimiter(), m_uiNumberOfTokens(0)
{}


CStringTokenizer::CStringTokenizer( const char *str )
        : m_szTokenizedString(str), m_cDelimiter(SPACE_STRING), m_uiNumberOfTokens(0)
{
        tokenize( );
}

CStringTokenizer::CStringTokenizer( const char *str, const char *delimiter )
        : m_szTokenizedString(str), m_cDelimiter(delimiter), m_uiNumberOfTokens(0)
{
        assert( std::strlen( str ) > 0 || std::strlen( delimiter ) > 0 );
        tokenize( );
}

CStringTokenizer::CStringTokenizer( const std::string &str )
	: m_szTokenizedString(str), m_cDelimiter(SPACE_STRING), m_uiNumberOfTokens(0)
{
	tokenize( );
}

CStringTokenizer::CStringTokenizer( const std::string &str, const std::string &delimiter )
	: m_szTokenizedString(str), m_cDelimiter(delimiter), m_uiNumberOfTokens(0)
{
	assert( str != "" || delimiter != "" );
	tokenize( );
}

void CStringTokenizer::tokenize( )
{
	if( m_szTokenizedString.length( ) < 0 || m_cDelimiter.length( ) < 0 ) return;
		std::string::size_type index = 0;
	unsigned int delimiter_length = m_cDelimiter.length( );
				 
	// Check the first char if equal to demlimiter...
	if( m_szTokenizedString.substr( 0, delimiter_length )  == m_cDelimiter )
		m_szTokenizedString = m_szTokenizedString.substr( delimiter_length /*+ 1*/ );
	// Check the last char if equal to demlimiter...
	if( m_szTokenizedString.size( ) > delimiter_length &&
	    m_szTokenizedString.substr( m_szTokenizedString.size( ) - delimiter_length, m_szTokenizedString.size( ) ) == m_cDelimiter )
		m_szTokenizedString = m_szTokenizedString.substr( 0, m_szTokenizedString.size( ) - delimiter_length );
	
	
	while( (index = m_szTokenizedString.find( m_cDelimiter, index )) != std::string::npos )
	{		
		m_szTokenizedString[ index ] = '\0';
		m_szTokenizedString.erase( index + 1, delimiter_length - 1 );
		//m_szTokenizedString.erase( index, delimiter_length );
		m_uiNumberOfTokens++;
	}

	// remove consecutive '\0' chars
	for( unsigned int i = 0; m_szTokenizedString.length( ) > 0 && i < m_szTokenizedString.length( ) - 1; i++ )
		if( m_szTokenizedString.length( ) > 0 && m_szTokenizedString.at( i ) == '\0' && m_szTokenizedString.at( i + 1 ) == '\0' )
		{
			m_szTokenizedString.erase( i--, 1 );
			m_uiNumberOfTokens--;
		}
	
	// remove the '\0' char from the begining
	if( m_szTokenizedString.length( ) > 0 && m_szTokenizedString[ 0 ] == '\0' )
	{
		m_uiNumberOfTokens--;
		m_szTokenizedString.erase( 0, 1 );
	}
	// remove the '\0' char from the end
	if( m_szTokenizedString.length( ) > 0 && m_szTokenizedString[ m_szTokenizedString.length( ) - 1 ] == '\0' )
	{
		m_uiNumberOfTokens--;
		m_szTokenizedString.erase( m_szTokenizedString.length( ) - 1, 1 );
	}
	


	// NumberOfTokens = Number of delimiters + 1; So, add 1.
	m_uiNumberOfTokens++; 

	#ifdef __STRINGTOKENIZER_DEBUG
	cout << "DEBUG CStringTokenizer::tokenize( ): ";
	for( unsigned int i = 0; i <  m_szTokenizedString.length( ); i++ )
		if(  m_szTokenizedString[ i ] == '\0' )
			cout << "\\0";
		else
			cout << m_szTokenizedString[ i ];
	cout << endl;
	cout << "Number of Tokens = " << m_uiNumberOfTokens << endl;
	#endif


}

} //end of namespace
