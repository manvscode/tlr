#pragma once
#ifndef _PRQUEUE_
#define _PRQUEUE_
/* -----------------------------------
	Templated Priority Queue Data Structure header
	Coded by: Peter Blanco
	Code Date: 4/25/05
-----------------------------------*/

#include <stdio.h>

namespace Utility
{


template<class AnyType, class cmpType> class PrQueue
{
public:
	PrQueue(cmpType comparator);
	~PrQueue();
	PrQueue(const PrQueue<AnyType, cmpType> &toBeCopied);
	PrQueue<AnyType, cmpType> operator = 
						(const PrQueue<AnyType, cmpType> &toBeCopied);
	AnyType dequeue();
	void enqueue (AnyType toBeQueued);
	AnyType peek();
	int getSize();
	bool isEmpty();

private: 
	class Node
	{
	public:
		//constructs a new node with the data this Node will hold
		//and the pointer to the next node
		Node(AnyType toBeSet, Node * pt)
		{
			data = toBeSet;
			next = pt;
		}
		Node(const Node &toBeCopied)
		{	
			data = toBeCopied.data;
			next = NULL;
		}
		//returns the data this Node holds
		AnyType getData()
		{
			return data;
		}
		//returns the pointer to the next node in the list
		Node * getPointer()
		{
			return next;
		}
		Node * next;
		AnyType data;
	};
	void copy(Node* listNode);
	cmpType cmp;
	Node* firstPointer;
	Node* lastPointer;
	int size;
};
#include "PrQueue.inl"
}
#endif