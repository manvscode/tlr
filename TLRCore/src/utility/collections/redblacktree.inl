/*
 *		RedBlackTree.inl
 *
 *		A Red-black tree implementation.
 *		By Joseph A. Marrero, 10/13/06 - 11/10/06
 *		http://www.l33tprogrammer.com/
 */


/*
 *	Red-Black Tree Container
 */	
template <class T, class Comparer, class Allocator>
CRedBlackTree<T, Comparer, Allocator>::CRedBlackTree( )
  : NIL(), m_Root(&NIL), /*m_Current(&NIL),*/ m_Compare(), m_ObjectPool(), m_NumberOfNodes(0)
{
	//NIL.isRed = false; // cannot change
	//NIL.left = NIL.right = NIL.parent = &NIL;
}

template <class T, class Comparer, class Allocator>
CRedBlackTree<T, Comparer, Allocator>::CRedBlackTree( const CRedBlackTree<T, Comparer, Allocator> &container )
  : NIL(), m_Root(&NIL), /*m_Current(&NIL),*/ m_Compare(container.m_Compare), m_ObjectPool(), m_NumberOfNodes(0)
{
	const_iterator itr;
	clear( );

	// insert all of the nodes into this tree; m_NumberOfNodes should be equal after this...
	for( itr = container.begin( ); itr != container.end( ); ++itr )
		insert( *itr );
}

template <class T, class Comparer, class Allocator>
CRedBlackTree<T, Comparer, Allocator>::~CRedBlackTree( )
{
	clear( );
}


template <class T, class Comparer, class Allocator>
void CRedBlackTree<T, Comparer, Allocator>::insert( const T &key )
{
	RBNode<T> *y = &NIL;
	RBNode<T> *x = m_Root;
	RBNode<T> *newNode = m_ObjectPool.allocate( 1 );
	

	while( x != &NIL )
	{
		y = x;
		if( m_Compare( key, x->data ) < 0 )
			x = x->left;
		else
			x = x->right;
	}
	
	

	if( y == &NIL )
		m_Root = newNode;
	else if( m_Compare( key, y->data ) < 0 )
		y->left = newNode;
	else
		y->right = newNode;
	
	//newNode->parent = y;
	//newNode->data = key;
	//newNode->left = &NIL;
	//newNode->right = &NIL;
	//newNode->isRed = true;
	m_ObjectPool.construct( newNode, RBNode<T>( key, y, &NIL, &NIL, true ) );

	insertFixup( newNode );
	m_NumberOfNodes++;
}

template <class T, class Comparer, class Allocator>
bool CRedBlackTree<T, Comparer, Allocator>::remove( const T &key )
{
	RBNode<T> *t = search( key );
	RBNode<T> *x = &NIL;
	RBNode<T> *y = &NIL;
	if( t == NULL ) return false; // item is not even in the tree!

	if( t->left == &NIL || t->right == &NIL ) // one brach is empty
		y = t;
	else  // t has two children...
		y = &successor( t );

	if( y->left != &NIL )
		x = y->left;
	else
		x = y->right;

	x->parent = y->parent;

	if( y->parent == &NIL )
		m_Root = x;
	else {
		if( y == y->parent->left )
			y->parent->left = x;
		else
			y->parent->right = x;
		
	}

	if( y != t )
	{
		t->data = y->data;
		//t->parent = y->parent;
		//t->left = y->left;
		//t->right = y->right;
		//t->isRed = y->isRed;
	}

	if( y->isRed == false ) // y is black
		deleteFixup( x );

	m_ObjectPool.destroy( y );
	m_ObjectPool.deallocate( y, 1 );
	m_NumberOfNodes--;
	return true;
}

template <class T, class Comparer, class Allocator>
bool CRedBlackTree<T, Comparer, Allocator>::search( const T &key ) const
{
	RBNode<T> *x = m_Root;

	while( x != &NIL )
	{
		if( m_Compare( key, x->data ) == 0 )
			return true; // found it
		else if( m_Compare( key, x->data ) < 0 )
			x = x->left;
		else
			x = x->right;
	}

	return false;
}

template <class T, class Comparer, class Allocator>
RBNode<T> *CRedBlackTree<T, Comparer, Allocator>::search( const T &key )
{
	RBNode<T> *x = m_Root;
	
	while( x != &NIL )
		if( m_Compare( key, x->data) == 0 )
			return x;
		else if( m_Compare( key, x->data) < 0 )
			x = x->left;
		else
			x = x->right;
	
	return NULL;
}

template <class T, class Comparer, class Allocator>
typename CRedBlackTree<T, Comparer, Allocator>::iterator CRedBlackTree<T, Comparer, Allocator>::find( const T &key )
{
	RBNode<T> *x = m_Root;
	
	while( x != &NIL )
		if( m_Compare( key, x->data) == 0 )
			return iterator( *this, x );
		else if( m_Compare( key, x->data) < 0 )
			x = x->left;
		else
			x = x->right;
	
	return end();
}

template <class T, class Comparer, class Allocator>
void CRedBlackTree<T, Comparer, Allocator>::clear( )
{
	RBNode<T> *x = m_Root;
	RBNode<T> *y = &NIL;


	while( m_NumberOfNodes > 0 )
	{
		y = x;

		// find minimum...
		while( y->left != &NIL )
			y = y->left;

		if( y->right == &NIL )
		{
			y->parent->left = &NIL;
			x = y->parent;
			//free...
			m_ObjectPool.destroy( y );
			m_ObjectPool.deallocate( y, 1 );
			m_NumberOfNodes--;
		}
		else {
			x = y->right;
			x->parent = y->parent;
			if( x->parent == &NIL )
				m_Root = x;
			else
				y->parent->left = x;
			// free...
			m_ObjectPool.destroy( y );
			m_ObjectPool.deallocate( y, 1 );
			m_NumberOfNodes--;
		}
	}

	//reset the root and current pointers
	m_Root = &NIL;
	//m_Current = &NIL;
}

template <class T, class Comparer, class Allocator>
bool CRedBlackTree<T, Comparer, Allocator>::verifyTree( )
{
	//check if root is black...
	if( m_Root->isRed )
		return false;

	return verifySubTree( m_Root );
}

template <class T, class Comparer, class Allocator>
bool CRedBlackTree<T, Comparer, Allocator>::verifySubTree( RBNode<T> *t )
{
	if( t == &NIL )
		return true;

	if( t->isRed )
	{
		if( t->left->isRed || t->right->isRed )
			return false;
	}

	return verifySubTree( t->left ) && verifySubTree( t->right );
}

template <class T, class Comparer, class Allocator>
void CRedBlackTree<T, Comparer, Allocator>::gotoRoot( )
{ m_Current = m_Root; }

template <class T, class Comparer, class Allocator>
void CRedBlackTree<T, Comparer, Allocator>::gotoMinimum( )
{ m_Current = &minimum( m_Root ); }

template <class T, class Comparer, class Allocator>
void CRedBlackTree<T, Comparer, Allocator>::gotoMaximum( )
{ m_Current = &maximum( m_Root ); }

template <class T, class Comparer, class Allocator>
RBNode<T> &CRedBlackTree<T, Comparer, Allocator>::getMinimum( )
{ return minimum( m_Root ); }

template <class T, class Comparer, class Allocator>
RBNode<T> &CRedBlackTree<T, Comparer, Allocator>::getMaximum( )
{ return maximum( m_Root ); }

template <class T, class Comparer, class Allocator>
const RBNode<T> &CRedBlackTree<T, Comparer, Allocator>::getMinimum( ) const
{ return minimum( m_Root ); }

template <class T, class Comparer, class Allocator>
const RBNode<T> &CRedBlackTree<T, Comparer, Allocator>::getMaximum( ) const
{ return maximum( m_Root ); }


template <class T, class Comparer, class Allocator>
RBNode<T> &CRedBlackTree<T, Comparer, Allocator>::getCurrentNode( )
{ return *m_Current; }

template <class T, class Comparer, class Allocator>
void CRedBlackTree<T, Comparer, Allocator>::traverseUp( )
{ m_Current = m_Current->parent; }

template <class T, class Comparer, class Allocator>
void CRedBlackTree<T, Comparer, Allocator>::traverseLeft( )
{ m_Current = m_Current->left; }

template <class T, class Comparer, class Allocator>
void CRedBlackTree<T, Comparer, Allocator>::traverseRight( )
{ m_Current = m_Current->right; }


template <class T, class Comparer, class Allocator>
void CRedBlackTree<T, Comparer, Allocator>::next( )
{ m_Current = &successor( m_Current ); }

template <class T, class Comparer, class Allocator>
void CRedBlackTree<T, Comparer, Allocator>::previous( )
{ m_Current = &predecessor( m_Current ); }


template <class T, class Comparer, class Allocator>
const CRedBlackTree<T, Comparer, Allocator> &CRedBlackTree<T, Comparer, Allocator>::operator=( const CRedBlackTree<T, Comparer, Allocator> &other )
{
	if( this != &other )
	{
		const_iterator itr;
		clear( );

		// insert all of the nodes into this tree; m_NumberOfNodes should be equal after this...
		for( itr = other.begin( ); itr != other.end( ); ++itr )
			insert( *itr );

		m_Compare = other.m_Compare;
		//m_ObjectPool = container.m_ObjectPool;
	}

	return *this;
}






/*
 *	Red-Black Tree Bidirectional Iterator
 */
template <class T, class Comparer, class Allocator>
CRBIterator<T, Comparer, Allocator>::CRBIterator( )
: CConstRBIterator( )//position(NULL)
{}

template <class T, class Comparer, class Allocator>
CRBIterator<T, Comparer, Allocator>::CRBIterator( CRedBlackTree<T, Comparer, Allocator> &container, RBNode<T> *p, RBNode<T> *s )
 : CConstRBIterator( container, p, s )//m_Container(&container), position(p), safe_position(s)
{
}

template <class T, class Comparer, class Allocator>
CRBIterator<T, Comparer, Allocator>::~CRBIterator( )
{
}

template <class T, class Comparer, class Allocator>
bool CRBIterator<T, Comparer, Allocator>::operator==( const CRBIterator<T, Comparer, Allocator> &other ) const
{ return m_Container == other.m_Container && position == other.position && safe_position == other.safe_position; }

template <class T, class Comparer, class Allocator>
bool CRBIterator<T, Comparer, Allocator>::operator!=( const CRBIterator<T, Comparer, Allocator> &other ) const
{  return m_Container != other.m_Container || position != other.position || safe_position != other.safe_position; }

template <class T, class Comparer, class Allocator>
T &CRBIterator<T, Comparer, Allocator>::operator*( )
{ 
	if( position == &m_Container->NIL )
		position = safe_position;
	return position->data;
}

template <class T, class Comparer, class Allocator>
T *CRBIterator<T, Comparer, Allocator>::operator->( )
{ return &**this; }


template <class T, class Comparer, class Allocator>
CRBIterator<T, Comparer, Allocator> & CRBIterator<T, Comparer, Allocator>::operator++( )
{
	if( position != &m_Container->NIL )
	{
		safe_position = position;
		position = &m_Container->successor( position );
	}
	else {
		position = safe_position;
		position = &m_Container->successor( position );
	}

	return *this;
}

template <class T, class Comparer, class Allocator>
CRBIterator<T, Comparer, Allocator> CRBIterator<T, Comparer, Allocator>::operator++( int )
{
	if( position != &m_Container->NIL )
	{
		safe_position = position;
		CRBIterator<T, Comparer, Allocator> itr( *m_Container, position );
		position = &m_Container->successor( position );
		return itr;
	}
	else {
		position = safe_position;
		CRBIterator<T, Comparer, Allocator> itr( *m_Container, position );
		position = &m_Container->successor( position );
		return itr;
	}
}

template <class T, class Comparer, class Allocator>
CRBIterator<T, Comparer, Allocator> & CRBIterator<T, Comparer, Allocator>::operator--( )
{
	if( position != &m_Container->NIL )
	{
		safe_position = position;
		position = &m_Container->predecessor( position );
	}
	else {
		position = safe_position;
		position = &m_Container->predecessor( position );
	}

	return *this;
}

template <class T, class Comparer, class Allocator>
CRBIterator<T, Comparer, Allocator> CRBIterator<T, Comparer, Allocator>::operator--( int )
{
	
	if( position != &m_Container->NIL )
	{
		safe_position = position;
		CRBIterator<T, Comparer, Allocator> itr( *m_Container, position );
		position = &m_Container->predecessor( position );
		return itr;
	}
	else {
		position = safe_position;
		CRBIterator<T, Comparer, Allocator> itr( *m_Container, position );
		position = &m_Container->predecessor( position );
		return itr;
	}

	
}

template <class T, class Comparer, class Allocator>
CRBIterator<T, Comparer, Allocator> &CRBIterator<T, Comparer, Allocator>::operator=( const CRBIterator<T, Comparer, Allocator> &other )
{
	if( this != &other )
	{
		m_Container = other.m_Container;
		position = other.position;
		safe_position = other.safe_position;
	}
	return *this;
}

/*
 *	Red-Black Tree Bidirectional Const Iterator
 */
template <class T, class Comparer, class Allocator>
CConstRBIterator<T, Comparer, Allocator>::CConstRBIterator( )
: position(NULL)
{}

template <class T, class Comparer, class Allocator>
CConstRBIterator<T, Comparer, Allocator>::CConstRBIterator( const CRedBlackTree<T, Comparer, Allocator> &container, const RBNode<T> *p, const RBNode<T> *s )
 : m_Container( const_cast<CRedBlackTree<T, Comparer, Allocator> *>(&container) ),
   position( const_cast<RBNode<T> *>(p) ),
   safe_position( const_cast<RBNode<T> *>(s) )
{
}

template <class T, class Comparer, class Allocator>
CConstRBIterator<T, Comparer, Allocator>::~CConstRBIterator( )
{
}

template <class T, class Comparer, class Allocator>
bool CConstRBIterator<T, Comparer, Allocator>::operator==( const CConstRBIterator<T, Comparer, Allocator> &other ) const
{ return m_Container == other.m_Container && position == other.position && safe_position == other.safe_position; }

template <class T, class Comparer, class Allocator>
bool CConstRBIterator<T, Comparer, Allocator>::operator!=( const CConstRBIterator<T, Comparer, Allocator> &other ) const
{  return m_Container != other.m_Container || position != other.position || safe_position != other.safe_position; }

template <class T, class Comparer, class Allocator>
const T &CConstRBIterator<T, Comparer, Allocator>::operator*( )
{ 
	if( position == &m_Container->NIL )
		position = safe_position;
	return position->data;
}

template <class T, class Comparer, class Allocator>
const T *CConstRBIterator<T, Comparer, Allocator>::operator->( )
{ 
	return &**this;
}

template <class T, class Comparer, class Allocator>
CConstRBIterator<T, Comparer, Allocator> & CConstRBIterator<T, Comparer, Allocator>::operator++( )
{
	if( position != &m_Container->NIL )
	{
		safe_position = position;
		position = &m_Container->successor( position );
	}
	else {
		position = safe_position;
		position = &m_Container->successor( position );
	}

	return *this;
}

template <class T, class Comparer, class Allocator>
CConstRBIterator<T, Comparer, Allocator> CConstRBIterator<T, Comparer, Allocator>::operator++( int )
{
	if( position != &m_Container->NIL )
	{
		safe_position = position;
		CRBIterator<T, Comparer, Allocator> itr( *m_Container, position );
		position = &m_Container->successor( position );
		return itr;
	}
	else {
		position = safe_position;
		CRBIterator<T, Comparer, Allocator> itr( *m_Container, position );
		position = &m_Container->successor( position );
		return itr;
	}
}

template <class T, class Comparer, class Allocator>
CConstRBIterator<T, Comparer, Allocator> & CConstRBIterator<T, Comparer, Allocator>::operator--( )
{
	if( position != &m_Container->NIL )
	{
		safe_position = position;
		position = &m_Container->predecessor( position );
	}
	else {
		position = safe_position;
		position = &m_Container->predecessor( position );
	}

	return *this;
}

template <class T, class Comparer, class Allocator>
CConstRBIterator<T, Comparer, Allocator> CConstRBIterator<T, Comparer, Allocator>::operator--( int )
{
	if( position != &m_Container->NIL )
	{
		safe_position = position;
		CRBIterator<T, Comparer, Allocator> itr( *m_Container, position );
		position = &m_Container->predecessor( position );
		return itr;
	}
	else {
		position = safe_position;
		CRBIterator<T, Comparer, Allocator> itr( *m_Container, position );
		position = &m_Container->predecessor( position );
		return itr;
	}
}

/*
 *  Red Black Node Iterator: a raw iterator for when one is needed.
 */
template <class T, class Comparer, class Allocator>
CRBNodeIterator<T, Comparer, Allocator>::CRBNodeIterator( )
 : m_Container( NULL ), m_Position( NULL )
{}

template <class T, class Comparer, class Allocator>
CRBNodeIterator<T, Comparer, Allocator>::CRBNodeIterator( CRedBlackTree<T, Comparer, Allocator> &container, RBNode<T> *position )
 : m_Container( &container ), m_Position( position )
{}

template <class T, class Comparer, class Allocator>
CRBNodeIterator<T, Comparer, Allocator>::~CRBNodeIterator( )
{}

template <class T, class Comparer, class Allocator>
bool CRBNodeIterator<T, Comparer, Allocator>::operator==( const CRBNodeIterator<T, Comparer, Allocator> &other ) const
{
	return ( m_Container == other.m_Container && m_Position == other.m_Position );
}

template <class T, class Comparer, class Allocator>
bool CRBNodeIterator<T, Comparer, Allocator>::operator!=( const CRBNodeIterator<T, Comparer, Allocator> &other ) const
{
	return ( m_Container != other.m_Container || m_Position != other.m_Position );
}

template <class T, class Comparer, class Allocator>
T &CRBNodeIterator<T, Comparer, Allocator>::operator*( )
{ return m_Position->data; }

template <class T, class Comparer, class Allocator>
T *CRBNodeIterator<T, Comparer, Allocator>::operator->( )
{ return &**this; }



template <class T, class Comparer, class Allocator>
bool CRBNodeIterator<T, Comparer, Allocator>::isNil( ) const
{ return m_Position == &m_Container->NIL; }

template <class T, class Comparer, class Allocator>
bool CRBNodeIterator<T, Comparer, Allocator>::isRoot( ) const
{ return m_Position == m_Container->m_Root; }

template <class T, class Comparer, class Allocator>
bool CRBNodeIterator<T, Comparer, Allocator>::isDone( ) const
{ return m_Position == &m_Container->NIL; }

template <class T, class Comparer, class Allocator>
void CRBNodeIterator<T, Comparer, Allocator>::traverseLeft( )
{
	if( m_Position == &m_Container->NIL )
	{
		m_Position = m_SafePosition;
		m_Position = m_Position->left;
	}
	else {
		m_SafePosition = m_Position;
		m_Position = m_Position->left;
	}
}

template <class T, class Comparer, class Allocator>
void CRBNodeIterator<T, Comparer, Allocator>::traverseRight( )
{	
	if( m_Position == &m_Container->NIL )
	{
		m_Position = m_SafePosition;
		m_Position = m_Position->right;
	}
	else {
		m_SafePosition = m_Position;
		m_Position = m_Position->right;
	}
}

template <class T, class Comparer, class Allocator>
void CRBNodeIterator<T, Comparer, Allocator>::traverseUp( )
{ 
	if( m_Position == &m_Container->NIL )
	{
		m_Position = m_SafePosition;
		m_Position = m_Position->parent;
	}
	else {
		m_SafePosition = m_Position;
		m_Position = m_Position->parent;
	}
}