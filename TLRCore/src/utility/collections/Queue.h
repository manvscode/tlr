#pragma once
/* -----------------------------------
	Templated Queue Data Structure header
	Coded by: Peter Blanco
	Code Date: 4/18/05
-----------------------------------*/

#include <stdio.h>
//#include "ShadowDS/ShadowDS.h"
//Our Namespace
namespace Utility 
{
	//Generic Queue Class
	template <class AnyType> class Queue
	{
	public:
		Queue();
		Queue(const Queue<AnyType> &toBeCopied);
		~Queue();
		Queue<AnyType> operator = (const Queue<AnyType> &toBeCopied);
		AnyType dequeue();
		void enqueue (AnyType toBeQueued);
		AnyType peek();
		int getSize();
		bool isEmpty();

	private:
		//Private inner Node class for singly LL implementation of Queue
		class Node
		{
		public:
			//constructs a new node with the data this Node will hold
			//and the pointer to the next node
			Node(AnyType toBeSet, Node * pt)
			{
				data = toBeSet;
				next = pt;
			}
			Node(const Node &toBeCopied)
			{	
				data = toBeCopied.data;
				next = NULL;
			}
			//returns the data this Node holds
			AnyType getData()
			{
				return data;
			}
			//returns the pointer to the next node in the list
			Node * getPointer()
			{
				return next;
			}
			Node * next;
			AnyType data;
		};
		//always points to the first node in the list,
		//a.k.a., the "top" of the Queue
		Node* firstPointer;
		Node* lastPointer;
		int size;
		void copy(Node* listNode);
		
	};

#include "Queue.inl"

}


