/*
 *		HashTable.inl
 *
 *		Seperate chaining hash table implementation that uses singly linked lists.
 *		Performance could be increased if you peroidically check the load factor 
 *		and double it accordingly.
 *		
 *		insert( )			-	O(1) to O(N)
 *		remove( )			-	O(1) to O(N)
 *		lookup( )			-   O(1)
 *		getSize( )			-	O(1)
 *		getTableSize( )		-	O(1)
 *		getLoadFactor( )	-	O(1)
 *		doubleSize( )		-	O(N)
 *		increaseSize( )		-	O(N)
 *
 *		5/02/05 - Coded by Joseph A. Marrero
 */

template <class AnyType, class HashFunctor, class Comparer>
HashTable<AnyType, HashFunctor, Comparer>::HashTable( void )
 : size( 0 ), m_lookedupData( NULL ), tableSize( 31 ) // 31 is prime
{
	table = new Node*[ tableSize ];
	//memset( table, 0, sizeof(Node *) * tableSize ); // same speed as code below
	for(unsigned int i = 0; i < tableSize; i++)
		table[ i ] = NULL;
}

template <class AnyType, class HashFunctor, class Comparer>
HashTable<AnyType, HashFunctor, Comparer>::HashTable( const unsigned int initialTableSize )
 : size( 0 ), m_lookedupData( NULL ), tableSize( initialTableSize )
{
	table = new Node*[ tableSize ];
	//memset( table, 0, sizeof(Node *) * tableSize ); // same speed as code below
	for(unsigned int i = 0; i < tableSize; i++)
		table[ i ] = NULL;
}

template <class AnyType, class HashFunctor, class Comparer>
HashTable<AnyType, HashFunctor, Comparer>::~HashTable( void )
{
    clear( );
	delete [] table;
}

template <class AnyType, class HashFunctor, class Comparer>
bool HashTable<AnyType, HashFunctor, Comparer>::insert( const AnyType &Data )
{
 	unsigned int hashCode = hashFunction( Data );
	unsigned int index = hashCode % tableSize;

	if( lookup( Data ) )
		return false;

	Node *newNode = new Node( Data, hashCode );

	/*
	if( table[ index ] == NULL )
		table[ index ] = newNode;
	else {
		newNode->next = table[ index ];
		table[ index ] = newNode;
	}*/
	if( table[ index ] == NULL )
		table[ index ] = newNode;
	else {
		Node *tmp = m_lookedupData->next;
		m_lookedupData->next = newNode;
		newNode->next = tmp;
	}
	size++;
	return true;
}

template <class AnyType, class HashFunctor, class Comparer> 
bool HashTable<AnyType, HashFunctor, Comparer>::remove( const AnyType &Data )
{
	unsigned int hashCode = hashFunction( Data );
	unsigned int index = hashCode % tableSize;

	Node *current = table[ index ],
		 *previousNode = NULL;

	while( current != NULL )
	{
		if( current->hashCode == hashCode && compare( current->data, Data) == 0 )
		{
			Node *temp = NULL;
			if( previousNode == NULL )
			{
				temp = table[ index ]->next;
				delete table[ index ];
				table[ index ] = temp;
				size--;
			}
			else if( previousNode->next != NULL )
			{
				temp = previousNode->next->next;
				delete previousNode->next;
				previousNode->next = temp;
				size--;
			}
			return true;
		}
		previousNode = current;
		current = current->next;
	}
	return false;
}

template <class AnyType, class HashFunctor, class Comparer>
bool HashTable<AnyType, HashFunctor, Comparer>::lookup( const AnyType &Data )
{
	unsigned int hashCode = hashFunction( Data );
	unsigned int index = hashCode % tableSize;
	
	Node *current = table[ index ];
	Node *previous = current;

	while( current != NULL )
	{
		if( current->hashCode == hashCode && compare( current->data, Data) == 0 )
		{
			m_lookedupData = current;
			return true;
		}
		previous = current;
		current = current->next;
	}
	m_lookedupData = previous;
	return false;
}

template <class AnyType, class HashFunctor, class Comparer> 
void HashTable<AnyType, HashFunctor, Comparer>::clear( )
{
	Node *current = NULL;
	Node *temp = NULL;

	for( unsigned int index = 0; !isEmpty( ) && index < tableSize; index++ )
	{
		current = table[ index ];
		while( current != NULL ) 
		{
			temp = current->next;
			delete current;
			size--;
			current = temp; // current = current->next;
		}
	}
}

/*
template <class AnyType, class HashFunctor, class Comparer>
AnyType *HashTable<AnyType, HashFunctor, Comparer>::get( void )
{
	if( m_lookedupData != NULL )
		return &m_lookedupData->data;
	else
		return NULL;
}
*/

template <class AnyType, class HashFunctor, class Comparer> 
bool HashTable<AnyType, HashFunctor, Comparer>::get( AnyType *Data )
{
	if( m_lookedupData != NULL && Data != NULL )
	{
		*Data = m_lookedupData->data;
		return true;
	}
	else
		return false;
}

template <class AnyType, class HashFunctor, class Comparer>
void HashTable<AnyType, HashFunctor, Comparer>::halveSize( )
{
	unsigned int newtableSize = tableSize / 2 + 1;
	Node **newTable = new Node*[ newtableSize ];
	Node *current = NULL,
		*temp = NULL;
	unsigned int newIndex = 0;

	// Initialize the new table
	for(unsigned int i = 0; i < newtableSize; i++)
		newTable[ i ] = NULL;
	// Rehash
	for( unsigned int index = 0; index < tableSize; index++ )
	{
		current = table[ index ];
		while( current != NULL ) 
		{
			temp = current->next;
			newIndex = current->hashCode % newtableSize;

			if( newTable[ newIndex ] == NULL )
			{
				current->next = NULL;
				newTable[ newIndex ] = current;
			}
			else {
				current->next = newTable[ newIndex ];
				newTable[ newIndex ] = current;
			}

			current = temp; // current = current->next;
		}
	}

	tableSize = newtableSize;
	delete [] table;
	table = newTable;
}

template <class AnyType, class HashFunctor, class Comparer>
void HashTable<AnyType, HashFunctor, Comparer>::doubleSize( )
{	
	unsigned int newtableSize = 2 * tableSize + 1;
	Node **newTable = new Node*[ newtableSize ];
	Node *current = NULL,
		 *temp = NULL;
	unsigned int newIndex = 0;

	// Initialize the new table
	for(unsigned int i = 0; i < newtableSize; i++)
		newTable[ i ] = NULL;
	// Rehash
	for( unsigned int index = 0; index < tableSize; index++ )
	{
		current = table[ index ];
		//if( current == NULL )
		//	newTable[ index ] = NULL;
		while( current != NULL ) 
		{
			temp = current->next;
			newIndex = current->hashCode % newtableSize;
	
			if( newTable[ newIndex ] == NULL )
			{
				current->next = NULL;
				newTable[ newIndex ] = current;
			}
			else {
				current->next = newTable[ newIndex ];
				newTable[ newIndex ] = current;
			}

			current = temp; // current = current->next;
		}
	}
	
	tableSize = newtableSize;
	delete [] table;
	table = newTable;
}

template <class AnyType, class HashFunctor, class Comparer>
void HashTable<AnyType, HashFunctor, Comparer>::increaseSize( const unsigned int newTableSize )
{	
	unsigned int newtableSize = newTableSize;
	Node **newTable = new Node*[ newtableSize ];
	Node *current = NULL,
		 *temp = NULL;
	unsigned int newIndex = 0;

	// Initialize the new table
	for(unsigned int i = 0; i < newtableSize; i++)
		newTable[ i ] = NULL;
	// Rehash
	for( unsigned int index = 0; index < tableSize; index++ )
	{
		current = table[ index ];
		//if( current == NULL )
		//	newTable[ index ] = NULL;
		while( current != NULL ) 
		{
			temp = current->next;
			newIndex = current->hashCode % newtableSize;

			if( newTable[ newIndex ] == NULL )
			{
				current->next = NULL;
				newTable[ newIndex ] = current;
			}
			else {
				current->next = newTable[ newIndex ];
				newTable[ newIndex ] = current;
			}

			current = temp; // current = current->next;
		}
	}

	tableSize = newtableSize;
	delete [] table;
	table = newTable;
}
