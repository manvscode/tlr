#pragma once
#ifndef __PHONGHASH_H__
#define __PHONGHASH_H__
/*
 *	PhongHash.h
 */

namespace Utility {

class CPhongHash
{
  public:
 	unsigned int operator()( unsigned char const * data, size_t size )
	{
		unsigned int hash = 0;
		
		while( size-- > 0 )
		{
			// 0x63c63cd9 = 1673936089 is prime?
			hash = 0x63c63cd9 * hash + *data + 0x9c39c33d;
      		++data;
		}
		return hash;
	}
};

} // end of namespace
#endif
