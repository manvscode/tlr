#pragma once
#ifndef __DEFAULTHASHFUNCTION_H__
#define __DEFAULTHASHFUNCTION_H__
/*
*	DefaultHashFunction.h
*
*
*/
namespace Utility {

class DefaultHashFunction
{
public:
	template <class AnyType> unsigned int operator()( const AnyType &obj ) const
	{
		return &obj;
	}

	template <class AnyType> unsigned int operator()( AnyType &obj ) const
	{
		return &obj;
	}

};

}

#endif