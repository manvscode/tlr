/* -----------------------------------
	Templated Queue Data Structure
	Coded by: Peter Blanco
	Code Date: 4/18/05
   -----------------------------------*/

template <class AnyType> Queue<AnyType>::Queue()
{
	size = 0;
	firstPointer = lastPointer = NULL;
}

template <class AnyType> Queue<AnyType>::~Queue()
{
	if (firstPointer)
	{
		Node * pt = firstPointer->getPointer();

		while(pt)
		{
			Node * nextPt = pt->getPointer();
			delete pt;
			pt = nextPt;
		}
		delete firstPointer;	
	}
}

template <class AnyType> Queue<AnyType> 
Queue<AnyType>::operator = (const Queue<AnyType> &toBeCopied)
{
	return Queue<AnyType>(toBeCopied);
}

template <class AnyType> bool Queue<AnyType>::isEmpty()
{
	return size == 0;
}

template <class AnyType> int Queue<AnyType>::getSize()
{
	return size;
}
template <class AnyType> Queue<AnyType>::Queue(const Queue<AnyType> &toBeCopied)
{
	size = 0;
	copy(toBeCopied.firstPointer);
}

template <class AnyType> void Queue<AnyType>::copy(Node* listNode)
{
	if (!listNode)
	{
		firstPointer = lastPointer = NULL;
		return;
	}

	Node *newNode;
	Node *head = newNode = new Node(*listNode);
	lastPointer = head;
	listNode = listNode->getPointer();
	size++;
    while(listNode)
	{
		newNode->next = new Node(*listNode);
		newNode = newNode->next;
		lastPointer = listNode;
		listNode = listNode->next;
		size++;
	}
	firstPointer = head;
}

template <class AnyType> void Queue<AnyType>::enqueue(AnyType toBeEnqueued)
{
	if (!firstPointer)
		firstPointer = lastPointer = new Node(toBeEnqueued, NULL);

	else
	{		
		Node * newNode = new Node(toBeEnqueued, NULL);
		lastPointer->next = newNode;
		lastPointer = newNode;
	}
	size++;
}

template <class AnyType> AnyType Queue<AnyType>::dequeue()
{
	if (!firstPointer)
		return AnyType();

	AnyType toBeDequeued = firstPointer->getData();
	Node * temp = firstPointer->getPointer();
	delete firstPointer;
	firstPointer = temp;
	size--;
	return toBeDequeued;
}

template <class AnyType> AnyType Queue<AnyType>::peek()
{
	if (firstPointer)
		return firstPointer->getData();
	return AnyType();
}


