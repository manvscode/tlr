/*
 *		HashMap.inl
 *
 *		Map data structure implemented with a hash table to provide fast
 *		lookups, insertions, and removes. The HashMap will manage the table
 *		size for you such that you are roughly around the specified load
 *		factor or the default load factor of 0.75.
 *
 *		contains( )				-	O(1) to O(N)
 *		put( )					-	O(1) to O(N)
 *		remove( )				-	O(1) to O(N)
 *		get( )					-	O(1) to O(N)
 *		clear( )				-	O(N)
 *		isEmpty( )				-	O(1)
 *		getSize( )				-	O(1)
 *
 *		Coded by Joseph A. Marrero
 *		5/12/05
 */

#include "HashMap.h"

template <class Key, class Value, class KeyHasher, class Equals> HashMap<Key, Value, KeyHasher, Equals>::HashMap( void ) : m_size( 0 ), m_tablesize( 31 ), m_LoadFactor( 0.75 )
{
	m_MapTable = new MapNode*[ m_tablesize ];
	for(unsigned int i = 0; i < m_tablesize; i++)
		m_MapTable[ i ] = NULL;
}
template <class Key, class Value, class KeyHasher, class Equals> HashMap<Key, Value, KeyHasher, Equals>::HashMap( const float initialLoadFactor ) : m_size( 0 ), m_tablesize( 31 ), m_LoadFactor( initialLoadFactor )
{
	m_MapTable = new MapNode*[ m_tablesize ];
	for(unsigned int i = 0; i < m_tablesize; i++)
		m_MapTable[ i ] = NULL;
}
template <class Key, class Value, class KeyHasher, class Equals> HashMap<Key, Value, KeyHasher, Equals>::HashMap( unsigned int initiailTableSize, const float initialLoadFactor = 0.75 ) : m_size( 0 ), m_tablesize( initiailTableSize ), m_LoadFactor( initialLoadFactor )
{
	m_MapTable = new MapNode*[ m_tablesize ];
	for(unsigned int i = 0; i < m_tablesize; i++)
		m_MapTable[ i ] = NULL;
}
template <class Key, class Value, class KeyHasher, class Equals> HashMap<Key, Value, KeyHasher, Equals>::~HashMap( void )
{
	clear( );
	delete [] m_MapTable;
}

template <class Key, class Value, class KeyHasher, class Equals> bool HashMap<Key, Value, KeyHasher, Equals>::contains( const Key key )
{
	unsigned int hashCode = hasher( key );
	unsigned int index = hashCode % m_tablesize;

	MapNode *current = m_MapTable[ index ];

	while( current != NULL )
	{
		if( current->hashCode == hashCode && compareKeys( current->key, key ) == 0 )
			return true;
		current = current->next;
	}
	return false;
}
template <class Key, class Value, class KeyHasher, class Equals> bool HashMap<Key, Value, KeyHasher, Equals>::put( const Key key, const Value value )
{
	unsigned int hashCode = hasher( key );
	unsigned int index = hashCode % m_tablesize;

	if( contains( key ) )
		return false;
	if( getLoadFactor( ) > m_LoadFactor )
		doubleSize( );

	MapNode *newNode = new MapNode( key, value, hashCode );


	if( m_MapTable[ index ] == NULL )
		m_MapTable[ index ] = newNode;
	else {
		newNode->next = m_MapTable[ index ];
		m_MapTable[ index ] = newNode;
	}

	m_size++;
	return true;
}
template <class Key, class Value, class KeyHasher, class Equals> bool HashMap<Key, Value, KeyHasher, Equals>::remove( const Key key )
{
	if( getLoadFactor( ) < m_LoadFactor )
		halveSize( );

	unsigned int hashCode = hasher( key );
	unsigned int index = hashCode % m_tablesize;

	MapNode *current = m_MapTable[ index ],
			*previousNode = NULL;

	while( current != NULL )
	{
		if( current->hashCode == hashCode && compareKeys( current->key, key ) == 0 )
		{
			MapNode *temp = NULL;
			if( previousNode == NULL )
			{
				temp = m_MapTable[ index ]->next;
				delete m_MapTable[ index ];
				m_MapTable[ index ] = temp;
				m_size--;
			}
			else if( previousNode->next != NULL )
			{
				temp = previousNode->next->next;
				delete previousNode->next;
				previousNode->next = temp;
				m_size--;
			}
			return true;
		}
		previousNode = current;
		current = current->next;
	}

	return false;
}
template <class Key, class Value, class KeyHasher, class Equals> bool HashMap<Key, Value, KeyHasher, Equals>::get( const Key key, Value &value )
{
	unsigned int hashCode = hasher( key );
	unsigned int index = hashCode % m_tablesize;

	MapNode *current = m_MapTable[ index ];

	while( current != NULL )
	{
		if( current->hashCode == hashCode && compareKeys( current->key, key ) == 0 )
		{
			value = current->value;
			return true;
		}
		current = current->next;
	}
	return false;
}
template <class Key, class Value, class KeyHasher, class Equals> void HashMap<Key, Value, KeyHasher, Equals>::clear( void )
{
	MapNode *current = NULL;
	MapNode *temp = NULL;

	for( unsigned int index = 0; !isEmpty( ) /* && index < m_tablesize*/; index++ )
	{
		current = m_MapTable[ index ];
		while( current != NULL ) 
		{
			temp = current->next;
			delete current;
			m_size--;
			current = temp; // current = current->next;
		}
	}
}
template <class Key, class Value, class KeyHasher, class Equals> void HashMap<Key, Value, KeyHasher, Equals>::halveSize( )
{
	unsigned int newtableSize = m_tablesize / 2 + 1;
	MapNode **newTable = new MapNode*[ newtableSize ];
	MapNode *current = NULL,
			*temp = NULL;
	unsigned int newIndex = 0;

	// Initialize the new table
	for(unsigned int i = 0; i < newtableSize; i++)
		newTable[ i ] = NULL;
	// Rehash
	for( unsigned int index = 0; index < m_tablesize; index++ )
	{
		current = m_MapTable[ index ];

		while( current != NULL ) 
		{
			temp = current->next;
			newIndex = current->hashCode % newtableSize;

			if( newTable[ newIndex ] == NULL )
			{
				current->next = NULL;
				newTable[ newIndex ] = current;
			}
			else {
				current->next = newTable[ newIndex ];
				newTable[ newIndex ] = current;
			}

			current = temp; // current = current->next;
		}
	}

	m_tablesize = newtableSize;
	delete [] m_MapTable;
	m_MapTable = newTable;
}
template <class Key, class Value, class KeyHasher, class Equals> void HashMap<Key, Value, KeyHasher, Equals>::doubleSize( )
{	
	unsigned int newtableSize = 2 * m_tablesize + 1;
	MapNode **newTable = new MapNode*[ newtableSize ];
	MapNode *current = NULL,
			*temp = NULL;
	unsigned int newIndex = 0;

	// Initialize the new table
	for(unsigned int i = 0; i < newtableSize; i++)
		newTable[ i ] = NULL;
	// Rehash
	for( unsigned int index = 0; index < m_tablesize; index++ )
	{
		current = m_MapTable[ index ];

		while( current != NULL ) 
		{
			temp = current->next;
			newIndex = current->hashCode % newtableSize;

			if( newTable[ newIndex ] == NULL )
			{
				current->next = NULL;
				newTable[ newIndex ] = current;
			}
			else {
				current->next = newTable[ newIndex ];
				newTable[ newIndex ] = current;
			}

			current = temp; // current = current->next;
		}
	}

	m_tablesize = newtableSize;
	delete [] m_MapTable;
	m_MapTable = newTable;
}