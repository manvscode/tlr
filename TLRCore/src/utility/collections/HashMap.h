#pragma once
#ifndef __HASHMAP_H__
#define __HASHMAP_H__
/*
 *		HashMap.h
 *
 *		Map data structure implemented with a hash table to provide fast
 *		lookups, insertions, and removes. The HashMap will manage the table
 *		size for you such that you are roughly around the specified load
 *		factor or the default load factor of 0.75.
 *
 *		contains( )				-	O(1) to O(N)
 *		put( )					-	O(1) to O(N)
 *		remove( )				-	O(1) to O(N)
 *		get( )					-	O(1) to O(N)
 *		clear( )				-	O(N)
 *		isEmpty( )				-	O(1)
 *		getSize( )				-	O(1)
 *
 *		Coded by Joseph A. Marrero
 *		5/12/05
 */
#include "DefaultCompare.h"
#include "DefaultHashFunction.h"

namespace Utility {

template <class Key, class Value, class KeyHasher = DefaultHashFunction, class Equals = DefaultCompare> 
class HashMap
{
private:
	class MapNode {
			friend class HashMap;
			friend class Mapper;
		private:
			Key	key;
			Value value;
			MapNode *next;
			unsigned int hashCode;
		public:
			MapNode( const Key theKey, const Value theValue, unsigned int theHashCode ) : key(theKey), value(theValue),
																						  hashCode(theHashCode), next(NULL)
			{}
			~MapNode( )
			{}
	};

	Equals	compareKeys;
	KeyHasher hasher;
	MapNode **m_MapTable;
	unsigned int m_tablesize, m_size;
	float m_LoadFactor;
public:
	HashMap( void );
	//HashMap( unsigned int initiailTableSize );
	HashMap( const float initialLoadFactor );
	HashMap( unsigned int initiailTableSize, const float initialLoadFactor = 0.75 );
	~HashMap( void );

	bool contains( const Key key );
	bool put( const Key key, const Value value );
	bool remove( const Key key );
	bool get( const Key key, Value &value );
	void clear( void );

	unsigned int getSize( ) const { return m_size; }
	bool isEmpty( ) const { return m_size <= 0; }
	
private:
	void halveSize( );
	void doubleSize( );
	float getLoadFactor( ) const { return (float) m_size / m_tablesize; }
};
#include "HashMap.inl"
} // end of namespace
#endif