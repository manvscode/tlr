/* -----------------------------------
	Templated Priority Queue Data Structure implementation
	Coded by: Peter Blanco
	Code Date: 4/25/05
-----------------------------------*/

//constructor
template<class AnyType, class cmpType> 
PrQueue<AnyType, cmpType>::PrQueue(cmpType comparator)
{
	cmp = comparator;
	firstPointer = lastPointer = NULL;
	size = 0;
}

//destructor
template<class AnyType, class cmpType> 
PrQueue<AnyType, cmpType>::~PrQueue()
{
	if (firstPointer)
	{
		Node * pt = firstPointer->getPointer();

		while(pt)
		{
			Node * nextPt = pt->getPointer();
			delete pt;
			pt = nextPt;
		}
		delete firstPointer;	
	}
}

//copy constructor
template<class AnyType, class cmpType> 
PrQueue<AnyType, cmpType>::PrQueue(
					const PrQueue<AnyType, cmpType> &toBeCopied)
{
	size = 0;
	cmp = toBeCopied.cmp;
	copy(toBeCopied.firstPointer);
}

//copy function
template <class AnyType, class cmpType> 
void PrQueue<AnyType, cmpType>::copy(Node* listNode)
{
	if (!listNode)
	{
		firstPointer = lastPointer = NULL;
		return;
	}

	Node *newNode;
	Node *head = newNode = new Node(*listNode);
	lastPointer = head;
	listNode = listNode->getPointer();
	size++;
	while(listNode)
	{
		newNode->next = new Node(*listNode);
		newNode = newNode->next;
		lastPointer = listNode;
		listNode = listNode->next;
		size++;
	}
	firstPointer = head;
}

//assignment operator overload
template <class AnyType, class cmpType> PrQueue<AnyType,cmpType> 
PrQueue<AnyType,cmpType>::operator = (const PrQueue<AnyType,cmpType> &toBeCopied)
{
	return PrQueue<AnyType>(toBeCopied);
}

//enqueue
template <class AnyType, class cmpType> 
void PrQueue<AnyType, cmpType>::enqueue(AnyType toBeEnqueued)
{
	if (!firstPointer)
	{
		firstPointer = lastPointer = new Node(toBeEnqueued, NULL);
		size++;
   	}
	else
	{		
		for (Node *next = firstPointer; next; next = next->getPointer())
			if (cmp(next->getData(), toBeEnqueued))
			{
				Node *swapper = new Node(next->getData(), next->next);
				next->next = swapper;
				next->data = toBeEnqueued;
				size++;
				//in case the node we just added is supposed
				//to be the last node
				if (lastPointer == next)
					lastPointer = swapper;
				return;
			}
			
		Node *newNode = new Node(toBeEnqueued, NULL);
		lastPointer->next = newNode;
		lastPointer = newNode;
		size++;
	}	
}

//peek
template <class AnyType, class cmpType> 
AnyType PrQueue<AnyType, cmpType>::peek()
{
	if (firstPointer)
	return firstPointer->getData();
	return AnyType();
}
	
//dequeue
template <class AnyType, class cmpType> 
AnyType PrQueue<AnyType, cmpType>::dequeue()
{
	if (!firstPointer)
		return AnyType();

	AnyType toBeDequeued = firstPointer->getData();
	Node * temp = firstPointer->getPointer();
	delete firstPointer;
	firstPointer = temp;
	size--;
	return toBeDequeued;
}

//is empty function
template <class AnyType, class cmpType> 
bool PrQueue<AnyType, cmpType>::isEmpty()
{
	return size == 0;
}

//size accessor
template <class AnyType, class cmpType> 
int PrQueue<AnyType, cmpType>::getSize()
{
	return size;
}