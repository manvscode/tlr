#pragma once
#ifndef __REDBLACKTREE_H__
#define __REDBLACKTREE_H__
/*
 *		RedBlackTree.h
 *
 *		A Red-black tree implementation.
 *		By Joseph A. Marrero, 10/13/06 - 11/10/06
 *		http://www.l33tprogrammer.com/
 */
//#include <list>

#include <memory>
#include <cassert>
#include "DefaultCompare.h"

namespace Utility {
/*
 *	Red-Black Tree Node
 */
template <class T>
class RBNode {    // Has to be outside due to the allocator...
  public:
	T data;
	RBNode<T> *parent, *left, *right;
	bool isRed;

	RBNode( )
		: data(), parent(this), left(this), right(this), isRed(false) // cannot change
	{}

	RBNode( const T &key, RBNode<T> *_parent, RBNode<T> *_left, RBNode<T> *_right, bool _isRed )
		: data(key), parent(_parent), left(_left), right(_right), isRed(_isRed)
	{}
};

/*
 *	Const Red-Black Tree Bidirectional In-order Iterator
 */
template <class T, class Comparer, class Allocator>
class CConstRBIterator;

/*
 *	Red-Black Tree Bidirectional In-order Iterator
 */
template <class T, class Comparer, class Allocator>
class CRBIterator;

/*
 *  Red-Black Node Iterator: a raw iterator for when one is needed.
 */
template <class T, class Comparer, class Allocator>
class CRBNodeIterator;

/*
 *	Red-Black Tree Container
 */	
template <class T, class Comparer = DefaultCompare, class Allocator = std::allocator< RBNode<T> > >
class CRedBlackTree
{
	friend class CRBIterator<T, Comparer, Allocator>;
	friend class CConstRBIterator<T, Comparer, Allocator>;
	friend class CRBNodeIterator<T, Comparer, Allocator>;

  public:
	typedef ptrdiff_t difference_type;
	typedef size_t size_type;
	typedef T value_type;
	typedef T * pointer;
	typedef T & reference;
	typedef CRBIterator<T, Comparer, Allocator> iterator;
	typedef CConstRBIterator<T, Comparer, Allocator> const_iterator;
	typedef CRBNodeIterator<T, Comparer, Allocator> node_iterator;

  private:
	RBNode<T> NIL;  // Typical leaf node (always black); has to be the first member var
	RBNode<T> *m_Root;
	Comparer m_Compare;
	Allocator m_ObjectPool;
	

	/* tree traversals */
	void gotoRoot( ); 
	void gotoMinimum( );
	void gotoMaximum( );
	RBNode<T> &getMinimum( );
	RBNode<T> &getMaximum( );
	const RBNode<T> &getMinimum( ) const;
	const RBNode<T> &getMaximum( ) const;
	RBNode<T> &getCurrentNode( );
	void traverseUp( );
	void traverseLeft( );
	void traverseRight( );
	void next( );
	void previous( );

  protected:
	bool verifySubTree( RBNode<T> *t );



	RBNode<T> &minimum( RBNode<T> *t );
	RBNode<T> &maximum( RBNode<T> *t );
	const RBNode<T> &minimum( RBNode<T> *t ) const;
	const RBNode<T> &maximum( RBNode<T> *t ) const;
	RBNode<T> &successor( RBNode<T> *t );
	RBNode<T> &predecessor( RBNode<T> *t );

	void leftRotate( RBNode<T> *t );
	void rightRotate( RBNode<T> *t );
	void insertFixup( RBNode<T> *& t );
	void deleteFixup( RBNode<T> *& t );
	

	unsigned int m_NumberOfNodes;

  public:
	CRedBlackTree( );
	CRedBlackTree( const CRedBlackTree<T, Comparer, Allocator> &container );
	~CRedBlackTree( );
	bool verifyTree( );

	void insert( const T &key );
	bool remove( const T &key );
	bool search( const T &key ) const;
	RBNode<T> *search( const T &key );
	iterator find( const T &key );

	void clear( );
	unsigned int size( ) const;
	bool empty( ) const;

	const_iterator begin( ) const;
	const_iterator end( ) const;
	iterator begin( );
	iterator end( );
	node_iterator nbegin( );
	node_iterator nend( );

	const CRedBlackTree<T, Comparer, Allocator> &operator=( const CRedBlackTree<T, Comparer, Allocator> &other );
};

template <class T, class Comparer, class Allocator>
inline unsigned int CRedBlackTree<T, Comparer, Allocator>::size( ) const
{ return m_NumberOfNodes; }

template <class T, class Comparer, class Allocator>
inline bool CRedBlackTree<T, Comparer, Allocator>::empty( ) const
{ return m_NumberOfNodes <= 0; }

template <class T, class Comparer, class Allocator>
inline RBNode<T> &CRedBlackTree<T, Comparer, Allocator>::minimum( RBNode<T> *t )
{
	while( t->left != &NIL )
		t = t->left;
	
	return *t;
}

template <class T, class Comparer, class Allocator>
inline RBNode<T> &CRedBlackTree<T, Comparer, Allocator>::maximum( RBNode<T> *t )
{
	while( t->right != &NIL )
		t = t->right;
	
	return *t;
}

template <class T, class Comparer, class Allocator>
inline const RBNode<T> &CRedBlackTree<T, Comparer, Allocator>::minimum( RBNode<T> *t ) const
{
	while( t->left != &NIL )
		t = t->left;

	return *t;
}

template <class T, class Comparer, class Allocator>
inline const RBNode<T> &CRedBlackTree<T, Comparer, Allocator>::maximum( RBNode<T> *t ) const
{
	while( t->right != &NIL )
		t = t->right;

	return *t;
}

template <class T, class Comparer, class Allocator>
inline RBNode<T> &CRedBlackTree<T, Comparer, Allocator>::successor( RBNode<T> *t )
{
	if( t->right != &NIL )
		return minimum( t->right );

	RBNode<T> *y = t->parent;
	
	while( y != &NIL && t == y->right )
	{
		t = y;
		y = y->parent;
	}

	return *y;
}

template <class T, class Comparer, class Allocator>
inline RBNode<T> &CRedBlackTree<T, Comparer, Allocator>::predecessor( RBNode<T> *t )
{
	if( t->left != &NIL )
		return maximum( t->left );

	RBNode<T> *y = t->parent;
	
	while( y != &NIL && t == y->left )
	{
		t = y;
		y = y->parent;
	}

	return *y;
}

template <class T, class Comparer, class Allocator>
inline void CRedBlackTree<T, Comparer, Allocator>::leftRotate( RBNode<T> *x )
{
	if( x->right == &NIL ) return;

	RBNode<T> *y = x->right;
	x->right = y->left;

	if( y->left != &NIL )
		y->left->parent = x;

	y->parent = x->parent;

	if( x->parent == &NIL )
		m_Root = y;
	else {
		if( x == x->parent->left )
			x->parent->left = y;
		else
			x->parent->right = y;
	}

	y->left = x;
	x->parent = y;
}

template <class T, class Comparer, class Allocator>
inline void CRedBlackTree<T, Comparer, Allocator>::rightRotate( RBNode<T> *x )
{
	if( x->left == &NIL ) return;

	RBNode<T> *y = x->left;
	x->left = y->right;

	if( y->right != &NIL )
		y->right->parent = x;

	y->parent = x->parent;

	if( x->parent == &NIL )
		m_Root = y;
	else {
		if( x == x->parent->right )
			x->parent->right = y;
		else
			x->parent->left = y;
	}

	y->right = x;
	x->parent = y;
}


template <class T, class Comparer, class Allocator>
inline void CRedBlackTree<T, Comparer, Allocator>::insertFixup( RBNode<T> *& t )
{
	RBNode<T> *y = &NIL;

	while( t->parent->isRed )
	{
		if( t->parent == t->parent->parent->left ) //is parent on left side of grandparent...
		{
			y = t->parent->parent->right; // y is uncle
			//case 1
			if( y->isRed ) //uncle is red, just recolor
			{
				t->parent->isRed = false;
				y->isRed = false;
				t->parent->parent->isRed = true;
				t = t->parent->parent;
			}
			else 
			{
				//case 2
				if( t == t->parent->right ) // uncle is black and t is on the right
				{
					t = t->parent;
					leftRotate( t );
				}
				//case 3
				t->parent->isRed = false;
				t->parent->parent->isRed = true;
				rightRotate( t->parent->parent );
			}
		}
		else  // parent is on right side of grandparent...
		{
			y = t->parent->parent->left; // y is uncle
			//case 1
			if( y->isRed ) // uncle is black
			{
				t->parent->isRed = false;
				y->isRed = false;
				t->parent->parent->isRed = true;
				t = t->parent->parent;
			}
			else
			{
				//case 2
				if( t == t->parent->left ) // uncle black and t is on the left
				{
					t = t->parent;
					rightRotate( t );
				}
				//case 3
				t->parent->isRed = false;
				t->parent->parent->isRed = true;
				leftRotate( t->parent->parent );
			}
		}
	}

	m_Root->isRed = false;
}

template <class T, class Comparer, class Allocator>
inline void CRedBlackTree<T, Comparer, Allocator>::deleteFixup( RBNode<T> *& t )
{
	RBNode<T> *w = &NIL;

	while( t != &NIL && t->isRed == false )
	{
		if( t == t->parent->left )
		{
			w = t->parent->right;

			if( w->isRed )
			{
				w->isRed = false;
				t->parent->isRed = true;
				leftRotate( t->parent );
				w = t->parent->right;
			}

			if( w->left->isRed == false && w->right->isRed == false )
			{
				w->isRed = true;
				t = t->parent;
			}
			else {
				if( w->right->isRed == false )
				{
					w->left->isRed = false;
					w->isRed = true;
					rightRotate( w );
					w = t->parent->right;
				}
			
				w->isRed = t->parent->isRed;
				t->parent->isRed = false;
				w->right->isRed = false;
				leftRotate( t->parent );
				t = m_Root;
			}

		}
		else {
			w = t->parent->left;

			if( w->isRed )
			{
				w->isRed = false;
				t->parent->isRed = true;
				rightRotate( t->parent );
				w = t->parent->left;
			}

			if( w->right->isRed == false && w->left->isRed == false )
			{
				w->isRed = true;
				t = t->parent;
			}
			else {
				if( w->left->isRed == false )
				{
					w->right->isRed = false;
					w->isRed = true;
					leftRotate( w );
					w = t->parent->left;
				}
			
				w->isRed = t->parent->isRed;
				t->parent->isRed = false;
				w->left->isRed = false;
				rightRotate( t->parent );
				t = m_Root;
			}
		}
	}

	t->isRed = false;
}

template <class T, class Comparer, class Allocator>
inline typename CRedBlackTree<T, Comparer, Allocator>::const_iterator CRedBlackTree<T, Comparer, Allocator>::begin( ) const
{ return const_iterator( *this, getMinimum( ).left, &getMinimum( ) ); }

template <class T, class Comparer, class Allocator>
inline typename CRedBlackTree<T, Comparer, Allocator>::const_iterator CRedBlackTree<T, Comparer, Allocator>::end( ) const
{ return const_iterator( *this, getMaximum( ).right, &getMaximum( ) ); }

template <class T, class Comparer, class Allocator>
inline typename CRedBlackTree<T, Comparer, Allocator>::iterator CRedBlackTree<T, Comparer, Allocator>::begin( )
{ return iterator( *this, getMinimum( ).left, &getMinimum( ) ); }

template <class T, class Comparer, class Allocator>
inline typename CRedBlackTree<T, Comparer, Allocator>::iterator CRedBlackTree<T, Comparer, Allocator>::end( )
{ return iterator( *this, getMaximum( ).right, &getMaximum( ) ); }

template <class T, class Comparer, class Allocator>
inline typename CRedBlackTree<T, Comparer, Allocator>::node_iterator CRedBlackTree<T, Comparer, Allocator>::nbegin( )
{ return node_iterator( *this, m_Root ); }

template <class T, class Comparer, class Allocator>
inline typename CRedBlackTree<T, Comparer, Allocator>::node_iterator CRedBlackTree<T, Comparer, Allocator>::nend( )
{ return node_iterator( *this, &NIL ); }




/*
 *	Const Red-Black Tree Bidirectional In-order Iterator
 */
template <class T, class Comparer, class Allocator>
class CConstRBIterator
{
	friend class CRBIterator<T, Comparer, Allocator>;
  protected:
	CRedBlackTree<T, Comparer, Allocator> *m_Container;
	RBNode<T> *position, *safe_position;

  public:
	CConstRBIterator( );
	CConstRBIterator( const CRedBlackTree<T, Comparer, Allocator> &container, const RBNode<T> *p, const RBNode<T> *s = NULL );
	~CConstRBIterator( );

	bool operator==( const CConstRBIterator<T, Comparer, Allocator> &other ) const;
	bool operator!=( const CConstRBIterator<T, Comparer, Allocator> &other ) const;
	const T & operator*( );
	const T * operator->( );

	CConstRBIterator & operator++( );
	CConstRBIterator operator++( int );
	CConstRBIterator & operator--( );
	CConstRBIterator operator--( int );
};



/*
*	Red-Black Tree Bidirectional In-order Iterator
*/
template <class T, class Comparer, class Allocator>
class CRBIterator : public CConstRBIterator<T, Comparer, Allocator>
{

public:
	CRBIterator( );
	CRBIterator( CRedBlackTree<T, Comparer, Allocator> &container, RBNode<T> *p, RBNode<T> *s = NULL );
	~CRBIterator( );

	bool operator==( const CRBIterator<T, Comparer, Allocator> &other ) const;
	bool operator!=( const CRBIterator<T, Comparer, Allocator> &other ) const;
	T & operator*( );
	T * operator->( );

	CRBIterator & operator++( );
	CRBIterator operator++( int );
	CRBIterator & operator--( );
	CRBIterator operator--( int );

	CRBIterator &operator=( const CRBIterator<T, Comparer, Allocator> &other );
};


/*
 *  Red Black Node Iterator: a raw iterator for when one is needed.
 */
template <class T, class Comparer, class Allocator>
class CRBNodeIterator
{
  protected:
	CRedBlackTree<T, Comparer, Allocator> *m_Container;
	RBNode<T> *m_Position;
	RBNode<T> *m_SafePosition;

  public:
	CRBNodeIterator( );
	CRBNodeIterator( CRedBlackTree<T, Comparer, Allocator> &container, RBNode<T> *position );
	~CRBNodeIterator( );

	bool operator==( const CRBNodeIterator<T, Comparer, Allocator> &other ) const;
	bool operator!=( const CRBNodeIterator<T, Comparer, Allocator> &other ) const;
	T & operator*( );
	T * operator->( );


	bool isNil( ) const;
	bool isRoot( ) const;
	bool isDone( ) const;

	void traverseLeft( );
	void traverseRight( );
	void traverseUp( );
};



#include "RedBlackTree.inl"
} //end of namespace
#endif