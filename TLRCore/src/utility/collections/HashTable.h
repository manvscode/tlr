#pragma once
#ifndef __HASHTABLE_H__
#define __HASHTABLE_H__
/*
 *		HashTable.h
 *
 *		Seperate chaining hash table implementation that uses singly linked lists.
 *		Performance could be increased if you peroidically check the load factor 
 *		and double it accordingly.
 *		
 *		insert( )			-	O(1) to O(N)
 *		remove( )			-	O(1) to O(N)
 *		lookup( )			-   O(1)
 *		getSize( )			-	O(1)
 *		getTableSize( )		-	O(1)
 *		getLoadFactor( )	-	O(1)
 *		doubleSize( )		-	O(N)
 *		increaseSize( )		-	O(N)
 *
 *		5/02/05 - Coded by Joseph A. Marrero
 */
#include "DefaultHashFunction.h"
#include "DefaultCompare.h"

namespace Utility {

template <class AnyType, class HashFunctor = DefaultHashFunction, class Comparer = DefaultCompare> class HashTable
{
private:
	class Node {
			friend class HashTable;
		public:
			Node( const AnyType &Data, const unsigned int HashCode ) : next( 0 )
			{
				data = Data;
				hashCode = HashCode;
				//next = NULL;
			}
			Node( const AnyType &Data, const unsigned int HashCode, Node *Next )
			{
				data = Data;
				hashCode = HashCode;
				next = Next;
			}
		private:
			AnyType data;
			unsigned int hashCode;
			Node *next;
	};

	HashFunctor hashFunction;
	Comparer compare;
	Node **table; // array of Node*
	Node *m_lookedupData;
	unsigned int tableSize, size;
public:
	HashTable( void );													// Creates a hash table with a table size of 31.
	HashTable( const unsigned int initialTableSize );							// Creates a hash table with a table size of initialTableSize.
	~HashTable( void );

	bool insert( const AnyType &Data );									// Inserts the specified data into the hash table.
	bool remove( const AnyType &Data );									// Removes the specified data from the hash table.
	bool lookup( const AnyType &Data );									// Returns true if the data is in the hash table.
	void clear( void );													// Clears the Hash table.
	bool get( AnyType *data );											// returns the lasted looked up data.
	
	bool isEmpty( ) const { return (size <= 0); }						// Returns true when the hash table is empty.
	unsigned int getTableSize( ) const { return tableSize; }			// Get the length of the table.
	unsigned int getSize( ) const { return size; }						// Get the number of elements in the hash table.
	float getLoadFactor( ) const { return (float) size / tableSize; }	// Load factor is the average length of a list.
	/* Increase TableSize */
	void halveSize( );													// Decrease the table size by a factor of 2. Ensures Log(N) halving until we reach N.
	void doubleSize( );													// Increases the table size by a factor of 2. Ensures Log(N) doubling until we reach N. 
	void increaseSize( const unsigned int newTableSize );				// Increases the table size by a fixed amount.

};
#include "HashTable.inl"
} //end of namespace
#endif