#pragma once
#ifndef _STACK_H_
#define _STACK_H_
#include <stdio.h>
#include <lib.h>

//Our Namespace
namespace Utility 
{
//Generic Stack Class
template <class AnyType> 
class TLR_API Stack
{
public:
	Stack();
	Stack(const Stack<AnyType> &toBeCopied);
	~Stack();
	Stack<AnyType> operator = (const Stack<AnyType> &toBeCopied);
	AnyType pop();
	void push (AnyType toBePushed);
	AnyType peek();
	int getSize();
	bool isEmpty();
		
private:
	//Private inner Node class for singly LL implementation of stack
	class Node
	{
	public:
		//constructs a new node with the data this Node will hold
		//and the pointer to the next node
		Node(AnyType toBeSet, Node * pt)
		{
			data = toBeSet;
			next = pt;
		}
		Node(const Node &toBeCopied)
		{	
			data = toBeCopied.data;
			next = NULL;
		}
		//returns the data this Node holds
		AnyType getData()
		{
			return data;
		}
		//returns the pointer to the next node in the list
		Node * getPointer()
		{
			return next;
		}
		Node * next;
		AnyType data;
	};
	//always points to the first node in the list,
	//a.k.a., the "top" of the stack
	Node* stackPointer;
	void copy(Node* listNode);
	int size;
};
};
#include "Stack.inl"

#endif



