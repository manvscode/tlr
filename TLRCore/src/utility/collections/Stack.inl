/* -----------------------------------
	Templated Stack Data Structure implementation
	Coded by: Peter Blanco
	Code Date: 4/18/05
-----------------------------------*/
namespace Utility 
{
template <class AnyType> Stack<AnyType>::Stack()
{
	size = 0;
	stackPointer = NULL;
}

//copy constructor
template <class AnyType> Stack<AnyType>::Stack(const Stack<AnyType> &toBeCopied)
{
	size = 0;
	//copy(toBeCopied.stackPointer);
}

//copies the provided Node list;
template <class AnyType> int Stack<AnyType>::getSize()
{
	return size;
}

template <class AnyType> bool Stack<AnyType>::isEmpty()
{
	return size == 0;
}

template <class AnyType> void Stack<AnyType>::copy(Node* listNode)
{
	if (!listNode)
	{
		stackPointer = NULL;
		return;
	}
	Node *newNode;
	Node *head = newNode = new Node(*listNode);
	listNode = listNode->getPointer();
	size++;
	while(listNode)
	{
		newNode->next = new Node(*listNode);
		newNode = newNode->next;
		listNode = listNode->next;
		size++;
	}
	stackPointer = head;
}

//copies the provided stack
template <class AnyType> Stack<AnyType> 
	Stack<AnyType>::operator = (const Stack<AnyType> &toBeCopied)
{
	return Stack<AnyType>(toBeCopied);
}

//Clears all the pointers, preventing memory leaks
template <class AnyType> Stack<AnyType>::~Stack()
{
	//in case the last node was popped, theres nothing left
	//to delete
	if (stackPointer)
	{
		Node * pt = stackPointer->getPointer();

		while(pt)
		{
			Node * nextPt = pt->getPointer();
			delete pt;
			pt = nextPt;
		}
		delete stackPointer;	
	}
}

//makes the current top of the stack the next to top one
//the new node now points to the old head and stackPointer points
//to the new head.
template <class AnyType> void Stack<AnyType>::push(AnyType toBeSorted)
{
	if (!stackPointer)
		stackPointer = new Node(toBeSorted, NULL);

	else
	{
		Node * newNode = new Node(toBeSorted, stackPointer);
		stackPointer = newNode;
	}
	size++;
}

//pops and returns the element at the top of the stack
//the next to last element now becomes the new top
//and the pointer to the old head is deleted
template <class AnyType> AnyType Stack<AnyType>::pop()
{
	if (stackPointer == NULL)
		return AnyType();

	AnyType toBeReturned = stackPointer->getData();
	Node * temp = stackPointer->getPointer();
	delete stackPointer;
	stackPointer = temp;
	size--;
	return toBeReturned;
}

//returns the data being held at the top of the stack
//peek does NOT pop the element off the top of the stack
template <class AnyType> AnyType Stack<AnyType>::peek()
{
	if (stackPointer)
		return stackPointer->getData();
	return AnyType();
}
};
