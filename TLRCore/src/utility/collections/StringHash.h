#pragma once
#ifndef __STRINGHASH_H__
#define __STRINGHASH_H__
/*
 *	StringHash.h
 */
#include <lib.h>

namespace Utility
{
	class TLR_API CStringHash
	{
	  public:
 		unsigned int operator()( const std::string &s )
		{
			unsigned int hash = 0;
		
			for( register unsigned int i = 0; i < s.length(); i++ )
				hash = 31 * hash + s[ i ]; // 31 is prime
			return hash;
		}
	};
} //end of namespace
#endif
