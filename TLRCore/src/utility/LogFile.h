#pragma once
#ifndef __LOGFILE_H__
#define __LOGFILE_H__
/*
 *	LogFile.h
 *	Coded by Joseph A. Marrero
 */
 
#include <fstream>
#include <ostream>
#include <iostream>
#include <string>
using namespace std;

namespace Utility {

class CLogFile
{
  public:
	explicit CLogFile( const char *logFile );
	explicit CLogFile( const string &logFile );
	explicit CLogFile( const char *logFile, bool bLogTime );
	explicit CLogFile( const string &logFile, bool bLogTime );
  	~CLogFile( );
  
	bool initialize( );
  	const string &getLogFilename( ) const;

	// Unformatted and without an output prefix.
	void write( const char *logText );
	void write( const string &logText );
	void writeLine( const char *logText );
	void writeLine( const string &logText );


	// Formatted output
	//void writef( const char *logText );
	void writef( const string &logText );
	void writef( const char *logTextFormat, ... );
	///void writeLinef( const char *logText );
	void writeLinef( const string &logText );
	void writeLinef( const char *logTextFormat, ... );
	
	
	void operator<< ( ostream &os );
	
	
	ostream &operator<< ( bool val );
	ostream &operator<< ( short val );
	ostream &operator<< ( unsigned short val );
	ostream &operator<< ( int val );
	ostream &operator<< ( unsigned int val );
	ostream &operator<< ( long val );
	ostream &operator<< ( unsigned long val );
	ostream &operator<< ( float val );
	ostream &operator<< ( double val );
	ostream &operator<< ( long double val );
	ostream &operator<< ( char val );
	ostream &operator<< ( const char *val );
	ostream &operator<< ( const string &val );
	
  	void newLine( );

	string &getTimeString( );

	ostream &getStream( );
  private:
	ofstream 	m_FileStream;
  	string 		m_LogFilename;
	bool		m_bLogTime;
	string		m_localTime;
};

} //end of namespace
#endif
