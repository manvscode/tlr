#pragma once
#ifndef __DEBUG_H__
#define __DEBUG_H__
/*
 *		debug.h
 * 
 * 		ASSERT(), VERIFY(), and TRACE() macros.
 * 		Very useful for debugging.
 *		Coded by Joseph A. Marrero
 */
#include <iostream>
#include <cstdio>
#include <cstdarg>
#include <cassert>
using namespace std;

namespace Utility {

/*
 *	make sure to include std::assert()
 */


#ifdef _DEBUG
void DEBUG_TRACE( char *fmt, ... );
#define ASSERT(x)			(assert((x)))
#define VERIFY(x)			(assert((x)))
#define printDebug( expr )	cerr << __FILE__  << " [" << __LINE__  \
               				<< "] (" << #expr << "): " << ( expr ) << endl
#define printDebugTrace( )	cerr << " @ " << __FILE__  << " [" << __LINE__ << "]" << endl
#else
#define ASSERT(x)
#define VERIFY(x)		x
#define printDebug( expr )
#define printDebugTrace( )
#endif



#ifdef _DEBUG
	inline void DEBUG_TRACE( const char *fmt, ... )
	{
		va_list body;
		va_start( body, fmt );
		vfprintf( stderr, fmt, body );
		va_end( body );
	}
#else

inline void DEBUG_TRACE( char *fmt, ... ) {}

#endif

const char *getDateLastCompiled( );
const unsigned int getLineNumber( );
char *getFilename( );
char *getLastTimeCompiled( );
char *getLastTimeModified( );



inline const char *getDateLastCompiled( )
{ return __DATE__; }

inline const unsigned int getLineNumber( )
{ return __LINE__; }

inline char *getFilename( )
{ return __FILE__; }

inline char *getLastTimeCompiled( )
{ return __TIME__; }

inline char *getLastTimeModified( )
{ return __TIMESTAMP__; }

} //end of namespace
#endif
