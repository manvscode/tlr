#pragma once
#ifndef _STRINGTOKENIZER_H_
#define _STRINGTOKENIZER_H_
/*
 *		StringTokenizer.h
 *
 *		Breaks a string into tokens that are seperated by delimiter
 *		strings.
 *
 *		Coded by Joseph A. Marrero, May 5, 2006
 */
#include <lib.h>
#include <string>
#include "debug.h"

namespace Utility {

#define 	SPACE_STRING		" "
#define		TAB_STRING			"\t"
#define		NEWLINE_STRING		"\n"

class TLR_API CStringTokenizer
{
  public:
	explicit CStringTokenizer( );
	explicit CStringTokenizer( const char *str );
	explicit CStringTokenizer( const char *str, const char *delimiter );
  	explicit CStringTokenizer( const std::string &str );
  	explicit CStringTokenizer( const std::string &str, const std::string &delimiter );
  	
  	void setString( const std::string &s );
  	void setDelimiter( const std::string &delimiter );
  	void setStringAndDelimiter( const std::string &str, const std::string &delimiter );
  	void tokenize( );
	void clear( );
  	
  	
  	std::string nextToken( );
  	int nextIntToken( );
  	long int nextLongToken( );
  	//long long int nextLongLongToken( ); // Causes problems with long long and portability
  	double nextFloatToken( );
  	
  	unsigned int getNumberOfTokens( ) const { return m_uiNumberOfTokens; }
  	bool hasMoreTokens( ) const { return m_uiNumberOfTokens > 0; }
  	
  private:
  	
  	std::string m_szTokenizedString;
  	std::string m_cDelimiter;
  	unsigned int m_uiNumberOfTokens;
};


/*                                                              
 * Inlines                                                                
 */
inline void CStringTokenizer::setString( const std::string &s )
{
	assert( s != "" );
	m_szTokenizedString = s;
}

inline void CStringTokenizer::setDelimiter( const std::string &delimiter )
{
	assert( delimiter != "" );
	m_cDelimiter = delimiter;
}

inline void CStringTokenizer::setStringAndDelimiter( const std::string &str, const std::string &delimiter )
{
	assert( str != "" || delimiter != "" );
	m_szTokenizedString = str;
	m_cDelimiter = delimiter;
}

inline void CStringTokenizer::clear( )
{
	m_szTokenizedString.clear( );
	m_cDelimiter.clear( );
	m_uiNumberOfTokens = 0;
}

inline std::string CStringTokenizer::nextToken( )
{
	std::string token;
	//while( token.empty( ) && hasMoreTokens( ) )
	//{
		register unsigned int i = 0;
	
		while( m_szTokenizedString[ i ] != '\0' && i < m_szTokenizedString.size( ) )
		{
			token.append( 1, m_szTokenizedString[ i++ ] );
		}
	
	
		m_szTokenizedString.erase( 0, i + 1 );
		m_uiNumberOfTokens--;
	//}
	return token;
}

inline int CStringTokenizer::nextIntToken( )
{
	std::string token = nextToken( );
	return atoi( token.c_str( ) );
}

inline long int CStringTokenizer::nextLongToken( )
{
	std::string token = nextToken( );
	return atol( token.c_str( ) );
}

inline double CStringTokenizer::nextFloatToken( )
{
	std::string token = nextToken( );
	return atof( token.c_str( ) );
}	




} //end of namespace
#endif
