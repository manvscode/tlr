#pragma once
#ifndef _LIB_H_
#define _LIB_H_


#ifdef WIN32 // Windows
	#ifdef BUILD_TLR_CORE
		#define TLR_API			__declspec( dllexport )
	#else
		#define TLR_API			__declspec( dllimport )
	#endif
#endif

//#ifdef WIN32 // Windows
//	#ifndef IMPORT_TLR
//	#define IMPORT_TLR			__declspec( dllimport )
//	#endif
//
//	#ifndef TLR_API
//	#define TLR_API			__declspec( dllexport )
//	#endif
//#else // Linux / Unix
//	#define IMPORT_TLR
//	#define TLR_API
//#endif




#endif