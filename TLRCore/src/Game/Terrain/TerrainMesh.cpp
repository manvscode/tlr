
//#include "StdAfx.h"
#include <windows.h>
#include <cassert>
#include "TerrainMesh.h"


namespace Terrain {


CTerrainMesh::CTerrainMesh( )
 :	m_fWidth(0),
	m_fLength(0),
	m_fMaxHeight(0),
	m_fMinHeight(0),
	m_Vertices(),
	m_uiElementsWidth(0),
	m_uiElementsLength(0)
{
}

CTerrainMesh::CTerrainMesh( const CTerrainMesh::TerrainCollection &geometry, 
						    const float width, const float length, 
							const float minHeight, const float maxHeight, 
						    const unsigned int elementWidth, const unsigned int elementLength )
 :	m_fWidth(width),
	m_fLength(length),
	m_fMaxHeight(maxHeight),
	m_fMinHeight(minHeight),
	m_Vertices(geometry),
	m_fUnitWidth(width / elementWidth),
	m_fUnitLength(length / elementLength),
	m_uiElementsWidth(elementWidth),
	m_uiElementsLength(elementLength)
{
	
}

CTerrainMesh::CTerrainMesh( const CTerrainMesh &tm )
 :	m_fWidth(tm.m_fWidth),
	m_fLength(tm.m_fLength),
	m_fMaxHeight(tm.m_fMaxHeight),
	m_fMinHeight(tm.m_fMinHeight),
	m_Vertices(tm.m_Vertices),
	m_uiElementsWidth(tm.m_fUnitWidth),
	m_uiElementsLength(tm.m_fUnitLength)
{

}

CTerrainMesh::~CTerrainMesh( )
{
}

const CTerrainMesh &CTerrainMesh::operator=( const CTerrainMesh &tm )
{
	if( &tm != this )
	{
		m_fWidth = tm.m_fWidth;
		m_fLength = tm.m_fLength;
		m_fMaxHeight = tm.m_fMaxHeight;
		m_fMinHeight = tm.m_fMinHeight;

		m_Vertices = tm.m_Vertices;

		m_fUnitWidth = tm.m_fUnitWidth;
		m_fUnitLength = tm.m_fUnitLength;

		m_uiElementsWidth = tm.m_uiElementsWidth;
		m_uiElementsLength = tm.m_uiElementsLength;
	}

	return *this;
}

void CTerrainMesh::render( GLenum renderMode )
{
	if( m_Vertices.size( ) > 0 )
	{
		glPointSize( 3.0f );
		glPushMatrix( );
			glInterleavedArrays( GL_T2F_N3F_V3F, 0, &m_Vertices[ 0 ] );
			glDrawArrays( renderMode, 0, m_Vertices.size( ) );
		glPopMatrix( );
	}
}

//void CTerrainMesh::setWidth( const float w )
//{ 
//	float rescaleFactor = w / m_fWidth;
//	m_fUnitWidth *= rescaleFactor;
//	m_fWidth = w;
//}
//
//void CTerrainMesh::setLength( const float l )
//{
//	float rescaleFactor = l / m_fLength;
//	m_fUnitLength *= rescaleFactor;
//	m_fLength = l;
//}

const CTerrainMesh::TerrainVertex &CTerrainMesh::operator[]( int idx ) const
{ return m_Vertices[ idx ]; }

CTerrainMesh::TerrainVertex &CTerrainMesh::operator[]( int idx )
{ return m_Vertices[ idx ]; }

//const CTerrainMesh::TerrainVertex &CTerrainMesh::getVertexAt( float x, float z ) const
//{ 
//	unsigned int xUInt = static_cast<unsigned int>( floor( m_fWidth / x ) );
//	unsigned int zUInt = static_cast<unsigned int>( floor( m_fLength / z ) );
//	unsigned int widthUInt = static_cast<unsigned int>( floor( m_fWidth ) );
//
//	return m_Vertices[ zUInt * widthUInt + xUInt ];
//}
//
//CTerrainMesh::TerrainVertex &CTerrainMesh::getVertexAt( float x, float z )
//{ return m_Vertices[ getIndexForVertexAt( x, z ) ]; }
//
//unsigned int CTerrainMesh::getIndexForVertexAt( float x, float z ) const
//{
//	assert( m_fUnitWidth != 0.0f );
//	unsigned int widthUInt = static_cast<unsigned int>( m_fWidth / m_fUnitWidth);
//	unsigned int xUInt = static_cast<unsigned int>( x / m_fUnitWidth )/* % widthUInt*/;
//	unsigned int zUInt = static_cast<unsigned int>( z / m_fUnitLength ) /*% widthUInt*/;
//	
//
//	assert( zUInt * widthUInt + xUInt >= 0 ); 
//	assert( zUInt * widthUInt + xUInt < m_Vertices.size( ) );
//
//	return zUInt * widthUInt + xUInt;
//
//}

#ifdef _DEBUG
unsigned int CTerrainMesh::getIndexForVertexAt( unsigned int xElement, unsigned int zElement ) const
{
	unsigned int idx = zElement * m_uiElementsWidth + xElement;
	assert( xElement < m_uiElementsWidth );
	assert( zElement < m_uiElementsLength );
	assert( idx < m_Vertices.size( ) );
	return idx;
}
#endif

void CTerrainMesh::setGeometry( const CTerrainMesh::TerrainCollection &geometry, 
								const float width, const float length, 
								const float minHeight, const float maxHeight, 
								const unsigned int elementWidth, const unsigned int elementLength )
{
	m_fWidth = width;
	m_fLength = length;
	m_fMaxHeight = maxHeight;
	m_fMinHeight = minHeight;
	m_fUnitWidth = width / elementWidth;
	m_fUnitLength = length / elementLength;

	m_uiElementsWidth = elementWidth;
	m_uiElementsLength = elementLength;

	m_Vertices.clear( );
	m_Vertices = geometry;
}


} // end of namespace