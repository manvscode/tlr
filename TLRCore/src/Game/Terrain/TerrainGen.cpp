#ifdef _MAPEDITOR
#include "../ProgressDlg.h"
#endif
#include "TerrainGen.h"
#include <Math/Vector3.h>
#include <cmath>
#include <cassert>




namespace Terrain {



CTerrainGen::CTerrainGen( )
 : m_fWidth(0.0f), m_fLength(0.0f), m_fMaxHeight(0.0f), m_fMinHeight(0.0f), m_uiElementWidth(0), m_uiElementLength(0)
{
}

//CTerrainGen::CTerrainGen( const unsigned int width, const unsigned int height, const unsigned int byteCount, const unsigned char *bitmap, 
//					  	  const float terrainWidth, const float terrainLength, const float terrainMaxHeight, const float terrainMinHeight )
// : m_fWidth(terrainWidth), m_fLength(terrainLength), m_fMaxHeight(terrainMaxHeight), m_fMinHeight(terrainMinHeight)
//{
//	generatePoints( width, height, byteCount, bitmap, terrainWidth, terrainLength, terrainMaxHeight, terrainMinHeight );
//}

CTerrainGen::~CTerrainGen( )
{
}

void CTerrainGen::generateTriStrip( const unsigned int width, const unsigned int height, const unsigned int byteCount, const unsigned char *bitmap, 
   							const float terrainWidth, const float terrainLength, const float terrainMaxHeight, const float terrainMinHeight )
{
	unsigned int position1, position2, position3, position4;
	CVector3<float> vColor;
	float xScaleFactor = terrainWidth / width;
	float zScaleFactor = terrainLength / height;
	float halfTerrainWidth = terrainWidth / 2.0f;
	float halfTerrainLength = terrainLength / 2.0f;
	float heightLength = terrainMaxHeight - terrainMinHeight;
	
	m_fUnitWidth = xScaleFactor;
	m_fUnitLength = zScaleFactor;
	m_fWidth = terrainWidth;
	m_fLength = terrainLength;
	m_fMaxHeight = terrainMaxHeight;
	m_fMinHeight = terrainMinHeight;

	unsigned int j = 0;
	unsigned int i = 0;
	float calculatedHeight = 0.0f;
	CVector3<float> colorRampVector;

	for( ; j < height-1; j++ )
	{
		i = 0;
		GameVertex v1, v2, v3;
		position1 = j * width * byteCount + i * byteCount;
		
		// vert 0
		vColor.setX( bitmap[ position1 ] );
		vColor.setY( bitmap[ position1 + 1 ] );
		vColor.setZ( bitmap[ position1 + 2 ] );
		calculatedHeight = ( vColor.getMagnitude( ) / WHITEVECTOR_MAGNITUDE ) * heightLength + terrainMinHeight;
		colorRampVector = colorRamp( calculatedHeight );
		v1.x = xScaleFactor * i - halfTerrainWidth;
		v1.y = calculatedHeight;
		v1.z = zScaleFactor * j - halfTerrainLength;
		v1.u = static_cast<float>( i );
		v1.v = static_cast<float>( j );
		////v1.r = colorRampVector.getX( );
		////v1.g = colorRampVector.getY( );
		////v1.b = colorRampVector.getZ( );
		////v1.a = 1.0f;
		v1.nx = 0.0f;
		v1.ny = 1.0f;
		v1.nz = 0.0f;
		m_Vertices.push_back( v1 );


		for( ; i < width-1; i++ )
		{
			CVector3<float> p1, p2;
			position1 = j * width * byteCount + i * byteCount;
			position2 = (j+1) * width * byteCount + i * byteCount;

			// vert 1
			vColor.setX( bitmap[ position1 ] );
			vColor.setY( bitmap[ position1 + 1 ] );
			vColor.setZ( bitmap[ position1 + 2 ] );
			calculatedHeight = ( vColor.getMagnitude( ) / WHITEVECTOR_MAGNITUDE ) * heightLength + terrainMinHeight;
			colorRampVector = colorRamp( calculatedHeight );
			v2.x = xScaleFactor * i - halfTerrainWidth;
			v2.y = calculatedHeight;
			v2.z = zScaleFactor * j - halfTerrainLength;
			v2.u = static_cast<float>( i );
			v2.v = static_cast<float>( j );
			////v2.r = colorRampVector.getX( );
			////v2.g = colorRampVector.getY( );
			////v2.b = colorRampVector.getZ( );
			////v2.a = 1.0f;

			
			// vert 2
			vColor.setX( bitmap[ position2 ] );
			vColor.setY( bitmap[ position2 + 1 ] );
			vColor.setZ( bitmap[ position2 + 2 ] );
			calculatedHeight = ( vColor.getMagnitude( ) / WHITEVECTOR_MAGNITUDE ) * heightLength + terrainMinHeight;
			colorRampVector = colorRamp( calculatedHeight );
			v3.x = xScaleFactor * i - halfTerrainWidth;
			v3.y = calculatedHeight;
			v3.z = zScaleFactor * (j+1) - halfTerrainLength;
			v3.u = static_cast<float>( i );
			v3.v = static_cast<float>( j + 1 );
			////v3.r = colorRampVector.getX( );
			////v3.g = colorRampVector.getY( );
			////v3.b = colorRampVector.getZ( );
			////v3.a = 1.0f;


			p1.setX( v2.x - v1.x );
			p1.setY( v2.y - v1.y );
			p1.setZ( v2.z - v1.z );

			p2.setX( v3.x - v1.x );
			p2.setY( v3.y - v1.y );
			p2.setZ( v3.z - v1.z );

			p2 = p2.crossProduct( p1 );
			p2.normalize( );

			v2.nx = p2.getX( );
			v2.ny = p2.getY( );
			v2.nz = p2.getZ( );
			v3.nx = p2.getX( );
			v3.ny = p2.getY( );
			v3.nz = p2.getZ( );

			m_Vertices.push_back( v2 );
			m_Vertices.push_back( v3 );
		}
		
		// vert 3
		m_Vertices.push_back( v3 );

	}


	//calculate vertex normals...


}


void CTerrainGen::generatePoints( const unsigned int width, const unsigned int height, const unsigned int byteCount, const unsigned char *bitmap, 
   							const float terrainWidth, const float terrainLength, const float terrainMaxHeight, const float terrainMinHeight,
							const bool bScaleUVs )
{
	assert( terrainWidth >= width );
	assert( terrainLength >= height );
	unsigned int position1, position2, position3, position4;
	CVector3<float> vColor, vWhite( 1.0f, 1.0f, 1.0f );
	float xScaleFactor = terrainWidth / width;
	float zScaleFactor = terrainLength / height;
	float halfTerrainWidth = terrainWidth / 2.0f;
	float halfTerrainLength = terrainLength / 2.0f;
	float heightLength = terrainMaxHeight - terrainMinHeight;

	#ifdef _MAPEDITOR
	CProgressDlg dlg( 0, static_cast<int>( width * height ), width );
	dlg.Create( IDD_PROGRESS_DIALOG );
	dlg.ShowWindow( SW_SHOW );
	#endif	

	m_fUnitWidth = xScaleFactor;
	m_fUnitLength = zScaleFactor;
	m_fWidth = terrainWidth;
	m_fLength = terrainLength;
	m_fMaxHeight = terrainMaxHeight;
	m_fMinHeight = terrainMinHeight;

	unsigned int j = 0;
	unsigned int i = 0;
	float calculatedHeight = 0.0f;
	CVector3<float> colorRampVector;

	// UV Scaling...
	const float uScaleFactor = bScaleUVs ? 1.0f / width : getUnitWidth( );
	const float vScaleFactor = bScaleUVs ? 1.0f / height : getUnitLength( );

	m_uiElementWidth = width;
	m_uiElementLength = height;

	for( ; j < height; j++ )
	{
		i = 0;
		TerrainVertex v1, v2, v3;
		position1 = j * width * byteCount + i * byteCount;
		
		// vert 0
		vColor.setX( bitmap[ position1 ] );
		vColor.setY( bitmap[ position1 + 1 ] );
		vColor.setZ( bitmap[ position1 + 2 ] );
		calculatedHeight = ( vColor.getMagnitude( ) / WHITEVECTOR_MAGNITUDE ) * heightLength + terrainMinHeight;
		colorRampVector = vWhite;//colorRamp( calculatedHeight );
		v1.x = xScaleFactor * i - halfTerrainWidth;
		v1.y = calculatedHeight;
		v1.z = zScaleFactor * j - halfTerrainLength;
		v1.u = static_cast<float>( i ) * uScaleFactor;
		v1.v = static_cast<float>( j ) * vScaleFactor;
		////v1.r = colorRampVector.getX( );
		////v1.g = colorRampVector.getY( );
		////v1.b = colorRampVector.getZ( );
		////v1.a = 1.0f;
		v1.nx = 0.0f;
		v1.ny = 1.0f;
		v1.nz = 0.0f;


		for( ; i < width; i++ )
		{
			CVector3<float> p1, p2;
			position1 = j * width * byteCount + i * byteCount;
			if( j < width - 1)
				position2 = (j+1) * width * byteCount + i * byteCount;
			else 
				position2 = 0;
			// vert 1
			vColor.setX( bitmap[ position1 ] );
			vColor.setY( bitmap[ position1 + 1 ] );
			vColor.setZ( bitmap[ position1 + 2 ] );
			calculatedHeight = ( vColor.getMagnitude( ) / WHITEVECTOR_MAGNITUDE ) * heightLength + terrainMinHeight;
			colorRampVector = vWhite;//colorRamp( calculatedHeight );
			v2.x = xScaleFactor * i - halfTerrainWidth;
			v2.y = calculatedHeight;
			v2.z = zScaleFactor * j - halfTerrainLength;
			v2.u = static_cast<float>( i ) * uScaleFactor;
			v2.v = static_cast<float>( j ) * vScaleFactor;
			////v2.r = colorRampVector.getX( );
			////v2.g = colorRampVector.getY( );
			////v2.b = colorRampVector.getZ( );
			////v2.a = 1.0f;

			
			// vert 2
			vColor.setX( bitmap[ position2 ] );
			vColor.setY( bitmap[ position2 + 1 ] );
			vColor.setZ( bitmap[ position2 + 2 ] );
			calculatedHeight = ( vColor.getMagnitude( ) / WHITEVECTOR_MAGNITUDE ) * heightLength + terrainMinHeight;
			colorRampVector = vWhite;//colorRamp( calculatedHeight );
			v3.x = xScaleFactor * i - halfTerrainWidth;
			v3.y = calculatedHeight;
			v3.z = zScaleFactor * (j+1) - halfTerrainLength;
			v3.u = static_cast<float>( i ) * uScaleFactor;
			v3.v = static_cast<float>( j + 1 ) * vScaleFactor;
			////v3.r = colorRampVector.getX( );
			////v3.g = colorRampVector.getY( );
			////v3.b = colorRampVector.getZ( );
			////v3.a = 1.0f;


			p1.setX( v2.x - v1.x );
			p1.setY( v2.y - v1.y );
			p1.setZ( v2.z - v1.z );

			p2.setX( v3.x - v1.x );
			p2.setY( v3.y - v1.y );
			p2.setZ( v3.z - v1.z );

			p2 = p2.crossProduct( p1 );
			p2.normalize( );

			v2.nx = p2.getX( );
			v2.ny = p2.getY( );
			v2.nz = p2.getZ( );
			v3.nx = p2.getX( );
			v3.ny = p2.getY( );
			v3.nz = p2.getZ( );

			m_Vertices.push_back( v2 );
			
		}

		#ifdef _MAPEDITOR
		dlg.stepIt( );
		#endif
		
		
	}


	//calculate vertex normals...
}

CVector3<float> CTerrainGen::colorRamp( const float height )
{
	float trueHeight = m_fMaxHeight - m_fMinHeight;
	float terrainLevel = trueHeight * 0.10f;
	assert( height >= m_fMinHeight );

	if( height <  m_fMinHeight + 2.5 * terrainLevel )
		return CVector3<float>( 0.1f, 0.1f, 0.9f );
	else if( height <  m_fMinHeight + 3 * terrainLevel )
		return CVector3<float>( 0.7f, 0.7f, 0.3f );
	else if( height <  m_fMinHeight + 4 * terrainLevel )
		return CVector3<float>( 0.3f, 0.25f, 0.15f );
	else if( height <  m_fMinHeight + 5 * terrainLevel )
		return CVector3<float>( 0.3f, 0.6f, 0.3f );
	else if( height <  m_fMinHeight + 6 * terrainLevel )
		return CVector3<float>( 0.3f, 0.85f, 0.3f );
	else if( height <  m_fMinHeight + 9 * terrainLevel )
		return CVector3<float>( 0.0f, 0.8f, 0.0f );
	else 
		return CVector3<float>( 1.0f, 1.0f, 1.0f );
}




} // end of namespace