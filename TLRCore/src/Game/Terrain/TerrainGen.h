#pragma once
#ifndef _TERRAINGEN_H_
#define _TERRAINGEN_H_
#include <lib.h>
#include <vector>
#include <Math/Vector3.h>
#include <Math/Vertex.h>
using namespace Math;



namespace Terrain {


#define WHITEVECTOR_MAGNITUDE  (441.67295593006f)//(std::sqrt( 255.0f * 255.0f + 255.0f * 255.0f + 255.0f * 255.0f ))

class TLR_API CTerrainGen
{
  public:
	typedef Math::GameVertex TerrainVertex;
	typedef std::vector<TerrainVertex> TerrainCollection;


	CTerrainGen( );
	//CTerrainGen( const unsigned int width, const unsigned int height, const unsigned int byteCount, const unsigned char *bitmap, 
	//			 const float terrainWidth, const float terrainLength, const float terrainMaxHeight, const float terrainMinHeight );
	virtual ~CTerrainGen( );

	void generateTriStrip( const unsigned int width, const unsigned int height, const unsigned int byteCount, const unsigned char *bitmap, 
				   const float terrainWidth, const float terrainLength, const float terrainMaxHeight, const float terrainMinHeight );
	void generatePoints( const unsigned int width, const unsigned int height, const unsigned int byteCount, const unsigned char *bitmap, 
				   const float terrainWidth, const float terrainLength, const float terrainMaxHeight, const float terrainMinHeight,
				   const bool bScaleUVs = false );


	CVector3<float> colorRamp( const float height );

	TerrainCollection getHeightField( ) const;
	TerrainCollection &getHeightField( );

	unsigned int getElementWidth( ) const;
	unsigned int getElementLength( ) const;
	float getUnitWidth( ) const;
	float getUnitLength( ) const;
	float getWidth( ) const;
	float getLength( ) const;
	float getMaxHeight( ) const;
	float getMinHeight( ) const;
	

  protected:
	float m_fUnitWidth;
	float m_fUnitLength;
	float m_fWidth;
	float m_fLength;
	float m_fMaxHeight;
	float m_fMinHeight;
	unsigned int m_uiElementWidth;
	unsigned int m_uiElementLength;
	TerrainCollection m_Vertices;
};


inline CTerrainGen::TerrainCollection CTerrainGen::getHeightField( ) const
{ return m_Vertices; }

inline CTerrainGen::TerrainCollection &CTerrainGen::getHeightField( )
{ return m_Vertices; }

inline unsigned int CTerrainGen::getElementWidth( ) const
{ return m_uiElementWidth; }

inline unsigned int CTerrainGen::getElementLength( ) const
{ return m_uiElementLength; }

inline float CTerrainGen::getUnitWidth( ) const
{ return m_fUnitWidth; }

inline float CTerrainGen::getUnitLength( ) const
{ return m_fUnitLength; }

inline float CTerrainGen::getWidth( ) const
{ return m_fWidth; }

inline float CTerrainGen::getLength( ) const
{ return m_fLength; }

inline float CTerrainGen::getMaxHeight( ) const
{ return m_fMaxHeight; }

inline float CTerrainGen::getMinHeight( ) const
{ return m_fMinHeight; }


} // end of namespace
#endif
