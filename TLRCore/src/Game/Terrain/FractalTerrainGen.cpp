#ifdef _MAPEDITOR
#include "../ProgressDlg.h"
#endif
#include <cmath>
#include <ctime>
#include <algorithm>
#include <functional>
#include "FractalTerrainGen.h"



namespace Terrain {



CFractalTerrainGen::CFractalTerrainGen( )
: m_uiIterations(10), m_fWidth(25.0f), m_fLength(25.0f), m_fMaxHeight(1.0f), m_fMinHeight(-1.0f), m_fUnitWidth(1.0f), m_fUnitLength(1.0f),
  m_Vertices(), heightField(NULL)
{
	std::srand( (unsigned int) std::time( (time_t *) NULL ) );
}

CFractalTerrainGen::CFractalTerrainGen( const unsigned int iterations, const float width, const float length,
								  const float minHeight, const float maxHeight, const float average_push )
 : m_uiIterations(iterations), m_fWidth(width), m_fLength(length),
   m_fMaxHeight(maxHeight), m_fMinHeight(minHeight), m_fUnitWidth(1.0f), m_fUnitLength(1.0f),
   m_fPush(average_push), m_Vertices(), heightField(NULL)
{
	std::srand( (unsigned int) std::time( (time_t *) NULL ) );

	heightField = new float[ (unsigned int) width * length ];
	memset( heightField, 0, sizeof(float) * width * length );
}

CFractalTerrainGen::~CFractalTerrainGen( )
{
	delete [] heightField;
}

void CFractalTerrainGen::generate1( )
{
	float sum = 0.0f;
	unsigned int dstIndex = 0;
	unsigned int index = 0;
	unsigned int dy = 0, dx = 0;
	const unsigned int length = static_cast<unsigned int>(m_fLength);
	const unsigned int width = static_cast<unsigned int>(m_fWidth);


	// pass one: populate terrain heights; Note the += is neccessary with preseeding
	for( unsigned int j = 0; j < length; j++ )
		for( unsigned int i = 0; i < width; i++ )
			heightField[ getIndex( i, j ) ] += getNewPush( m_fPush )/* + pow( -1.0, 1.0*i ) * sin( i * (rand() % 100) / 100.0 )*/; 



	// pass two: smooth terrain out.
	for( unsigned int j = 1; j < length - 1; j++ )
		for( unsigned int i = 1; i < width - 1; i++ )
		{
			dstIndex = getIndex( i, j );
			heightField[ dstIndex ] = (heightField[ dstIndex ] +
									   heightField[ getIndex( i-1, j ) ] +
									   heightField[ getIndex( i+1, j ) ] +
									   heightField[ getIndex( i, j-1 ) ] +
									   heightField[ getIndex( i, j+1 ) ] 
									   ) / 5.0f;
		}

	//for( unsigned int j = 1; j < length - 1; j++ )
	//	for( unsigned int i = 1; i < width - 1; i++ )
	//	{
	//		dstIndex = getIndex( i, j );
	//		heightField[ dstIndex ] = (heightField[ dstIndex ] +
	//								   heightField[ getIndex( i-1, j ) ] +
	//								   heightField[ getIndex( i+1, j ) ] +
	//								   heightField[ getIndex( i, j-1 ) ] +
	//								   heightField[ getIndex( i, j+1 ) ] +
	//								   heightField[ getIndex( i+1, j+1 ) ] +
	//								   heightField[ getIndex( i-1, j+1 ) ] +
	//								   heightField[ getIndex( i-1, j-1 ) ] +
	//								   heightField[ getIndex( i+1, j-1 ) ]
	//								   ) / 9.0f;
	//	}

	// pass three: add extra vertices for degenerate triangles.
	TerrainVertex v1;
	TerrainVertex v2;
	TerrainVertex v3;
		
	for( unsigned int j = 0; j < length-1; j++ )
	{
		v1.x = 0 - 0.5f * m_fWidth;
		v1.y = heightField[ getIndex( 0, j ) ];
		v1.z = j - 0.5f * m_fLength;
		////v1.r = 0.0f;
		////v1.g = 0.5f + 1.0f * heightField[ getIndex( 0 + 1, j ) ];
		////v1.b = 0.0f;
		////v1.a = 1.0f;
		//m_Vertices.push_back( v1 );

		for( unsigned int i = 0; i < width-1; i++ )
		{
			v2.x = i - 0.5f * m_fWidth;
			v2.y = heightField[ getIndex( i, j ) ];
			v2.z = j - 0.5f * m_fLength;
			////v2.r = 0.0f;
			////v2.g = 0.5f + 1.0f * heightField[ getIndex( i , j ) ];
			////v2.b = 0.0f;
			////v2.a = 1.0f;
			m_Vertices.push_back( v2 );

			v3.x = i - 0.5f * m_fWidth;
			v3.y = heightField[ getIndex( i, j + 1 ) ];
			v3.z = (j + 1) - 0.5f * m_fLength;
			////v3.r = 0.0f;
			////v3.g = 0.5f + 1.0f * heightField[ getIndex( i, j + 1 ) ];
			////v3.b = 0.0f;
			////v3.a = 1.0f;
			//m_Vertices.push_back( v3 );
		}
		
		//v3.a = 0.0f;
		//m_Vertices.push_back( v3 );
	}


}

void CFractalTerrainGen::generate2( )
{
	const unsigned int length = static_cast<unsigned int>(m_fLength);
	const unsigned int width = static_cast<unsigned int>(m_fWidth);
	m_Vertices.clear( );

	#ifdef _MAPEDITOR
	CProgressDlg dlg( 0, static_cast<int>( width * length ), width );

	dlg.Create( IDD_PROGRESS_DIALOG );
	dlg.ShowWindow( SW_SHOW );
	#endif
	// generate heightfield
	subDivideAndPush( m_uiIterations, 0, 0, m_fWidth, m_fLength, m_fPush );

	

	// pass two: smooth terrain out.
	//unsigned int dstIndex = 0;
	//for( unsigned int j = 1; j < length - 1; j++ )
	//	for( unsigned int i = 1; i < width - 1; i++ )
	//	{
	//		dstIndex = getIndex( i, j );
	//		heightField[ dstIndex ] = (heightField[ dstIndex ] +
	//								   heightField[ getIndex( i-1, j ) ] +
	//								   heightField[ getIndex( i+1, j ) ] +
	//								   heightField[ getIndex( i, j-1 ) ] +
	//								   heightField[ getIndex( i, j+1 ) ] 
	//								   ) / 5.0f;
	//	}

	// generate mesh...
	TerrainVertex v1;
	TerrainVertex v2;
	TerrainVertex v3;
		
	for( unsigned int j = 0; j < length - 1; j++ )
	{
		v1.x = 0 - 0.5f * m_fWidth;
		v1.y = heightField[ getIndex( 0, j ) ];
		v1.z = j - 0.5f * m_fLength;
		//v1.r = 0.0f;
		//v1.g = 0.5f + 1.0f * heightField[ getIndex( 0 + 1, j ) ];
		//v1.b = 0.0f;
		//v1.a = 1.0f;
		//m_Vertices.push_back( v1 );

		for( unsigned int i = 0; i < width - 1; i++ )
		{
			v2.x = i - 0.5f * m_fWidth;
			v2.y = heightField[ getIndex( i, j ) ];
			v2.z = j - 0.5f * m_fLength;
			//v2.r = 0.0f;
			//v2.g = 0.5f + 1.0f * heightField[ getIndex( i , j ) ];
			//v2.b = 0.0f;
			//v2.a = 1.0f;
			m_Vertices.push_back( v2 );

			v3.x = i - 0.5f * m_fWidth;
			v3.y = heightField[ getIndex( i, j + 1 ) ];
			v3.z = (j + 1) - 0.5f * m_fLength;
			//v3.r = 0.0f;
			//v3.g = 0.5f + 1.0f * heightField[ getIndex( i, j + 1 ) ];
			//v3.b = 0.0f;
			//v3.a = 1.0f;
			//m_Vertices.push_back( v3 );
		}
		#ifdef _MAPEDITOR
		dlg.stepIt( );
		#endif
		//v3.a = 0.0f;
		//m_Vertices.push_back( v3 );
	}
}

void CFractalTerrainGen::subDivideAndPush( unsigned int iteration, const unsigned int x, const unsigned int y, const unsigned int width, const unsigned int height, float p )
//subDivideAndPush( unsigned int iteration, float x, float y, float width, float height )
{
	if( iteration <= 0 ) return;
	
	//p /= 2.0f;
	// subdivide...
	subDivideAndPush( iteration - 1, x, y, width >> 1, height >> 1, p / 2.0f );
	subDivideAndPush( iteration - 1, x + (width >> 1), y, width >> 1, height >> 1, p / 2.0f  );
	subDivideAndPush( iteration - 1, x, y + (height >> 1), width >> 1, height >> 1, p / 2.0f  );
	subDivideAndPush( iteration - 1, x + (width >> 1), y + (height >> 1), width >> 1, height >> 1, p / 2.0f  );

	
	// push boundary points...
	if( iteration % 2 == 0 )
	{
		heightField[ getIndex(x + (width >> 1), y) ] += getNewPush( p );
		heightField[ getIndex(x + width, y + (height >> 1)) ] += getNewPush( p );
		heightField[ getIndex(x + (width >> 1), y + height) ] += getNewPush( p );
		heightField[ getIndex(x, y + (height >> 1)) ] += getNewPush( p );
		heightField[ getIndex(x + (width >> 1), y + (height >> 1)) ] += getNewPush( p );
	}
	else {
		heightField[ getIndex(x, y) ] += getNewPush( p );
		heightField[ getIndex(x + width, y) ] += getNewPush( p );
		heightField[ getIndex(x + width - 1, y + height - 1) ] += getNewPush( p );
		heightField[ getIndex(x, y + height) ] += getNewPush( p );
		heightField[ getIndex(x + (width >> 1), y + (height >> 1)) ] += getNewPush( p );
	}

}

void CFractalTerrainGen::preSeedHeight( unsigned int x, unsigned int z, float height )
{
	heightField[ getIndex(x,z) ] = height;
}




} // end of namespace