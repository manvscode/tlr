#pragma once
#ifndef _TERRAINMESH_H_
#define _TERRAINMESH_H_

#include <lib.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <vector>
#include <Math/Vector3.h>
#include <Math/Vertex.h>
#include <cassert>
using namespace Math;


namespace Terrain {

class TLR_API CTerrainMesh
{
  public:
	typedef Math::GameVertex TerrainVertex;
	typedef std::vector<TerrainVertex> TerrainCollection;

	CTerrainMesh( );
	explicit CTerrainMesh( const TerrainCollection &geometry, 
		                   const float width, const float length,
						   const float minHeight, const float maxHeight, 
						   const unsigned int elementWidth, const unsigned int elementLength );
	CTerrainMesh( const CTerrainMesh &tm );
	virtual ~CTerrainMesh( );
	const CTerrainMesh &operator=( const CTerrainMesh &tm );

	void render( GLenum renderMode );

	TerrainCollection &getMesh( );

	//void setWidth( const float w );
	float getWidth( ) const;
	//void setLength( const float l );
	float getLength( ) const;
	float getMinHeight( ) const;
	float getMaxHeight( ) const;
	float getUnitWidth( ) const;
	float getUnitLength( ) const;
	unsigned int size( ) const;
	const TerrainVertex &operator[]( int idx ) const;
	TerrainVertex &operator[]( int idx );

	unsigned int getElementWidth( ) const;
	unsigned int getElementLength( ) const;

	//const CompleteVertex &getVertexAt( float x, float z ) const;
	//CompleteVertex &getVertexAt( float x, float z );

	//unsigned int getIndexForVertexAt( float x, float z ) const;

	unsigned int getIndexForVertexAt( unsigned int xElement, unsigned int zElement ) const;
	TerrainVertex &getVertexAt( unsigned int xElement, unsigned int zElement );
	const TerrainVertex &getVertexAt( unsigned int xElement, unsigned int zElement ) const;



	void setGeometry( const TerrainCollection &geometry, 
				      const float width, const float length, 
					  const float minHeight, const float maxHeight, 
					  const unsigned int elementWidth, const unsigned int elementLength );

  protected:
	float m_fWidth;
	float m_fLength;
	float m_fMaxHeight;
	float m_fMinHeight;
	TerrainCollection m_Vertices;
	float m_fUnitWidth;
	float m_fUnitLength;
	unsigned int m_uiElementsWidth;
	unsigned int m_uiElementsLength;

};

#ifndef _DEBUG
inline unsigned int CTerrainMesh::getIndexForVertexAt( unsigned int xElement, unsigned int zElement ) const
{ return zElement * m_uiElementsWidth + xElement; }
#endif

inline CTerrainMesh::TerrainVertex &CTerrainMesh::getVertexAt( unsigned int xElement, unsigned int zElement )
{ return m_Vertices[ getIndexForVertexAt( xElement, zElement ) ]; }

inline const CTerrainMesh::TerrainVertex &CTerrainMesh::getVertexAt( unsigned int xElement, unsigned int zElement ) const
{ return m_Vertices[ getIndexForVertexAt( xElement, zElement ) ]; }

inline CTerrainMesh::TerrainCollection &CTerrainMesh::getMesh( )
{ return m_Vertices; }

inline float CTerrainMesh::getWidth( ) const
{ return m_fWidth; }

inline float CTerrainMesh::getLength( ) const
{ return m_fLength; }

inline float CTerrainMesh::getMinHeight( ) const
{ return m_fMinHeight; }

inline float CTerrainMesh::getMaxHeight( ) const
{ return m_fMaxHeight; }

inline float CTerrainMesh::getUnitWidth( ) const
{ return m_fUnitWidth; }

inline float CTerrainMesh::getUnitLength( ) const
{ return m_fUnitLength; }

inline unsigned int CTerrainMesh::getElementWidth( ) const
{ return m_uiElementsWidth; }

inline unsigned int CTerrainMesh::getElementLength( ) const
{ return m_uiElementsLength; }

inline unsigned int CTerrainMesh::size( ) const
{ return (unsigned int) m_Vertices.size( ); }




} // end of namespace
#endif