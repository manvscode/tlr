#pragma once
#ifndef _FRACTALTERRAIN_H_
#define _FRACTALTERRAIN_H_
#include <lib.h>
#include <vector>
#include <Math/Vertex.h>
#include <Game/Terrain/TerrainMesh.h>
#include <cassert>



namespace Terrain {

                                                                                                                                                                                                                                                                                 
class TLR_API CFractalTerrainGen
{
  public:
	typedef GameVertex TerrainVertex;
	typedef std::vector<TerrainVertex> TerrainCollection;

	CFractalTerrainGen( );
	CFractalTerrainGen( const unsigned int iterations, const float width, const float length, const float minHeight, const float maxHeight, const float average_push );
	virtual ~CFractalTerrainGen( );

	void generate1( );
	void generate2( );
	float getNewPush( float pushFactor );

	TerrainCollection getHeightField( ) const;
	TerrainCollection &getHeightField( );

	CTerrainMesh getMesh( );

	void preSeedHeight( unsigned int x, unsigned int z, float height = 5.0f );

	float getUnitWidth( ) const;
	float getUnitLength( ) const;
	float getWidth( ) const;
	float getLength( ) const;
	float getMaxHeight( ) const;
	float getMinHeight( ) const;

  protected:
	unsigned int getIndex( unsigned int x, unsigned int z );
	void subDivideAndPush( unsigned int iteration, const unsigned int x, const unsigned int y, const unsigned int width, const unsigned int height, float p );
	//subDivideAndPush( unsigned int iteration, float x, float y, float width, float height );

	unsigned int m_uiIterations;
	float m_fWidth;
	float m_fLength;
	float m_fMaxHeight;
	float m_fMinHeight;
	float m_fUnitWidth;
	float m_fUnitLength;
	float m_fPush;
	
	TerrainCollection m_Vertices;
	float *heightField;
};

inline float CFractalTerrainGen::getNewPush( float pushFactor )
{
	return ( (std::rand( ) % 200 - 100.0f ) / 100.0f ) * pushFactor;
}

inline CFractalTerrainGen::TerrainCollection CFractalTerrainGen::getHeightField( ) const
{
	return m_Vertices;
}

inline CFractalTerrainGen::TerrainCollection &CFractalTerrainGen::getHeightField( )
{
	return m_Vertices;
}

inline CTerrainMesh CFractalTerrainGen::getMesh( )
{
	CTerrainMesh mesh( m_Vertices, m_fWidth, m_fLength, m_fMinHeight, m_fMaxHeight, 0, 0 );
	assert(false);
	return mesh;
}

inline unsigned int CFractalTerrainGen::getIndex( unsigned int x, unsigned int z )
{
	return z * ( (unsigned int) m_fWidth) + x;
}


inline float CFractalTerrainGen::getUnitWidth( ) const
{ return m_fUnitWidth; }

inline float CFractalTerrainGen::getUnitLength( ) const
{ return m_fUnitLength; }

inline float CFractalTerrainGen::getWidth( ) const
{ return m_fWidth; }

inline float CFractalTerrainGen::getLength( ) const
{ return m_fLength; }

inline float CFractalTerrainGen::getMaxHeight( ) const
{ return m_fMaxHeight; }

inline float CFractalTerrainGen::getMinHeight( ) const
{ return m_fMinHeight; }



} // end of namespace
#endif