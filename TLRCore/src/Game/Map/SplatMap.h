#pragma once
#ifndef _SPLATMAP_H_
#define _SPLATMAP_H_
//#pragma message("Included _SPLATMAP_H_") 
#include <lib.h>
#include <map>
#include <vector>
#include <cassert>
#include <Game/Map/Map.h>
#include <Game/Map/MapCell.h>
#include <Game/Map/SplatMapChunk.h>
#include <Game/Map/SplatMapCellTexturing.h>
#include <cassert>



class CMapCell;

namespace Game {
	

static const unsigned int CHUNK_SIZE = 3;	// the number of tiles along a chunk's dimension
static const unsigned int NUMBER_OF_MAPCELLS_PER_CHUNK = CHUNK_SIZE * CHUNK_SIZE;	// the number of map cells per chunk


class CSplatMapChunk;
class CMapRowContainer;
class Terrain::CTerrainMesh;
class CAlphamapManager;

//template <>
//class CMap<CSplatMapTexturingPolicy>;
using namespace Terrain;


class TLR_API CSplatMap : private Game::CMap//Game::CMap<Game::CSplatMapTexturingPolicy>
{
	friend class Game::CSplatMapChunk;

  public:
	  //typedef Game::CMap<Game::CSplatMapTexturingPolicy> BaseMap;
	  typedef Game::CMap BaseMap;

  private:
//	Game::BaseMap m_Map;
	unsigned int m_uiNumberOfXChunks;
	unsigned int m_uiNumberOfZChunks;
	CSplatMapChunk  **m_Chunks;
	unsigned int m_TextureLayerMappings[ Game::MAX_TEXTURE_LAYERS ];
	bool m_bSplatMapMemoryAllocated;
	CAlphamapManager *m_pAlphamapManager;


	void cleanUp( );



  public:
	CSplatMap( );
	virtual ~CSplatMap( );


	void setTerrain( Terrain::CTerrainMesh &mesh, unsigned int numberOfXChunks = 1, unsigned int numberOfZChunks = 1 );
	CTerrainMesh &getTerrainMesh( );


	void initialize( );
	void deinitialize( );

	void render( );
	void setupTextureSettings( );

	
	void setTexture( unsigned int layer, unsigned int texture );
	unsigned int getTexture( unsigned int layer ) const;
	CAlphamapManager *getAlphamapManager( );

	void setMapName( std::string &name );
	void setMissionBriefing( std::string &brief );

	void setWindowDimensions( unsigned int width, unsigned int height );
	void setWindowPosition( unsigned int x, unsigned int y );
	void addWindowPosition( int dx, int dy );
	void incrementXWindowPosition( );
	void incrementYWindowPosition( );
	void decrementXWindowPosition( );
	void decrementYWindowPosition( );

	bool getContainerAtZ( float z, CMapRowContainer *&container );

	CMapCell *getMapCell( unsigned int x, unsigned int z );
	float getWidth( ) const;
	float getLength( ) const;
	float getMapCellWidth( ) const;
	float getMapCellLength( ) const;
	unsigned int getNumberOfXCells( ) const;
	unsigned int getNumberOfZCells( ) const;
	unsigned int getNumberOfXChunks( ) const;
	unsigned int getNumberOfZChunks( ) const;
};

inline void CSplatMap::setWindowDimensions( unsigned int width, unsigned int height )
{ return BaseMap::setWindowDimensions( width, height ); }

inline void CSplatMap::setWindowPosition( unsigned int x, unsigned int y )
{ return BaseMap::setWindowPosition( x, y ); }

inline void CSplatMap::addWindowPosition( int dx, int dy )
{ return BaseMap::addWindowPosition( dx, dy ); }

inline void CSplatMap::incrementXWindowPosition( )
{ return BaseMap::incrementXWindowPosition( ); }

inline void CSplatMap::incrementYWindowPosition( )
{ return BaseMap::incrementYWindowPosition( ); }

inline void CSplatMap::decrementXWindowPosition( )
{ return BaseMap::decrementXWindowPosition( ); }

inline void CSplatMap::decrementYWindowPosition( )
{ return BaseMap::decrementYWindowPosition( ); }


inline CTerrainMesh &CSplatMap::getTerrainMesh( )
{ return BaseMap::getTerrainMesh( ); }

inline unsigned int CSplatMap::getTexture( unsigned int layer ) const
{
	assert( layer < Game::MAX_TEXTURE_LAYERS );
	assert( layer >= 0 );
	return m_TextureLayerMappings[ layer ];
}

inline CAlphamapManager *CSplatMap::getAlphamapManager( )
{ return m_pAlphamapManager; }


inline float CSplatMap::getWidth( ) const
{ return BaseMap::getMapCellWidth( ); }

inline float CSplatMap::getLength( ) const
{ return BaseMap::getMapCellWidth( ); }


inline bool CSplatMap::getContainerAtZ( float z, CMapRowContainer *&container )
{ return BaseMap::getContainerAtZ( z, container ); }

inline CMapCell *CSplatMap::getMapCell( unsigned int x, unsigned int z )
{ return BaseMap::getMapCell( x, z ); }

inline float CSplatMap::getMapCellWidth( ) const
{ return BaseMap::getMapCellWidth( ); }

inline float CSplatMap::getMapCellLength( ) const
{ return BaseMap::getMapCellLength( ); }

inline unsigned int CSplatMap::getNumberOfXCells( ) const
{ return BaseMap::getNumberOfXCells( ); }

inline unsigned int CSplatMap::getNumberOfZCells( ) const
{ return BaseMap::getNumberOfZCells( ); }

inline unsigned int CSplatMap::getNumberOfXChunks( ) const
{ return m_uiNumberOfXChunks; }

inline unsigned int CSplatMap::getNumberOfZChunks( ) const
{ return m_uiNumberOfZChunks; }


} // end of namespace
#endif