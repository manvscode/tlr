#ifndef _MAP_H_
#define _MAP_H_
#include <lib.h>
#include <Game/Terrain/TerrainMesh.h>
#include <Game/Map/MapRowContainer.h>
#include <utility/collections/redblacktree.h>
#include <Game/Map/MapCellTexturing.h>
//#include "MapCell.h"



namespace Game {





class CMapCell;


class TEST
{
};

//template <class TerrainTexturingPolicy = CDefaultMapCellTexturingPolicy>
class TLR_API CMap
{
  public:
	typedef UTILITY::CRedBlackTree<CMapRowContainer> MapRowCollection;
	typedef CMapCell MapCell;

  private:
	typedef struct tagWindow {
		unsigned int xStart;
		unsigned int yStart;
		unsigned int xCells;
		unsigned int yCells;
	} Window;

  public:
	CMap( );
	CMap( Terrain::CTerrainMesh &mesh, unsigned int numberOfXCells = 0, unsigned int numberOfYCells = 0, bool scaleUVsForCell = true );
	virtual ~CMap( );

	void initialize( );
	void setTerrain( Terrain::CTerrainMesh &mesh, unsigned int numberOfXCells = 0, unsigned int numberOfYCells = 0, bool scaleUVsForCell = true );

	Terrain::CTerrainMesh &getTerrainMesh( );
	const Terrain::CTerrainMesh &getTerrainMesh( ) const;
	void render( );
	void cleanUp( );


	//bool isRenderBoundaryModeSet( );
	//void setRenderBoundaryMode( bool bRenderBounderies = true );
	void setMapName( std::string &name );
	void setMissionBriefing( std::string &brief );

	float getWidth( ) const;
	float getLength( ) const;

	float getMapCellWidth( ) const;
	float getMapCellLength( ) const;

	void setWindowDimensions( unsigned int width, unsigned int height );
	void setWindowPosition( unsigned int x, unsigned int y );
	void addWindowPosition( int dx, int dy );
	void incrementXWindowPosition( );
	void incrementYWindowPosition( );
	void decrementXWindowPosition( );
	void decrementYWindowPosition( );


	bool getContainerAtZ( float z, CMapRowContainer *&container );


	CMap::MapCell *getMapCell( unsigned int x, unsigned int z );
	unsigned int getNumberOfXCells( ) const;
	unsigned int getNumberOfZCells( ) const;


  private:
	void buildRowTree( );
	void scaleVertexUV( const unsigned int vertexXElement, const unsigned int vertexZElement, const bool bWholeMap );

	Terrain::CTerrainMesh m_TerrainMesh;
	MapCell  **m_Map;
	unsigned int m_NumberOfXCells;
	unsigned int m_NumberOfZCells;
	std::string m_Name;
	std::string m_MissionBriefing;

	//float m_fWidth;
	//float m_fLength;

	float m_fMapCellWidth;
	float m_fMapCellLength;
	Window m_Window;
	bool m_bMapMemoryAllocated;
	
	MapRowCollection m_RowTree;

	//TerrainTexturingPolicy m_TexturingPolicy;
};	



inline Terrain::CTerrainMesh &CMap::getTerrainMesh( )
{ return m_TerrainMesh; }


inline const Terrain::CTerrainMesh &CMap::getTerrainMesh( ) const
{ return m_TerrainMesh; }


inline float CMap::getWidth( ) const
{ return m_TerrainMesh.getWidth( ); }


inline float CMap::getLength( ) const
{ return m_TerrainMesh.getLength( ); }


inline float CMap::getMapCellWidth( ) const
{ return m_fMapCellWidth; }


inline float CMap::getMapCellLength( ) const
{ return m_fMapCellLength; }


inline unsigned int CMap::getNumberOfXCells( ) const
{ return m_NumberOfXCells; }


inline unsigned int CMap::getNumberOfZCells( ) const
{ return m_NumberOfZCells; }


} // end of namespace
//#include "Map.inl"
#endif