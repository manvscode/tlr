#ifdef WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <Game/Map/MapCell.h>
#include <Core.h>
#include <vector>
#include <cassert>
#include <math.h>

// light0
static GLfloat light_position[4] = { 20.0f, 20.0f, 20.0f, 0.0f }; //Must relate to grid, light point down at a slight angle
static GLfloat light_vector[4] = { 2.0f, -1.0f, 2.0f, 0.0f }; //Must relate to grid, light point down at a slight angle
static GLfloat ambient_light[4] = { 2.0f, 2.0f, 2.0f, 0.0f }; //not to bright white light




namespace Game {




CMapCell::CMapCell( unsigned int indexX, unsigned int indexZ )
 : CMapSelectable( ), m_TerrainIndices( ), /*m_BaseTextureID(0),*/ m_IndexX(indexX), m_IndexZ(indexZ)
{
	//for( unsigned int i = 0; i < 5; i++ )
	//	m_Splats[ i ].setLayer( i + 1 );
}


CMapCell::~CMapCell( )
{
}


void CMapCell::render( )
{
	//if( Core::selections.isSelectMode( ) )
	//	renderSelectable( );
	//else {

	//	

		// render terrain...
		renderTerrain( );

		// TO DO...

		// render ground objects...
		// render air objects...
		// render bounding box when testing...
	//}
}






void CMapCell::renderTerrain( )
{
	Terrain::CTerrainMesh &terrainVertices = Core::pMap->getTerrainMesh( );

	if(  m_TerrainIndices.size( ) > 0 )
	{
		// turn on the appropriate texture units...
		//setupTexturing( );


		glPushMatrix( );
			//glInterleavedArrays( GL_T2F_N3F_V3F, 0, &terrainVertices[0] );
			glDrawElements( /*GL_POINTS*/GL_TRIANGLE_STRIP, (GLsizei) m_TerrainIndices.size( ), GL_UNSIGNED_INT, &m_TerrainIndices[ 0 ] );
		glPopMatrix( );
	}
} 

void CMapCell::setupTexturing( )
{


}

void CMapCell::renderBounds( const float red, const float green, const float blue, const float alpha ) const
{
	const Terrain::CTerrainMesh &m = Core::pMap->getTerrainMesh( );
	glPushAttrib( GL_ENABLE_BIT | GL_HINT_BIT );
		glDisable( GL_LIGHTING );
		glDisable( GL_TEXTURE_2D );
		glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );

		glPushMatrix( );
			glTranslatef( m_Center.getX( ) + m_BoundingBox.getWidth( ) / 2.0f, m.getMaxHeight( ), m_Center.getZ( ) + m_BoundingBox.getLength( ) / 2.0f );
			glColor4f( red, green, blue, alpha );
			glBegin( GL_LINE_STRIP );
				glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
			glEnd( );
		glPopMatrix( );

		glPushMatrix( );
			glTranslatef( m_Center.getX( ) + m_BoundingBox.getWidth( ) / 2.0f, m.getMinHeight( ), m_Center.getZ( ) + m_BoundingBox.getLength( ) / 2.0f );
			glColor4f( red, green, blue, alpha );
			glBegin( GL_LINE_STRIP );
				glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
			glEnd( );
		glPopMatrix( );

		glHint( GL_LINE_SMOOTH_HINT, GL_FASTEST ); 
	glPopAttrib( );
}


void CMapCell::renderSelectable( )
{
	Terrain::CTerrainMesh &m = Core::pMap->getTerrainMesh( );

	glPushAttrib( GL_ENABLE_BIT /*| GL_LIGHTING_BIT */| GL_TEXTURE_BIT | GL_LINE_BIT | GL_CURRENT_BIT );
	glDisable( GL_TEXTURE_2D );
	glDisable( GL_LIGHTING );
	glPushName( m_ID );

	glPushMatrix( );
		glTranslatef( m_Center.getX( ) + m_BoundingBox.getWidth( ) / 2.0f, m.getMaxHeight( ), m_Center.getZ( ) + m_BoundingBox.getLength( ) / 2.0f );
		glColor3f( 1.0f, 0.0f, 0.0f );
		glBegin( GL_QUADS );
			glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
			glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
			glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
			glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
		glEnd( );
	glPopMatrix( );

	glPopName( );
	glPopAttrib( );
}



void CMapCell::highlight( )
{
	Terrain::CTerrainMesh &m = Core::pMap->getTerrainMesh( );

	glPushAttrib( GL_ENABLE_BIT | GL_LINE_BIT );
		glDisable( GL_LIGHTING );
		glDisable( GL_TEXTURE_2D );
		glEnable( GL_LINE_STIPPLE );

		glLineWidth( 4.0 );

		glLineStipple( 1, 0x00FF );
		//renderBounds( m, 1, 1, 0 );

		glPushMatrix( );
			glTranslatef( m_Center.getX( ) + m_BoundingBox.getWidth( ) / 2.0f, m.getMaxHeight( ), m_Center.getZ( ) + m_BoundingBox.getLength( ) / 2.0f );
			glColor3f( 1.0f, 1.0f, 0.0f );
			glBegin( GL_LINE_STRIP );
				glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
				glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
			glEnd( );
		glPopMatrix( );
	glPopAttrib( );
}


//void CMapCell::setTerrainTexture( unsigned int texture, unsigned int layer )
//{
//	if( layer == -1 ) // remove textureIDs for this map cell.
//	{
//		m_BaseTextureID = 0;
//		for( int i = 0; i < 6; i++ )
//			m_Splats[ i ].m_TextureID = 0;
//	}
//	else if( layer == 0 )
//		m_BaseTextureID = texture;
//	else {
//		assert( layer >= 0 || layer <= 5 );
//		m_Splats[ layer - 1 ].setTextureID( texture );
//		m_Splats[ layer - 1 ].generateAlphaMap( *this );
//
//		std::vector<CMapCell *> &list = this->getNeighborsAndMyself( );
//
//		for( unsigned int i = 0; i < list.size( ); i++ )
//		{
//			if( list[ i ]->m_IndexX == m_IndexX && list[ i ]->m_IndexZ == m_IndexZ )
//				continue;
//			else if( list[ i ]->m_Splats[ layer - 1 ].m_TextureID == m_Splats[ layer - 1 ].m_TextureID )
//			{
//				list[ i ]->m_Splats[ layer - 1 ].setTextureID( texture );
//				list[ i ]->m_Splats[ layer - 1 ].generateAlphaMap( *this );
//			}
//			
//		}
//	}
//}


void CMapCell::setMapIndexPositions( unsigned int x, unsigned int z )
{ m_IndexX = x; m_IndexZ = z; }


std::vector<CMapCell *> CMapCell::getNeighborsAndMyself( ) const
{ 
	unsigned int idx = 0;
	unsigned int numberOfXCells = Core::pMap->getNumberOfXCells( );
	unsigned int numberOfZCells = Core::pMap->getNumberOfZCells( );
	unsigned int maxZ = getIndexZ( ) + 1;
	unsigned int maxX = getIndexX( ) + 1;


	std::vector<CMapCell *> result( 9 );

	// the order of these for loops matters in the alphamap generation! Do not change!
	for( unsigned int j = getIndexZ( ) - 1; j <= maxZ; j++ )
		for( unsigned int i = getIndexX( ) - 1; i <= maxX; i++ )
		{
			if( i < 0 || i > numberOfXCells - 1 || j < 0 || j > numberOfZCells - 1 /*|| ( j == numberOfZCells && i == numberOfXCells )*/ )
				continue;
			result[ idx++ ] = Core::pMap->getMapCell( i, j );
		}

	return result;
}


void CMapCell::setTextureLayer( unsigned int layer, bool bOn )
{
	assert( layer >= 0 ); // layer 0 is always on
	assert( layer < MAX_TEXTURE_LAYERS );
	m_TextureLayerBits.set( layer, bOn );
}

const CMapCell &CMapCell::operator =( const CMapCell &right )
{
	if( this != &right )
	{
		// stuff from MapSelectable...
		m_Name = right.m_Name;
		m_ID = right.m_ID;
		m_Center = right.m_Center;	
		m_BoundingBox = right.m_BoundingBox;

		// CMapCell stuff...
		m_TerrainIndices = right.m_TerrainIndices;
		m_IndexX = right.m_IndexX; 
		m_IndexZ = right.m_IndexZ;
		m_TextureLayerBits.reset( );
		m_TextureLayerBits = right.m_TextureLayerBits;
	}

	return *this;
}


} // end of namespace
