#include <Game/Terrain/TerrainMesh.h>
#include <Game/Map/Map.h>
#include <Game/Map/MapCell.h>
#include <Game/Map/MapRowContainer.h>
//#include <Game/Map/MapSelectionManager.h>
#include <cassert>
#include <sstream>
#include <Core.h>
#include <utility/debug.h>

using namespace std;

namespace Game {

using namespace Terrain;



CMap::CMap( )
 : m_TerrainMesh(), m_Map(0), m_NumberOfXCells(0), m_NumberOfZCells(0), 
   m_Name(""), m_MissionBriefing(""), m_fMapCellWidth(), m_fMapCellLength(), 
   m_RowTree(), m_bMapMemoryAllocated(false)//, m_TexturingPolicy()
{
	m_Window.xStart = 0;
	m_Window.yStart = 0;
	m_Window.xCells = WINDOW_SIZE;
	m_Window.yCells = WINDOW_SIZE;
}



CMap::CMap( CTerrainMesh &mesh, unsigned int numberOfXCells, unsigned int numberOfYCells, bool scaleUVsForCell )
 : m_Map(0), m_NumberOfXCells(numberOfXCells), m_NumberOfZCells(numberOfYCells), 
   m_Name(""), m_MissionBriefing(""), m_fMapCellWidth(), m_fMapCellLength(), 
   m_RowTree(), m_bMapMemoryAllocated(false)//, m_TexturingPolicy()
{
	m_Window.xStart = 0;
	m_Window.yStart = 0;
	m_Window.xCells = WINDOW_SIZE;
	m_Window.yCells = WINDOW_SIZE;
	setTerrain( mesh, numberOfXCells, numberOfYCells, scaleUVsForCell );
}



CMap::~CMap( ) 
{ cleanUp( ); }



void CMap::initialize( )
{
}



void CMap::setTerrain( CTerrainMesh &mesh, unsigned int numberOfXCells, unsigned int numberOfYCells, bool scaleUVsForCell )
{
	cleanUp( );
	m_TerrainMesh = mesh;

	unsigned int uiElementWidth = m_TerrainMesh.getElementWidth( );
	unsigned int uiElementLength = m_TerrainMesh.getElementLength( );

	assert( uiElementWidth % numberOfXCells == 0 ); // Can't divide the mesh by this number
	assert( uiElementLength % numberOfYCells == 0 ); // Can't divide the mesh by this number

	unsigned int uiVerticesPerXCell = uiElementWidth / numberOfXCells;
	unsigned int uiVerticesPerYCell = uiElementLength / numberOfYCells;


	m_NumberOfXCells = ( numberOfXCells <= 0 ? 1 : numberOfXCells ); 
	m_NumberOfZCells = ( numberOfYCells <= 0 ? 1 : numberOfYCells );
	m_fMapCellWidth = getWidth( ) / m_NumberOfXCells;
	m_fMapCellLength = getLength( ) / m_NumberOfZCells;


	/* Allocate memory for the Map's cells */
	m_Map = new CMapCell*[ m_NumberOfXCells ];

	for( unsigned int idx = 0; idx < m_NumberOfXCells; idx++ )
		m_Map[ idx ] = new CMapCell[ m_NumberOfZCells ];

	m_bMapMemoryAllocated = true;


	
	float j = 0.0f;
	float i = 0.0f;

	// for each map cell block...

	for( unsigned int cellY = 0; cellY < m_NumberOfZCells; cellY++ )//for( float j = 0.0f; j < m_fLength; j += m_fMapCellLength )
	{
		//cellX = 0;
		i = 0.0f;
		for( unsigned int cellX = 0; cellX < m_NumberOfXCells; cellX++ )//for( float i = 0.0f; i < m_fWidth; i += m_fMapCellWidth )
		{
			//float z = j <= 0.0f ? j + m_TerrainMesh.getUnitLength( ) : j;
			
			// set bounding volume
			C3DPoint<float> center(i - (getWidth( ) / 2.0f), 0, j - (getLength( ) / 2.0f) );
			C3DBox<float> boundingBox( center, m_fMapCellWidth, 18, m_fMapCellLength );
			m_Map[ cellX ][ cellY ].setCenter( center );
			m_Map[ cellX ][ cellY ].setBoundingVolume( boundingBox );
			m_Map[ cellX ][ cellY ].setMapIndexPositions( cellX, cellY );

			// add this map cell to the map selectable's manager...
			ostringstream oss;
			oss << "Map cell (" << cellX << ", " << cellY << ") ";
			Core::selections.add( m_Map[ cellX ][ cellY ], oss.str() );


			unsigned int startYVertex = uiVerticesPerYCell * cellY;
			unsigned int startXVertex = uiVerticesPerXCell * cellX;

			// for each vertex in a map cell block...
			unsigned int yVert = j <= 0.0f ? startYVertex + 1 : startYVertex;
			unsigned int xVert = startXVertex;

			for( ; yVert <= startYVertex + uiVerticesPerYCell; yVert++ )
			{
				if( yVert >= m_TerrainMesh.getElementLength( ) ) break;

				CMapCell::TerrainIndexCollection &terrainIndices = m_Map[ cellX ][ cellY ].getTerrainIndices( );

				xVert = startXVertex;
				scaleVertexUV( xVert, yVert, !scaleUVsForCell );
				terrainIndices.push_back( m_TerrainMesh.getIndexForVertexAt( xVert, yVert ) );

				for( ; xVert <= startXVertex + uiVerticesPerXCell; xVert++ )
				{
					if( xVert >= m_TerrainMesh.getElementWidth( ) )
						break;
					
					
					scaleVertexUV( xVert, yVert - 1, !scaleUVsForCell );

					terrainIndices.push_back( m_TerrainMesh.getIndexForVertexAt( xVert, yVert ) );
					terrainIndices.push_back( m_TerrainMesh.getIndexForVertexAt( xVert, yVert - 1 ) );
				}
				scaleVertexUV( xVert - 1, yVert - 1, !scaleUVsForCell );
				terrainIndices.push_back( m_TerrainMesh.getIndexForVertexAt( xVert - 1, yVert - 1 ) );
			}		
			i += m_fMapCellWidth;
		}
		j += m_fMapCellLength;
	}

	// build a tree of the map's rows...
	buildRowTree( );
}

//void CMap::setTerrainTexturingPolicy( const IMapCellTexturingPolicy *pPolicy )
//{ m_pTexturingPolicy = pPolicy; }
//
//void CMap::resetTerrainTexturingPolicy( )
//{ m_pTexturingPolicy = &Game::defaultMapCellTexturingPolicy; }



void CMap::render( )
{
	for( unsigned int x = m_Window.xStart; x < m_NumberOfXCells && x < (m_Window.xStart + m_Window.xCells); x++ )
		for( unsigned int y = m_Window.yStart; y < m_NumberOfZCells && y < (m_Window.yStart + m_Window.yCells); y++ )
		{
			// apply texturing settings...
			//m_TexturingPolicy( m_Map[ x ][ y ] );

			// render this map cell...
			m_Map[ x ][ y ].render( );
		}
}



void CMap::cleanUp( )
{
	if( m_bMapMemoryAllocated )
	{
		for( unsigned int i = 0; i < m_NumberOfXCells; i++ )
			delete [] m_Map[ i ];
		delete [] m_Map;

		m_NumberOfXCells = 0;
		m_NumberOfZCells = 0;
		m_bMapMemoryAllocated = true;
	}
}



void CMap::setMapName( std::string &name )
{ m_Name = name; }



void CMap::setMissionBriefing( std::string &brief )
{ m_MissionBriefing = brief; }



void CMap::setWindowDimensions( unsigned int width, unsigned int height )
{
	m_Window.xCells = width;
	m_Window.yCells = height;
	if( m_Window.xCells >= m_NumberOfXCells ) m_Window.xCells = m_NumberOfXCells;
	if( m_Window.yCells >= m_NumberOfZCells ) m_Window.yCells = m_NumberOfZCells;
}



void CMap::setWindowPosition( unsigned int x, unsigned int y )
{
	m_Window.xStart = x;
	m_Window.yStart = y;
	if( m_Window.xStart > m_NumberOfXCells ) m_Window.xStart = m_NumberOfXCells - 1;
	if( m_Window.yStart > m_NumberOfZCells ) m_Window.yStart = m_NumberOfZCells - 1;
}



void CMap::addWindowPosition( int dx, int dy )
{
	int x = m_Window.xStart + dx;
	int y = m_Window.yStart + dy;
	int maxX = m_NumberOfXCells;
	int maxY = m_NumberOfZCells;

	if( (x + (int) m_Window.xCells) >= maxX )
		x = maxX - m_Window.xCells;
	else if( x < 0 )
		x = 0;

	if( (y + (int) m_Window.yCells) >= maxY )
		y = maxY - m_Window.yCells;
	else if( y < 0 )
		y = 0;

	//assert( x >= 0 && y >= 0 );
	m_Window.xStart = (unsigned int) x;
	m_Window.yStart = (unsigned int) y;
}



void CMap::incrementXWindowPosition( )
{ if( m_Window.xStart < m_NumberOfXCells ) m_Window.xStart++; }



void CMap::incrementYWindowPosition( )
{ if( m_Window.yStart < m_NumberOfZCells ) m_Window.yStart++; }



void CMap::decrementXWindowPosition( )
{ if( m_Window.xStart > 0 ) m_Window.xStart--; }



void CMap::decrementYWindowPosition( )
{ if( m_Window.yStart > 0 ) m_Window.yStart--; }



void CMap::buildRowTree( )
{
	
	for( unsigned int y = 0; y < m_NumberOfZCells; y++ )
	{
		C3DPoint<float> &pt = m_Map[ 0 ][ y ].getCenter( );
		CMapRowContainer container( static_cast<int>(  floor(pt.getZ( ) / getMapCellLength()) ) );
		CMapSelectable *sel = NULL;

		for( unsigned int x = 0; x < m_NumberOfXCells; x++ )
		{
			//sel = dynamic_cast<CMapSelectable *>( &m_Map[ x ][ y ] );
			sel = static_cast<CMapSelectable *>( &m_Map[ x ][ y ] );
			container.add( CMapRowContainer::MOT_MAPCELL, sel );
		}

		m_RowTree.insert( container );
	}
}



bool CMap::getContainerAtZ( float z, CMapRowContainer *&container )
{
	MapRowCollection::node_iterator itr = m_RowTree.nbegin( );
	int _z = static_cast<int>( floor(z / Core::pMap->getMapCellLength()));

	while( !itr.isDone( ) )
	{
		int containersZ = itr->getZIndex( );
		if( containersZ < _z )
			itr.traverseRight( );
		else if( containersZ > _z )
			itr.traverseLeft( );
		else if( containersZ == _z )
		{
			container = &( *itr );
			return true;
		}
	}

	return false;
}




CMapCell *CMap::getMapCell( unsigned int x, unsigned int z )
{ 
	ASSERT( m_Map != NULL );
	ASSERT( x >= 0 || x < m_NumberOfXCells );
	ASSERT( z >= 0 || z < m_NumberOfZCells );
	return &m_Map[ x ][ z ];
}



void CMap::scaleVertexUV( const unsigned int vertexXElement, const unsigned int vertexZElement, const bool bWholeMap )
{
	GameVertex &v = m_TerrainMesh.getVertexAt( vertexXElement, vertexZElement );


	// scale UVs for the entire terrain map...
	if( bWholeMap )
	{	
		// fix UV's
		v.u = ((float) vertexXElement / (float) m_TerrainMesh.getElementWidth( ));
		v.v = ((float) vertexZElement / (float) m_TerrainMesh.getElementLength( ));
	}
	else // scale UVs for just the map cell...
	{
		// fix UV's
		v.u = ((float) vertexXElement / (float) m_TerrainMesh.getElementWidth( )) * m_NumberOfXCells;
		v.v = ((float) vertexZElement / (float) m_TerrainMesh.getElementLength( )) * m_NumberOfZCells;
	}
}







} // end of namespace