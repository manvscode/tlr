#pragma once
#ifndef _MAPSELECTABLE_H_
#define _MAPSELECTABLE_H_

#include <lib.h>
#include <string>
#include <Math/3DBox.h>
using namespace Math;

namespace Game {

class TLR_API CMapSelectable
{
  protected:
	std::string m_Name;
	unsigned int m_ID;
	C3DPoint<float> m_Center;	
	C3DBox<float> m_BoundingBox;

  public:
	CMapSelectable( );
	CMapSelectable( const unsigned int id, const std::string &name, const C3DBox<float> &proxy );
	virtual ~CMapSelectable( );

	virtual void highlight( ) = 0;
	virtual void renderSelectable( );


	std::string getName( ) const;
	void setName( const std::string &name );

	unsigned int getID( ) const;
	void setID( const unsigned int id );
	
	C3DPoint<float> &CMapSelectable::getCenter( );
	const C3DPoint<float> &getCenter( ) const;
	void setCenter( const C3DPoint<float> &pt );

	C3DBox<float> &CMapSelectable::getBoundingVolume( );
	const C3DBox<float> &getBoundingVolume( ) const;
	void setBoundingVolume( const C3DBox<float> &box );

	float getWidth( ) const;
	float getLength( ) const;
	float getHeight( ) const;

	const CMapSelectable &operator =( const CMapSelectable &right );
};

inline std::string CMapSelectable::getName( ) const
{ return m_Name; }

inline unsigned int CMapSelectable::getID( ) const
{ return m_ID; }

inline C3DPoint<float> &CMapSelectable::getCenter( )
{ return m_Center; }

inline const C3DPoint<float> &CMapSelectable::getCenter( ) const
{ return m_Center; }

inline C3DBox<float> &CMapSelectable::getBoundingVolume( ) 
{ return m_BoundingBox; }

inline const C3DBox<float> &CMapSelectable::getBoundingVolume( ) const
{ return m_BoundingBox; }

inline float CMapSelectable::getWidth( ) const
{ return m_BoundingBox.getWidth( ); }

inline float CMapSelectable::getLength( ) const
{ return m_BoundingBox.getLength( ); }

inline float CMapSelectable::getHeight( ) const
{ return m_BoundingBox.getHeight( ); }


} // end of namespace
#endif