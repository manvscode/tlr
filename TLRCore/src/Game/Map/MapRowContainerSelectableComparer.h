#pragma once
#ifndef _MAPROWCONTAINERSELECTABLECOMPARER_H_
#define _MAPROWCONTAINERSELECTABLECOMPARER_H_

#include <lib.h>
#include <Game/Map/MapSelectable.h>

namespace Game {

/*
*	CMapRowContainerSelectableComparer
*
*	This is a compare function object to sort map selector pointers by x.
*/
class TLR_API CMapRowContainerSelectableComparer
{
  public:
	CMapRowContainerSelectableComparer( );
	~CMapRowContainerSelectableComparer( );

	int operator()( const CMapSelectable * &o1, const CMapSelectable * &o2 ) const;
	int operator()( CMapSelectable *o1, CMapSelectable * &o2 ) const;
	int operator()( int x1, int x2 ) const;

};


} // end of namespace
#endif