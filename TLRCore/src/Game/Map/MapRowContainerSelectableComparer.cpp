#include <Core.h>
#include "MapRowContainerSelectableComparer.h"


namespace Game {

/*
*	CMapRowContainerSelectableComparer
*
*	This is a compare function object to sort map selector pointers by x.
*/

CMapRowContainerSelectableComparer::CMapRowContainerSelectableComparer( )
{}

CMapRowContainerSelectableComparer::~CMapRowContainerSelectableComparer( )
{}

int CMapRowContainerSelectableComparer::operator()( const CMapSelectable * &o1, const CMapSelectable * &o2 ) const
{
	int x1 = static_cast<int>( floor( o1->getCenter( ).getX( ) / Core::pMap->getMapCellWidth() ) );
	int x2 = static_cast<int>( floor( o2->getCenter( ).getX( ) / Core::pMap->getMapCellWidth() ) );

	if( x1 < x2 )
		return -1;
	else if( x1 == x2 )
		return 0;
	else // x1 > x2
		return 1;
}

int CMapRowContainerSelectableComparer::operator()( CMapSelectable *o1, CMapSelectable * &o2 ) const
{
	int x1 = static_cast<int>( floor( o1->getCenter( ).getX( ) / Core::pMap->getMapCellWidth() ) );
	int x2 = static_cast<int>( floor( o2->getCenter( ).getX( ) / Core::pMap->getMapCellWidth() ) );

	if( x1 < x2 )
		return -1;
	else if( x1 == x2 )
		return 0;
	else // x1 > x2
		return 1;
}

int CMapRowContainerSelectableComparer::operator()( int x1, int x2 ) const
{
	if( x1 < x2 )
		return -1;
	else if( x1 == x2 )
		return 0;
	else // x1 > x2
		return 1;
}

} // end of namespace
