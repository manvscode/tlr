#pragma once
#ifndef _MAPROWCONTAINER_H_
#define _MAPROWCONTAINER_H_


//#include <Core.h>
#include <lib.h>
#include <cmath>
#include <Game/Map/MapRowContainerSelectableComparer.h>
#include <utility/collections/redblacktree.h>
using namespace Utility;

namespace Game {


class CMapSelectable;

/*
 *	CMapRowContainer
 *
 *	This is a container for all of a map's row objects with respect to z.
 */
class TLR_API CMapRowContainer
{
  public:

	typedef enum tagMapObjectType {
		MOT_MAPCELL,
		MOT_BUILDING,
		MOT_ENTITY
	} MapObjectType;

	explicit CMapRowContainer( );
	CMapRowContainer( const int zIndex );
	CMapRowContainer( const float z );
	virtual ~CMapRowContainer( );

	bool add( const MapObjectType type, CMapSelectable *&obj );
	bool remove( const MapObjectType type, CMapSelectable *&obj );

	CRedBlackTree<CMapSelectable *, CMapRowContainerSelectableComparer> &getObjects( const MapObjectType type = MOT_MAPCELL );
	CMapSelectable *getObjectAtX( const MapObjectType type, const float x );



	int getZIndex( ) const;
	bool operator<( const CMapRowContainer &other ) const;
	bool operator==( const CMapRowContainer &other ) const;
	const CMapRowContainer &operator=( const CMapRowContainer &other ) const; // Needed for copy-value semantics.

  protected:
	
	CMapSelectable *getMapCellAtX( const int &x );
	CMapSelectable *getBuildingAtX( const int &x );
	CMapSelectable *getEntityAtX( const int &x );

	int m_zIndex;
	CRedBlackTree<CMapSelectable *, CMapRowContainerSelectableComparer> m_MapCellCollection;
	CRedBlackTree<CMapSelectable *, CMapRowContainerSelectableComparer> m_BuildingCollection;
	CRedBlackTree<CMapSelectable *, CMapRowContainerSelectableComparer> m_EntityCollection;
};

inline int CMapRowContainer::getZIndex( ) const
{ return m_zIndex; }

inline bool CMapRowContainer::operator<( const CMapRowContainer &other ) const
{ return ( m_zIndex < other.m_zIndex ? true : false ); }

inline bool CMapRowContainer::operator==( const CMapRowContainer &other ) const
{ return m_zIndex == other.m_zIndex; }


} // end of namespace
#endif