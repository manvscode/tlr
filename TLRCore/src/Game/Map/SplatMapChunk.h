#pragma once
#ifndef _SPLATMAPCHUNK_H_
#define _SPLATMAPCHUNK_H_

#include <lib.h>
#include <vector>
#include <memory>
#include <Game/Map/SplatMap.h>

namespace Game {


static const unsigned int SPLAT_ALPHA_MAP_SIZE = 64;


class TLR_API CSplatMapChunk
{
	friend class CSplatMap;
  public:
	typedef std::vector<CMapCell *> MapCellCollection;
  protected:
	MapCellCollection m_MapCells;

	float distanceSquared( float x1, float y1, float x2, float y2 ) const;
	float weight( float x1, float y1, float x2, float y2 ) const;
	float noise( int x ) const;

	const CSplatMapChunk &operator =( const CSplatMapChunk &right );

	void beginAlphamapStretch( float texelS, float texelT );
	void endAlphamapStretch( );

  public:
	CSplatMapChunk( );
	virtual ~CSplatMapChunk( );

	
	void setMapCells( const MapCellCollection &mapCells );

	void render( unsigned int layer );
	void setupTextureSettings( );


};


inline float CSplatMapChunk::distanceSquared( float x1, float y1, float x2, float y2 ) const
{ return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2); }

inline float CSplatMapChunk::weight( float x1, float y1, float x2, float y2 ) const
{
	// This function depends on the upper and lower bounds you 
	// define for element space. I chose to have my bounds set
	// between -1.0 to 1.0 along both dimensions to optimizie
	// the division required by the weight function. IF YOU 
	// CHANGE YOUR BOUNDS, THIS WEIGHT FUNCTION MAY NOT WORK!!!
	// 
	// If you divide by a value * value where value is less then
	// your bound, you essentially pull back fading...
	float w = 1 - distanceSquared(x1, y1, x2, y2) / ( 0.75f * 0.75f );  /// (1.0f * 1.0f)*/;
	return w > 0 ? w : 0;
}



inline float CSplatMapChunk::noise( int x ) const
{
	x = (x<<13) ^ x;
	return ( 1.0f - ( (x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f); 
}

} // end of namespace
#endif