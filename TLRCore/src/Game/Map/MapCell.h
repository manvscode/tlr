#pragma once
#ifndef _MAPCELL_H_
#define _MAPCELL_H_
#include <Math/3DBox.h>
#include <Game/Map/MapCellTexturing.h>
#include <Game/Map/MapSelectable.h>
#include <Game/Map/Map.h>
#include <list>
#include <vector>
#include <bitset>
#include <cassert>


namespace Game {


class TLR_API CMapCell : public CMapSelectable
{

  public:
	typedef std::vector<unsigned int> TerrainIndexCollection;
	typedef std::bitset<MAX_TEXTURE_LAYERS> LayerBits;


	CMapCell( unsigned int indexX = 0, unsigned int indexZ = 0 );
	virtual ~CMapCell( );
	
	void render( );
	void renderBounds( const float red = 1.0f, const float green = 0.0f, const float blue = 0.0f, const float alpha = 0.0f ) const;
	void renderSelectable( );
	void highlight( );

	void setMapIndexPositions( unsigned int x, unsigned int z );
	unsigned int getIndexX( ) const;
	unsigned int getIndexZ( ) const;

	CMapCell::TerrainIndexCollection &getTerrainIndices( );
	const TerrainIndexCollection &getTerrainIndices( ) const;


	std::vector<CMapCell *> getNeighborsAndMyself( ) const;

	void setTextureLayer( unsigned int layer, bool bOn = true );
	bool isTextureLayerSet( unsigned int layer ) const;

	const CMapCell &operator =( const CMapCell &right );

  private:
	void renderTerrain( );
	void setupTexturing( );

  protected:
	TerrainIndexCollection m_TerrainIndices;
	unsigned int m_IndexX, m_IndexZ;
	LayerBits m_TextureLayerBits;
};


inline unsigned int CMapCell::getIndexX( ) const
{ return m_IndexX; }


inline unsigned int CMapCell::getIndexZ( ) const
{ return m_IndexZ; }

inline CMapCell::TerrainIndexCollection &CMapCell::getTerrainIndices( )
{ return m_TerrainIndices; }

inline const CMapCell::TerrainIndexCollection &CMapCell::getTerrainIndices( ) const
{ return m_TerrainIndices; }


inline bool CMapCell::isTextureLayerSet( unsigned int layer ) const
{ 
	assert( layer >= 0 ); 
	assert( layer < MAX_TEXTURE_LAYERS ); 
	return m_TextureLayerBits.test( layer ); 
}


} // end of namespace
#endif