#ifdef WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <Game/Map/MapCell.h>
#include <Game/Map/SplatMapChunk.h>
#include <Game/Map/AlphamapManager.h>
#include <Core.h>
#include <cassert>

using namespace std;

namespace Game {



CSplatMapChunk::CSplatMapChunk( )
  : m_MapCells( )
{
}

CSplatMapChunk::~CSplatMapChunk( )
{
}

void CSplatMapChunk::setMapCells( const CSplatMapChunk::MapCellCollection &mapCells )
{ m_MapCells = mapCells; }

void CSplatMapChunk::render( unsigned int layer )
{
	// build the tile bits...
	unsigned int tileBits = 0;
	for( unsigned int i = 0; i < NUMBER_OF_MAPCELLS_PER_CHUNK; i++ )
	{
		//bool bLayerSet = m_MapCells[ i ]->isTextureLayerSet( layer );

		if( m_MapCells[ i ]->isTextureLayerSet( layer + 1 ) )
			tileBits |= TILE_BIT_MASKS[ i ];
	}


	Terrain::CTerrainMesh &terrainMesh = Core::pMap->getTerrainMesh( );

	static float color[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	float texelS = 0.0f;
	float texelT = 0.0f;

	for( unsigned int i = 0; i < CHUNK_SIZE; i++ )
	{

		for( unsigned int j = 0; j < CHUNK_SIZE; j++ )
		{

			// alpha map
			glActiveTexture( GL_TEXTURE0 );
			glClientActiveTexture( GL_TEXTURE0 );
			glEnable( GL_TEXTURE_2D );
			beginAlphamapStretch( texelS, texelT );			
			glInterleavedArrays( GL_T2F_N3F_V3F, 0, &terrainMesh[0] );
			glBindTexture( GL_TEXTURE_2D, Core::pMap->getAlphamapManager( )->getAlphmapTextureId( /*16*/ tileBits ) /* alpha map */ );

			// layer 0
			glActiveTexture( GL_TEXTURE1 );
			glClientActiveTexture( GL_TEXTURE1 ); 
			unsigned int texture1 = Core::pMap->getTexture( layer );
			
			if( texture1 != 0 )
			{
				glEnable( GL_TEXTURE_2D );
				glInterleavedArrays( GL_T2F_N3F_V3F, 0, &terrainMesh[0] );
				glBindTexture( GL_TEXTURE_2D,  texture1 /* lower layer */ );
				glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE );
				glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_MODULATE );
				glTexEnvi( GL_TEXTURE_ENV, GL_SRC0_RGB, GL_PRIMARY_COLOR );
				glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR );	
				glTexEnvi( GL_TEXTURE_ENV, GL_SRC1_RGB, GL_TEXTURE );
				glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR );	
				glTexEnvfv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, color );
				glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_REPLACE );
				glTexEnvi( GL_TEXTURE_ENV, GL_SRC0_ALPHA, GL_CONSTANT );
				glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA );
				Core::checkGLError( );
			}
			else
			{
				glDisable( GL_TEXTURE_2D );
			}

			// layer 2
			glActiveTexture( GL_TEXTURE2 );
			glClientActiveTexture( GL_TEXTURE2 );
			unsigned int texture2 = Core::pMap->getTexture( layer + 1 );
			
			if( texture2 != 0 )
			{
				glEnable( GL_TEXTURE_2D );
				glInterleavedArrays( GL_T2F_N3F_V3F, 0, &terrainMesh[0] );
				glBindTexture( GL_TEXTURE_2D,  texture2 /* higher layer */ );
				glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE );
				glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_INTERPOLATE /*Arg0 � Arg2 + Arg1 � (1 - Arg2)*/ );
				 
				glTexEnvi( GL_TEXTURE_ENV, GL_SRC0_RGB, GL_TEXTURE /* higher layer */);
				glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR );

				glTexEnvi( GL_TEXTURE_ENV, GL_SRC1_RGB, GL_PREVIOUS /* lower layer */ );
				glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR );

				glTexEnvi( GL_TEXTURE_ENV, GL_SRC2_RGB, GL_TEXTURE0 );
				glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB, GL_SRC_ALPHA );

				glTexEnvfv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, color );
				glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_REPLACE );
				glTexEnvi( GL_TEXTURE_ENV, GL_SRC0_ALPHA, GL_CONSTANT );
				glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA ); // replace the alpha with 1.0f (see color above)
				Core::checkGLError( );

				//glEnable( GL_TEXTURE_2D );
				//glInterleavedArrays( GL_T2F_N3F_V3F, 0, &terrainMesh[0] );
				//glBindTexture( GL_TEXTURE_2D,  texture2 /* higher layer */ );
				//glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE );
				//glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_MODULATE );

				//glTexEnvi( GL_TEXTURE_ENV, GL_SRC0_RGB, GL_PRIMARY_COLOR );
				//glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR );	

				//glTexEnvi( GL_TEXTURE_ENV, GL_SRC1_RGB, GL_TEXTURE );
				//glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR );	

				//glTexEnvfv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, color );
				//glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_REPLACE );
				//glTexEnvi( GL_TEXTURE_ENV, GL_SRC0_ALPHA, GL_CONSTANT );
				//glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA );
				//Core::checkGLError( );

				// layer 1 & layer 2 interpolated
				//glActiveTexture( GL_TEXTURE3 );
				//glClientActiveTexture( GL_TEXTURE3 );
				//glEnable( GL_TEXTURE_2D );
				//glInterleavedArrays( GL_T2F_N3F_V3F, 0, &terrainMesh[0] );
				//glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE );
				//glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_INTERPOLATE /*Arg0 � Arg2 + Arg1 � (1 - Arg2)*/ );
				// 
				//glTexEnvi( GL_TEXTURE_ENV, GL_SRC0_RGB, GL_TEXTURE2 /* higher layer */);
				//glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR );

				//glTexEnvi( GL_TEXTURE_ENV, GL_SRC1_RGB, GL_TEXTURE1 /* lower layer */ );
				//glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR );

				//glTexEnvi( GL_TEXTURE_ENV, GL_SRC2_RGB, GL_TEXTURE0 );
				//glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB, GL_SRC_ALPHA );

				//glTexEnvfv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, color );
				//glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_REPLACE );
				//glTexEnvi( GL_TEXTURE_ENV, GL_SRC0_ALPHA, GL_CONSTANT );
				//glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA ); // replace the alpha with 1.0f (see color above)
				//Core::checkGLError( );
			}
			else
			{
				glDisable( GL_TEXTURE_2D );
				glActiveTexture( GL_TEXTURE3 );
				glClientActiveTexture( GL_TEXTURE3 );
				glDisable( GL_TEXTURE_2D );
			}



				glActiveTexture( GL_TEXTURE3 );
				glClientActiveTexture( GL_TEXTURE3 );
				glDisable( GL_TEXTURE_2D );

			// render the map cell...
			m_MapCells[ i * CHUNK_SIZE + j ]->render( );
			
			
			glActiveTexture( GL_TEXTURE0 );
			glClientActiveTexture( GL_TEXTURE0 );
			endAlphamapStretch( ); 
			
			texelS += 1.0f /*/ CHUNK_SIZE*/;
		}
		texelT += 1.0f /*/ CHUNK_SIZE*/;
	}




}

void CSplatMapChunk::beginAlphamapStretch( float texelS, float texelT )
{
	// now we stretch the alpha map over the chunk...
	glMatrixMode( GL_TEXTURE );
	glPushMatrix( );
	glLoadIdentity( );
	//glTranslatef( texelS, texelT, 0.0f );
	glScalef( 1.0f / 3.0f, 1.0f / 3.0f, 1.0f / 3.0f );
	glMatrixMode( GL_MODELVIEW );
}

void CSplatMapChunk::endAlphamapStretch( )
{
	// now we restore the texture transform so that they scale for a map cell.
	//glClientActiveTexture( GL_TEXTURE0 );
	glMatrixMode( GL_TEXTURE );
	glPopMatrix( );
	glMatrixMode( GL_MODELVIEW );
}

void CSplatMapChunk::setupTextureSettings( )
{


}

const CSplatMapChunk &CSplatMapChunk::operator=( const CSplatMapChunk &right )
{
	if( this != &right )
	{
		m_MapCells = right.m_MapCells;
	}

	return *this;
}






} // end of namespace
