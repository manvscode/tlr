#include <Game/Map/Map.h>
#include <Game/Map/SplatMap.h>
#include <Game/Map/SplatMapChunk.h>
#include <Game/Terrain/TerrainMesh.h>
#include <Game/Map/MapCell.h>
#include <Game/Map/AlphamapManager.h>
#include <vector>
#include <cassert>
#include <Core.h>

using namespace std;

namespace Game {



CSplatMap::CSplatMap( )
 : BaseMap( ), 
   m_uiNumberOfXChunks(0), 
   m_uiNumberOfZChunks(0), 
   m_Chunks(NULL), 
   m_bSplatMapMemoryAllocated(false),
   m_pAlphamapManager(NULL)
{
	m_pAlphamapManager = CAlphamapManager::getInstance( );
	initialize( );
}

CSplatMap::~CSplatMap( )
{
	cleanUp( );
	deinitialize( );

	if( m_pAlphamapManager != NULL )
		delete m_pAlphamapManager;
}

void CSplatMap::cleanUp( )
{
	if( m_bSplatMapMemoryAllocated )
	{
		for( unsigned int i = 0; i < m_uiNumberOfXChunks; i++ )
			delete [] m_Chunks[ i ];
		delete [] m_Chunks;

		m_uiNumberOfXChunks = 0;
		m_uiNumberOfZChunks = 0;
		m_bSplatMapMemoryAllocated = false;
	}
}


void CSplatMap::setTerrain( Terrain::CTerrainMesh &mesh, unsigned int numberOfXChunks, unsigned int numberOfZChunks )
{
	cleanUp( );
	unsigned int uiNumberOfXTiles = numberOfXChunks * CHUNK_SIZE;
	unsigned int uiNumberOfYTiles = numberOfZChunks * CHUNK_SIZE;

	assert( numberOfXChunks > 0 && numberOfZChunks > 0 ); // Must be at least 1!
	assert( mesh.getElementWidth( ) % uiNumberOfXTiles == 0 ); // mesh must be divisible into this number of tiles.
	assert( mesh.getElementLength( ) % uiNumberOfYTiles == 0 ); // mesh must be divisible into this number of tiles.



	// build map cells...
	BaseMap::setTerrain( mesh, numberOfXChunks * CHUNK_SIZE, numberOfZChunks * CHUNK_SIZE, true );


	m_Chunks = new Game::CSplatMapChunk*[ numberOfXChunks ];

	for( unsigned int idx = 0; idx < numberOfXChunks; idx++ )
		m_Chunks[ idx ] = new Game::CSplatMapChunk[ numberOfZChunks ];

	m_bSplatMapMemoryAllocated = true;

	unsigned int xCell = 1; // skip map cell at index 0
	unsigned int zCell = 1;

	// build chunks...
	for( unsigned int zChunk = 0; zChunk < numberOfZChunks; zChunk++ )
	{
		xCell = 1;
		for( unsigned int xChunk = 0; xChunk < numberOfXChunks; xChunk++ )
		{
			// Get map cell at (xCell, zCell)
			Game::CMapCell *pCenterCell = BaseMap::getMapCell( xCell, zCell );
			// get map cells surrounding map cell at (xCell, zCell)
			vector<Game::CMapCell *> &neighborsAndMyself = pCenterCell->getNeighborsAndMyself( );

			m_Chunks[ xChunk ][ zChunk ].setMapCells( neighborsAndMyself );
			xCell += 3;
		}
		zCell += 3;
	}

	m_uiNumberOfXChunks = numberOfXChunks;
	m_uiNumberOfZChunks = numberOfZChunks;
}


void CSplatMap::initialize( )
{
	GLint maxTextureUnits;
	glGetIntegerv( GL_MAX_TEXTURE_UNITS, &maxTextureUnits );

	memset( m_TextureLayerMappings, 0, sizeof(unsigned int) * Game::MAX_TEXTURE_LAYERS );

	Core::checkGLError( );

	// for now load all alphamaps into texture memory.
	// this can be optimized later to only load the alpha maps
	// that are needed for this particular map.
	for( unsigned int i = 0; i < MAX_POSSIBLE_ALPHMAPS; i++ )
		m_pAlphamapManager->loadAlphamap( i );

	Core::checkGLError( );
}

void CSplatMap::deinitialize( )
{
	for( unsigned int i = 0; i < MAX_POSSIBLE_ALPHMAPS; i++ )
		m_pAlphamapManager->unloadAlphamap( i );
}

void CSplatMap::render( )
{
	glPushAttrib( GL_ENABLE_BIT /*| GL_LIGHTING_BIT */| GL_TEXTURE_BIT | GL_CURRENT_BIT );
		
		setupTextureSettings( );


		for( unsigned int layer = 0; layer < 2/*Game::MAX_TEXTURE_LAYERS*/ - 1; layer++ )
		{
			for( unsigned int zChunk = 0; zChunk < m_uiNumberOfZChunks; zChunk++ )
			{
				for( unsigned int xChunk = 0; xChunk < m_uiNumberOfXChunks; xChunk++ )
					m_Chunks[ xChunk ][ zChunk ].render( layer );
			}

		}
	glPopAttrib( );
}


void CSplatMap::setupTextureSettings( )
{
	Terrain::CTerrainMesh &terrainMesh = getTerrainMesh( );

	if( terrainMesh.size( ) > 0 )
	{
		static float color[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	
	}
}



void CSplatMap::setMapName( std::string &name )
{ return BaseMap::setMapName( name ); }

void CSplatMap::setMissionBriefing( std::string &brief )
{ BaseMap::setMissionBriefing( brief ); }

void CSplatMap::setTexture( unsigned int layer, unsigned int texture )
{
	assert( layer < Game::MAX_TEXTURE_LAYERS );
	assert( layer >= 0 );
	m_TextureLayerMappings[ layer ] = texture;
}

} // end of namespace
