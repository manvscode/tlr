#include <cmath>
//#include <utility/debug.h>
#include <Core.h>
#include <cassert>
#include <Game/Map/MapSelectable.h>
#include <Game/Map/MapRowContainer.h>
using namespace Utility;

namespace Game {

CMapRowContainer::CMapRowContainer( )
: m_zIndex(0), m_MapCellCollection(), m_BuildingCollection(), m_EntityCollection()
{}

CMapRowContainer::CMapRowContainer( const int zIndex )
: m_zIndex(zIndex), m_MapCellCollection(), m_BuildingCollection(), m_EntityCollection()
{}

CMapRowContainer::CMapRowContainer( const float z )
: m_zIndex((int) floor(z)), m_MapCellCollection(), m_BuildingCollection(), m_EntityCollection()
{}

CMapRowContainer::~CMapRowContainer( )
{
}

bool CMapRowContainer::add( const MapObjectType type, CMapSelectable *&obj )
{
	assert( obj != NULL );

	switch( type )
	{
		case MOT_MAPCELL:
			m_MapCellCollection.insert( obj );
			break;
		case MOT_BUILDING:
			m_BuildingCollection.insert( obj );
			break;
		case MOT_ENTITY:
			m_EntityCollection.insert( obj );
			break;
		default:
			return false;
	}

	return true;
}

bool CMapRowContainer::remove( const MapObjectType type, CMapSelectable *&obj )
{
	assert( obj != NULL );

	switch( type )
	{
		case MOT_MAPCELL:
			return m_MapCellCollection.remove( obj );
		case MOT_BUILDING:
			return m_BuildingCollection.remove( obj );
		case MOT_ENTITY:
			return m_EntityCollection.remove( obj );
		default:
			return false;
	}
}

CRedBlackTree<CMapSelectable *, CMapRowContainerSelectableComparer> &CMapRowContainer::getObjects( const MapObjectType type )
{
	switch( type )
	{
		case MOT_MAPCELL:
			return m_MapCellCollection;
		case MOT_BUILDING:
			return m_BuildingCollection;
		case MOT_ENTITY:
			return m_EntityCollection;
		default:
			assert( false ); // bug
			break;
	}
}

CMapSelectable *CMapRowContainer::getObjectAtX( const MapObjectType type, const float x )
{
	int _x = static_cast<int>( floor( x / Core::pMap->getMapCellWidth( ) ) );

	switch( type )
	{
		case MOT_MAPCELL:
			return getMapCellAtX( _x );
		case MOT_BUILDING:
			return getBuildingAtX( _x );
		case MOT_ENTITY:
			return getEntityAtX( _x );
		default:
			return NULL;
	}
}

CMapSelectable *CMapRowContainer::getMapCellAtX( const int &x )
{
	CRedBlackTree<CMapSelectable *, CMapRowContainerSelectableComparer>::node_iterator itr = m_MapCellCollection.nbegin( );
	CMapRowContainerSelectableComparer cmp;

	while( !itr.isDone( ) )
	{
		const CMapSelectable *sel = const_cast<const CMapSelectable *>(*itr);
		int sel_x = static_cast<int>( floor( sel->getCenter( ).getX( ) / Core::pMap->getMapCellWidth( ) ) );

		if( cmp( sel_x, x ) < 0 )
			itr.traverseRight( );
		else if( cmp( sel_x, x ) > 0 )
			itr.traverseLeft( );
		else if( cmp( sel_x, x ) == 0 )
			return *itr;
	}

	return NULL;
}

CMapSelectable *CMapRowContainer::getBuildingAtX( const int &x )
{
	assert( false );
	//CRedBlackTree<CMapSelectable *, CMapRowContainerSelectableComparer>::node_iterator itr = m_BuildingCollection.nbegin( );
	//CMapRowContainerSelectableComparer cmp;

	//while( !itr.isDone( ) )
	//{
	//	const CMapSelectable *sel = const_cast<const CMapSelectable *>(*itr);

	//	if( cmp( sel, x ) < 0 )
	//		itr.traverseLeft( );
	//	else if( cmp( sel, x ) > 0 )
	//		itr.traverseRight( );
	//	if( cmp( sel, x ) == 0 )
	//		return *itr;
	//}

	return NULL;
}

CMapSelectable *CMapRowContainer::getEntityAtX( const int &x )
{
	assert( false );
	//CRedBlackTree<CMapSelectable *, CMapRowContainerSelectableComparer>::node_iterator itr = m_EntityCollection.nbegin( );
	//CMapRowContainerSelectableComparer cmp;

	//while( !itr.isDone( ) )
	//{
	//	const CMapSelectable *sel = const_cast<const CMapSelectable *>(*itr);

	//	if( cmp( sel, x ) < 0 )
	//		itr.traverseLeft( );
	//	else if( cmp( sel, x ) > 0 )
	//		itr.traverseRight( );
	//	if( cmp( sel, x ) == 0 )
	//		return *itr;
	//}

	return NULL;
}

const CMapRowContainer &CMapRowContainer::operator=( const CMapRowContainer &other ) const // Needed for copy-value semantics.
{
	assert( false );
	//if( this != &other )
	//{
	//	m_zIndex = other.m_zIndex;
	//	m_MapCellCollection = other.m_MapCellCollection;
	//	m_BuildingCollection = other.m_MapCellCollection;
	//	m_EntityCollection = other.m_MapCellCollection;
	//}

	return *this;
}


} // end of namespace