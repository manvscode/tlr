#pragma once
#ifndef _MAP_H_
#define _MAP_H_
#include <lib.h>
#include <Game/Terrain/TerrainMesh.h>
#include <utility/collections/redblacktree.h>
#include <Game/Map/MapCellTexturing.h>
#include <Game/Map/MapRowContainer.h>


namespace Game {


static const unsigned int WINDOW_SIZE = 8;
static const unsigned int MAX_TEXTURE_LAYERS = 6;	// this is the number of textures that can be used


class CMapCell;

using namespace Terrain;



template <class TerrainTexturingPolicy = CDefaultMapTexturingPolicy>
class CMap
{
  public:
	typedef Utility::CRedBlackTree<CMapRowContainer> MapRowCollection;

  protected:
	typedef struct tagWindow {
		unsigned int xStart;
		unsigned int yStart;
		unsigned int xCells;
		unsigned int yCells;
	} Window;

  public:
	CMap( );
	CMap( CTerrainMesh &mesh, unsigned int numberOfXCells = 0, unsigned int numberOfYCells = 0, bool scaleUVsForCell = true );
	virtual ~CMap( );

	void initialize( );
	void setTerrain( CTerrainMesh &mesh, unsigned int numberOfXCells = 0, unsigned int numberOfYCells = 0, bool scaleUVsForCell = true );

	CTerrainMesh &getTerrainMesh( );
	const CTerrainMesh &getTerrainMesh( ) const;
	void render( );
	void cleanUp( );


	//bool isRenderBoundaryModeSet( );
	//void setRenderBoundaryMode( bool bRenderBounderies = true );
	void setMapName( std::string &name );
	void setMissionBriefing( std::string &brief );

	float getWidth( ) const;
	float getLength( ) const;

	float getMapCellWidth( ) const;
	float getMapCellLength( ) const;

	void setWindowDimensions( unsigned int width, unsigned int height );
	void setWindowPosition( unsigned int x, unsigned int y );
	void addWindowPosition( int dx, int dy );
	void incrementXWindowPosition( );
	void incrementYWindowPosition( );
	void decrementXWindowPosition( );
	void decrementYWindowPosition( );


	bool getContainerAtZ( float z, CMapRowContainer *&container );


	CMapCell *getMapCell( unsigned int x, unsigned int z );
	unsigned int getNumberOfXCells( ) const;
	unsigned int getNumberOfZCells( ) const;


  protected:
	void buildRowTree( );
	void scaleVertexUV( const unsigned int vertexXElement, const unsigned int vertexZElement, const bool bWholeMap );

	CTerrainMesh m_TerrainMesh;
	CMapCell  **m_Map;
	unsigned int m_NumberOfXCells;
	unsigned int m_NumberOfZCells;
	std::string m_Name;
	std::string m_MissionBriefing;


	float m_fMapCellWidth;
	float m_fMapCellLength;
	Window m_Window;
	bool m_bMapMemoryAllocated;
	
	MapRowCollection m_RowTree;

	TerrainTexturingPolicy m_TexturingPolicy;
};	


template <class TerrainTexturingPolicy>
inline CTerrainMesh &CMap<TerrainTexturingPolicy>::getTerrainMesh( )
{ return m_TerrainMesh; }


template <class TerrainTexturingPolicy>
inline const CTerrainMesh &CMap<TerrainTexturingPolicy>::getTerrainMesh( ) const
{ return m_TerrainMesh; }


template <class TerrainTexturingPolicy>
inline float CMap<TerrainTexturingPolicy>::getWidth( ) const
{ return m_TerrainMesh.getWidth( ); }


template <class TerrainTexturingPolicy>
inline float CMap<TerrainTexturingPolicy>::getLength( ) const
{ return m_TerrainMesh.getLength( ); }


template <class TerrainTexturingPolicy>
inline float CMap<TerrainTexturingPolicy>::getMapCellWidth( ) const
{ return m_fMapCellWidth; }


template <class TerrainTexturingPolicy>
inline float CMap<TerrainTexturingPolicy>::getMapCellLength( ) const
{ return m_fMapCellLength; }


template <class TerrainTexturingPolicy>
inline unsigned int CMap<TerrainTexturingPolicy>::getNumberOfXCells( ) const
{ return m_NumberOfXCells; }


template <class TerrainTexturingPolicy>
inline unsigned int CMap<TerrainTexturingPolicy>::getNumberOfZCells( ) const
{ return m_NumberOfZCells; }


} // end of namespace

#include "Map.inl"
#endif