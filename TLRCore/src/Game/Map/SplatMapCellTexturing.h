#pragma once
#ifndef _SPLATMAPCELLTEXTURING_H_
#define _SPLATMAPCELLTEXTURING_H_


namespace Game {

class CMapCell;

class CSplatMapTexturingPolicy
{
  public:
	CSplatMapTexturingPolicy();
	void operator()( const CMapCell &mapCell ) const;
};

//inline void CSplatMapTexturingPolicy::operator()( const CMapCell &mapCell ) const
//{
//
//}

} //end of namespace
#endif