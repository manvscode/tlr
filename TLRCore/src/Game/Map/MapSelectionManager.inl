#ifdef WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <Game/Map/MapSelectionManager.h>
#include <Game/Map/MapCell.h>
#include <Core.h>

namespace Game {

template <unsigned int BUFFER_SIZE>
CMapSelectionManager<BUFFER_SIZE>::CMapSelectionManager( )
 : /*m_SelectionBuffer( BUFFER_SIZE ), */m_SelectableObjects(),
   m_bisSelectMode(false), m_bPickingOn(false), m_NumberOfHits(0), m_IDCount(0)
{
	for( int i = 0; i < BUFFER_SIZE; i++ )
		m_IDs[ i ] = i;
}

template <unsigned int BUFFER_SIZE>
CMapSelectionManager<BUFFER_SIZE>::~CMapSelectionManager( )
{
	//m_SelectionBuffer.clear( );
	m_SelectableObjects.clear( );
}

template <unsigned int BUFFER_SIZE>
void CMapSelectionManager<BUFFER_SIZE>::initialize( )
{ glSelectBuffer( BUFFER_SIZE, &m_SelectionBuffer[ 0 ] ); }

template <unsigned int BUFFER_SIZE>
unsigned int CMapSelectionManager<BUFFER_SIZE>::add( CMapSelectable &obj, const std::string &name )
{
	int id = m_IDs[ m_IDCount ];
	obj.setID( id );
	obj.setName( name );


	m_SelectableObjects[ id ] = &obj;

	m_IDCount++;
	return id;
}

template <unsigned int BUFFER_SIZE>
void CMapSelectionManager<BUFFER_SIZE>::remove( unsigned int id )
{
	std::map<unsigned int, CMapSelectable *>::iterator itr = m_SelectableObjects.find( id );
	
	if( itr != m_SelectableObjects.end( ) )
	{
		m_SelectableObjects.erase( itr );
		m_IDs[ m_IDCount-- ] = id;
	}
}


template <unsigned int BUFFER_SIZE>
void CMapSelectionManager<BUFFER_SIZE>::beginSelection( )
{
	setSelectMode( true );
	glRenderMode( GL_SELECT );
	glInitNames( );
}

template <unsigned int BUFFER_SIZE>
void CMapSelectionManager<BUFFER_SIZE>::endSelection( )
{
	glMatrixMode( GL_PROJECTION );
	glPopMatrix( );
	glMatrixMode( GL_MODELVIEW );
	glFlush( );
	m_NumberOfHits = glRenderMode( GL_RENDER );
	setSelectMode( false );
}

template <unsigned int BUFFER_SIZE>
template <class ProjectionTransform>
void CMapSelectionManager<BUFFER_SIZE>::beginPicking( unsigned int mouse_x, unsigned int mouse_y, unsigned int sensitivity, ProjectionTransform &v )
{
	GLuint viewport[ 4 ];
	glGetIntegerv( GL_VIEWPORT, viewport );
	beginSelection( );
	glMatrixMode( GL_PROJECTION );
	glPushMatrix( );
	glLoadIdentity( );
	gluPickMatrix( mouse_x, mouse_y, sensitivity, sensitivity, viewport );
	p( );
	glPopMatrix( );
	glMatrixMode( GL_MODELVIEW );
}

template <unsigned int BUFFER_SIZE>
void CMapSelectionManager<BUFFER_SIZE>::endPicking( )
{
	endSelection( );
}

template <unsigned int BUFFER_SIZE>
void CMapSelectionManager<BUFFER_SIZE>::processHits( )
{


}

template <unsigned int BUFFER_SIZE>
void CMapSelectionManager<BUFFER_SIZE>::clear( )
{
	glRenderMode( GL_RENDER );
	m_NumberOfHits = 0;
}

template <unsigned int BUFFER_SIZE>
CMapSelectable *CMapSelectionManager<BUFFER_SIZE>::getSelectable( unsigned int id )
{
	std::map<unsigned int, CMapSelectable *>::iterator itr = m_SelectableObjects.find( id );
	if( itr != m_SelectableObjects.end() )
	{
		//memset( &m_SelectionBuffer[ 0 ], 0, sizeof(unsigned int) * m_SelectionBuffer.size() );
		return itr->second;
	}
	else
		return NULL;
}



} // end of namespace
