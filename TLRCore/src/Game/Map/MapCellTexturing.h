#pragma once
#ifndef __MAPCELLTEXTURING_H__
#define __MAPCELLTEXTURING_H__


namespace Game {

	

class CMapCell;

class CDefaultMapTexturingPolicy
{
  public:
	CDefaultMapTexturingPolicy( );

	void operator()( const CMapCell &mapCell ) const;
};

inline void CDefaultMapTexturingPolicy::operator()( const CMapCell &mapCell ) const
{ /* Do nothing. Since this is inlined it will be optimized away. */ }

} // end of namespace
#endif