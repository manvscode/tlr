#pragma once
#ifndef _MAPSELECTIONMANAGER_H_
#define _MAPSELECTIONMANAGER_H_

#include <lib.h>
#include <map>
#include <vector>
#include <string>
#include <Game/Map/MapSelectable.h>



namespace Game {

class CMapSelectable;

template <unsigned int BUFFER_SIZE = 128>
class CMapSelectionManager
{
  public:
	CMapSelectionManager( );
	virtual ~CMapSelectionManager( );

	void initialize();
	unsigned int add( CMapSelectable &obj, const std::string &name = "Unknown object" );
	void remove( unsigned int id );

	void beginSelection( );
	void endSelection( );

	template <class ProjectionTransform>
	void beginPicking( unsigned int mouse_x, unsigned int mouse_y, unsigned int sensitivity, ProjectionTransform &p );
	void endPicking( );
	void processHits( );
	void clear( );

	void setSelectMode( bool m = true );
	bool isSelectMode( ) const;

	void setPicking( bool on = true );
	bool isPickingOn( ) const;

	int getNumberOfHits( ) const;

	unsigned int operator[]( int idx ) const;
	unsigned int &operator[]( int idx );

	CMapSelectable *getSelectable( unsigned int id );

  protected:
	//std::vector<unsigned int> m_SelectionBuffer;
	unsigned int m_SelectionBuffer[ BUFFER_SIZE ];
	//std::vector<SelectableObject> m_SelectableObjects;
	std::map<unsigned int, CMapSelectable *> m_SelectableObjects;

	bool m_bisSelectMode;
	bool m_bPickingOn;
	int m_NumberOfHits;
	unsigned int m_IDCount;

	unsigned int m_IDs[ BUFFER_SIZE ];
};

template <unsigned int BUFFER_SIZE>
inline void CMapSelectionManager<BUFFER_SIZE>::setSelectMode( bool m = true )
{ m_bisSelectMode = m; }

template <unsigned int BUFFER_SIZE>
inline bool CMapSelectionManager<BUFFER_SIZE>::isSelectMode( ) const
{ return m_bisSelectMode; }


template <unsigned int BUFFER_SIZE>
inline void CMapSelectionManager<BUFFER_SIZE>::setPicking( bool on )
{ m_bPickingOn = on; }

template <unsigned int BUFFER_SIZE>
inline bool CMapSelectionManager<BUFFER_SIZE>::isPickingOn( ) const
{ return m_bPickingOn; }


template <unsigned int BUFFER_SIZE>
inline unsigned int CMapSelectionManager<BUFFER_SIZE>::operator[]( int idx ) const
{ return m_SelectionBuffer[ idx ]; }

template <unsigned int BUFFER_SIZE>
inline unsigned int &CMapSelectionManager<BUFFER_SIZE>::operator[]( int idx )
{ return m_SelectionBuffer[ idx ]; }

template <unsigned int BUFFER_SIZE>
inline int CMapSelectionManager<BUFFER_SIZE>::getNumberOfHits( ) const
{ return m_NumberOfHits; }


} // end of namespace
#include "MapSelectionManager.inl"
#endif