#pragma once
#ifndef _ALPHAMAPMANAGER_H_
#define _ALPHAMAPMANAGER_H_
/*
 *	Alphamap manager for the splat terrain texturing
 *  technique.
 *
 *  Coded by Joseph A. Marrero
 */
#include <lib.h>
#include <vector>


namespace Game {
//
// The total possible alpha maps can be calculated using combinatorics
// as:
//
//      9C9 + 9C9 + 9C7 + ... + 9C2 + 9C1 + 9C0 = 512
//
// There are 511 possible transition alpha maps (all may not even 
// be used). At 64x64 @ GL_ALPHA8, an alpha map would require 4096 
// bytes. For all 512 alpha maps, we would need 512 * 4096 bytes
// = 2097152 bytes = 2 megs
//
// This shows how efficient it is on graphics hardware.
//
// Now, you want to have N number of textures on your terrain, then 
// you will need N - 1 splats (not counting the base texture). Thus,
// the total texture memory requirements are (N - 1) * (2 megs).
//
// So, if we want 6 textures, then we need 10 megs
//
static const unsigned int MAX_POSSIBLE_ALPHMAPS = 512;
static const unsigned int ALPHA_MAP_SIZE		= 32;
// 
// By convention, we assume...
//     ___________
//    | 0 | 1 | 2 |
//    |___|___|___|
//    | 3 | 4 | 5 |
//    |___|___|___|
//    | 6 | 7 | 8 |
//    |___|___|___|
// 
static const unsigned int TILE_BIT_MASKS[] = {
										0x001, // 0
										0x002, // 1
										0x004, // 2
										0x008, // 3
										0x010, // 4
										0x020, // 5
										0x040, // 6
										0x080, // 7
										0x100  // 8
									};
 
/*
 *	Alphamap manager for the splat terrain texturing
 *  technique. We map a bitmask to a particular tile 
 *  arrangement. This is how I know that a set of tiles
 *  are suppose to have a specific texture.
 */
class TLR_API CAlphamapManager
{
  public:
	typedef unsigned char byte;
	typedef std::vector< std::pair<byte *, unsigned int> > AlphamapCollection;

  protected:
	static CAlphamapManager *m_pInstance;
	AlphamapCollection m_Alphamaps;

	CAlphamapManager( );

	float distanceSquared( float x1, float y1, float x2, float y2 ) const;
	float weight( float x1, float y1, float x2, float y2 ) const;
	float noise( int x ) const;
	void buildAlphamaps( );
	void buildAlphamap( byte *&alphaMap, const unsigned int tileBits, const bool bUseNoise = true, const float alphaBias = 1.0f ) const;


  public:
	static CAlphamapManager *getInstance( );
	virtual ~CAlphamapManager( );

	unsigned int getAlphmapTextureId( unsigned int tileBits ) const;
	byte *getAlphmapBitmap( unsigned int tileBits );
	const byte *getAlphmapBitmap( unsigned int tileBits ) const;

	void loadAlphamap( unsigned int tileBits );
	void unloadAlphamap( unsigned int tileBits );


	// copying is not allowed...
	const CAlphamapManager &operator =( const CAlphamapManager &right );
};

//
// Inlined Members
//
inline float CAlphamapManager::distanceSquared( float x1, float y1, float x2, float y2 ) const
{ return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2); }

inline float CAlphamapManager::weight( float x1, float y1, float x2, float y2 ) const
{
	// This function depends on the upper and lower bounds you 
	// define for element space. I chose to have my bounds set
	// between -1.0 to 1.0 along both dimensions to optimizie
	// the division required by the weight function. IF YOU 
	// CHANGE YOUR BOUNDS, THIS WEIGHT FUNCTION MAY NOT WORK!!!
	// 
	// If you divide by a value * value where value is less then
	// your bound, you essentially pull back fading...
	float w = 1 - distanceSquared(x1, y1, x2, y2) / ( 0.75f * 0.75f );  /// (1.0f * 1.0f)*/;
	return w > 0 ? w : 0;
}

inline float CAlphamapManager::noise( int x ) const
{
	x = (x<<13) ^ x;
	return ( 1.0f - ( (x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f); 
}


inline unsigned int CAlphamapManager::getAlphmapTextureId( unsigned int tileBits ) const
{ return m_Alphamaps[ tileBits ].second; }

inline CAlphamapManager::byte *CAlphamapManager::getAlphmapBitmap( unsigned int tileBits )
{ return m_Alphamaps[ tileBits ].first; }

inline const CAlphamapManager::byte *CAlphamapManager::getAlphmapBitmap( unsigned int tileBits ) const
{ return m_Alphamaps[ tileBits ].first; }

} //end of namespace...
#endif