/*
 *	Alphamap manager for the splat terrain texturing
 *  technique.
 *
 *  Coded by Joseph A. Marrero
 */
#ifdef WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <Game/Map/AlphamapManager.h>
#include <cassert>
#include <Core.h>

namespace Game {
//
// Static members...
//
CAlphamapManager *CAlphamapManager::m_pInstance = NULL;

//
// Constructors / Deconstructors
//
CAlphamapManager::CAlphamapManager( )
 : m_Alphamaps( Game::MAX_POSSIBLE_ALPHMAPS )
{
	buildAlphamaps( );
}

CAlphamapManager::~CAlphamapManager( )
{
	// free the memory for all of the alpha maps...
	for( unsigned int i = 0; i < m_Alphamaps.size( ); i++ )
		delete [] m_Alphamaps[ i ].first;
}

CAlphamapManager *CAlphamapManager::getInstance( )
{
	if( !m_pInstance )
		m_pInstance = new CAlphamapManager( );
	
	return m_pInstance;
}

//
// Member Functions
//
void CAlphamapManager::buildAlphamaps( )
{
	for( unsigned int i = 0; i < MAX_POSSIBLE_ALPHMAPS; i++ )
	{
		byte *alphamap = NULL;
		buildAlphamap( alphamap, i, true, 1.0f ); 
		assert( alphamap != NULL );
		m_Alphamaps[ i ].first = alphamap;
		m_Alphamaps[ i ].second = 0; // no texture id
	}
}

void CAlphamapManager::buildAlphamap( byte *&alphaMap, 
									  const unsigned int tileBits, 
									  const bool bUseNoise, 
									  const float alphaBias ) const 
{
	// build alpha map...
	const unsigned int byteNum = 1; // this is only here for debugging
	assert( byteNum == 4 || byteNum == 1 );

	alphaMap = new byte[ ALPHA_MAP_SIZE * ALPHA_MAP_SIZE * byteNum * sizeof(byte) ];

	for( unsigned int texelY = 0; texelY < ALPHA_MAP_SIZE; texelY++ )
	{
		for( unsigned int texelX = 0; texelX < ALPHA_MAP_SIZE; texelX++ )
		{
			float totalContribution = 0.0f;
			float contribution = 0.0f;

			for( int y = 0; y < 3; y++ )
			{
				for( int x = 0; x < 3; x++ )
				{
					float texelXInElementSpace = ((float) 2.0f * 1.0f * texelX / (float) ALPHA_MAP_SIZE) - 1.0f;
					float texelYInElementSpace = ((float) 2.0f * 1.0f * texelY / (float) ALPHA_MAP_SIZE) - 1.0f;

					//cout << "(" << texelXInElementSpace << ", " << texelYInElementSpace << ")" << endl;

					assert( texelXInElementSpace >= -1.0f && texelXInElementSpace <= 1.0f );
					assert( texelYInElementSpace >= -1.0f && texelYInElementSpace <= 1.0f );


					float w = weight( x - 1.0f, y - 1.0f, texelXInElementSpace, texelYInElementSpace );
					float n = 1.0f;

					if( bUseNoise )
					{
						n = noise( rand( ) );
						n = n > 0 ? n : -n;
					}

					if( TILE_BIT_MASKS[ y * 3 + x ] & tileBits )
						contribution += w * n;
					
					
					totalContribution += w * n;
				}
			}
			

			float alphaLevel = (contribution / totalContribution) * alphaBias;
			if( alphaLevel > 1.0f ) alphaLevel = 1.0f;
			byte b = (byte) 255 * alphaLevel;
			alphaMap[ texelY * ALPHA_MAP_SIZE * byteNum + texelX * byteNum /* + k */ ] = b;
		}
	}


}


void CAlphamapManager::loadAlphamap( unsigned int tileBits )
{
	unsigned int textureId = 0;
	glGenTextures( 1, &textureId );
	assert( textureId != 0 );

	glBindTexture( GL_TEXTURE_2D, textureId );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT /*GL_CLAMP_TO_EDGE*/ /*GL_CLAMP_TO_BORDER*/ );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT /*GL_CLAMP_TO_EDGE*/ /*GL_CLAMP_TO_BORDER*/ );

	glTexImage2D( GL_TEXTURE_2D, 0, GL_ALPHA, ALPHA_MAP_SIZE, ALPHA_MAP_SIZE, 0, GL_ALPHA, GL_UNSIGNED_BYTE, m_Alphamaps[ tileBits ].first );

	Core::checkGLError( );
	m_Alphamaps[ tileBits ].second = textureId;
}

void CAlphamapManager::unloadAlphamap( unsigned int tileBits )
{ glDeleteTextures( 1, &m_Alphamaps[ tileBits ].second ); }

const CAlphamapManager &CAlphamapManager::operator =( const CAlphamapManager &right )
{
	if( this != &right )
	{
		assert( false ); //  copy semantics are not allowed...
	}

	return *this;
}



} //end of namespace...