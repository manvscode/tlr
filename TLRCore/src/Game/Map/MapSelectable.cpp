#ifdef WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <string>
#include <Math/3DBox.h>
#include "MapSelectable.h"
#include <Core.h>
#include <Game/Terrain/TerrainMesh.h>

#ifdef _DEBUG
#include <sstream>
#include <utility/debug.h>

#endif


namespace Game {

CMapSelectable::CMapSelectable( )
	: m_Name(""), m_ID(0), m_BoundingBox()
{}

CMapSelectable::CMapSelectable( const unsigned int id, const std::string &name, const C3DBox<float> &box )
	: m_Name(name), m_ID(id), m_BoundingBox(box)
{}

CMapSelectable::~CMapSelectable( )
{
#ifdef _DEBUG
	std::ostringstream oss;
	oss << "Destroying " << m_Name << std::endl;
	Utility::DEBUG_TRACE( oss.str( ).c_str( ) );
#endif
}

void CMapSelectable::renderSelectable( )
{
	//Terrain::CTerrainMesh &m = Core::pMap->getTerrainMesh( );

	glPushName( m_ID );

	glPushMatrix( );
		glTranslatef( m_Center.getX( ), m_Center.getY( ) + m_BoundingBox.getHeight( ) / 2.0f, m_Center.getZ( ) );
		glColor3f( 1.0f, 0.0f, 0.0f );
		glBegin( GL_QUADS );
			glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
			glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
			glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
			glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
		glEnd( );
	glPopMatrix( );

	glPushMatrix( );
		glTranslatef( m_Center.getX( ), m_Center.getY( ) - m_BoundingBox.getHeight( ) / 2.0f, m_Center.getZ( ) );
		glColor3f( 1.0f, 0.0f, 0.0f );
		glBegin( GL_QUADS );
			glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
			glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, -m_BoundingBox.getLength( ) / 2.0f );
			glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
			glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, 0, m_BoundingBox.getLength( ) / 2.0f );
		glEnd( );
	glPopMatrix( );

	//glPushMatrix( );
	//	glTranslatef( m_Center.getX( ), m_Center.getY( ), m_Center.getZ( ) + m_BoundingBox.getLength( ) / 2.0f );
	//	glColor3f( 0.0f, 1.0f, 0.0f );
	//	glBegin( GL_QUADS );
	//		glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, -m_BoundingBox.getHeight( ) / 2.0f, 0 );
	//		glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, -m_BoundingBox.getHeight( ) / 2.0f, 0 );
	//		glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, m_BoundingBox.getHeight( ) / 2.0f, 0 );
	//		glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, m_BoundingBox.getHeight( ) / 2.0f, 0 );
	//	glEnd( );
	//glPopMatrix( );

	//glPushMatrix( );
	//	glTranslatef( m_Center.getX( ), m_Center.getY( ), m_Center.getZ( ) - m_BoundingBox.getLength( ) / 2.0f );
	//	glColor3f( 0.0f, 1.0f, 0.0f );
	//	glBegin( GL_QUADS );
	//		glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, -m_BoundingBox.getHeight( ) / 2.0f, 0 );
	//		glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, -m_BoundingBox.getHeight( ) / 2.0f, 0 );
	//		glVertex3f( m_BoundingBox.getWidth( ) / 2.0f, m_BoundingBox.getHeight( ) / 2.0f, 0 );
	//		glVertex3f( -m_BoundingBox.getWidth( ) / 2.0f, m_BoundingBox.getHeight( ) / 2.0f, 0 );
	//	glEnd( );
	//glPushMatrix( );

	//glPushMatrix( );
	//	glTranslatef( m_Center.getX( ) + m_BoundingBox.getWidth( ) / 2.0f, m_Center.getY( ), m_Center.getZ( ) );
	//	glColor3f( 0.0f, 1.0f, 0.0f );
	//	glBegin( GL_QUADS );
	//		glVertex3f( 0, -m_BoundingBox.getHeight( ) / 2.0f, -m_BoundingBox.getLength( ) / 2.0f );
	//		glVertex3f( 0, -m_BoundingBox.getHeight( ) / 2.0f, -m_BoundingBox.getLength( ) / 2.0f );
	//		glVertex3f( 0, m_BoundingBox.getHeight( ) / 2.0f, m_BoundingBox.getLength( ) / 2.0f );
	//		glVertex3f( 0, m_BoundingBox.getHeight( ) / 2.0f, m_BoundingBox.getLength( ) / 2.0f );
	//	glEnd( );
	//glPopMatrix( );

	//glPushMatrix( );
	//	glTranslatef( m_Center.getX( ) - m_BoundingBox.getWidth( ) / 2.0f, m_Center.getY( ), m_Center.getZ( ) );
	//	glColor3f( 0.0f, 1.0f, 0.0f );
	//	glBegin( GL_QUADS );
	//		glVertex3f( 0, -m_BoundingBox.getHeight( ) / 2.0f, -m_BoundingBox.getLength( ) / 2.0f );
	//		glVertex3f( 0, -m_BoundingBox.getHeight( ) / 2.0f, -m_BoundingBox.getLength( ) / 2.0f );
	//		glVertex3f( 0, m_BoundingBox.getHeight( ) / 2.0f, m_BoundingBox.getLength( ) / 2.0f );
	//		glVertex3f( 0, m_BoundingBox.getHeight( ) / 2.0f, m_BoundingBox.getLength( ) / 2.0f );
	//	glEnd( );
	//glPopMatrix( );

	glPopName( );

}

void CMapSelectable::setName( const std::string &name )
{ m_Name = name; }

void CMapSelectable::setID( const unsigned int id )
{ m_ID = id; }

void CMapSelectable::setCenter( const C3DPoint<float> &pt )
{ m_Center = pt; }

void CMapSelectable::setBoundingVolume( const C3DBox<float> &box )
{ m_BoundingBox = box; }


const CMapSelectable &CMapSelectable::operator =( const CMapSelectable &right )
{
	if( this != &right )
	{
		m_Name = right.m_Name;
		m_ID = right.m_ID;
		m_Center = right.m_Center;	
		m_BoundingBox = right.m_BoundingBox;
	}

	return *this;
}



} // end of namespace
