#include <UI/UIManager.h>
#include <UI/UI.h>
#include <list>
#include <cassert>
#include <cmath>
#include <GL/glew.h>
#include <GL/gl.h>
#include <utility/debug.h>
using namespace std;

namespace UI {

CUIManager *CUIManager::m_pInstance = 0;


CUIManager::CUIManager( )
 : m_StateMachine( ), m_bShowConsole(false), m_MainScreen(0), m_InFocus(0), m_WindowList( ), m_WindowsToDestroy( )
{
	UI::setup_fade_out( );

	/* Register to listen for all events... */
	keyPressEventNotifier.attach( *this );
	timerEventNotifier.attach( *this );
	mouseEventNotifier.attach( *this );
}

CUIManager *CUIManager::getInstance( )
{
	if( m_pInstance == 0 )
		m_pInstance = new CUIManager( );

	return m_pInstance;
}

CUIManager::~CUIManager( )
{
	keyPressEventNotifier.detach( *this );
	timerEventNotifier.detach( *this );
	mouseEventNotifier.detach( *this );
}




void CUIManager::registerWindow( const IUIWindow &window )
{
	m_WindowList.push_back( const_cast<IUIWindow *>(&window) );
}

void CUIManager::unregisterWindow( const IUIWindow &window )
{
	m_WindowsToDestroy.push_back(  const_cast<IUIWindow *>(&window) );
}

void CUIManager::setCurrentUI( const UIStates ui )
{
	m_StateMachine.setState( ui );
}

void CUIManager::render( )
{
	switch( m_StateMachine.getState( ) )
	{
		case UI::ST_INTRO:
			UI::mainMenu->render( );
			UI::mouse.render( );
			UI::fade_out( 0.01f );

			//if( UI::alpha > 1.0f || UI::alpha < 0.0f )
				m_StateMachine.setState( UI::ST_MAINMENU );
			
			break;
		case UI::ST_MAINMENU:		
			UI::mainMenu->render( );
			if( m_bShowConsole )
				UI::consoleWindow->render( );
			UI::mouse.render( );
			UI::fade_out( 0.01f );
			break;
		case UI::ST_INGAME:
			// render game stuff...
			// then render HUD...
			break;
		default:
			//assert( false ); // should not reach here ever.
			break;
	}
	

	
	assert( glGetError( ) == GL_NO_ERROR );
}

void CUIManager::update( CUIEvent &e )
{
	// destroy any unregistered windows from previous update...
	if( m_WindowsToDestroy.size( ) > 0 )
	{
		
		WindowCollection::iterator itr1;
		WindowCollection::iterator itr2 = m_WindowsToDestroy.begin( );

		for( itr1 = m_WindowList.begin( ); itr1 != m_WindowList.end( ); ++itr1 )
			if( *itr1 == *itr2 )
			{
				m_WindowList.erase( itr1++ );
				itr2++;
				return;
			}

	}

	if( e.getType( ) == UI::EVT_MOUSEBUTTON )
	{
		int *x = (int *) &e[ 3 ];
		int *y = (int *)  &e[ 3 + sizeof(int) ];

		assert( x != NULL && y != NULL );
		
		if( e[ 0 ] )
		{
			WindowCollection::iterator itr;
			IUIWindow *infocus = NULL;

			for( itr = m_WindowList.begin( ); itr != m_WindowList.end( ); ++itr )
			{
				IUIWindow *win = (*itr);
				int minX = win->getXPosition( );
				int maxX = minX + win->width( );
				int maxY = win->getYPosition( ) + TITLEBAR_HEIGHT;
				int minY = maxY - win->height( ) - TITLEBAR_HEIGHT - BOTTOM_BORDOR_HEIGHT;

				if( minX <= *x && *x <= maxX && minY <= *y && *y <= maxY && m_bShowConsole )
				{
					infocus = win;
					break;
				}
			}

			if( infocus != NULL )
				m_InFocus = infocus;

		}
	}

	if( m_InFocus != NULL )
		m_InFocus->update( e );
	else
		m_MainScreen->update( e );

	if( e.getType( ) == UI::EVT_TIMER )
		m_MainScreen->update( e );


}

void CUIManager::showConsole( bool yes )
{
	m_bShowConsole = yes;
}

void CUIManager::toggleConsole( )
{
	m_bShowConsole = !m_bShowConsole;

	if( m_bShowConsole )
		m_InFocus = UI::consoleWindow;
	else 
		m_InFocus = NULL;
}

void CUIManager::setMainScreen( IUIScreen &screen )
{
	m_MainScreen = &screen;
}

} //end of namespace