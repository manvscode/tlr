#include "UI.h"
#include "UIScreen.h"

namespace UI {

IUIScreen::IUIScreen( )
{
	//UI::keyPressEventNotifier.attach( *this );
	//UI::timerEventNotifier.attach( *this );
}

IUIScreen::~IUIScreen( )
{
}

void IUIScreen::update( CUIEvent &e )
{
	switch( e.getType( ) )
	{
		case UI::EVT_KEYDOWN:
			onKeyDown( e[ 0 ] );
			break;
		case UI::EVT_KEYUP:
			onKeyUp( e[ 0 ] );
			break;
		case UI::EVT_TIMER:
			onTimer( );
			break;
		case UI::EVT_MOUSEBUTTON:
		{
			int x = 0;
			int y = 0;
			memcpy( &x, &e[ 3 ], sizeof(int) );
			memcpy( &y, &e[ 3 + sizeof(int) ], sizeof(int) );
			onMouseButton( e[ 0 ], e[ 1 ], e[ 2 ], x, y );
			break;
		}
		case UI::EVT_MOUSEMOVE:
		{
			int x = 0;
			int y = 0;
			memcpy( &x, &e[ 0 ], sizeof(int) );
			memcpy( &y, &e[ 0 + sizeof(int) ], sizeof(int) );
			onMouseMove( x, y );
			break;
		}
		default:
			break;
	}
}

/*
 *	Default Event Handlers
 */

void IUIScreen::onKeyDown( int key )
{
}

void IUIScreen::onKeyUp( int key )
{
}

void IUIScreen::onMouseButton( bool left, bool middle, bool right, int x, int y )
{
}

void IUIScreen::onMouseMove( int x, int y )
{
}

void IUIScreen::onTimer( )
{
}


} // end of namespace 