#pragma once
#ifndef _CONSOLEWINDOW_H_
#define _CONSOLEWINDOW_H_
#include <lib.h>
#include <deque>

#include <UI/UIWindow.h>
#include <UI/legacy/TextRasterizer.h>


#define CONSOLE_MAX_LINES		500

namespace UI {

class TLR_API CConsoleWindow : public IUIWindow
{
  public:
	CConsoleWindow( );
	virtual ~CConsoleWindow( );

	virtual void render( );

	virtual void onKeyDown( OS::LogicalKey key );
	virtual void onKeyUp( OS::LogicalKey key );
	virtual void onMouseButton( bool left, bool middle, bool right, int x, int y );
	virtual void onMouseMove( int x, int y );
	virtual void onTimer( );
	void closeWindow( );
	void restoreWindow( );
	void minimizeWindow( );
	void maximizeWindow( );
	
	void processCommand( const std::string &cmd );
	void prunBuffer( );

  protected:
	std::deque<std::string>  m_ConsoleBuffer;
	std::string m_InputLine;
#ifdef WIN32
	TextRasterizer m_TextRasterizer;
#endif
	unsigned int m_NumberOfDisplayableRows;
	unsigned int m_NumberOfDisplayableColumns;
	float input_alpha;

};


} // end of namespace
#endif