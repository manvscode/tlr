

template <class KeyDownEventHandler,
		  class TimerEventHandler>
CUIGenericUI<KeyDownEventHandler, TimerEventHandler>::CUIGenericUI( )
{}

template <class KeyDownEventHandler,
		  class TimerEventHandler>
CUIGenericUI<KeyDownEventHandler, TimerEventHandler>::~CUIGenericUI( )
{}

template <class KeyDownEventHandler,
		  class TimerEventHandler>
void CUIGenericUI<KeyDownEventHandler, TimerEventHandler>::update( CUIEvent &e )
{
	// handle all events by passing to each specialized handler...
	switch( e.getType( ) )
	{
		case UI::EVT_KEYDOWN:
			m_keyEventHandler.handle( e );
			break;
		case UI::EVT_KEYUP:
			
			break;
		case UI::EVT_TIMER:
			m_timerEventHandler.handle( e );
			break;
		default:
			break;
	} 
}

