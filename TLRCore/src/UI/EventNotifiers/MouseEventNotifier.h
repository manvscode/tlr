#pragma once
#ifndef _MOUSEEVENTNOTIFIER_H_
#define _MOUSEEVENTNOTIFIER_H_
#include <lib.h>
#include <UI/UISubject.h>

namespace UI {

class TLR_API CMouseEventNotifier : public CUISubject
{
  public:
	CMouseEventNotifier( );
	virtual ~CMouseEventNotifier( );


	void setButtonState( bool leftButton, bool middleButton, bool rightButton, int x, int y );
	bool isLeftButtonDown( ) const;
	bool isMiddleButtonDown( ) const;
	bool isRightButtonDown( ) const;

	void setMousePosition( int x, int y );
	int getMouseXPosition( ) const;
	int getMouseYPosition( ) const;

  protected:
	bool m_bLeftButtonDown;
	bool m_bMiddleButtonDown;
	bool m_bRightButtonDown;

	/* Mouse Position */
	int m_X;
	int m_Y;
};

inline bool CMouseEventNotifier::isLeftButtonDown( ) const
{ return m_bLeftButtonDown; }

inline bool CMouseEventNotifier::isMiddleButtonDown( ) const
{ return m_bMiddleButtonDown; }

inline bool CMouseEventNotifier::isRightButtonDown( ) const
{ return m_bRightButtonDown; }

inline int CMouseEventNotifier::getMouseXPosition( ) const
{ return m_X; }

inline int CMouseEventNotifier::getMouseYPosition( ) const
{ return m_Y; }

} // end of namespace
#endif