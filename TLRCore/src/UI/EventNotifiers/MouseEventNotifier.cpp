#include "MouseEventNotifier.h"
#include "../UI.h"

namespace UI {

CMouseEventNotifier::CMouseEventNotifier( )
 : m_bLeftButtonDown(false), m_bMiddleButtonDown(false), m_bRightButtonDown(false), m_X(0), m_Y(0)
{
}

CMouseEventNotifier::~CMouseEventNotifier( )
{
}

void CMouseEventNotifier::setButtonState( bool leftButton, bool middleButton, bool rightButton, int x, int y )
{
	CUIEvent e( UI::EVT_MOUSEBUTTON );
	// package the payload...
	e[ 0 ] = leftButton;
	e[ 1 ] = middleButton;
	e[ 2 ] = rightButton;
	memcpy( &e[ 3 ], &m_X, sizeof(int) );
	memcpy( &e[ 3 + sizeof(int) ], &m_Y, sizeof(int) );

	m_bLeftButtonDown = leftButton;
	m_bMiddleButtonDown = middleButton;
	m_bRightButtonDown = rightButton;
	m_X = x;
	m_Y = UI::windowHeight - y;

	notify( e );
}

void CMouseEventNotifier::setMousePosition( int x, int y )
{
	CUIEvent e( UI::EVT_MOUSEMOVE );
	m_X = x;
	m_Y = UI::windowHeight - y;
	
	memcpy( &e[ 0 ], &m_X, sizeof(int) );
	memcpy( &e[ 0 + sizeof(int) ], &m_Y, sizeof(int) );

	notify( e );
}

} // end of namespace