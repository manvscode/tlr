#include <cmath>
#include "TimerEventNotifier.h"
#include "../UIEVent.h"

namespace UI {

CTimerEventNotifier::CTimerEventNotifier( int timerInterval )
:  m_TimerInterval(timerInterval/1000.0f), m_Timer()
{
	m_Timer.resetTimer( );
}

CTimerEventNotifier::~CTimerEventNotifier( )
{
}


void CTimerEventNotifier::alert( )
{
	CUIEvent e( UI::EVT_TIMER );
	notify( e );
}

bool CTimerEventNotifier::isExpired( )
{
	if( abs( m_Timer.getTime( ) - m_TimerInterval) > 0.08225f )
	{
		m_Timer.resetTimer( );
		alert( );
		return true;
	}

	return false;
}

} // end of namespace