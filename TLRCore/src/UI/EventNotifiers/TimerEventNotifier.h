#pragma once
#include <lib.h>
#include <UI/UISubject.h>
#include <Timer/Timer.h>

namespace UI {

class TLR_API CTimerEventNotifier : public CUISubject
{
  public:
	CTimerEventNotifier( int timerInterval );
	virtual ~CTimerEventNotifier( );

	bool isExpired( );
	
  protected:
	float m_TimerInterval;
	CTimer m_Timer;
	void alert( );
	
};


} // end of namespace