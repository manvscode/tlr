#include "KeyPressEventNotifier.h"

namespace UI {

CKeyPressEventNotifier::CKeyPressEventNotifier( )
: isDirty(false)
{
	for( register int i = 0; i < 256; i++ )
		keys[ i ] = false;
}

CKeyPressEventNotifier::~CKeyPressEventNotifier( )
{}

void CKeyPressEventNotifier::setKeyState( unsigned int key, bool isDown )
{
	CUIEvent e( ( isDown == true ? UI::EVT_KEYDOWN : UI::EVT_KEYUP ) );

	/* set event's payload */
	e[ 0 ] = key;
	e[ 1 ] = isDown;

	keys[ key ] = isDown;
	notify( e );
}

bool CKeyPressEventNotifier::getKeyState( unsigned int key )
{
	return keys[ key ];
}



} // end of namespace