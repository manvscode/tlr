#pragma once
#include <UI/UISubject.h>
#include <lib.h>

namespace UI {

class TLR_API CKeyPressEventNotifier : public CUISubject
{
  public:
	CKeyPressEventNotifier( );
	virtual ~CKeyPressEventNotifier( );

	void setKeyState( unsigned int key, bool isDown = true );
	bool getKeyState( unsigned int key );

  protected:	
	bool isDirty;
	bool keys[ 256 ];
};


} // end of namespace