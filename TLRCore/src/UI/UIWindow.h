#pragma once
#ifndef _UIWINDOW_H_
#define _UIWINDOW_H_
#include <lib.h>
#include <OS/OSKeys.h>
#include <UI/UIScreen.h>

namespace UI {
#define WINDOW_SKIN_WIDTH		256	
#define WINDOW_SKIN_HEIGHT		64
#define TITLEBAR_HEIGHT			25
#define BORDER_WIDTH			8
#define BOTTOM_BORDOR_HEIGHT	9
#define BUTTON_AREA_WIDTH		50
#define BUTTON_WIDTH			(BUTTON_AREA_WIDTH / 3.0f )

class TLR_API IUIWindow : public IUIScreen
{
  public:
	typedef enum tagWindowState {
		WST_MINIMIZED,
		WST_MAXIMIZED,
		WST_NORMAL
	} WindowState;
	  
	IUIWindow( );
	virtual ~IUIWindow( );

	void initialize( );
	virtual void render( );

	virtual void update( CUIEvent &e );

	/* Events */
	virtual void onKeyDown( OS::LogicalKey key ) = 0;
	virtual void onKeyUp( OS::LogicalKey key ) = 0;
	virtual void onMouseButton( bool left, bool middle, bool right, int x, int y ) = 0;
	virtual void onMouseMove( int x, int y ) = 0;
	virtual void onTimer( ) = 0;
	/* End of Events */

	void setWindowSkin( unsigned int skin );
	void moveWindow( float x, float y );
	void setDimensions( int w, int h );

	virtual void minimizeWindow( );
	virtual void maximizeWindow( );
	virtual void normalizeWindow( );
	virtual void restoreWindow( );
	virtual void closeWindow( );


	bool isMinimized( ) const;
	bool isNormalized( ) const;
	bool isMaximized( ) const;
	unsigned int getXPosition( ) const;
	unsigned int getYPosition( ) const;
	unsigned int width( ) const;
	unsigned int height( ) const;

  private:
	void handleMouseClickInButtonArea( float x, float y );
	void handleMouseClickInTitleBar( float x, float y );
	bool testTitleBar( float x, float y );

  protected:
	unsigned int base;
	unsigned int m_WindowTexture;
	float m_X, m_Y;
	unsigned int m_Width, m_Height;
	unsigned int m_RestoreWidth, m_RestoreHeight;
	unsigned int m_RestoreX, m_RestoreY;
	WindowState m_State;
	unsigned int m_ZOrder;
	unsigned int stX, stY; // start (X,Y) when left mouse btn is first pressed
	bool m_bLeftMouseButtonDown;


};


inline bool IUIWindow::isMinimized( ) const
{ return m_State == WST_MINIMIZED; }

inline bool IUIWindow::isNormalized( ) const
{ return m_State == WST_NORMAL; }

inline bool IUIWindow::isMaximized( ) const
{ return m_State == WST_MAXIMIZED; }

inline unsigned int IUIWindow::getXPosition( ) const
{ return static_cast<unsigned int>(m_X); }

inline unsigned int IUIWindow::getYPosition( ) const
{ return static_cast<unsigned int>(m_Y); }

inline unsigned int IUIWindow::width( ) const
{ return m_Width; }

inline unsigned int IUIWindow::height( ) const
{ return m_Height; }

} // end of namespace

#endif