/*	TextRasterizer.h
 *
 *	Writes Text to a GL context.
 *	Coded by Joseph A. Marrero
 *
 */
#pragma once
#include <lib.h>
#include <windows.h>
#include <GL/glew.h>
#include <GL/gl.h>

class TLR_API TextRasterizer
{
private:
	unsigned int base;
	HDC m_HDC;
	HFONT hFont;
	unsigned int FontSize;
	
public:
	TextRasterizer(HDC hDC);
	~TextRasterizer(void);

	BOOL CreateBitmapFont(char *fontName, int fontSize);
	void PrintString(char *String);
	void PrintString(float x, float y, char *String);
	void ClearFont();
};
