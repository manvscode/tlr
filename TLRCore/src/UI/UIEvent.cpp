#include <cassert>
#include <UI/UIEvent.h>

namespace UI {

CUIEvent::CUIEvent( )
  : m_Type(EVT_NONE), m_DataSize(0)
{
}

CUIEvent::CUIEvent( EventType et )
  : m_Type(et), m_DataSize(0)
{
}

CUIEvent::~CUIEvent( )
{
}

unsigned char &CUIEvent::operator[]( unsigned int i )
{
	assert( i >= 0 );
	assert( i < UI_EVENT_PAYLOAD_SIZE );
	return m_Buffer[ i ];
}

const unsigned char &CUIEvent::operator[]( unsigned int i ) const
{
	assert( i >= 0 );
	assert( i < UI_EVENT_PAYLOAD_SIZE );
	return m_Buffer[ i ];
}

} // end of namespace