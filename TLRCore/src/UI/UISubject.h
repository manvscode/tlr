#pragma once
#include <lib.h>
#include <map>
#include <set>
#include <UI/UIObserver.h>
#include <UI/UIEvent.h>


namespace UI {

class TLR_API CUISubject
{
	typedef std::set<CUIObserver *, std::less<CUIObserver *>, std::allocator<CUIObserver *> > ObserverCollection;

  public:
	CUISubject( );
	virtual ~CUISubject( );
	
	void attach( CUIObserver &o );
	void detach( const CUIObserver &o );

	virtual void notify( CUIEvent &e );

  protected:
	 ObserverCollection m_Observers;
};

} // end of namespace