#include <windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <cassert>
#include <utility/debug.h>
#include "UIMouse.h"
#include "UI.h"

namespace UI {

CUIMouse::CUIMouse( )
: base(0), m_X(0), m_Y(0)
{
	
	mouseEventNotifier.attach( *this );
	//initialize( );
}

CUIMouse::~CUIMouse( )
{
	glDeleteLists( base, 1 );
}

void CUIMouse::render( )
{
	glPushMatrix( );
		//glLoadIdentity( );
		glBindTexture( GL_TEXTURE_2D, texture );
		glTranslatef( (GLfloat) m_X, (GLfloat) m_Y - 24, 2.0f );
		glCallList( base );
	glPopMatrix( );
}

void CUIMouse::setPosition( int x, int y )
{
	m_X = x;
	m_Y = y;
}

void CUIMouse::setMouseCursor( unsigned int cursor )
{
	texture = cursor;
}

void CUIMouse::update( CUIEvent &e )
{
	if( e.getType( ) == UI::EVT_MOUSEMOVE )
	{
		memcpy( &m_X, &e[ 0 ], sizeof(int) );
		memcpy( &m_Y, &e[ 0 + sizeof(int) ], sizeof(int) );

		GLdouble modelMatrix[ 16 ];
		glGetDoublev( GL_MODELVIEW_MATRIX, modelMatrix );
		
		GLdouble projMatrix[ 16 ];
		glGetDoublev( GL_PROJECTION_MATRIX, projMatrix );

		int viewport[ 4 ];
		glGetIntegerv( GL_VIEWPORT, viewport );
		
		double x, y, z;
		gluUnProject( m_X, m_Y,	0, modelMatrix, projMatrix, viewport, &x, &y, &z );
		m_X = static_cast<unsigned int>( x );
		m_Y = static_cast<unsigned int>( y );
		

		assert( glGetError( ) == GL_NO_ERROR );

	}
}

void CUIMouse::initialize( )
{
	base = glGenLists( 1 );
	
	
	glNewList( base, GL_COMPILE );
		glBegin( GL_QUADS );
			glTexCoord2i( 0, 0 ); glVertex2i( 0, 0 );
			glTexCoord2i( 1, 0 ); glVertex2i( 24, 0 );
			glTexCoord2i( 1, 1 ); glVertex2i( 24, 24 );
			glTexCoord2i( 0, 1 ); glVertex2i( 0, 24 );
		glEnd();
	glEndList();

	assert( glGetError( ) == GL_NO_ERROR );

}

} // end of namespace