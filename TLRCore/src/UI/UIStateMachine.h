#pragma once
#ifndef _UISTATEMACHINE_H_
#define _UISTATEMACHINE_H_

#include <lib.h>



namespace UI {


template <class StateEnum>
class CUIStateMachine
{
  public:
	CUIStateMachine( )
		: m_CurrentState()
	{}

	~CUIStateMachine( ){}
	
	void setState( const StateEnum &newState );
	StateEnum getState( ) const;

  protected:
	StateEnum m_CurrentState;
};

template <class StateEnum>
inline StateEnum CUIStateMachine<StateEnum>::getState( ) const
{ return m_CurrentState; }

template <class StateEnum>
inline void CUIStateMachine<StateEnum>::setState( const StateEnum &newState )
{
	m_CurrentState = newState;
}


} // end of namespace
#endif // _UISTATEMACHINE_H_