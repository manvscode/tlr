#pragma once
#ifndef _UIOBSERVER_H_
#define _UIOBSERVER_H_
#include <lib.h>
#include <UI/UIEvent.h>

namespace UI {

class TLR_API CUIObserver
{
  public:
	CUIObserver( ){}
	virtual ~CUIObserver( ){}

	virtual void update( CUIEvent &e ) = 0;
};


} // end of namespace
#endif