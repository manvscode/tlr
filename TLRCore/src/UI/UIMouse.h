#pragma once
#ifndef _UIMOUSE_H_
#define _UIMOUSE_H_
#include <lib.h>
#include "UIObserver.h"

namespace UI {

class TLR_API CUIMouse : public CUIObserver
{
  public:
	CUIMouse( );
	~CUIMouse( );

	void render( );
	void setPosition( int x, int y );
	void setMouseCursor( unsigned int cursor );
	void update( CUIEvent &e );
	void initialize( );

	unsigned int getX( ) const;
	unsigned int getY( ) const;

  protected:

	unsigned int base;
	unsigned int m_X, m_Y;
	unsigned int texture;
};

inline unsigned int CUIMouse::getX( ) const
{ return m_X; }

inline unsigned int CUIMouse::getY( ) const
{ return m_Y; }

} // end of namespace

#endif