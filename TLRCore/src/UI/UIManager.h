#pragma once
#ifndef _UIMANAGER_H_
#define _UIMANAGER_H_
#include <lib.h>
#include <UI/UIObserver.h>
#include <UI/UIStateMachine.h>
#include <UI/UIScreen.h>
#include <UI/UIWindow.h>
#include <list>


namespace UI {


TLR_API typedef enum tagUIStates {
	ST_INTRO,
	ST_MAINMENU,
	ST_CONSOLE, 
	ST_OPTIONS,
	ST_INGAME
} UIStates;

class TLR_API CUIManager : public CUIObserver
{
	typedef std::list<IUIWindow *, std::allocator<IUIWindow *> > WindowCollection;

  public:
	static CUIManager *getInstance( );
	~CUIManager( );

	void registerWindow( const IUIWindow &window );
	void unregisterWindow( const IUIWindow &window );
	
	void setCurrentUI( const UIStates ui );
	void render( );
	void update( CUIEvent &e ); // enter events into the gui...
	//void notify( CUIEvent &e ); // notify all windows & screens...
	void showConsole( bool yes = true );
	void toggleConsole( );

	void setMainScreen( IUIScreen &screen );
	
  protected:
	CUIManager( );
	static CUIManager *m_pInstance;
	CUIStateMachine<UIStates> m_StateMachine;
	
	bool m_bShowConsole;
	IUIScreen *m_MainScreen;
	IUIWindow *m_InFocus;
	WindowCollection m_WindowList;
	WindowCollection m_WindowsToDestroy;
};

} //end of namespace
#endif // _UIMANAGER_H_