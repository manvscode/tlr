#pragma once
#ifndef _UIEVENTHANDLER_H_
#define _UIEVENTHANDLER_H_
#include "UIEvent.h"

namespace UI {


class IUIEventHandler
{
  public:

	IUIEventHandler( )
	{}
	virtual ~IUIEventHandler( )
	{}

	virtual bool handle( CUIEvent &e ) = 0;
};



} // end of namespace

#endif