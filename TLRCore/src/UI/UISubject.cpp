#include "UISubject.h"
using namespace std;

namespace UI {

CUISubject::CUISubject( )
  : m_Observers()
{

}

CUISubject::~CUISubject( )
{
}


void CUISubject::attach( CUIObserver &o )
{
	m_Observers.insert( &o );
}

void CUISubject::detach( const CUIObserver &o )
{
	ObserverCollection::iterator itr;

	for( itr = m_Observers.begin( ); itr != m_Observers.end( ); ++itr )
		if( *itr == &o )
		{
			m_Observers.erase( itr );
			break;
		}
}

void CUISubject::notify( CUIEvent &e )
{
	ObserverCollection::iterator itr;

	for( itr = m_Observers.begin( ); itr != m_Observers.end( ); ++itr )
		(*itr)->update( e );
}


} // end of namespace