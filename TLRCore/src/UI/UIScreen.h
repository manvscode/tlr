#pragma once
#ifndef _UISCREEN_H_
#define _UISCREEN_H_
#include <lib.h>
#include "UIObserver.h"
#include "UISubject.h"

namespace UI {

class TLR_API IUIScreen : public CUIObserver, public CUISubject
{
  public:
	IUIScreen( );
	virtual ~IUIScreen( );
	
	virtual void render( ) = 0;

	virtual void update( CUIEvent &e );
	virtual void onKeyDown( int key );
	virtual void onKeyUp( int key );
	virtual void onMouseButton( bool left, bool middle, bool right, int x, int y );
	virtual void onMouseMove( int x, int y );
	virtual void onTimer( );
};


} // end of namespace
#endif