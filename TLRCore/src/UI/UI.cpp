#include <UI/UI.h>
#include <Core.h>
#include <OS/OSWindow.h>
#include <ResourceManagement/TextureManager/TextureManager.h>
#include <utility/debug.h>




namespace UI {

	/* UI Globals */
	CUIManager *pManager = NULL;
	CKeyPressEventNotifier keyPressEventNotifier;
	CMouseEventNotifier mouseEventNotifier;
	CTimerEventNotifier timerEventNotifier( 100 );
	CUIMainMenu *mainMenu = NULL;
	CConsoleWindow *consoleWindow = NULL;
	CUIMouse mouse;
	int textureIds[ 15 ];
	unsigned int windowWidth;
	unsigned int windowHeight;


	void initialize( )
	{
		assert( glGetError( ) == GL_NO_ERROR );
		
		textureIds[ 0 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/MainMenuBackground.tga" );
		textureIds[ 1 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/pda.tga" );
		textureIds[ 2 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/start.tga" );
		textureIds[ 3 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/options.tga" );
		textureIds[ 4 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/exit.tga" );
		textureIds[ 5 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/start_sel.tga" );
		textureIds[ 6 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/options_sel.tga" );
		textureIds[ 7 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/exit_sel.tga" );
		textureIds[ 8 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/pda_lights01.tga" );
		textureIds[ 9 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/pda_lights02.tga" );
		textureIds[ 10 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/pda_lights03.tga" );
		textureIds[ 11 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/bkg_hand.tga" );
		textureIds[ 12 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/finger.tga" );
		textureIds[ 13 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/mouse.tga" );
		textureIds[ 14 ] = Core::pTextureManager->addTexture( ResourceManagement::TX_TARGA, "assets/consolewindow.tga" );

		pManager = CUIManager::getInstance( );
		assert( glGetError( ) == GL_NO_ERROR );
		Core::pTextureManager->setupTexture( textureIds[ 12 ] );
		Core::pTextureManager->setupTexture( textureIds[ 13 ] );
		Core::pTextureManager->setupTexture( textureIds[ 14 ] );

		mouse.initialize( );
		mouse.setMouseCursor( textureIds[ 13 ] );

		mainMenu = new CUIMainMenu( );
		pManager->setMainScreen( *mainMenu );

		consoleWindow = new CConsoleWindow( );
		consoleWindow->initialize( );
		consoleWindow->setWindowSkin( textureIds[ 14 ] );

		for( unsigned int i = 0; i < 14; i++ )
			if( textureIds[ i ] < 0 )
			{
				MessageBox( NULL, "Some textures for the user interface could not be found or there was an error.", "UI Error", MB_OK | MB_ICONERROR );
				PostQuitMessage( -1 );
			}


		
	}

	void deinitialize( )
	{
		delete pManager;
		delete consoleWindow;
		delete mainMenu;
	}


	//void intro( )
	//{
	//}
	float alpha = 1.0f;
	
	void setup_fade_out( )
	{ alpha = 1.0f; }
	
	void setup_fade_in( )
	{ alpha = 0.0f; }

	void fade_in( float factor )
	{

		if( alpha > 1.0f )
			return;
			
		glDisable( GL_LIGHTING );
		glDisable( GL_TEXTURE_2D );
		glMatrixMode( GL_PROJECTION );
		glPushMatrix( );
		glOrtho( -1.0, 1.0, -1.0, 1.0, -5.0, 5.0 );
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix( );
		glPushAttrib( GL_CURRENT_BIT );
		glLoadIdentity( );
		


		glScalef( 3.0f, 3.0f, 3.0f );
		glColor4f( 0.0f, 0.0f, 0.0f, alpha );
		glBegin( GL_QUADS );
			glVertex3i( 0, 0, 1 );
			glVertex3i( 500, 0, 1 );
			glVertex3i( 500, 500, 1 );
			glVertex3i( 0, 500, 1 );
		glEnd( );
		Core::pWindow->swapBuffers( );

		alpha += factor;

		glPopAttrib( );
		glPopMatrix( );
		glMatrixMode( GL_PROJECTION );
		glPopMatrix( );
		glMatrixMode( GL_MODELVIEW );
		glEnable( GL_TEXTURE_2D );
		glEnable( GL_LIGHTING );
	}

	void fade_out( float factor )
	{

		if( alpha < 0.0f )
			return;

		glDisable( GL_LIGHTING );
		glDisable( GL_TEXTURE_2D );
		glMatrixMode( GL_PROJECTION );
		glPushMatrix( );
		glOrtho( -1.0, 1.0, -1.0, 1.0, -5.0, 5.0 );
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix( );
		glPushAttrib( GL_CURRENT_BIT );
		glLoadIdentity( );
		
		
		glScalef( 3.0f, 3.0f, 3.0f );
		glColor4f( 0.0f, 0.0f, 0.0f, alpha );
		glBegin( GL_QUADS );
			glVertex3i( 0, 0, 1 );
			glVertex3i( 500, 0, 1 );
			glVertex3i( 500, 500, 1 );
			glVertex3i( 0, 500, 1 );
		glEnd( );
		Core::pWindow->swapBuffers( );

		alpha -= factor;

		glPopAttrib( );
		glPopMatrix( );
		glMatrixMode( GL_PROJECTION );
		glPopMatrix( );
		glMatrixMode( GL_MODELVIEW );
		glEnable( GL_TEXTURE_2D );
		glEnable( GL_LIGHTING );
	}
	
	void fade_transition( float factor )
	{
		if( alpha > 1.0f )
			return;
	
		glDisable( GL_LIGHTING );
		glDisable( GL_TEXTURE_2D );
		glMatrixMode( GL_PROJECTION );
		glPushMatrix( );
		glOrtho( -1.0, 1.0, -1.0, 1.0, -5.0, 5.0 );
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix( );
		glPushAttrib( GL_CURRENT_BIT );
		glLoadIdentity( );
		
		
		glScalef( 3.0f, 3.0f, 3.0f );
		glColor4f( 0.0f, 0.0f, 0.0f, alpha );
		glBegin( GL_QUADS );
			glVertex3i( 0, 0, 1 );
			glVertex3i( 500, 0, 1 );
			glVertex3i( 500, 500, 1 );
			glVertex3i( 0, 500, 1 );
		glEnd( );
		Core::pWindow->swapBuffers( );

		alpha = abs( alpha - factor );

		glPopAttrib( );
		glPopMatrix( );
		glMatrixMode( GL_PROJECTION );
		glPopMatrix( );
		glMatrixMode( GL_MODELVIEW );
		glEnable( GL_TEXTURE_2D );
		glEnable( GL_LIGHTING );

	}


} // end of namespace