#pragma once
#ifndef _UI_H_
#define _UI_H_

#include "UIManager.h"
#include "EventNotifiers.h"
#include "UIMainMenu.h"
#include "ConsoleWindow.h"
#include "UIMouse.h"

namespace UI {

	TLR_API void initialize( );
	TLR_API void deinitialize( );
	
	TLR_API void setup_fade_in( );
	TLR_API void setup_fade_out( );
	TLR_API void fade_in( float factor = 0.020f );
	TLR_API void fade_out( float factor = 0.020f );
	TLR_API void fade_transition( float factor = 0.020f );


	extern TLR_API CKeyPressEventNotifier keyPressEventNotifier;
	extern TLR_API CMouseEventNotifier mouseEventNotifier;
	extern TLR_API CTimerEventNotifier timerEventNotifier;
	extern TLR_API CUIMainMenu *mainMenu;
	extern TLR_API CConsoleWindow *consoleWindow;
	extern TLR_API CUIMouse mouse;
	extern TLR_API CUIManager *pManager;
	extern TLR_API int textureIds[ 15 ];
	extern TLR_API unsigned int windowWidth;
	extern TLR_API unsigned int windowHeight;
	extern TLR_API float alpha;
} //end of namespace

#endif