#pragma once
#include <lib.h>
#include <UI/UIObserver.h>


namespace UI {


template <class KeyDownEventHandler,
		  class TimerEventHandler>
class CUIGenericUI : public CUIObserver
{
  public:
	CUIGenericUI( );
	virtual ~CUIGenericUI( );

	virtual void update( CUIEvent &e );

  protected:
	KeyDownEventHandler m_keyEventHandler;
	TimerEventHandler m_timerEventHandler;
};

#include "UIGenericUI.inl"

} //end of namespace