#ifdef WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/GL.h>
#include <UI/UI.h>
#include <UI/UIMainMenu.h>
#include <Core.h>
#include <OS/OSKeys.h>
#include <ResourceManagement/TextureManager/TextureManager.h>

namespace UI {

CUIMainMenu::CUIMainMenu( )
 : menu_selection(1), pda_lights(0)
{
	srand( (unsigned int) time(0) );
	//UI::pManager->reg( *this );
	//keyPressEventNotifier.attach( *this );
	//timerEventNotifier.attach( *this );
	//mouseEventNotifier.attach( *this );
}

CUIMainMenu::~CUIMainMenu( )
{
}

void CUIMainMenu::render( )
{
	static int fingerMove = 1;


	glPushMatrix( );
	glLoadIdentity( );
	glDisable( GL_TEXTURE_2D );
	glRasterPos2i( 0, 0 );
	
	ResourceManagement::CTexture2D *texture = Core::pTextureManager->find( textureIds[ 0 ] );
	glPixelZoom( (float) UI::windowWidth / texture->width(), (float) UI::windowHeight / texture->height() );
	glDrawPixels( texture->width(),
				  texture->height(), 
				  texture->format(),
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) texture->bitmap( ) );

	/* background hand */
	texture = Core::pTextureManager->find( textureIds[ 11 ] );
	glRasterPos2i( (GLint) ((UI::windowWidth - 10) / 2.0f), 0 );
	glDrawPixels( texture->width(),
				  texture->height(), 
				  texture->format(),
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) texture->bitmap( ) );

	/* pda */
	texture = Core::pTextureManager->find( textureIds[ 1 ] );
	glRasterPos2i( (GLint) ((UI::windowWidth - texture->width()) / 2.0f), 40 );
	glDrawPixels( texture->width(),
				  texture->height(), 
				  texture->format(),
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) texture->bitmap( ) );


	/* menu choices... */
	if( menu_selection == 0 )
		texture = Core::pTextureManager->find( textureIds[ 5 ] );
	else
		texture = Core::pTextureManager->find( textureIds[ 2 ] );
	glRasterPos2i( (GLint) ((UI::windowWidth - texture->width()) / 2.0f), 258 );
	glDrawPixels( texture->width(),
				  texture->height(), 
				  texture->format(),
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) texture->bitmap( ) );

	if( menu_selection == 1 )
		texture = Core::pTextureManager->find( textureIds[ 6 ] );
	else
		texture = Core::pTextureManager->find( textureIds[ 3 ] );
	glRasterPos2i( (GLint) ((UI::windowWidth - texture->width()) / 2.0f), 190 );
	glDrawPixels( texture->width(),
				  texture->height(), 
				  texture->format(),
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) texture->bitmap( ) );

	if( menu_selection == 2 )
		texture = Core::pTextureManager->find( textureIds[ 7 ] );
	else
		texture = Core::pTextureManager->find( textureIds[ 4 ] );
	glRasterPos2i( (GLint) ((UI::windowWidth - texture->width()) / 2.0f), 120 );
	glDrawPixels( texture->width(),
				  texture->height(), 
				  texture->format(),
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) texture->bitmap( ) );

	/* moving finger */
	glEnable( GL_TEXTURE_2D );
	texture = Core::pTextureManager->find( textureIds[ 12 ] );
	
	glBindTexture( GL_TEXTURE_2D, texture->getTextureID( ) );
	glTranslatef( (UI::windowWidth - 115) / 2.0f, ((menu_selection + 2) % 3 == 0 ? -45.0f : -40.0f ), 0.0f );
	glBegin( GL_QUADS );
		glTexCoord2i( 0, 0 ); glVertex2i( 0, 0 );
		glTexCoord2i( 1, 0 ); glVertex2i( 64, 0 );
		glTexCoord2i( 1, 1 ); glVertex2i( 64, 128 );
		glTexCoord2i( 0, 1 ); glVertex2i( 0, 128 );
	glEnd( );
	glLoadIdentity( );
	glDisable( GL_TEXTURE_2D );
	//glRasterPos2i( (UI::windowWidth - 115) / 2.0f, 0 );
	//glPixelZoom( 1.0f, 1.35f );
	//glDrawPixels( texture->width(),
	//			  texture->height(), 
	//			  texture->format(),
	//			  GL_UNSIGNED_BYTE,
	//			  (GLubyte *) texture->bitmap( ) );

	/* Pda lights */
	switch( pda_lights )
	{
		case 0: // no lights
			texture = Core::pTextureManager->find( textureIds[ 8 ] );
			glRasterPos2i( (GLint) ((UI::windowWidth + 156) / 2.0f), 343 );
			glPixelZoom( 0.9f, 0.99f );
			glDrawPixels( texture->width(),
				  texture->height(), 
				  texture->format(),
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) texture->bitmap( ) );
			break;
		case 1:  // green light
			texture = Core::pTextureManager->find( textureIds[ 9 ] );
			glRasterPos2i( (GLint) ((UI::windowWidth + 156) / 2.0f), 343 );
			glPixelZoom( 0.9f, 0.99f );
			glDrawPixels( texture->width(),
				  texture->height(), 
				  texture->format(),
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) texture->bitmap( ) );
			break;
		case 2: // red light
			texture = Core::pTextureManager->find( textureIds[ 10 ] );
			glRasterPos2i( (GLint) ((UI::windowWidth + 156) / 2.0f), 343 );
			glPixelZoom( 0.9f, 0.99f );
			glDrawPixels( texture->width(),
				  texture->height(), 
				  texture->format(),
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) texture->bitmap( ) );
			break;
		default:
			texture = Core::pTextureManager->find( textureIds[ 8 ] );
			glRasterPos2i( (GLint) ((UI::windowWidth + 155) / 2.0f), 343 );
			glPixelZoom( 0.9f, 0.99f );
			glDrawPixels( texture->width(),
				  texture->height(), 
				  texture->format(),
				  GL_UNSIGNED_BYTE,
				  (GLubyte *) texture->bitmap( ) );
			break;
	}

	glPopMatrix( );
	glEnable( GL_TEXTURE_2D );
}

void CUIMainMenu::onKeyDown( int key )
{
	switch( key )
	{
		case OS::LK_UP:
			menu_selection--;
			break;
		case OS::LK_DOWN:
			menu_selection++;
			break;
		case OS::LK_ENTER:
			switch( menu_selection )
			{
				case 0: // selected start...
					UI::setup_fade_out( );
					UI::pManager->setCurrentUI( UI::ST_MAINMENU );
					break;
				case 1: // selected options...
					break;
				case 2: // selected exit...
					PostQuitMessage( 0 );
					break;
				default:
					break;
			}
			break;
		case OS::LK_TILDA:
			UI::pManager->toggleConsole( );
			break;
		default:
			break;
	}


	/* Ensure the menu_selection is within bounds */
	if( menu_selection > 2 )
		menu_selection = 2;
	else if( menu_selection < 0 )
		menu_selection = 0;
}


//void CUIMainMenu::onKeyUp( int key )
//{
//}

void CUIMainMenu::onMouseButton( bool left, bool middle, bool right, int x, int y )
{
	if( !((420 <= x && x <= 670) && ( /*(UI::windowHeight - 322)*/120 <= y && y <=  322/*(UI::windowHeight - 120)*/) ) )
		return;

	if( left )
	{
		switch( menu_selection )
		{
			case 0: // selected start...
				UI::setup_fade_out( );
				UI::pManager->setCurrentUI( UI::ST_MAINMENU );
				break;
			case 1: // selected options...
				break;
			case 2: // selected exit...
				PostQuitMessage( 0 );
				break;
			default:
				break;
		}
	}

}

void CUIMainMenu::onMouseMove( int x, int y )
{
	if( isMouseOverButton( x, y, 0 ) )
		menu_selection = 0;
	if( isMouseOverButton( x, y, 1 ) )
		menu_selection = 1;
	if( isMouseOverButton( x, y, 2 ) )
		menu_selection = 2;
}

void CUIMainMenu::onTimer( )
{
	pda_lights = rand( ) % 3;
}

bool CUIMainMenu::isMouseOverButton( int x, int y, int button )
{
	switch( button )
	{
		case 0:
			if( (384 <= x && x <= 640) && ( /*(UI::windowHeight - 322)*/258 <= y && y <= 322/*(UI::windowHeight - 258)*/) )
				return true;
		case 1:
			if( (384 <= x && x <= 640) && (/* (UI::windowHeight - 254)*/190 <= y && y <= 254/*(UI::windowHeight - 190)*/) )
				return true;
		case 2:
			if( (384 <= x && x <= 640) && ( /*(UI::windowHeight - 184)*/120 <= y && y <= /*(UI::windowHeight - 120)*/184) )
				return true;
		default:
			break;
	}

	return false;
}

} // end of namespace