#include <windows.h>
#include <GL/glew.h>
#include <GL/gl.h>

#include <OS/OSWindow.h>
#include <OS/Microsoft/MSKeyMapper.h>
#include <OS/Microsoft/MSWindowImpl.h>
#include <Core.h>
#include "ConsoleWindow.h"
#include "UI.h"



using namespace std;

namespace UI {

CConsoleWindow::CConsoleWindow( )
:  m_ConsoleBuffer(0), m_InputLine(),
#ifdef WIN32
m_TextRasterizer(static_cast<OS::CMSWindowImpl *>(Core::pWindow->getImplementation( ))->getHandleDC( )), 
#endif
   m_NumberOfDisplayableRows(0), m_NumberOfDisplayableColumns(0), input_alpha(0.0f)
{
#ifdef WIN32
	m_TextRasterizer.CreateBitmapFont( "Consolas", 14 );
#endif
	m_NumberOfDisplayableRows = static_cast<unsigned int>( (m_Height / 12.0f) - 1.0f );
	m_NumberOfDisplayableColumns = static_cast<unsigned int>( m_Width / 2.0f );
	//m_State = WST_MINIMIZED;
}

CConsoleWindow::~CConsoleWindow( )
{
}

void CConsoleWindow::render( )
{
	IUIWindow::render( );

	if( m_State != WST_MINIMIZED )
	{
		glPushMatrix( );
		glLoadIdentity( );
		glTranslatef( (GLfloat) m_X, (GLfloat) m_Y - m_Height, 2.0f );
		glTranslatef( (GLfloat) BORDER_WIDTH, (GLfloat) 0, 2.0f );
		glPushAttrib( GL_CURRENT_BIT  );
	
		glColor3f( 0, 0, 0 );
		glBindTexture( GL_TEXTURE_2D, 0 );
		glDisable( GL_LIGHTING );
		glBegin( GL_QUADS );
			glVertex2i( 0, 0 );
			glVertex2i( m_Width - 2 * BORDER_WIDTH, 0 );
			glVertex2i( m_Width - 2 * BORDER_WIDTH, m_Height );
			glVertex2i( 0, m_Height );

		glEnd( );
		glColor3f( 0.0f, 0.8f, 0.0f );
		glLoadIdentity( );
		glEnable( GL_LIGHTING );
		glEnable( GL_SCISSOR_TEST );
		glScissor( static_cast<GLint>( m_X ), static_cast<GLint>( m_Y - m_Height ), m_Width - BORDER_WIDTH, m_Height );

		deque<string>::const_iterator itr;
		unsigned int i = 0;
		unsigned int idx = m_NumberOfDisplayableRows;
#ifdef WIN32
		glColor3f( 0.0f, 0.8f, 0.0f );
		if( m_ConsoleBuffer.size( ) < m_NumberOfDisplayableRows )
			for( itr = m_ConsoleBuffer.begin(); itr != m_ConsoleBuffer.end(); ++itr )
				m_TextRasterizer.PrintString( m_X + BORDER_WIDTH + 4/*130*/, m_Y - 12 * ++i, const_cast<char *>(itr->c_str( )) );		
		else
			for( itr = m_ConsoleBuffer.end() - idx; itr != m_ConsoleBuffer.end(); ++itr )
				m_TextRasterizer.PrintString( m_X + BORDER_WIDTH + 4/*130*/, m_Y - 12 * ++i, const_cast<char *>(itr->c_str( )) );		

		m_TextRasterizer.PrintString( m_X + BORDER_WIDTH + 4, m_Y - 12 * ++i, "[player@ThreatLevelRed ~]#  " );
#endif
		glEnable( GL_BLEND );
		glEnable( GL_ALPHA_TEST );
		glColor4f( 0.0f, 0.9f, input_alpha, input_alpha );
#ifdef WIN32
		m_TextRasterizer.PrintString( m_X + BORDER_WIDTH + 193, m_Y - 12 * i, const_cast<char *>(m_InputLine.c_str( )) );		
#endif
		glDisable( GL_SCISSOR_TEST );
		glPopAttrib( );
		glPopMatrix( );
	}
}

void CConsoleWindow::onKeyDown( OS::LogicalKey key )
{
	OS::CMSKeyMapper *keyMapper = static_cast<OS::CMSKeyMapper *>( Core::pWindow->getKeyMapper( ) );
	if( key == OS::LK_TILDA )
	{
		UI::pManager->toggleConsole( );
		return;
	}

	if( key == OS::LK_ENTER ) // Enter key was struck...
	{
		std::string line("[player@Space_Fighters ~]# ");
		line += m_InputLine;
		m_ConsoleBuffer.push_back( line );
		
		processCommand( m_InputLine );
		m_InputLine.clear( );
		prunBuffer( );
		input_alpha = 0.0f;
	}
	else if( key == OS::LK_ENTER )
	{
		if( m_InputLine.size( ) > 0 )
			m_InputLine.erase( m_InputLine.size( ) - 1, 1 );
	}
	else if( key == OS::LK_BACKSPACE )
	{
		if( m_InputLine.size( ) > 0 )
			m_InputLine.erase( m_InputLine.size( ) - 1, 1 );
	}
	else
		m_InputLine += keyMapper->translateLogicalKeyToChar(key);
	

}

void CConsoleWindow::onKeyUp( OS::LogicalKey key )
{
}

void CConsoleWindow::onMouseButton( bool left, bool middle, bool right, int x, int y )
{
}

void CConsoleWindow::onMouseMove( int x, int y )
{
}

void CConsoleWindow::onTimer( )
{
	input_alpha += 0.01f;
}

void CConsoleWindow::closeWindow( )
{
	UI::pManager->toggleConsole( );
}

void CConsoleWindow::restoreWindow( )
{
	IUIWindow::restoreWindow( );
	m_NumberOfDisplayableRows = static_cast<unsigned int>( (m_Height / 12.0f) - 1.0f );
	m_NumberOfDisplayableColumns = static_cast<unsigned int>( m_Width / 2.0f );
}

void CConsoleWindow::minimizeWindow( )
{
	UI::pManager->toggleConsole( );
}

void CConsoleWindow::maximizeWindow( )
{
	IUIWindow::maximizeWindow( );
	m_NumberOfDisplayableRows = static_cast<unsigned int>( (m_Height / 12.0f) - 1.0f );
	m_NumberOfDisplayableColumns = static_cast<unsigned int>( m_Width / 2.0f );
}

void CConsoleWindow::processCommand( const std::string &cmd )
{
	if( !cmd.compare("EXIT") )
		PostQuitMessage( 0 );
	else if( !cmd.compare("TEST") )
	{
		m_ConsoleBuffer.push_back( "   ######   " );
		m_ConsoleBuffer.push_back( " ########## " );
		m_ConsoleBuffer.push_back( "### #### ###" );
		m_ConsoleBuffer.push_back( "############" );
		m_ConsoleBuffer.push_back( "##  ####  ##" );
		m_ConsoleBuffer.push_back( " ###    ### " );
		m_ConsoleBuffer.push_back( "   ######   " );
	}
	else
		m_ConsoleBuffer.push_back( "Bad command." );
}

void CConsoleWindow::prunBuffer( )
{
	if( m_ConsoleBuffer.size( ) > CONSOLE_MAX_LINES )
		m_ConsoleBuffer.erase( m_ConsoleBuffer.begin( ), m_ConsoleBuffer.end( ) - CONSOLE_MAX_LINES );
}

} // end of namespace