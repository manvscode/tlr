#pragma once
#ifndef _UIEVENT_H_
#define _UIEVENT_H_
#include <lib.h>
#include <vector>

namespace UI {

/*
 *	Event Types...
 */
TLR_API typedef enum tagEventType {
	EVT_KEYDOWN,
	EVT_KEYUP,
	EVT_MOUSEMOVE,
	EVT_MOUSEBUTTON,
	EVT_MOUSEOVER,
	EVT_TIMER,
	EVT_NONE
} EventType;

#define UI_EVENT_PAYLOAD_SIZE		17

class TLR_API CUIEvent
{
  public:
	  
	CUIEvent( );
	CUIEvent( CUIEvent::EventType et );
	virtual ~CUIEvent( );

	EventType getType( ) const;

	unsigned char &operator[]( unsigned int i );
	const unsigned char &operator[]( unsigned int i ) const;

  protected:
	EventType m_Type;
	unsigned int m_DataSize;
	unsigned char m_Buffer[ UI_EVENT_PAYLOAD_SIZE ];
};

inline EventType CUIEvent::getType( ) const
{
	return m_Type;
}


} // end of namespace
#endif