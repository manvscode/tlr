#include <windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <cassert>
#include <utility/debug.h>
#include "UIWindow.h"
#include "UI.h"


namespace UI {

IUIWindow::IUIWindow( )
 : base(0), m_WindowTexture(0), m_X(23), m_Y(740),
   m_Width(400), m_Height(400), m_RestoreWidth(UI::windowWidth), m_RestoreHeight(UI::windowHeight), m_State(WST_NORMAL),
   m_ZOrder( 1 ), m_bLeftMouseButtonDown(false)
{
	//keyPressEventNotifier.attach( *this );
	//timerEventNotifier.attach( *this );
	//mouseEventNotifier.attach( *this );
	UI::pManager->registerWindow( *this );
}

IUIWindow::~IUIWindow( )
{
}

void IUIWindow::initialize( )
{
	base = glGenLists( 5 );
	
	
	glNewList( base, GL_COMPILE );
		glBegin( GL_QUADS );
			/* title bar */
			glTexCoord2f( 0, 0.81 ); glVertex2i( 0, 0 );
			glTexCoord2f( 0.866, 0.81 ); glVertex2i( m_Width - BUTTON_AREA_WIDTH, 0 );
			glTexCoord2f( 0.866, 1 ); glVertex2i( m_Width - BUTTON_AREA_WIDTH, TITLEBAR_HEIGHT );
			glTexCoord2f( 0, 1 ); glVertex2i( 0, TITLEBAR_HEIGHT );
		glEnd();
	glEndList();
	
	glNewList( base + 1, GL_COMPILE );
		glBegin( GL_QUADS );
			/* button area */
			glTexCoord2f( 0.886, 0.81 ); glVertex2i( 0, 0 );
			glTexCoord2f( 1, 0.81 ); glVertex2i( BUTTON_AREA_WIDTH, 0 );
			glTexCoord2f( 1, 1 ); glVertex2i( BUTTON_AREA_WIDTH, TITLEBAR_HEIGHT );
			glTexCoord2f( 0.886, 1 ); glVertex2i( 0, TITLEBAR_HEIGHT );
		glEnd();
	glEndList();

	glNewList( base + 2, GL_COMPILE );
		glBegin( GL_QUADS );
			/* left border */
			glTexCoord2f( 0, 0.2 ); glVertex2i( 0, 0 );
			glTexCoord2f( 0.017, 0.2 ); glVertex2i( BORDER_WIDTH, 0 );
			glTexCoord2f( 0.017, 0.8 ); glVertex2i( BORDER_WIDTH, m_Height );
			glTexCoord2f( 0, 0.8 ); glVertex2i( 0, m_Height );
		glEnd();
	glEndList();

	glNewList( base + 3, GL_COMPILE );
		glBegin( GL_QUADS );
			/* content area */
			glTexCoord2f( 0.017, 0.2 ); glVertex2i( 0, 0 );
			glTexCoord2f( 0.983, 0.2 ); glVertex2i( m_Width - 2 * BORDER_WIDTH, 0 );
			glTexCoord2f( 0.983, 0.8 ); glVertex2i( m_Width - 2 * BORDER_WIDTH, m_Height );
			glTexCoord2f( 0.017, 0.8 ); glVertex2i( 0, m_Height );
		glEnd();
	glEndList();

	glNewList( base + 4, GL_COMPILE );
		glBegin( GL_QUADS );
			/* right border */
			glTexCoord2f( 0.983, 0.2 ); glVertex2i( 0, 0 );
			glTexCoord2f( 1, 0.2 ); glVertex2i( BORDER_WIDTH, 0 );
			glTexCoord2f( 1, 0.8 ); glVertex2i( BORDER_WIDTH, m_Height );
			glTexCoord2f( 0.983, 0.8 ); glVertex2i( 0, m_Height );
		glEnd();
	glEndList();

	glNewList( base + 5, GL_COMPILE );
		glBegin( GL_QUADS );
			/* bottom border */
			glTexCoord2f( 0, 0 ); glVertex2i( 0, 0 );
			glTexCoord2f( 1, 0 ); glVertex2i( m_Width, 0 );
			glTexCoord2f( 1, 0.1 ); glVertex2i( m_Width, BOTTOM_BORDOR_HEIGHT );
			glTexCoord2f( 0, 0.1 ); glVertex2i( 0, BOTTOM_BORDOR_HEIGHT );
		glEnd();
	glEndList();

	assert( glGetError( ) == GL_NO_ERROR );
}

void IUIWindow::render( )
{



	if( m_State == WST_NORMAL || m_State == WST_MAXIMIZED )
	{
		glPushMatrix( );
			glLoadIdentity( );
			glBindTexture( GL_TEXTURE_2D, m_WindowTexture );

			// title bar 
			glTranslatef( (GLfloat) m_X, (GLfloat) m_Y, 2.0f );
			//glCallList( base );
			glBegin( GL_QUADS );
				/* title bar */
				glTexCoord2f( 0, 0.81 ); glVertex2i( 0, 0 );
				glTexCoord2f( 0.866, 0.81 ); glVertex2i( m_Width - BUTTON_AREA_WIDTH, 0 );
				glTexCoord2f( 0.866, 1 ); glVertex2i( m_Width - BUTTON_AREA_WIDTH, TITLEBAR_HEIGHT );
				glTexCoord2f( 0, 1 ); glVertex2i( 0, TITLEBAR_HEIGHT );
			glEnd();

			// button area
			glTranslatef( (GLfloat) m_Width - BUTTON_AREA_WIDTH, (GLfloat) 0, 2.0f );
			//glCallList( base + 1 );
			glBegin( GL_QUADS );
				/* button area */
				glTexCoord2f( 0.886, 0.81 ); glVertex2i( 0, 0 );
				glTexCoord2f( 1, 0.81 ); glVertex2i( BUTTON_AREA_WIDTH, 0 );
				glTexCoord2f( 1, 1 ); glVertex2i( BUTTON_AREA_WIDTH, TITLEBAR_HEIGHT );
				glTexCoord2f( 0.886, 1 ); glVertex2i( 0, TITLEBAR_HEIGHT );
			glEnd();

			glLoadIdentity( );
			// left border
			glTranslatef( (GLfloat) m_X, (GLfloat) m_Y - m_Height, 2.0f );
			//glCallList( base + 2 );
			glBegin( GL_QUADS );
				/* left border */
				glTexCoord2f( 0, 0.2 ); glVertex2i( 0, 0 );
				glTexCoord2f( 0.017, 0.2 ); glVertex2i( BORDER_WIDTH, 0 );
				glTexCoord2f( 0.017, 0.8 ); glVertex2i( BORDER_WIDTH, m_Height );
				glTexCoord2f( 0, 0.8 ); glVertex2i( 0, m_Height );
			glEnd();

			// content area
			glTranslatef( (GLfloat) BORDER_WIDTH, (GLfloat) 0, 2.0f );
			//glCallList( base + 3 );
			glBegin( GL_QUADS );
				/* content area */
				glTexCoord2f( 0.017, 0.2 ); glVertex2i( 0, 0 );
				glTexCoord2f( 0.983, 0.2 ); glVertex2i( m_Width - 2 * BORDER_WIDTH, 0 );
				glTexCoord2f( 0.983, 0.8 ); glVertex2i( m_Width - 2 * BORDER_WIDTH, m_Height );
				glTexCoord2f( 0.017, 0.8 ); glVertex2i( 0, m_Height );
			glEnd();

			// content area
			glTranslatef( (GLfloat) m_Width - 2 * BORDER_WIDTH, (GLfloat) 0, 2.0f );
			//glCallList( base + 4 );
			glBegin( GL_QUADS );
				/* right border */
				glTexCoord2f( 0.983, 0.2 ); glVertex2i( 0, 0 );
				glTexCoord2f( 1, 0.2 ); glVertex2i( BORDER_WIDTH, 0 );
				glTexCoord2f( 1, 0.8 ); glVertex2i( BORDER_WIDTH, m_Height );
				glTexCoord2f( 0.983, 0.8 ); glVertex2i( 0, m_Height );
			glEnd();

			// bottom border
			glLoadIdentity( );
			glTranslatef( (GLfloat) m_X, (GLfloat) m_Y - m_Height - BOTTOM_BORDOR_HEIGHT, 2.0f );
			//glCallList( base + 5 );
			glBegin( GL_QUADS );
				/* bottom border */
				glTexCoord2f( 0, 0 ); glVertex2i( 0, 0 );
				glTexCoord2f( 1, 0 ); glVertex2i( m_Width, 0 );
				glTexCoord2f( 1, 0.1 ); glVertex2i( m_Width, BOTTOM_BORDOR_HEIGHT );
				glTexCoord2f( 0, 0.1 ); glVertex2i( 0, BOTTOM_BORDOR_HEIGHT );
			glEnd();
		glPopMatrix( );
	}

}

void IUIWindow::update( CUIEvent &e )
{
	switch( e.getType( ) )
	{
		case UI::EVT_KEYDOWN:
			onKeyDown( static_cast<OS::LogicalKey>(e[ 0 ]) );
			break;
		case UI::EVT_KEYUP:
			onKeyUp( static_cast<OS::LogicalKey>(e[ 0 ]) );
			break;
		case UI::EVT_TIMER:
			onTimer( );
			break;
		case UI::EVT_MOUSEBUTTON:
		{
			int x = 0;
			int y = 0;
			memcpy( &x, &e[ 3 ], sizeof(int) );
			memcpy( &y, &e[ 3 + sizeof(int) ], sizeof(int) );

			if( e[ 0 ] && testTitleBar( static_cast<float>( x ), static_cast<float>( y ) ) ) // left buttion pressed within titlebar
			{
				m_bLeftMouseButtonDown = true;
				stX = x;
				stY = y;				
			}
			else if( e[ 0 ] ) // left buttion pressed; otherwise check button area
				handleMouseClickInButtonArea( static_cast<float>( x ), static_cast<float>( y ) );
			else 
				m_bLeftMouseButtonDown = false;
				

			onMouseButton( (bool) e[ 0 ], (bool) e[ 1 ], (bool) e[ 2 ], x, y );
			break;
		}
		case UI::EVT_MOUSEMOVE:
		{
			int x = 0;
			int y = 0;
			memcpy( &x, &e[ 0 ], sizeof(int) );
			memcpy( &y, &e[ 0 + sizeof(int) ], sizeof(int) );


			if( m_bLeftMouseButtonDown )
				handleMouseClickInTitleBar( static_cast<float>( x ), static_cast<float>( y ) );


			onMouseMove( x, y );
			break;
		}
		default:
			break;
	}
}

void IUIWindow::setWindowSkin( unsigned int skin )
{
	m_WindowTexture = skin;
}

void IUIWindow::moveWindow( float x, float y )
{
	assert( (x >= 0  && x <= UI::windowWidth) ||
			(y >= 0 && y <= UI::windowHeight ) );
	m_X = x;
	m_Y = y;
}

void IUIWindow::setDimensions( int w, int h )
{
	m_Width = w;
	m_Height = h;
}

void IUIWindow::handleMouseClickInButtonArea( float x, float y )
{
	float rx = m_X + m_Width - BUTTON_AREA_WIDTH;
	float ry = m_Y;


	if( rx <= x && x <= (rx + BUTTON_WIDTH) &&  ry <= y && y <= (ry + TITLEBAR_HEIGHT) )
		minimizeWindow( );
	

	if( (rx + BUTTON_WIDTH) <= x && x <= (rx + 2 * BUTTON_WIDTH) &&  ry <= y && y <= (ry + TITLEBAR_HEIGHT) )
		restoreWindow( );

	if( (rx + 2 * BUTTON_WIDTH )<= x && x <= (rx + 3 * BUTTON_WIDTH) &&  ry <= y && y <= (ry + TITLEBAR_HEIGHT) )
		closeWindow( );

}

void IUIWindow::handleMouseClickInTitleBar( float x, float y )
{
	if( m_bLeftMouseButtonDown )
		moveWindow( m_X + (x - stX), m_Y + (y - stY)  );
}

void IUIWindow::minimizeWindow( )
{
	m_State = WST_MINIMIZED;
}

void IUIWindow::maximizeWindow( )
{
	int w = m_Width;
	int h = m_Height;
		
	m_Width = m_RestoreWidth;
	m_Height = m_RestoreHeight;

	m_RestoreX = static_cast<unsigned int>( m_X );
	m_RestoreY = static_cast<unsigned int>( m_Y );
	m_RestoreWidth = w;
	m_RestoreHeight = h;

	m_X = 0.0f;
	m_Y = static_cast<float>( UI::windowHeight - TITLEBAR_HEIGHT );
	m_State = WST_MAXIMIZED;
}

void IUIWindow::normalizeWindow( )
{

	if( m_State == WST_NORMAL )
		return;
	else if( m_State == WST_MINIMIZED )
	{
		m_State = WST_NORMAL;
		return;
	}

	int w = m_Width;
	int h = m_Height;
	int x = static_cast<int>( m_X );
	int y = static_cast<int>( m_Y );
		
	m_Width = m_RestoreWidth;
	m_Height = m_RestoreHeight;

	m_X = static_cast<float>( m_RestoreX );
	m_Y = static_cast<float>( m_RestoreY );

	m_RestoreWidth = w;
	m_RestoreHeight = h;
	m_RestoreX = x;
	m_RestoreY = y;

	m_State = WST_NORMAL;
}

void IUIWindow::restoreWindow( )
{
	if( m_RestoreWidth == 0 )
		m_RestoreWidth = UI::windowWidth;
	if( m_RestoreHeight == 0 )
		m_RestoreHeight = UI::windowHeight - TITLEBAR_HEIGHT - BOTTOM_BORDOR_HEIGHT;

	if( m_State == WST_NORMAL )
		maximizeWindow( );
	else if( m_State == WST_MAXIMIZED )
		normalizeWindow( );
}

void IUIWindow::closeWindow( )
{
	assert( UI::pManager != NULL );
	UI::pManager->unregisterWindow( *this );
}

bool IUIWindow::testTitleBar( float x, float y )
{

	if( m_X <= x && x <= (m_X + m_Width - BUTTON_AREA_WIDTH) &&  (m_Y - TITLEBAR_HEIGHT) <= y && y <= m_Y+TITLEBAR_HEIGHT  )
		return true;

	return false;
}

} // end of namespace