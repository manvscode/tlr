#pragma once
#include <ctime>
#include "UIEventHandler.h"

#include <lib.h>
#include <UI/UIScreen.h>
#include <UI/UIGenericUI.h>

namespace UI {


class TLR_API CUIMainMenu : public IUIScreen 
{
  public:
	CUIMainMenu( );
	virtual ~CUIMainMenu( );


	void render( );

	virtual void onKeyDown( int key );
	//virtual void onKeyUp( int key );
	virtual void onMouseButton( bool left, bool middle, bool right, int x, int y );
	void onMouseMove( int x, int y );
	virtual void onTimer( );


  protected:
	bool isMouseOverButton( int x, int y, int button );
	int menu_selection;
	int pda_lights;
};



} // end of namespace