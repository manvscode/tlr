/*		Timer.cpp
 *
 *		Timer object to keep time from a initial moment.
 *		Coded by Joseph A. Marrero
 */
//
//	Note: You must use Winmm.lib on WIN32
//
#ifdef WIN32
#include <windows.h>
#include <mmsystem.h>
#pragma comment( lib, "WINMM.LIB" )
#else
#include <stddef.h> 
#endif
#include <utility/debug.h>
#include "timer.h"

CTimer::CTimer( )
  : timeFromStart(0.0f),
#ifdef WIN32
  ticksPerSecond(0)
#else
  m_tp()
#endif
{
	initTimer( );
}

CTimer::~CTimer( )
{
	
}

void CTimer::initTimer( )
{
#ifdef WIN32
	if( !QueryPerformanceFrequency( (LARGE_INTEGER *) &ticksPerSecond) )
		ticksPerSecond = 1000;

	timeFromStart = 0;
	timeFromStart = getTime( );
#else // Unix implementation
	timeFromStart = 0;
	m_tp.tv_sec = m_tp.tv_usec = 0;
	timeFromStart = getTime( );
#endif

}

void CTimer::resetTimer( )
{
	timeFromStart = 0;
	timeFromStart = getTime( );
}

bool CTimer::wait( float time_secs )
{
	float t_1 = getTime( );

	while(  getTime( ) - t_1 <= time_secs );
	return true;
}

float CTimer::getTime( )
{
#ifdef WIN32
	UINT64	ticks;
	float	time;

	//Get time since start
	if( !QueryPerformanceCounter( (LARGE_INTEGER *) &ticks ) )
		ticks = (UINT64) timeGetTime( );
	
	//Divide the freq. to get the time in seconds
	time = (float) (__int64)ticks/(float)(__int64)ticksPerSecond;
	
	//Subtract the time from start
	time -= timeFromStart;

	return time;
#else // Unix implementation
	gettimeofday( &m_tp, NULL );
	time = (float) m_tp.tv_sec * 1000 + m_tp.tv_usec;
	return time;
#endif
}