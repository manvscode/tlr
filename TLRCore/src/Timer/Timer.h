#pragma once
#ifndef __TIMER_H__
#define __TIMER_H__
#pragma message( "__TIMER_H__ is included..." )
/*		Timer.h
 *
 *		Timer object to keep time from an initial moment.
 *		Coded by Joseph A. Marrero
 */
#ifdef WIN32
#include <lib.h>
#include <windows.h>
#include <basetsd.h>
#else
#include <sys/time.h>
#endif

class TLR_API CTimer
{
  public:
	CTimer( );
	~CTimer( );

	void resetTimer( );
	bool wait( float time_secs );
	float getTime( );

  private:
	float		timeFromStart;


	#ifdef WIN32
	UINT64		ticksPerSecond;
	#else  // Unix implementation
	struct timeval m_tp;
	#endif
	void initTimer( );
};

#endif