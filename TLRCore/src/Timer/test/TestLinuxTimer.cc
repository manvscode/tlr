#include <cstdlib>
#include <iostream>
#include "../Timer.h"
#include "TestLinuxTimer.h"
using namespace std;

CTimer *pTimer = NULL;


int main( int argc, char *argv[] )
{
	if( argv[ 1 ] == NULL )
	{
		cout << "Linux Timer Test Driver, Coded by Joseph A. Marrero" << endl;
		cout << "Usage: " << argv[ 0 ] << " <seconds> " << endl;
		return 1;
	}
	pTimer = new CTimer( );
	int nPause = atoi( argv[ 1 ] );
	cout.precision( 20 );
	
	
	for( int i = 0; i < 10; i++ )
	{
		pTimer->resetTimer( );
		pTimer->wait( nPause );
		cout << nPause << " seconds expired. " << endl;		
	}
	
	//pTimer->resetTimer( );
	
	/*while( true )
	{
		cout << "time = " << pTimer->getTime( ) << endl;
		sleep( 1 );
	}*/
	
	delete pTimer;
	return 0;
}
