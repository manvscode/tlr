/*
 *	main.cpp
 *
 *	This is the entry point of execution of the Threat Level
 *	Red game.
 *
 *	Coded by Joseph A. Marrero
 */
#include <Core.h>


#if defined WIN32
/*
 *	Include Microsoft Windows implementation...
 */
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <OS/Microsoft/MSWindow.h>
#include <iostream>
using namespace std;

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd )
{
	Core::pWindow = new OS::CMSWindow( hInstance, 1024, 768 );
	bool bCreateWinSuccess = Core::pWindow->create( );
	
	if( !bCreateWinSuccess )
	{
		cerr << "ERROR: Could not create a window." << endl;
		delete Core::pWindow;
		return -1;
	}
	
	
	int r = Core::pWindow->mainLoop( );
	delete Core::pWindow;
	return r;
}

#elif defined LINUX
#include <GL/glew.h>
#include <GL/gl.h>
#include <OS/X11/X11Window.h>
#include <iostream>
using namespace std;
/*
 *	Include Linux implementation...
 */
int main( int argc, char *argv[] )
{
	cout << "Starting Threat Level Red..." << endl;
	Core::pWindow = new OS::CX11Window( );
	bool bCreateWinSuccess = Core::pWindow->create( );
	
	if( !bCreateWinSuccess )
	{
		cerr << "ERROR: Could not create a window." << endl;
		delete Core::pWindow;
		return -1;
	}
	
	int r = Core::pWindow->mainLoop( );
	delete Core::pWindow;
	return r;
}
#else
#error "You failed to define your build environment. Please define either WIN32 or LINUX. okthanxbi."
#endif
