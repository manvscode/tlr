#pragma once
#ifndef _VECTOR4_H_
#define _VECTOR4_H_
/*		Vector4.h
 *
 *		Vectors for 3D space.
 *		Coded by Joseph A. Marrero
 *		5/7/04
 */
#include <cmath>
#include <ostream>

namespace Math {

template <class AnyType = float>
class CVector4
{

  private:
	union {
		struct{
			AnyType x, y, z, w;
		};
		AnyType data[ 4 ];
	};

  public:


	explicit CVector4( );
	CVector4( const AnyType x, const AnyType y, const AnyType z, const AnyType w );
	CVector4( const CVector4<AnyType> &vec ); // copy constructor
	virtual ~CVector4( );

	// Get components 
	const AnyType getX( ) const;
	const AnyType getY( ) const;
	const AnyType getZ( ) const;
	const AnyType getW( ) const;
	void setX( const AnyType X );
	void setY( const AnyType Y );
	void setZ( const AnyType Z );
	void setW( const AnyType W );
	// Vector Operations
	const CVector4 &operator =( const CVector4 &vector );
	const CVector4 &operator +=( const CVector4 &vector );
	const CVector4 &operator -=( const CVector4 &vector );
	AnyType dotProduct( const CVector4 &vector );		// Dot Product
	//CVector4 crossProduct( const CVector4 &vector );	// Cross Product
	AnyType getMagnitude( ) const;							// Get Vector Magnitude
	void normalize( );								// Normalize Vector
	const bool isNormalized( ) const;
	void negate( );									// Negate Vector


	AnyType &operator[]( int idx );
	const AnyType &operator[]( int idx ) const;
	//friend ostream& operator <<(ostream&, const CVector4&);

	static const CVector4 X_VECTOR;
	static const CVector4 Y_VECTOR;
	static const CVector4 Z_VECTOR;
	static const CVector4 W_VECTOR;
};


template <class AnyType>
CVector4<AnyType> operator +( const CVector4<AnyType> &v1, const CVector4<AnyType> &v2 );				// Vector Sum
template <class AnyType>
CVector4<AnyType> operator -( const CVector4<AnyType> &v1, const CVector4<AnyType> &v2 );				// Vector Difference
template <class AnyType>
CVector4<AnyType> operator *( const CVector4<AnyType> &vector, const AnyType scalar );			// Vector-Scalar Multiplication
template <class AnyType>
CVector4<AnyType> operator *( const CVector4<AnyType> scalar, const CVector4<AnyType> &vector );			// Vector-Scalar Multiplication
template <class AnyType>
CVector4<AnyType> operator /( const CVector4<AnyType> &vector, const AnyType scalar );			// Vector- Scalar Division
template <class AnyType>
CVector4<AnyType> operator /( const CVector4<AnyType> &vector, const AnyType scalar );			// Vector- Scalar Division

template <class AnyType>
AnyType dotProduct( const CVector4<AnyType> &v1, const CVector4<AnyType> &v2 );	
template <class AnyType>
AnyType operator *( const CVector4<AnyType> &v1, const CVector4<AnyType> &v2 );		

//template <class AnyType>
//CVector4<AnyType> crossProduct( const CVector4<AnyType> &v1, const CVector4<AnyType> &v2 );		
//
//template <class AnyType>
//AnyType angleBetween( const CVector4<AnyType> &v1, const CVector4<AnyType> &v2 );


#include "Vector4.inl"
} // end of namespace
#endif