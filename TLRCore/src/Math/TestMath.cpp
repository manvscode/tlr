#include <iostream>
#include "Matrix4x4.h"

int main( )
{
	float mat[16] = {0};
	mat[0] = mat[1] = mat[6] = mat[10] = mat[15] = -1.0f;
	mat[2] = 2.0f;
	mat[4] = mat[5] = mat[11] = mat[12] = 1.0f;

	CMatrix4x4<float> m( mat );
	m.transpose();
	cout << " Marix A = \n" << m << endl;

	cout << " det(A) = " << m.determinant() << endl << endl;

	
	m.invert( );
	cout << " Matrix A inverse = \n" << m << endl;

	return 0;
}