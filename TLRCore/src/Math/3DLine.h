#pragma once
#ifndef __3DLINE_H__
#define __3DLINE_H__

#include <lib.h>
#include "3DPoint.h"
#include "Vector3.h"


namespace Math {

template <class AnyType = float>
class TLR_API C3DLine
{
  public:

	explicit C3DLine( ) : vSlope(0), vPoint(0)
	{}

	explicit C3DLine( const C3DPoint<AnyType> &pt1, const C3DPoint<AnyType> &pt2 )
	{
		vPoint1 = pt1;
		vPoint2 = pt2;
		vSlope = pt2 - pt1;
	}

	

	const C3DPoint<AnyType> getPoint( AnyType t ) const
	{
		AnyType tMax = ( vPoint2.getX( ) - vPoint1.getX( ) ) / vSlope.getX( );
		if( t < 0)
			t = 0;
		else if( t > tMax )
			t = tMax;
		
		C3DPoint<AnyType> r;

		r.setX( vPoint1.getX( ) + vSlope.getX( ) * t );
		r.setY( vPoint1.getY( ) + vSlope.getY( ) * t );
		r.setZ( vPoint1.getZ( ) + vSlope.getZ( ) * t );
		return r;
	}



	virtual ~C3DLine( )
	{}

  private:
	CVector3<AnyType> vSlope;
	C3DPoint<AnyType> vPoint1, vPoint2;
};


} // end of namespace
#endif