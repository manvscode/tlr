#pragma once
#ifndef __3DPOINT_H__
#define __3DPOINT_H__
/*
 *		3DPoint.h
 *
 *		A 3d point class.
 *		Coded by Joseph A. Marrero.
 */
#include <lib.h>
#include "Vector3.h"

namespace Math {

template <class AnyType = double>
class TLR_API C3DPoint
{
  public:

	C3DPoint( )
	{ m_x = m_y = m_z = static_cast<AnyType>(0); }

	C3DPoint( const AnyType x, const AnyType y, const AnyType z )
	{
		m_x = x;
		m_y = y;
		m_z = z;
	}
	
	C3DPoint &operator =( const C3DPoint &pt )
	{
		if( this != &pt )
		{
			m_x = pt.m_x;
			m_y = pt.m_y;
			m_z = pt.m_z;
			
		}
		return *this;
	}

	virtual ~C3DPoint( )
	{ }

	void set( const AnyType X, const AnyType Y, const AnyType Z )
	{ m_x = X; m_y = Y; m_z = Z; }

	void setX( const AnyType X )
	{ m_x = X; }

	void setY( const AnyType Y )
	{ m_y = Y; }

	void setZ( const AnyType Z )
	{ m_z = Z; }

	const AnyType getX( ) const
	{ return m_x; }

	const AnyType getY( ) const
	{ return m_y;	}

	const AnyType getZ( ) const
	{ return m_z; }

	const AnyType getDistanceFromOrigin( ) const
	{ return sqrt( m_x * m_x + m_y * m_y + m_z * m_z ); } 

	const AnyType getDistanceFrom( const C3DPoint<AnyType> &pt ) const
	{ return sqrt( (m_x - pt.getX( ))*(m_x - pt.getX( )) + 
				   (m_y - pt.getY( ))*(m_y - pt.getY( )) +
				   (m_z - pt.getZ( ))*(m_z - pt.getZ( )) ) };
	
	const AnyType getDistanceFrom( const AnyType x, const AnyType y, const AnyType z ) const
	{ return sqrt( (m_x - x)*(m_x - x) + 
				   (m_y - y)*(m_y - y) +
				   (m_z - z)*(m_z - z) ); }
  private:
	AnyType m_x,
			m_y,
			m_z;
};


template <class AnyType>
const CVector3<AnyType> operator -( const C3DPoint<AnyType> &pt1, const C3DPoint<AnyType> &pt2 )
{
	CVector3<AnyType> v( pt2.getX( ) - pt1.getX( ),
			   pt2.getY( ) - pt1.getY( ),
			   pt2.getZ( ) - pt1.getZ( ) );
	return v;
}

} // end of namespace
#endif