/*		Vector3.inl
 *
 *		Vectors for 3D space.
 *		Coded by Joseph A. Marrero
 *		5/7/04
 */

template <class AnyType>
CVector3<AnyType>::CVector3( )
{
	x = y = z = 0.0;
}

template <class AnyType>
CVector3<AnyType>::CVector3(const AnyType x, const AnyType y, const AnyType z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

template <class AnyType>
CVector3<AnyType>::CVector3( const CVector3<AnyType> &vec )
{
	x = vec.x;
	y = vec.y;
	z = vec.z;
}


template <class AnyType>
CVector3<AnyType>::~CVector3( )
{
}


template <class AnyType>
void CVector3<AnyType>::setX( const AnyType X )
{ x = X; }

template <class AnyType>
void CVector3<AnyType>::setY( const AnyType Y )
{ y = Y; }

template <class AnyType>
void CVector3<AnyType>::setZ( const AnyType Z )
{ z = Z; }

template <class AnyType>
const CVector3<AnyType> &CVector3<AnyType>::operator =(const CVector3<AnyType> &vector)
{
	if( this != &vector )
	{
		x = vector.x;
		y = vector.y;
		z = vector.z;
	}
	return *this;
}

template <class AnyType>
const CVector3<AnyType> &CVector3<AnyType>::operator +=(const CVector3<AnyType> &vector)
{
	if( this != &vector )
	{
		x = x + vector.getX( );
		y = y + vector.getY( );
		z = z + vector.getZ( );
	}
	return *this;
}

template <class AnyType>
const CVector3<AnyType> &CVector3<AnyType>::operator -=(const CVector3<AnyType> &vector)
{
	if( this != &vector )
	{
		x = x - vector.getX( );
		y = y - vector.getY( );
		z = z - vector.getZ( );
	}
    return *this;
}

template <class AnyType>
AnyType CVector3<AnyType>::dotProduct(const CVector3<AnyType> &vector)
{ return (x * vector.x + y * vector.y + z * vector.z); }

template <class AnyType>
CVector3<AnyType> CVector3<AnyType>::crossProduct(const CVector3<AnyType> &vector)
{ 
	return CVector3<AnyType>( y*vector.z - z*vector.y, z*vector.x - x*vector.z, x*vector.y - y*vector.x ); // Return value optimization
}

template <class AnyType>
AnyType CVector3<AnyType>::getMagnitude( ) const
{ return sqrt( x*x + y*y + z*z ); }

template <class AnyType>
void CVector3<AnyType>::normalize( )
{
	AnyType m = getMagnitude( );
	x = x / m;
	y = y / m;
	z = z / m;
}

template <class AnyType>
const bool CVector3<AnyType>::isNormalized( ) const
{ return ( x == 1 && y == 1 && z == 1 ); }

template <class AnyType>
void CVector3<AnyType>::negate()
{ x = -x; y = -y; z = -z; }



// static common vectors
template <class AnyType>
const CVector3<AnyType> CVector3<AnyType>::X_VECTOR( static_cast<AnyType>(1),  static_cast<AnyType>(0), static_cast<AnyType>(0) );

template <class AnyType>
const CVector3<AnyType> CVector3<AnyType>::Y_VECTOR( static_cast<AnyType>(0),  static_cast<AnyType>(1), static_cast<AnyType>(0) );

template <class AnyType>
const CVector3<AnyType> CVector3<AnyType>::Z_VECTOR( static_cast<AnyType>(0), static_cast<AnyType>(0),  static_cast<AnyType>(1) );


template <class AnyType>
CVector3<AnyType> operator +(const CVector3<AnyType> &v1, const CVector3<AnyType> &v2) // Vector Sum
{
	return CVector3<AnyType>( v1.getX( ) + v2.getX( ), v1.getY( ) + v2.getY( ), v1.getZ( ) + v2.getZ( ) ); // Return value optimization
}

template <class AnyType>
CVector3<AnyType> operator -(const CVector3<AnyType> &v1, const CVector3<AnyType> &v2) // Vector Difference
{
	return CVector3<AnyType>( v1.getX( ) - v2.getX( ), v1.getY( ) - v2.getY( ), v1.getZ( ) - v2.getZ( ) ); // Return value optimization
}

template <class AnyType>
CVector3<AnyType> operator *(const CVector3<AnyType> &vector, const AnyType scalar) // Vector-Scalar Multiplication
{
	return CVector3<AnyType>( scalar * vector.getX( ), scalar * vector.getY( ), scalar * vector.getZ( ) ); // Return value optimization
}

template <class AnyType>
CVector3<AnyType> operator *( const AnyType scalar, const CVector3<AnyType> &vector) // Vector-Scalar Multiplication
{
	return CVector3<AnyType>( scalar * vector.getX( ), scalar * vector.getY( ), scalar * vector.getZ( ) ); // Return value optimization
}

template <class AnyType>
CVector3<AnyType> operator /(const CVector3<AnyType> &vector, const AnyType scalar) // Vector- Scalar Division
{
	return CVector3<AnyType>( vector.getX( ) / scalar, vector.getY( ) / scalar, vector.getZ( ) / scalar );  // Return value optimization
}

template <class AnyType>
CVector3<AnyType> operator /(const AnyType scalar, const CVector3<AnyType> &vector)	// Vector- Scalar Division
{
	return CVector3<AnyType>( scalar / vector.getX( ), scalar / vector.getY( ), scalar / vector.getZ( ) );  // Return value optimization
}


template <class AnyType>
AnyType dotProduct( const CVector3<AnyType> &v1, const CVector3<AnyType> &v2 )
{
	return ( v1.getX( ) * v2.getX( ) + v1.getY( ) * v2.getY( ) + v1.getZ( ) * v2.getZ( ) );
}

template <class AnyType>
AnyType operator *( const CVector3<AnyType> &v1, const CVector3<AnyType> &v2 )
{
	return DotProduct( v1, v2 );
}

template <class AnyType>
CVector3<AnyType> crossProduct( const CVector3<AnyType> &v1, const CVector3<AnyType> &v2 )
{
	return CVector3<AnyType>( v1.getY( ) * v2.getZ( ) - v1.getZ( ) * v2.getY( ), 
							  v1.getZ( ) * v2.getX( ) - v1.getX( ) * v2.getZ( ), 
							  v1.getX( ) * v2.getY( ) - v1.getY( ) * v2.getX( ) ); // Return value optimization
}

template <class AnyType>
AnyType angleBetween( const CVector3<AnyType> &v1, const CVector3<AnyType> &v2 ) // in radians
{
	float dp = DotProduct( v1, v2 );
	float v1M = v1.getMagnitude( ),
		  v2M = v2.getMagnitude( );

	return ( (AnyType) acos( dp / ( v1M * v2M ) ) );
}


template <class AnyType>
std::ostream &operator <<( std::ostream &out, const CVector3<AnyType> &vector )
{
	out << "(" << vector.x << ", " << vector.y << ", " << vector.z << ")";
	return out;
}