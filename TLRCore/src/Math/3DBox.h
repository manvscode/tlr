#pragma once
#ifndef _3DBOX_H_
#define _3DBOX_H_
#include <lib.h>
#include "3DPoint.h"

namespace Math {


template <class AnyType = float>
class TLR_API C3DBox
{
  public:
	typedef enum tagOppositePointPair {
		OPPOSITE_POINT_PAIR_1_1 = 0,
		OPPOSITE_POINT_PAIR_1_2 = 6,
		OPPOSITE_POINT_PAIR_2_1 = 1,
		OPPOSITE_POINT_PAIR_2_2 = 7,
		OPPOSITE_POINT_PAIR_3_1 = 2,
		OPPOSITE_POINT_PAIR_3_2 = 4,
		OPPOSITE_POINT_PAIR_4_1 = 3,
		OPPOSITE_POINT_PAIR_4_2 = 5
	} OppositePointPair;

	explicit C3DBox( )
	{
		m_Width = static_cast<AnyType>( 0 );
		m_Length = static_cast<AnyType>( 0 );
		m_Height = static_cast<AnyType>( 0 );

		m_Points[ 0 ] = C3DPoint<AnyType>( );
		m_Points[ 1 ] = C3DPoint<AnyType>( );
		m_Points[ 2 ] = C3DPoint<AnyType>( );
		m_Points[ 3 ] = C3DPoint<AnyType>( );
		m_Points[ 4 ] = C3DPoint<AnyType>( );
		m_Points[ 5 ] = C3DPoint<AnyType>( );
		m_Points[ 6 ] = C3DPoint<AnyType>( );
		m_Points[ 7 ] = C3DPoint<AnyType>( );
	}

	/*	p1 and p2 are opposite points
	 */
	explicit C3DBox( const C3DPoint<AnyType> &p1, const C3DPoint<AnyType> &p2 )
	{
		m_Width = abs( p2.getX( ) - p1.getX( ) );
		m_Length = abs( p2.getZ( ) - p1.getZ( ) );
		m_Height = abs( p2.getY( ) - p1.getY( ) );

		m_Points[ 0 ].set( p1.getX( ), p1.getY( ), p1.getZ( ) );
		m_Points[ 1 ].set( p1.getX( ) + m_Width, p1.getY( ), p1.getZ( ) );
		m_Points[ 2 ].set( p1.getX( ) + m_Width, p1.getY( ), p1.getZ( ) + m_Length );
		m_Points[ 3 ].set( p1.getX( ), p1.getY( ), p1.getZ( ) + m_Length );
		m_Points[ 4 ].set( p2.getX( ) - m_Width, p2.getY( ), p2.getZ( ) - m_Length );
		m_Points[ 5 ].set( p2.getX( ), p2.getY( ), p2.getZ( ) - m_Length );
		m_Points[ 6 ].set( p2.getX( ), p2.getY( ), p2.getZ( ) );
		m_Points[ 7 ].set( p2.getX( ) - m_Width, p2.getY( ), p2.getZ( ) );
	}
	
	
	explicit C3DBox( const C3DPoint<AnyType> &center, const AnyType width, const AnyType height, const AnyType length )
	{
		float halfWidth = width / 2.0f;
		float halfHeight = height / 2.0f;
		float halfLength = length / 2.0f;

		m_Width = width;
		m_Length = length;
		m_Height = height;

		m_Points[ 0 ].set( center.getX( ) - halfWidth, center.getY( ) - halfHeight, center.getZ( ) - halfLength );
		m_Points[ 1 ].set( center.getX( ) + halfWidth, center.getY( ) - halfHeight, center.getZ( ) - halfLength );
		m_Points[ 2 ].set( center.getX( ) + halfWidth, center.getY( ) - halfHeight, center.getZ( ) + halfLength );
		m_Points[ 3 ].set( center.getX( ) - halfWidth, center.getY( ) - halfHeight, center.getZ( ) + halfLength );
		m_Points[ 4 ].set( center.getX( ) - halfWidth, center.getY( ) + halfHeight, center.getZ( ) - halfLength );
		m_Points[ 5 ].set( center.getX( ) + halfWidth, center.getY( ) + halfHeight, center.getZ( ) - halfLength );
		m_Points[ 6 ].set( center.getX( ) + halfWidth, center.getY( ) + halfHeight, center.getZ( ) + halfLength );
		m_Points[ 7 ].set( center.getX( ) - halfWidth, center.getY( ) + halfHeight, center.getZ( ) + halfLength );
	}

	virtual ~C3DBox( )
	{
	}

	AnyType getWidth( ) const
	{ return m_Width; }

	AnyType getLength( ) const
	{ return m_Length; }
	
	AnyType getHeight( ) const
	{ return m_Height; }


	bool isPointWithinXZ( const C3DPoint<AnyType> &pt ) const
	{
		if( pt.getX( ) < m_Points[ 0 ].getX( ) )
			return false;
		if( pt.getZ( ) < m_Points[ 0 ].getZ( ) )
			return false;
		if( pt.getX( ) > m_Points[ 2 ].getX( ) )
			return false;
		if( pt.getZ( ) > m_Points[ 2 ].getZ( ) )
			return false;

		return true;
	}

	bool isPointWithinXY( const C3DPoint<AnyType> &pt ) const
	{
		if( pt.getX( ) < m_Points[ 3 ].getX( ) )
			return false;
		if( pt.getY( ) < m_Points[ 3 ].getY( ) )
			return false;
		if( pt.getX( ) > m_Points[ 6 ].getX( ) )
			return false;
		if( pt.getY( ) > m_Points[ 6 ].getY( ) )
			return false;

		return true;
	}

	bool isPointWithinYZ( const C3DPoint<AnyType> &pt ) const
	{
		if( pt.getZ( ) < m_Points[ 0 ].getZ( ) )
			return false;
		if( pt.getY( ) < m_Points[ 0 ].getY( ) )
			return false;
		if( pt.getZ( ) > m_Points[ 7 ].getZ( ) )
			return false;
		if( pt.getY( ) > m_Points[ 7 ].getY( ) )
			return false;

		return true;
	}

	bool isPointWithin( const C3DPoint<AnyType> &pt ) const
	{
		return ( isPointWithinXZ( pt ) && isPointWithinXY( pt ) && isPointWithinYZ( pt ) );
	}

  protected:
	AnyType m_Width;
	AnyType m_Length;
	AnyType m_Height;
	C3DPoint<AnyType> m_CenterPoint;
	C3DPoint<AnyType> m_Points[ 8 ];
};


} // end of namespace
#endif