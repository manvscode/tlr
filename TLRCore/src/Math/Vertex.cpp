#include "Vertex.h"


namespace Math {


//bool operator<( const CompleteVertex &v1, const CompleteVertex &v2 )
//{
//	if( v1.x < v2.x )
//		return true;
//	else if( v1.x == v2.x && v1.z < v2.z )
//		return true;
//	else if( v1.x == v2.x && v1.z == v2.z && v1.y < v2.y )
//		return true;
//	else 
//		return false;
//}


bool operator<( const GameVertex &v1, const GameVertex &v2 )
{
	if( v1.x < v2.x )
		return true;
	else if( v1.x == v2.x && v1.z < v2.z )
		return true;
	else if( v1.x == v2.x && v1.z == v2.z && v1.y < v2.y )
		return true;
	else 
		return false;
}


} // end of namespace