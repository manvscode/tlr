#pragma once
#ifndef __VERTEX_H__
#define __VERTEX_H__

#include <lib.h>



namespace Math {

#pragma pack(push,1)

TLR_API typedef struct tagVertex {
	union {
		struct {
			float x, y, z;
		};
		float v[ 3 ];
	};
} Vertex;


//// GL_T2F_C4F_N3F_V3F
//TLR_API typedef struct tagCompleteVertex {
//
//	union {
//		struct {
//			float u;
//			float v;
//		};
//		float texture[ 2 ];
//	};
//
//	union {
//		struct {
//			float r, g, b, a;
//		};
//		float color[ 4 ];
//	};
//
//	union {
//		struct {
//			float nx, ny, nz;
//		};
//		float normal[ 3 ];
//	};
//
//	union {
//		struct {
//			float x, y, z;
//		};
//		float vertex[ 3 ];
//	};
//
//} CompleteVertex;


// GL_T2F_N3F_V3F
TLR_API typedef struct tagGameVertex {

	union {
		struct {
			float u;
			float v;
		};
		float texture[ 2 ];
	};

	union {
		struct {
			float nx, ny, nz;
		};
		float normal[ 3 ];
	};

	union {
		struct {
			float x, y, z;
		};
		float vertex[ 3 ];
	};

} GameVertex;
#pragma pack(pop)
typedef Vertex VERTEX;

//TLR_API bool operator<( const CompleteVertex &v1, const CompleteVertex &v2 );
TLR_API bool operator<( const GameVertex &v1, const GameVertex &v2 );

} // end of namespace
#endif