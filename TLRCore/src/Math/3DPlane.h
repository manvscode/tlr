#ifndef __3DPLANE_H__
#define __3DPLANE_H__

#include <lib.h>
#include "3DPoint.h"


namespace Math {


template <class AnyType>
class TLR_API C3DPlane
{
  public:

	explicit C3DPlane( )
	{}
	explicit C3DPlane( const C3DPoint<AnyType> &p1, const C3DPoint<AnyType> &p2, const C3DPoint<AnyType> &p3 )
	{
		CVector3 v1( p2 - p1 );
		CVector3 v2( p3 - p2 );
		vNormal = CrossProduct( v1, v2 ); //v1 >< v2;
		pt = p1;
	}
	explicit C3DPlane( const C3DPoint<AnyType> &p, const CVector3<AnyType> &normal )
	{
		pt = p;
		vNormal = normal;
	}


	virtual ~C3DPlane( )
	{
	}
	


  private:
	C3DPoint pt;
	CVector3 vNormal;
};

} // end of namespace
#endif