#pragma once
#ifndef _VECTOR3_H_
#define _VECTOR3_H_
/*		Vector3.h
 *
 *		Vectors for 3D space.
 *		Coded by Joseph A. Marrero
 *		5/7/04
 */
#include <cmath>
#include <ostream>

namespace Math {

template <class AnyType = float>
class CVector3
{

  private:
	union {
		struct{
			AnyType x, y, z;
		};
		AnyType data[ 3 ];
	};

  public:


	explicit CVector3( );
	CVector3( const AnyType x, const AnyType y, const AnyType z );
	CVector3( const CVector3<AnyType> &vec ); // copy constructor
	virtual ~CVector3( );

	// Get components 
	const AnyType getX( ) const;
	const AnyType getY( ) const;
	const AnyType getZ( ) const;
	void setX( const AnyType X );
	void setY( const AnyType Y );
	void setZ( const AnyType Z );
	// Vector Operations
	const CVector3 &operator =( const CVector3 &vector );
	const CVector3 &operator +=( const CVector3 &vector );
	const CVector3 &operator -=( const CVector3 &vector );
	AnyType dotProduct( const CVector3 &vector );		// Dot Product
	CVector3 crossProduct( const CVector3 &vector );	// Cross Product
	AnyType getMagnitude( ) const;							// Get Vector Magnitude
	void normalize( );								// Normalize Vector
	const bool isNormalized( ) const;
	void negate( );									// Negate Vector


	AnyType &operator[]( int idx );
	const AnyType &operator[]( int idx ) const;

	static const CVector3 X_VECTOR;
	static const CVector3 Y_VECTOR;
	static const CVector3 Z_VECTOR;
};

template <class AnyType>
inline const AnyType CVector3<AnyType>::getX( ) const
{ return x; }

template <class AnyType>
inline const AnyType CVector3<AnyType>::getY( ) const
{ return y; }

template <class AnyType>
inline const AnyType CVector3<AnyType>::getZ( ) const
{ return z; }

template <class AnyType>
inline AnyType &CVector3<AnyType>::operator[]( int idx )
{ return data[ idx ]; }

template <class AnyType>
inline const AnyType &CVector3<AnyType>::operator[]( int idx ) const
{ return data[ idx ]; }

template <class AnyType>
CVector3<AnyType> operator +( const CVector3<AnyType> &v1, const CVector3<AnyType> &v2 );				// Vector Sum
template <class AnyType>
CVector3<AnyType> operator -( const CVector3<AnyType> &v1, const CVector3<AnyType> &v2 );				// Vector Difference
template <class AnyType>
CVector3<AnyType> operator *( const CVector3<AnyType> &vector, const AnyType scalar );			// Vector-Scalar Multiplication
template <class AnyType>
CVector3<AnyType> operator *( const CVector3<AnyType> scalar, const CVector3<AnyType> &vector );			// Vector-Scalar Multiplication
template <class AnyType>
CVector3<AnyType> operator /( const CVector3<AnyType> &vector, const AnyType scalar );			// Vector- Scalar Division
template <class AnyType>
CVector3<AnyType> operator /( const CVector3<AnyType> &vector, const AnyType scalar );			// Vector- Scalar Division

template <class AnyType>
AnyType dotProduct( const CVector3<AnyType> &v1, const CVector3<AnyType> &v2 );	
template <class AnyType>
AnyType operator *( const CVector3<AnyType> &v1, const CVector3<AnyType> &v2 );		

template <class AnyType>
CVector3<AnyType> crossProduct( const CVector3<AnyType> &v1, const CVector3<AnyType> &v2 );		
//template <class AnyType>
//CVector3<AnyType> operator><( const CVector3<AnyType> &v1, const CVector3<AnyType> &v2 );



template <class AnyType>
AnyType angleBetween( const CVector3<AnyType> &v1, const CVector3<AnyType> &v2 );



template <class AnyType>
std::ostream &operator <<( std::ostream &out, const CVector3<AnyType> &vector );


#include "Vector3.inl"
} // end of namespace
#endif