#pragma once
#ifndef _MATRIX4X4_H_
#define _MATRIX4X4_H_
#include <lib.h>
#include <ios>
#include <iostream>
#include "Vector3.h"
using namespace std;

namespace Math {

template <class AnyType = float>
class TLR_API CMatrix4x4
{
  public:

	CMatrix4x4( )
	{
		m = new AnyType[ 16 ];
	}

	CMatrix4x4( const AnyType *Matrix )
	{
		m = new AnyType[ 16 ];

		for( unsigned int i = 0; i < 16; i++ )
			m[ i ] = Matrix[ i ];
	}

	CMatrix4x4( const CMatrix4x4<AnyType> &matrix )
	{
		CMatrix4x4( matrix.m );
	}

	virtual ~CMatrix4x4( )
	{ delete [] m; }

	inline const AnyType *getArray( ) const
	{	return m; }

	inline const AnyType operator[]( size_t i ) const
	{ return m[ i ]; }
	inline AnyType operator[]( size_t i )
	{ return m[ i ]; }

	ostream &operator<<( ostream &out )
	{
		for( int i = 0; i < 4; i++ )
			for( int j = 0; j < 4; j++ )
			{
				out << setw(3);
				out << mat[ i + 4 * j ] << "" << endl;
			}
	}

	const CMatrix4x4<AnyType> &operator=( const CMatrix4x4<AnyType> &other )
	{
		if( this != &other )
		{
			delete [] m; // delete existing matrix array
			CMatrix4x4( other );
		}

		return *this;
	}

/*	const CVector3<AnyType> operator*( const CVector3 &v )
	{
		CVector3<AnyType> r;

		r.setX( v.getX( ) * m[ 0 ] + v.getY( ) * m[ 1 ] + v.getZ( ) * m[ 2 ] + m[ 3 ] );
		r.setY( v.getX( ) * m[ 4 ] + v.getY( ) * m[ 5 ] + v.getZ( ) * m[ 6 ] + m[ 7 ] );
		r.setZ( v.getX( ) * m[ 8 ] + v.getY( ) * m[ 9 ] + v.getZ( ) * m[ 10 ] + m[ 11 ] );

		return r;
	}*/



	void transpose( )
	{
		//for( int i = 1; i <= 3; i++ )
		//	for( int j = 1; j <= 3; j++ )
		//		if( i !=j ) swap( m[] )
		swap( m[1], m[4] );
		swap( m[2], m[8] );
		swap( m[3], m[12] );
		swap( m[6], m[9] );
		swap( m[7], m[13] );
		swap( m[11], m[14] );
	}
	
	void adjoint( )
	{
		//calc determinant
		float d1 = ( m[5] * ( m[10] * m[15] - m[14] * m[11] ) - 
		 			 m[6] * ( m[9] * m[15] - m[13] * m[11] ) +
				 	 m[7] * ( m[9] * m[14] - m[13] * m[10] )
					);
		float d2 = ( m[4] * ( m[10] * m[15] - m[14] * m[11] ) - 
				 	 m[6] * ( m[8] * m[15] - m[12] * m[11] ) +
					 m[7] * ( m[8] * m[14] - m[12] * m[10] )
					);
		float d3 = ( m[4] * ( m[9] * m[15] - m[13] * m[11] ) - 
					 m[5] * ( m[8] * m[15] - m[12] * m[11] ) +
					 m[7] * ( m[8] * m[13] - m[12] * m[9] )
					);
		float d4 = ( m[4] * ( m[9] * m[14] - m[13] * m[10] ) - 
					 m[5] * ( m[8] * m[14] - m[12] * m[10] ) +
					 m[6] * ( m[8] * m[13] - m[12] * m[9] )
					);

		//float det = m[0]*d1 - m[1]*d2 + m[2]*d3 - m[3]*d4;

		//Computer cofactors
		float cofactors[ 16 ];
		
											//  | 0  1  2  3  |
											//  | 4  5  6  7  |   / 11 12 13 \
											//  | 8  9  10 11 |   | 21 22 23 |
											//  | 12 13 14 15 |   \ 31 32 33 /
	
		// Formula for determinant of 3x3 matrix A:
		// det(A) = a11*a22*a33 - a11*a32*a23 - a12*a21*a33 + a12*a31*a23 + a13*a21*a32 - a13*a31*a22
		// This is used below...
		cofactors[0] = d1; //m[5]*m[10]*m[15] - m[5]*m[14]*m[11] - m[6]*m[9]*m[15] + m[6]*m[13]*m[11] + m[7]*m[9]*m[14] - m[7]*m[13]*m[10];
		cofactors[1] = -d2; //m[4]*m[10]*m[15] - m[4]*m[14]*m[11] - m[6]*m[8]*m[15] + m[6]*m[12]*m[11] + m[7]*m[8]*m[14] - m[7]*m[12]*m[10];
		cofactors[2] = d3; //m[4]*m[9]*m[15] - m[4]*m[13]*m[11] - m[5]*m[8]*m[15] + m[5]*m[12]*m[11] + m[7]*m[8]*m[13] - m[7]*m[12]*m[9];
		cofactors[3] = -d4;

		// Could be improved... repeated mults
		cofactors[4] = -(m[1] * (m[10]*m[15]-m[14]*m[11]) - m[2] * (m[9]*m[15]-m[13]*m[11]) + m[3] * (m[9]*m[14]-m[13]*m[10]));
		cofactors[5] = (m[0] * (m[10]*m[15]-m[14]*m[11]) - m[2] * (m[8]*m[15]-m[12]*m[11]) + m[3] * (m[8]*m[14]-m[12]*m[10]));
		cofactors[6] = -(m[0] * (m[9]*m[15]-m[13]*m[11]) - m[1] * (m[8]*m[15]-m[12]*m[11]) + m[3] * (m[8]*m[13]-m[12]*m[9]));
		cofactors[7] = (m[0] * (m[9]*m[14]-m[13]*m[10]) - m[1] * (m[8]*m[14]-m[12]*m[10]) + m[2] * (m[8]*m[13]-m[12]*m[9]));
		cofactors[8] = (m[1] * (m[6]*m[15]-m[14]*m[7]) - m[2] * (m[5]*m[15]-m[13]*m[7]) + m[3] * (m[5]*m[14]-m[13]*m[6]));
		cofactors[9] = -(m[0] * (m[6]*m[15]-m[14]*m[7]) - m[2] * (m[4]*m[15]-m[12]*m[7]) + m[3] * (m[4]*m[14]-m[12]*m[6]));
		cofactors[10] = (m[0] * (m[5]*m[15]-m[13]*m[7]) - m[1] * (m[4]*m[15]-m[12]*m[7]) + m[3] * (m[4]*m[13]-m[12]*m[5]));
		cofactors[11] = -(m[0] * (m[5]*m[14]-m[13]*m[6]) - m[1] * (m[4]*m[14]-m[12]*m[6]) + m[2] * (m[4]*m[13]-m[12]*m[5]));
		cofactors[12] = -(m[1] * (m[6]*m[11]-m[10]*m[7]) - m[2] * (m[5]*m[11]-m[9]*m[7]) + m[3] * (m[5]*m[10]-m[9]*m[6]));
		cofactors[13] = (m[0] * (m[6]*m[11]-m[10]*m[7]) - m[2] * (m[4]*m[11]-m[8]*m[7]) + m[3] * (m[4]*m[10]-m[8]*m[6]));
		cofactors[14] = -(m[0] * (m[5]*m[11]-m[9]*m[7]) - m[1] * (m[4]*m[11]-m[8]*m[7]) + m[3] * (m[4]*m[9]-m[8]*m[5]));
		cofactors[15] = (m[0] * (m[5]*m[10]-m[9]*m[6]) - m[1] * (m[4]*m[10]-m[8]*m[6]) + m[2] * (m[4]*m[9]-m[8]*m[5]));

		
	
		transpose( );	
	}

	void invert( )
	{
		//if( determinant( ) == 0 ) return;

		//calc determinant
		float d1 = ( m[5] * ( m[10] * m[15] - m[14] * m[11] ) - 
					 m[6] * ( m[9] * m[15] - m[13] * m[11] ) +
					 m[7] * ( m[9] * m[14] - m[13] * m[10] )
			);
		float d2 = ( m[4] * ( m[10] * m[15] - m[14] * m[11] ) - 
					 m[6] * ( m[8] * m[15] - m[12] * m[11] ) +
					 m[7] * ( m[8] * m[14] - m[12] * m[10] )
			);
		float d3 = ( m[4] * ( m[9] * m[15] - m[13] * m[11] ) - 
					 m[5] * ( m[8] * m[15] - m[12] * m[11] ) +
					 m[7] * ( m[8] * m[13] - m[12] * m[9] )
			);
		float d4 = ( m[4] * ( m[9] * m[14] - m[13] * m[10] ) - 
				 	 m[5] * ( m[8] * m[14] - m[12] * m[10] ) +
					 m[6] * ( m[8] * m[13] - m[12] * m[9] )
			);

		float det = m[0]*d1 - m[1]*d2 + m[2]*d3 - m[3]*d4;
		
		if( det == 0 ) 
			return;
	
		

		//Computer cofactors
		float cofactors[ 16 ];

		//  | 0  1  2  3  |
		//  | 4  5  6  7  |   / 11 12 13 \
		//  | 8  9  10 11 |   | 21 22 23 |
		//  | 12 13 14 15 |   \ 31 32 33 /

		// Formula for determinant of 3x3 matrix A:
		// det(A) = a11*a22*a33 - a11*a32*a23 - a12*a21*a33 + a12*a31*a23 + a13*a21*a32 - a13*a31*a22
		// This is used below...
		cofactors[0] = d1; //m[5]*m[10]*m[15] - m[5]*m[14]*m[11] - m[6]*m[9]*m[15] + m[6]*m[13]*m[11] + m[7]*m[9]*m[14] - m[7]*m[13]*m[10];
		cofactors[1] = -d2; //m[4]*m[10]*m[15] - m[4]*m[14]*m[11] - m[6]*m[8]*m[15] + m[6]*m[12]*m[11] + m[7]*m[8]*m[14] - m[7]*m[12]*m[10];
		cofactors[2] = d3; //m[4]*m[9]*m[15] - m[4]*m[13]*m[11] - m[5]*m[8]*m[15] + m[5]*m[12]*m[11] + m[7]*m[8]*m[13] - m[7]*m[12]*m[9];
		cofactors[3] = -d4;

		// Could be improved... repeated mults
		cofactors[4] = -(m[1] * (m[10]*m[15]-m[14]*m[11]) - m[2] * (m[9]*m[15]-m[13]*m[11]) + m[3] * (m[9]*m[14]-m[13]*m[10]));
		cofactors[5] = (m[0] * (m[10]*m[15]-m[14]*m[11]) - m[2] * (m[8]*m[15]-m[12]*m[11]) + m[3] * (m[8]*m[14]-m[12]*m[10]));
		cofactors[6] = -(m[0] * (m[9]*m[15]-m[13]*m[11]) - m[1] * (m[8]*m[15]-m[12]*m[11]) + m[3] * (m[8]*m[13]-m[12]*m[9]));
		cofactors[7] = (m[0] * (m[9]*m[14]-m[13]*m[10]) - m[1] * (m[8]*m[14]-m[12]*m[10]) + m[2] * (m[8]*m[13]-m[12]*m[9]));
		cofactors[8] = (m[1] * (m[6]*m[15]-m[14]*m[7]) - m[2] * (m[5]*m[15]-m[13]*m[7]) + m[3] * (m[5]*m[14]-m[13]*m[6]));
		cofactors[9] = -(m[0] * (m[6]*m[15]-m[14]*m[7]) - m[2] * (m[4]*m[15]-m[12]*m[7]) + m[3] * (m[4]*m[14]-m[12]*m[6]));
		cofactors[10] = (m[0] * (m[5]*m[15]-m[13]*m[7]) - m[1] * (m[4]*m[15]-m[12]*m[7]) + m[3] * (m[4]*m[13]-m[12]*m[5]));
		cofactors[11] = -(m[0] * (m[5]*m[14]-m[13]*m[6]) - m[1] * (m[4]*m[14]-m[12]*m[6]) + m[2] * (m[4]*m[13]-m[12]*m[5]));
		cofactors[12] = -(m[1] * (m[6]*m[11]-m[10]*m[7]) - m[2] * (m[5]*m[11]-m[9]*m[7]) + m[3] * (m[5]*m[10]-m[9]*m[6]));
		cofactors[13] = (m[0] * (m[6]*m[11]-m[10]*m[7]) - m[2] * (m[4]*m[11]-m[8]*m[7]) + m[3] * (m[4]*m[10]-m[8]*m[6]));
		cofactors[14] = -(m[0] * (m[5]*m[11]-m[9]*m[7]) - m[1] * (m[4]*m[11]-m[8]*m[7]) + m[3] * (m[4]*m[9]-m[8]*m[5]));
		cofactors[15] = (m[0] * (m[5]*m[10]-m[9]*m[6]) - m[1] * (m[4]*m[10]-m[8]*m[6]) + m[2] * (m[4]*m[9]-m[8]*m[5]));

		
		
		for( int i = 0; i < 16; i++ )
			m[i] = cofactors[i] * (1.0f / det);

		transpose( );

	}

	inline float determinant( )
	{
		float d1 = ( m[5] * ( m[10] * m[15] - m[14] * m[11] ) - 
					 m[6] * ( m[9] * m[15] - m[13] * m[11] ) +
					 m[7] * ( m[9] * m[14] - m[13] * m[10] )
				   );
		float d2 = ( m[4] * ( m[10] * m[15] - m[14] * m[11] ) - 
				 	 m[6] * ( m[8] * m[15] - m[12] * m[11] ) +
					 m[7] * ( m[8] * m[14] - m[12] * m[10] )
				   );
		float d3 = ( m[4] * ( m[9] * m[15] - m[13] * m[11] ) - 
					 m[5] * ( m[8] * m[15] - m[12] * m[11] ) +
					 m[7] * ( m[8] * m[13] - m[12] * m[9] )
				   );
		float d4 = ( m[4] * ( m[9] * m[14] - m[13] * m[10] ) - 
					 m[5] * ( m[8] * m[14] - m[12] * m[10] ) +
					 m[6] * ( m[8] * m[13] - m[12] * m[9] )
				   );

		return m[0]*d1 - m[1]*d2 + m[2]*d3 - m[3]*d4;
	}

  private:
	inline void swap( float &x, float &y )
	{
		float tmp = x;
		x = y;
		y = tmp;
		
		// this may be faster; avoids tmp var
		//x += y; y = x - y; x -= y;

	}

	AnyType *m;
};





template <class AnyType>
ostream &operator <<( ostream &out, CMatrix4x4<AnyType> &mat );

template <class AnyType>
const CMatrix4x4<AnyType> &operator *( CMatrix4x4<AnyType> &m );




template <class AnyType>
ostream &operator <<( ostream &out, CMatrix4x4<AnyType> &mat )
{


	for( int i = 0; i < 4; i++ )
	{
		cout << "| ";
		for( int j = 0; j < 4; j++ )
		{
			out.width(3);
			out << mat[ i + 4 * j ] << " ";
		}
		out << "|" << endl;
	}
	
	return out;
}

template <class AnyType>
const CMatrix4x4<AnyType> &operator *( CMatrix4x4<AnyType> &m )
{
	

	return r;
}


} // end of namespace
#endif