#include "StdAfx.h"
#include <GL/glew.h>
#include <GL/gl.h>
#include "Renderer.h"
#include <Core.h>
#include <Game/Map/SplatMap.h>
#include <Game/Map/SplatMapChunk.h>
#include <Game/Map/MapCell.h>
#include "MapEditorDoc.h"
#include "RenderingGlobals.h"


CRenderer *CRenderer::m_pInstance = NULL;

CRenderer *CRenderer::getInstance( )
{
	if( !m_pInstance )
		m_pInstance = new CRenderer( );
	
	return m_pInstance;
}

CRenderer::CRenderer( )
{
	pQuadric = gluNewQuadric( );
	gluQuadricNormals( pQuadric, GLU_SMOOTH );
	gluQuadricTexture( pQuadric, true );
}

CRenderer::~CRenderer( )
{
	gluDeleteQuadric( pQuadric );
}


void CRenderer::render( )
{
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );


	glPushAttrib( GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_TEXTURE_BIT | GL_LINE_BIT | GL_CURRENT_BIT );
		glDisable( GL_LIGHTING );
		glDisable( GL_TEXTURE_2D );
		glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );

		// render map cell boundaries...
		if( pDoc->isRenderBoundaryModeSet( ) )
		{
			for( unsigned int x = 0; x < Core::pMap->getNumberOfXCells( ); x++ )
				for( unsigned int y = 0; y < Core::pMap->getNumberOfZCells( ); y++ )
					Core::pMap->getMapCell( x, y )->renderBounds( 1.0f, 0.0f, 0.0f, 0.25f );
		}
		// render map boundaries....
		glPushMatrix( );
			glColor3f( 0, 0, 1.0f );
			glLineWidth( 2.0f );
			glBegin( GL_LINE_STRIP );
				glVertex3f( -Core::pMap->getWidth( ) / 2.0f, 0, -Core::pMap->getLength( ) / 2.0f );
				glVertex3f( Core::pMap->getWidth( ) / 2.0f, 0, -Core::pMap->getLength( ) / 2.0f );
				glVertex3f( Core::pMap->getWidth( ) / 2.0f, 0, Core::pMap->getLength( ) / 2.0f );
				glVertex3f( -Core::pMap->getWidth( ) / 2.0f, 0, Core::pMap->getLength( ) / 2.0f );
				glVertex3f( -Core::pMap->getWidth( ) / 2.0f, 0, -Core::pMap->getLength( ) / 2.0f );
			glEnd( );
		glPopMatrix( );
	glPopAttrib( );


	glColor3f( 1.0f, 1.0f, 1.0f );
	
	
	Core::pMap->render( );
	//gluSphere( pQuadric, 20, 12, 10 );

	//cubeTest( );


	GLenum error = glGetError( );
	if( error != GL_NO_ERROR )
	{
		const GLubyte *pErrorString = gluErrorString( error );
		ASSERT( false );
	}
}

