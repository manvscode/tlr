#pragma once
#include "afxwin.h"
#include "CoolButton.h"

// CAddTileSetDlg dialog
typedef enum tagTileSetType {
	TST_TERRAIN,
	TST_ENTITY,
	TST_BUILDINGS
} TileSetType;



class CAddTileSetDlg : public CDialog
{
	DECLARE_DYNAMIC(CAddTileSetDlg)
protected:
	CComboBox m_TypeOfTileSetCombo;
	CString m_FolderPath;
	TileSetType m_TileSetType;
	CCoolButton m_Buttons[ 3 ];

public:
	CAddTileSetDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddTileSetDlg();

// Dialog Data
	enum { IDD = IDD_ADDTILESET_DIALOG };



protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBrowseButton();
	CString &getTileSetPath( );
	TileSetType getTileSetType( ) const;
public:
	afx_msg void OnCbnSelchangeTypeOfTilesetCombo();
};
