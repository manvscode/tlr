#include "GLRCSharer.h"

CGLRCSharer *CGLRCSharer::m_pInstance = NULL;


CGLRCSharer::CGLRCSharer( )
{
}

CGLRCSharer::~CGLRCSharer( )
{
}

CGLRCSharer *CGLRCSharer::getInstance( )
{
	if( !m_pInstance )
		m_pInstance = new CGLRCSharer( );
	return m_pInstance;
}

void CGLRCSharer::addRC( HGLRC hGLRC )
{
	m_RCArray.Add( hGLRC );

	if( m_RCArray.GetSize() >= 4 )
		shareContexts( );
}

void CGLRCSharer::shareContexts( )
{
	wglShareLists( m_RCArray[ 0 ], m_RCArray[ 1 ] );
	wglShareLists( m_RCArray[ 0 ], m_RCArray[ 2 ] );
	wglShareLists( m_RCArray[ 0 ], m_RCArray[ 3 ] );
	wglShareLists( m_RCArray[ 1 ], m_RCArray[ 2 ] );
	wglShareLists( m_RCArray[ 1 ], m_RCArray[ 3 ] );
	wglShareLists( m_RCArray[ 2 ], m_RCArray[ 3 ] );
}