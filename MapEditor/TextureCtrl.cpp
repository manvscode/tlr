// TextureCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "MapEditor.h"
#include "TextureCtrl.h"
#include <ResourceManagement/TextureManager/TextureManager.h>
#include <ResourceManagement/TextureManager/Texture2D.h>
#include <ImageIO.h>
#include "mfc_util.h"



// CTextureCtrl

IMPLEMENT_DYNAMIC(CTextureCtrl, CWnd)

CTextureCtrl::CTextureCtrl( )
: m_bBitmapDataAllocated(false), m_uiTextureID(0)
{

}

CTextureCtrl::~CTextureCtrl()
{
	DestroyWindow( );

	//if( m_bBitmapDataAllocated )
	//	delete [] m_BitmapData.bmBits;

	//if( m_hBitmap )
	//	DeleteObject( m_hBitmap );
}


BEGIN_MESSAGE_MAP(CTextureCtrl, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()



// CTextureCtrl message handlers



int CTextureCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	initialize( );

	return 0;
}

void CTextureCtrl::initialize( )
{
	BOOL success = m_DropWndTarget.Register( this );

	if( !success )
		MessageBox( _T("Ole Register Drop Target Failed") );  

	reset( );
}

/*
 *	void setTexture( const BITMAP &bmp )
 *
 *	Copies the bitmap bmp and sets this control to it.
 */
void CTextureCtrl::setTexture( const BITMAP &bmp, unsigned int textureID /*= 0*/ )
{
	if( m_bBitmapDataAllocated )
		delete [] m_BitmapData.bmBits;


	CRect rect;
	GetClientRect( rect );

	m_BitmapData = bmp;
	unsigned int size = rect.Width( ) * rect.Height( ) * (bmp.bmBitsPixel >> 3);

	m_BitmapData.bmBits = new BYTE[ size ];
	m_BitmapData.bmPlanes = 1;
	//memcpy( m_BitmapData.bmBits, bmp.bmBits, size );
	ImageIO::resizeImage( bmp.bmWidth, bmp.bmHeight, static_cast<BYTE*>(bmp.bmBits),
						  rect.Width( ), rect.Height( ), static_cast<BYTE*>(m_BitmapData.bmBits),
						  bmp.bmBitsPixel, ImageIO::ALG_NEARESTNEIGHBOR );
	m_BitmapData.bmWidth = rect.Width( );
	m_BitmapData.bmHeight = rect.Height( );
	ImageIO::flipYImage( m_BitmapData.bmWidth, m_BitmapData.bmHeight, static_cast<BYTE *>(m_BitmapData.bmBits), static_cast<BYTE *>(m_BitmapData.bmBits), m_BitmapData.bmBitsPixel >> 3 );


	m_bBitmapDataAllocated = true;

	DeleteObject( m_hBitmap );
	m_hBitmap = CreateBitmap( m_BitmapData.bmWidth, m_BitmapData.bmHeight, m_BitmapData.bmPlanes, m_BitmapData.bmBitsPixel, m_BitmapData.bmBits );

	// save the texture id ...
	m_uiTextureID = textureID;

	Invalidate( TRUE );
}

void CTextureCtrl::setTexture( unsigned int textureID )
{
	ResourceManagement::CTextureManager *pMananger = ResourceManagement::CTextureManager::getInstance( );
	ResourceManagement::CTexture2D *pTexture = pMananger->find( textureID );

	if( m_bBitmapDataAllocated )
		delete [] m_BitmapData.bmBits;

	CRect rect;
	GetClientRect( rect );

	unsigned int size = rect.Width( ) * rect.Height( ) * pTexture->byteCount( );


	m_BitmapData.bmBits = new BYTE[ size ];
	m_BitmapData.bmBitsPixel = pTexture->bitCount( );
	m_BitmapData.bmWidthBytes = rect.Width( ) * pTexture->byteCount( );
	m_BitmapData.bmPlanes = 1;

	ImageIO::resizeImage( pTexture->width( ), pTexture->height( ), pTexture->bitmap( ),
						  rect.Width( ), rect.Height( ), static_cast<BYTE*>(m_BitmapData.bmBits),
						  pTexture->bitCount( ), ImageIO::ALG_NEARESTNEIGHBOR );
	m_BitmapData.bmWidth = rect.Width( );
	m_BitmapData.bmHeight = rect.Height( );

	ImageIO::swapRBinRGB( m_BitmapData.bmWidth, m_BitmapData.bmHeight, pTexture->byteCount( ), static_cast<BYTE *>(m_BitmapData.bmBits) );
	ImageIO::flipYImage( m_BitmapData.bmWidth, m_BitmapData.bmHeight, static_cast<BYTE *>(m_BitmapData.bmBits), static_cast<BYTE *>(m_BitmapData.bmBits), pTexture->byteCount( ) );

	m_bBitmapDataAllocated = true;

	DeleteObject( m_hBitmap );
	m_hBitmap = CreateBitmap( m_BitmapData.bmWidth, m_BitmapData.bmHeight, m_BitmapData.bmPlanes, m_BitmapData.bmBitsPixel, m_BitmapData.bmBits );
	
	// save the texture id ...
	m_uiTextureID = textureID;

	Invalidate( TRUE );
}

/*
*	void reset( )
*
*	Resets the control to use no texture.
*/
void CTextureCtrl::reset( )
{
	if( m_bBitmapDataAllocated )
	{
		delete [] m_BitmapData.bmBits;
		DeleteObject( m_hBitmap );
		m_bBitmapDataAllocated = false;
	}

	m_hBitmap = LoadBitmap( AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_TEXTURECTRL_BITMAP) );
	m_BitmapData.bmWidth = 84;
	m_BitmapData.bmHeight = 84;
	m_uiTextureID = 0;
}

void CTextureCtrl::OnPaint( )
{
	CPaintDC dc(this); // device context for painting
	// Do not call CWnd::OnPaint() for painting messages
	CRect rect;
	CDC *pDC = &dc; //GetDC( );
	CDC bmpDC;
	bmpDC.CreateCompatibleDC( &dc );

	CBitmap *bmp = CBitmap::FromHandle( m_hBitmap );

	CBitmap *pOldBmp = bmpDC.SelectObject( bmp );


	GetClientRect( rect );


	//pDC->StretchBlt( 0, 0, rect.Width( ), rect.Height( ), &bmpDC, 0, 0, m_BitmapData.bmWidth, m_BitmapData.bmHeight, SRCCOPY );
	pDC->BitBlt( 0, 0, rect.Width( ),rect.Height( ), &bmpDC, 0, 0, SRCCOPY | NOMIRRORBITMAP );
	bmpDC.SelectObject( pOldBmp );
	pDC->DrawEdge( rect, EDGE_SUNKEN, BF_FLAT | BF_SOFT );
	bmpDC.DeleteDC( );
	bmp->Detach( );
}


void CTextureCtrl::OnDestroy()
{
	CWnd::OnDestroy();

	if( m_bBitmapDataAllocated )
		delete [] m_BitmapData.bmBits;

	if( m_hBitmap )
		DeleteObject( m_hBitmap );
}
