// 2DMapEditorView.h : interface of the CPerspectiveView class
//
#include "OpenGLWnd.h"

#pragma once


class CPerspectiveView : public COpenGLWnd
{
protected: // create from serialization only
	CPerspectiveView();
	DECLARE_DYNCREATE(CPerspectiveView)



	int	m_lastMouseX,
		m_lastMouseY;
	CPoint m_ptLeftDownPos;

	double m_Modelview[ 16 ];
	double m_Projection[ 16 ];
	int m_Viewport[ 4 ];


public:
	CMapEditorDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	virtual bool InitOpenGL( );
// Implementation
public:
	virtual ~CPerspectiveView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
};

#ifndef _DEBUG  // debug version in 2DMapEditorView.cpp
inline CMapEditorDoc* CPerspectiveView::GetDocument() const
   { return reinterpret_cast<CMapEditorDoc*>(m_pDocument); }
#endif

