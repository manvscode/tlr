// MissionObjectiveDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MapEditor.h"
#include "MapEditorDoc.h"
#include "MissionObjectiveDlg.h"
#include "StringConverter.h"
#include <ImageIO.h>

// CMissionObjectiveDlg dialog

IMPLEMENT_DYNAMIC(CMissionObjectiveDlg, CDialog)

CMissionObjectiveDlg::CMissionObjectiveDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMissionObjectiveDlg::IDD, pParent)
{
	memset( &m_bmpImage, 0, sizeof(BITMAP) );
	memset( &m_bmpSmallImage, 0, sizeof(BITMAP) );
	
}

CMissionObjectiveDlg::~CMissionObjectiveDlg()
{
	DeleteObject( m_hbmpMissionObjectiveImage );
	if( m_bmpSmallImage.bmBits != NULL )
		delete [] m_bmpSmallImage.bmBits;
}

void CMissionObjectiveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MISSION_OBJECTIVE_EDIT, m_MissionObjective);
	DDX_Control(pDX, IDC_MISSIONBITMAP_STATIC, m_MissionObjectiveImageCtrl);
}


BEGIN_MESSAGE_MAP(CMissionObjectiveDlg, CDialog)

	ON_BN_CLICKED(IDOK, &CMissionObjectiveDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BROWSE_BUTTON, &CMissionObjectiveDlg::OnBnClickedBrowseButton)
	ON_BN_CLICKED(IDCANCEL, &CMissionObjectiveDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CMissionObjectiveDlg message handlers

BOOL CMissionObjectiveDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );

	m_Buttons[ 0 ].SubclassDlgItem( IDOK, this );
	m_Buttons[ 1 ].SubclassDlgItem( IDCANCEL, this );
	m_Buttons[ 2 ].SubclassDlgItem( IDC_BROWSE_BUTTON, this );


	m_MissionObjective.SetWindowText( pDoc->getMissionObjective( ) );
	
	m_bmpImage = pDoc->getMissionObjectiveBitmap( );
	m_bmpSmallImage.bmBits = NULL;


	refreshMissionObjectiveImage( );

	return TRUE;
}

void CMissionObjectiveDlg::OnBnClickedOk()
{
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	CString objective;
	m_MissionObjective.GetWindowText( objective );
	pDoc->setMissionObjective( objective );
	pDoc->setMissionObjectiveBitmap( m_bmpImage );

	OnOK();
}

void CMissionObjectiveDlg::OnBnClickedBrowseButton()
{
	CFileDialog oDlg( TRUE, _T("bmp"), _T(""), OFN_EXPLORER, _T("Bitmap Files (*.bmp)|*.bmp|Targa Files (*.tga)|*.tga|All Files (*.*)|*.*||"), this );

	if( oDlg.DoModal( ) == IDOK )
	{
		CString ext = oDlg.GetFileExt( );
		CString filename = oDlg.GetFileName( );

		if( ext.Compare(_T("bmp")) == 0 )
			loadBitmap( filename );
		else if( ext.Compare(_T("tga")) == 0 )
			loadTarga( filename );

		// TO DO CBitmap
		refreshMissionObjectiveImage( );

	}
}

void CMissionObjectiveDlg::loadBitmap( CString &file )
{
	CStringConverter conv( false );
	char *filename = conv( file.GetString( ) );
	ImageIO::Image imgFile;

	if( !ImageIO::loadImage( &imgFile, filename, ImageIO::BITMAP ) )
	{
		MessageBox( _T("There was a problem loading the bitmap."), _T("Error"), MB_OK | MB_ICONERROR );
		return;
	}

	ImageIO::swapRBinRGB( imgFile.width, imgFile.height, imgFile.bitsPerPixel >> 3, imgFile.pixelData );
	ImageIO::flipYImage( imgFile.width, imgFile.height, imgFile.pixelData, imgFile.pixelData, imgFile.bitsPerPixel >> 3 );

	memset( &m_bmpImage, 0, sizeof(BITMAP) );
	m_bmpImage.bmBitsPixel = imgFile.bitsPerPixel;
	m_bmpImage.bmHeight = imgFile.height;
	m_bmpImage.bmWidth = imgFile.width;
	m_bmpImage.bmPlanes = 1;
	size_t size = m_bmpImage.bmHeight * m_bmpImage.bmWidth * (m_bmpImage.bmBitsPixel >> 3) * sizeof(BYTE);

	m_bmpImage.bmBits = new BYTE[ size ];

	memcpy( m_bmpImage.bmBits, imgFile.pixelData, size );
	ImageIO::destroyImage( &imgFile );
	conv.freeMemory( filename );
}

void CMissionObjectiveDlg::loadTarga( CString &file )
{
	CStringConverter conv( false );
	char *filename = conv( file.GetString( ) );
	ImageIO::Image imgFile;

	if( !ImageIO::loadImage( &imgFile, filename, ImageIO::TARGA ) )
	{
		MessageBox( _T("There was a problem loading the targa."), _T("Error"), MB_OK | MB_ICONERROR );
		return;
	}

	ImageIO::swapRBinRGB( imgFile.width, imgFile.height, imgFile.bitsPerPixel >> 3, imgFile.pixelData );
	ImageIO::flipYImage( imgFile.width, imgFile.height, imgFile.pixelData, imgFile.pixelData, imgFile.bitsPerPixel >> 3 );

	memset( &m_bmpImage, 0, sizeof(BITMAP) );
	m_bmpImage.bmBitsPixel = imgFile.bitsPerPixel;
	m_bmpImage.bmHeight = imgFile.height;
	m_bmpImage.bmWidth = imgFile.width;
	m_bmpImage.bmPlanes = 1;
	size_t size = m_bmpImage.bmHeight * m_bmpImage.bmWidth * (m_bmpImage.bmBitsPixel >> 3) * sizeof(BYTE);

	m_bmpImage.bmBits = new BYTE[ size ];

	memcpy( m_bmpImage.bmBits, imgFile.pixelData, size );
	ImageIO::destroyImage( &imgFile );
	conv.freeMemory( filename );
}

void CMissionObjectiveDlg::refreshMissionObjectiveImage( )
{
	if( m_bmpImage.bmBits != NULL )
	{
		// Make small screen shot to fit in window
		CRect rcImageArea;
		m_MissionObjectiveImageCtrl.GetClientRect( rcImageArea );
		m_bmpSmallImage.bmBitsPixel = m_bmpImage.bmBitsPixel;
		m_bmpSmallImage.bmPlanes = m_bmpImage.bmPlanes;
		m_bmpSmallImage.bmWidth = rcImageArea.Width();
		m_bmpSmallImage.bmHeight = rcImageArea.Height();

		if( m_bmpSmallImage.bmBits != NULL )
			delete [] m_bmpSmallImage.bmBits;
		m_bmpSmallImage.bmBits = new BYTE[ m_bmpSmallImage.bmWidth * m_bmpSmallImage.bmHeight * (m_bmpSmallImage.bmBitsPixel >> 3) ];

		BYTE *bitmap1 = static_cast<BYTE *>( m_bmpImage.bmBits );
		BYTE *bitmap2 = static_cast<BYTE *>( m_bmpSmallImage.bmBits );

		ImageIO::resizeImage( m_bmpImage.bmWidth, m_bmpImage.bmHeight, bitmap1,
			m_bmpSmallImage.bmWidth, m_bmpSmallImage.bmHeight, bitmap2,
			m_bmpImage.bmBitsPixel, ImageIO::ALG_NEARESTNEIGHBOR );
		
		DeleteObject( m_hbmpMissionObjectiveImage );
		m_hbmpMissionObjectiveImage = CreateBitmap( m_bmpSmallImage.bmWidth, m_bmpSmallImage.bmHeight, m_bmpSmallImage.bmPlanes,
			m_bmpSmallImage.bmBitsPixel, m_bmpSmallImage.bmBits );

		m_MissionObjectiveImageCtrl.SetBitmap( m_hbmpMissionObjectiveImage );
	}
	//m_MissionObjectiveImageCtrl.Invalidate( TRUE );
}


void CMissionObjectiveDlg::OnBnClickedCancel()
{

	if( m_bmpImage.bmBits != NULL )
		delete [] m_bmpImage.bmBits;
	OnCancel();
}
