// MainFrm.h : interface of the CMainFrame class
//


#pragma once

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	void setTerrainPaintMode( bool mode );


// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CSplitterWnd m_mainSplitter,
				 m_dividerSplitter,// Splitter windows
				 m_viewportSplitter;
	bool	m_initSplitters;
	CImageList m_ToolbarImageList;

	enum {
		MI_CHECKMARK,
		MI_UNCHECKMARK,
		MI_CENTER,
		MI_TILEBOUNDS,
		MI_WIREFRAME,
		MI_SOLID
	};

	CBitmap m_MenuIcons[ 7 ];

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnRendermodeSolid();
	afx_msg void OnRendermodeWireframe();
	afx_msg void OnViewCenteratorigin();
	afx_msg void OnHelpMakeadonation();
	afx_msg void OnHelpOthergreatprograms();
	afx_msg void OnViewShowterrainboundaries();
	afx_msg void OnUpdateViewShowterrainboundaries(CCmdUI *pCmdUI);
	afx_msg void OnTerrainTogglePaintMode();
	afx_msg void OnUpdateTerrainTogglePaintMode(CCmdUI *pCmdUI);

public:
	afx_msg void OnTerrainTerrainTexturingSettings();
	afx_msg void OnDestroy();
};


