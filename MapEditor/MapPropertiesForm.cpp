// MapPropertiesForm.cpp : implementation file
//

#include "stdafx.h"
#include "MapEditor.h"
#include "MapPropertiesForm.h"
#include "MapEditorDoc.h"
#include "RenderingGlobals.h"
#include "MissionObjectiveDlg.h"
#include "MainFrm.h"
#include "OpenGLWnd.h"

#include <ResourceManagement/TextureManager/TextureManager.h>
#include <ResourceManagement/TextureManager/Texture2D.h>
#include <ImageIO.h>


#include <Game/Terrain/TerrainGen.h>
#include <Game/Terrain/FractalTerrainGen.h>
#include <Core.h>

// CMapPropertiesForm

IMPLEMENT_DYNCREATE(CMapPropertiesForm, CFormView)

CMapPropertiesForm::CMapPropertiesForm()
	: CFormView(CMapPropertiesForm::IDD)
	, m_fTerrainWidth(100.0f)
	, m_fTerrainLength(100.0f)
	, m_fTerrainMinHeight(-2.0f)
	, m_fTerrainMaxHeight(30.0f)
	, m_MapName(_T(""))
	, m_uiNumberOfXChunks(4)
	, m_uiNumberOfYChunks(4)
	, m_WindowSizeSlider(8)
	, m_bControlsSubclassed(false)
	//, m_SelectedTextureBrush(0)
	, m_bBitmapDataAllocated(false)
{

}

CMapPropertiesForm::~CMapPropertiesForm()
{
	if( m_bBitmapDataAllocated )
	{
		delete [] m_BrushBitmap.bmBits;
		DeleteObject( m_hBrushBitmap );
	}
}

void CMapPropertiesForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TERRAIN_WIDTH_EDIT, m_fTerrainWidth);
	DDX_Text(pDX, IDC_TERRAIN_LENGTH_EDIT, m_fTerrainLength);
	DDX_Text(pDX, IDC_TERRAIN_MINHEIGHT_EDIT, m_fTerrainMinHeight);
	DDX_Text(pDX, IDC_TERRAIN_MAXHEIGHT_EDIT, m_fTerrainMaxHeight);
	DDX_Text(pDX, IDC_MAPNAME_EDIT, m_MapName);
	DDV_MaxChars(pDX, m_MapName, 100);
	DDX_Text(pDX, IDC_TERRAIN_NUMBER_OF_XCELLS_EDIT, m_uiNumberOfXChunks);
	DDX_Text(pDX, IDC_TERRAIN_NUMBER_OF_YCELLS_EDIT, m_uiNumberOfYChunks);
	DDX_Slider(pDX, IDC_WINDOW_SIZE_SLIDER, m_WindowSizeSlider);
	DDV_MinMaxInt(pDX, m_WindowSizeSlider, 0, 64);
	DDX_Control(pDX, IDC_TEXTUREBRUSH_COMBO, m_TextureBrushCombo);
	DDX_Control(pDX, IDC_TEXTUREBRUSH_STATIC, m_BrushImage);
}

BEGIN_MESSAGE_MAP(CMapPropertiesForm, CFormView)

	ON_BN_CLICKED(IDC_GENRANDOMTERRAIN_BUTTON, &CMapPropertiesForm::OnBnClickedGenrandomterrainButton)
	ON_BN_CLICKED(IDC_GENTERRAINFROMFILE_BUTTON, &CMapPropertiesForm::OnBnClickedGenterrainfromfileButton)
	ON_EN_CHANGE(IDC_TERRAIN_WIDTH_EDIT, &CMapPropertiesForm::OnEnChangeTerrainWidthEdit)
	ON_EN_CHANGE(IDC_TERRAIN_LENGTH_EDIT, &CMapPropertiesForm::OnEnChangeTerrainLengthEdit)
	ON_EN_CHANGE(IDC_TERRAIN_MINHEIGHT_EDIT, &CMapPropertiesForm::OnEnChangeTerrainMinheightEdit)
	ON_EN_CHANGE(IDC_TERRAIN_MAXHEIGHT_EDIT, &CMapPropertiesForm::OnEnChangeTerrainMaxheightEdit)
	ON_EN_CHANGE(IDC_MAPNAME_EDIT, &CMapPropertiesForm::OnEnChangeMapnameEdit)
	ON_BN_CLICKED(IDC_SETBRIEFING_BUTTON, &CMapPropertiesForm::OnBnClickedSetbriefingButton)
	ON_BN_CLICKED(IDC_TERRAINTEXTURINGSETTINGS_BUTTON, &CMapPropertiesForm::OnBnClickedTerraintexturingsettingsButton)
	ON_WM_CREATE()
	ON_EN_CHANGE(IDC_TERRAIN_NUMBER_OF_XCELLS_EDIT, &CMapPropertiesForm::OnEnChangeTerrainNumberOfXcellsEdit)
	ON_EN_CHANGE(IDC_TERRAIN_NUMBER_OF_YCELLS_EDIT, &CMapPropertiesForm::OnEnChangeTerrainNumberOfYcellsEdit)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_WINDOW_SIZE_SLIDER, &CMapPropertiesForm::OnNMReleasedcaptureWindowSizeSlider)
	ON_CBN_SELCHANGE(IDC_TEXTUREBRUSH_COMBO, &CMapPropertiesForm::OnCbnSelchangeTexturebrushCombo)
END_MESSAGE_MAP()


// CMapPropertiesForm diagnostics

#ifdef _DEBUG
void CMapPropertiesForm::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CMapPropertiesForm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CMapPropertiesForm message handlers

void CMapPropertiesForm::OnBnClickedGenrandomterrainButton()
{
	ASSERT( false );
	//ASSERT( m_fTerrainWidth > 0.0f );
	//ASSERT( m_fTerrainLength > 0.0f );
	//ASSERT( -500.0f <= m_fTerrainMinHeight && m_fTerrainMinHeight <= 500.0f );
	//ASSERT( -500.0f <= m_fTerrainMaxHeight && m_fTerrainMaxHeight <= 500.0f );
	//ASSERT( m_fTerrainMinHeight < m_fTerrainMaxHeight );
	//Terrain::CFractalTerrainGen fracTer( 1, 16 * m_fTerrainWidth, 16 * m_fTerrainLength, m_fTerrainMinHeight, m_fTerrainMaxHeight, 10.0f );
	//
	//fracTer.preSeedHeight( 25, 25, 10 );
	//fracTer.preSeedHeight( 21, 21, 5.5 );
	//fracTer.generate2( );

	//Terrain::CTerrainMesh tmesh( fracTer.getHeightField( ), fracTer.getWidth( ), fracTer.getLength( ), fracTer.getMinHeight( ), fracTer.getMaxHeight( ), fracTer.getUnitWidth( ), fracTer.getUnitLength( ) );

	//Core::pMap->setTerrain( tmesh );

	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	pDoc->setSelectedObject( NULL ); // deselect everything...
	UpdateType update = UPD_OPENGL;
	pDoc->UpdateAllViews( this, update );
}

void CMapPropertiesForm::OnBnClickedGenterrainfromfileButton()
{
	bool bResizedImage = false;
	Terrain::CTerrainGen terr;
	CFileDialog odlg( TRUE, _T(".tga"), NULL, OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST, _T("Targa Files (*.tga)|*.tga|Bitmap Files (*.bmp)|*.bmp|All Files (*.*)|*.*||") );

	if( odlg.DoModal( ) == IDOK )
	{
		CString wcFilename = odlg.GetFileName( );
		int filename_size = WideCharToMultiByte( CP_ACP, WC_NO_BEST_FIT_CHARS, wcFilename.GetString( ), -1, NULL, 0, NULL, NULL );
		PCHAR filename = new CHAR[ filename_size ];
		WideCharToMultiByte( CP_ACP, WC_NO_BEST_FIT_CHARS, wcFilename.GetString( ), -1, filename, filename_size, NULL, NULL );
		

		
		ImageIO::Image imgfile;


		if( odlg.GetFileExt().Compare( _T("tga") ) == 0 )
		{
			if( ImageIO::loadImage( &imgfile, filename, ImageIO::TARGA ) != ImageIO::SUCCESS )
			{
				MessageBox( _T("Failed to load targa image!"), _T("Error"), MB_ICONERROR | MB_OK );
				delete [] filename;
				return;
			}

			// resize image if neccessary...
			if( imgfile.width % (3 * m_uiNumberOfXChunks) != 0 || imgfile.height % (3 * m_uiNumberOfYChunks) != 0 )
			{
				unsigned int newWidth = imgfile.width + (imgfile.width % (3 * m_uiNumberOfXChunks) ) / 2;
				unsigned int newHeight = imgfile.height + (imgfile.height % (3 * m_uiNumberOfYChunks) ) / 2;

				ImageIO::byte *dstBitmap = new ImageIO::byte[ newWidth * newHeight * ( imgfile.bitsPerPixel >> 3 ) ];

				ImageIO::resizeImage( imgfile.width, imgfile.height, imgfile.pixelData, newWidth, newHeight, dstBitmap, imgfile.bitsPerPixel, ImageIO::ALG_BICUBIC );
				
				ImageIO::destroyImage( &imgfile );
				imgfile.width = newWidth;
				imgfile.height = newHeight;
				imgfile.pixelData = dstBitmap;
				bResizedImage = true;

				m_fTerrainWidth = newWidth;
				m_fTerrainLength = newHeight;
			}

			if( m_fTerrainWidth < imgfile.width )
				m_fTerrainWidth = imgfile.width;
			if( m_fTerrainLength < imgfile.height )
				m_fTerrainLength = imgfile.height;
				

			ASSERT( m_fTerrainWidth > 0.0f );
			ASSERT( m_fTerrainLength > 0.0f );
			ASSERT( -500.0f <= m_fTerrainMinHeight && m_fTerrainMinHeight <= 500.0f );
			ASSERT( -500.0f <= m_fTerrainMaxHeight && m_fTerrainMaxHeight <= 500.0f );
			ASSERT( m_fTerrainMinHeight < m_fTerrainMaxHeight );
			//generate terrain
			terr.generatePoints( imgfile.width, imgfile.height, imgfile.bitsPerPixel >> 3, imgfile.pixelData, m_fTerrainWidth, m_fTerrainLength, m_fTerrainMaxHeight, m_fTerrainMinHeight );
			
			
			if( bResizedImage) delete [] imgfile.pixelData;
			else ImageIO::destroyImage( &imgfile );

			Terrain::CTerrainMesh tmesh( terr.getHeightField( ), terr.getWidth( ), terr.getLength( ), terr.getMinHeight( ), terr.getMaxHeight( ), terr.getElementWidth( ), terr.getElementLength( ) );

			Core::pMap->setTerrain( tmesh, m_uiNumberOfXChunks, m_uiNumberOfYChunks );
			delete [] filename;
		}
		else 
		{

			if( ImageIO::loadImage( &imgfile, filename, ImageIO::BITMAP ) != ImageIO::SUCCESS )
			{
				MessageBox( _T("Failed to load bitmap image!"), _T("Error"), MB_ICONERROR | MB_OK );
				delete [] filename;
				return;
			}

			// resize image if neccessary...
			if( imgfile.width % (3 * m_uiNumberOfXChunks) != 0 || imgfile.height % (3 * m_uiNumberOfYChunks) != 0 )
			{
				unsigned int newWidth = imgfile.width + (imgfile.width % (3 * m_uiNumberOfXChunks) ) / 2;
				unsigned int newHeight = imgfile.height + (imgfile.height % (3 * m_uiNumberOfYChunks) ) / 2;

				ImageIO::byte *dstBitmap = new ImageIO::byte[ newWidth * newHeight * ( imgfile.bitsPerPixel >> 3 ) ];

				ImageIO::resizeImage( imgfile.width, imgfile.height, imgfile.pixelData, newWidth, newHeight, dstBitmap, imgfile.bitsPerPixel, ImageIO::ALG_BICUBIC );
				
				ImageIO::destroyImage( &imgfile );
				imgfile.width = newWidth;
				imgfile.height = newHeight;
				imgfile.pixelData = dstBitmap;
				bResizedImage = true;

				m_fTerrainWidth = newWidth;
				m_fTerrainLength = newHeight;
			}

			if( m_fTerrainWidth < imgfile.width )
				m_fTerrainWidth = imgfile.width;
			if( m_fTerrainLength < imgfile.height )
				m_fTerrainLength = imgfile.height;
				

			ASSERT( m_fTerrainWidth > 0.0f );
			ASSERT( m_fTerrainLength > 0.0f );
			ASSERT( -500.0f <= m_fTerrainMinHeight && m_fTerrainMinHeight <= 500.0f );
			ASSERT( -500.0f <= m_fTerrainMaxHeight && m_fTerrainMaxHeight <= 500.0f );
			ASSERT( m_fTerrainMinHeight < m_fTerrainMaxHeight );
			//generate terrain
			terr.generatePoints( imgfile.width, imgfile.height, imgfile.bitsPerPixel >> 3, imgfile.pixelData, m_fTerrainWidth, m_fTerrainLength, m_fTerrainMaxHeight, m_fTerrainMinHeight );
			
			
			if( bResizedImage) delete [] imgfile.pixelData;
			else ImageIO::destroyImage( &imgfile );

			Terrain::CTerrainMesh tmesh( terr.getHeightField( ), terr.getWidth( ), terr.getLength( ), terr.getMinHeight( ), terr.getMaxHeight( ), terr.getElementWidth( ), terr.getElementLength( ) );

			Core::pMap->setTerrain( tmesh, m_uiNumberOfXChunks, m_uiNumberOfYChunks );
			delete [] filename;
		}

		CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );

		pDoc->setSelectedObject( NULL ); // deselect everything...
		pDoc->UpdateAllViews( this, UPD_OPENGL );
		pDoc->UpdateAllViews( this, UPG_MAPASSETSFORM );
		((CMainFrame *) AfxGetMainWnd( ))->setTerrainPaintMode( false );
		COpenGLWnd::resetSelectedTile( );
	}
}

void CMapPropertiesForm::OnEnChangeTerrainWidthEdit()
{
	UpdateData( );
}

void CMapPropertiesForm::OnEnChangeTerrainLengthEdit()
{
	UpdateData( );
}

void CMapPropertiesForm::OnEnChangeTerrainMinheightEdit()
{
	UpdateData( );
}

void CMapPropertiesForm::OnEnChangeTerrainMaxheightEdit()
{
	UpdateData( );	
}

void CMapPropertiesForm::OnEnChangeTerrainNumberOfXcellsEdit()
{
	UpdateData( );	
}

void CMapPropertiesForm::OnEnChangeTerrainNumberOfYcellsEdit()
{
	UpdateData( );	
}

void CMapPropertiesForm::OnEnChangeMapnameEdit()
{
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );

	pDoc->setMissionName( m_MapName );

	UpdateData( );
	int mapname_size = WideCharToMultiByte( CP_ACP, WC_NO_BEST_FIT_CHARS, m_MapName.GetString( ), -1, NULL, 0, NULL, NULL );
	PCHAR mapname = new CHAR[ mapname_size ];

	WideCharToMultiByte( CP_ACP, WC_NO_BEST_FIT_CHARS, m_MapName.GetString( ), -1, mapname, mapname_size, NULL, NULL );
		
	Core::pMap->setMapName( std::string(mapname) );
	delete [] mapname;
}

void CMapPropertiesForm::OnBnClickedSetbriefingButton()
{
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	CMissionObjectiveDlg dlg;


	dlg.DoModal( );
}

void CMapPropertiesForm::OnBnClickedTerraintexturingsettingsButton()
{
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	CTerrainTexturingDlg &dlg = pDoc->getTerrainTexturingDlg( );
	dlg.ShowWindow( SW_SHOW );
}

int CMapPropertiesForm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	

	return 0;
}



void CMapPropertiesForm::OnNMReleasedcaptureWindowSizeSlider(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	UpdateData( );

	Core::pMap->setWindowDimensions( m_WindowSizeSlider, m_WindowSizeSlider );

	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	UpdateType update = UPD_OPENGL;
	pDoc->UpdateAllViews( this, update );
	*pResult = 0;
}

void CMapPropertiesForm::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	switch( lHint )
	{
		case UPD_OPENGL:
			break;
		case UPG_TEXTUREBRUSHES:
			break;
		default:
			InvalidateRect( NULL, false );
			break;
	}
}

void CMapPropertiesForm::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	if( !m_bControlsSubclassed )
	{
		m_Buttons[ 0 ].SubclassDlgItem( IDC_SETBRIEFING_BUTTON, this );
		m_Buttons[ 1 ].SubclassDlgItem( IDC_GENRANDOMTERRAIN_BUTTON, this );
		m_Buttons[ 2 ].SubclassDlgItem( IDC_GENTERRAINFROMFILE_BUTTON, this );
		m_Buttons[ 3 ].SubclassDlgItem( IDC_TERRAINTEXTURINGSETTINGS_BUTTON, this );
		m_Buttons[ 4 ].SubclassDlgItem( IDC_TERRAINTEXTURINGCLEAR_BUTTON, this );

		//buttons[ 4 ].SubclassDlgItem( IDC_SETBRIEFING_BUTTON, this );
		//buttons[ 5 ].SubclassDlgItem( IDC_SETBRIEFING_BUTTON, this );
		m_bControlsSubclassed = true;
		
		m_WindowSizeSliderCtrl.SubclassDlgItem( IDC_WINDOW_SIZE_SLIDER, this );
		m_WindowSizeSliderCtrl.SetRange( 0, 64, FALSE );
		m_WindowSizeSliderCtrl.SetTicFreq( 2 );
		m_WindowSizeSliderCtrl.SetPageSize( 1 );
		m_WindowSizeSliderCtrl.SetTipSide( TBTS_BOTTOM );


		for( int i = 5; i >= 0; i-- )
		{
			CString str;
			str.AppendFormat( _T("Texture %d"), i );
			m_TextureBrushCombo.InsertString( 0, str );
		}

		//m_TextureBrushCombo.InsertString( 0, _T("Clear") );
		//m_TextureBrushCombo.SetExtendedUI( TRUE );
		//m_TextureBrushCombo.SetCurSel( 0 );


	}



	
}

void CMapPropertiesForm::OnCbnSelchangeTexturebrushCombo()
{
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	int selectedBrush = m_TextureBrushCombo.GetCurSel( );
	const CArray<unsigned int> &textureLayers = pDoc->getTextureLayers( );


	pDoc->setCurrentTextureLayer( selectedBrush );

	updateBrushImage( pDoc->getCurrentTextureLayer( ) );
}

void CMapPropertiesForm::updateBrushImage( int brush )
{
	CRect rect;
	m_BrushImage.GetClientRect( rect );

	//if( brush == 0 )
	//{
	//	DeleteObject( m_hBrushBitmap );
	//	m_hBrushBitmap = LoadBitmap( AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_CLEARTILE_BITMAP) );
	//}
	//else {
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	const CArray<unsigned int> &textureLayers = pDoc->getTextureLayers();

		ResourceManagement::CTextureManager *pMananger = ResourceManagement::CTextureManager::getInstance( );
		ResourceManagement::CTexture2D *pTexture = pMananger->find( textureLayers[ brush ] );
		if( m_bBitmapDataAllocated )
			delete [] m_BrushBitmap.bmBits;


		unsigned int size = rect.Width( ) * rect.Height( ) * pTexture->byteCount( );


		m_BrushBitmap.bmBits = new BYTE[ size ];
		m_bBitmapDataAllocated = true;
		m_BrushBitmap.bmBitsPixel = pTexture->bitCount( );
		m_BrushBitmap.bmWidthBytes = rect.Width( ) * pTexture->byteCount( );
		m_BrushBitmap.bmPlanes = 1;

		ImageIO::resizeImage( pTexture->width( ), pTexture->height( ), pTexture->bitmap( ),
							  rect.Width( ), rect.Height( ), static_cast<BYTE*>(m_BrushBitmap.bmBits),
							  pTexture->bitCount( ), ImageIO::ALG_NEARESTNEIGHBOR );
		m_BrushBitmap.bmWidth = rect.Width( );
		m_BrushBitmap.bmHeight = rect.Height( );

		ImageIO::swapRBinRGB( m_BrushBitmap.bmWidth, m_BrushBitmap.bmHeight, pTexture->byteCount( ), static_cast<BYTE *>(m_BrushBitmap.bmBits) );
		ImageIO::flipYImage( m_BrushBitmap.bmWidth, m_BrushBitmap.bmHeight, static_cast<BYTE *>(m_BrushBitmap.bmBits), static_cast<BYTE *>(m_BrushBitmap.bmBits), pTexture->byteCount( ) );


		DeleteObject( m_hBrushBitmap );
		m_hBrushBitmap = CreateBitmap( m_BrushBitmap.bmWidth, m_BrushBitmap.bmHeight, m_BrushBitmap.bmPlanes, m_BrushBitmap.bmBitsPixel, m_BrushBitmap.bmBits );
	//}

	
	m_BrushImage.SetBitmap( m_hBrushBitmap );
}