// 2DMapEditor.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
//#define VLD_MAX_DATA_DUMP			25
//#define VLD_AGGREGATE_DUPLICATES 
//#include <vld.h>
//#pragma comment(lib, "vld.lib")
//#pragma comment(lib, "vldmtdll.lib")

#include "MapEditor.h"
#include "MainFrm.h"

#include "MapEditorDoc.h"
#include "PerspectiveView.h"
#include "Renderer.h"
#include "RenderingGlobals.h"
#include "SplashDlg.h"
#include "StringConverter.h"
#include <utility/memleakcheck.h>
#include <Core.h>


#include <GL/glew.h>
#include <GL/gl.h>



//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif


// CMapEditorApp

BEGIN_MESSAGE_MAP(CMapEditorApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CMapEditorApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()


// CMapEditorApp construction

CMapEditorApp::CMapEditorApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CMapEditorApp object

CMapEditorApp theApp;


// CMapEditorApp initialization

BOOL CMapEditorApp::InitInstance()
{
	//LEAK_CHECK;

	
	#ifndef _DEBUG // Display splash screen in release
	CSplashDlg  splashDlg(NULL);
	splashDlg.DoModal( );
	#endif
	
	

	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();


	

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	MapEditorWndClass = AfxRegisterWndClass( CS_DBLCLKS | CS_BYTEALIGNWINDOW );

	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	LoadStdProfileSettings(5);  // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CMapEditorDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CPerspectiveView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);


	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);




	pRenderer = CRenderer::getInstance( );

	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOWMAXIMIZED);
	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();
	return TRUE;
}

int CMapEditorApp::ExitInstance()
{
	delete pRenderer;

	
	return CWinApp::ExitInstance();
}


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
public:
	afx_msg void OnPaint();
public:
	virtual BOOL OnInitDialog();
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	ON_WM_LBUTTONDOWN()
	ON_WM_PAINT()
END_MESSAGE_MAP()

// App command to run the dialog
void CMapEditorApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


// CMapEditorApp message handlers


void CAboutDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	EndDialog( 0 );
	CDialog::OnLButtonDown(nFlags, point);
}


void CAboutDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CDC memDC;
	memDC.CreateCompatibleDC( NULL );

	CBitmap bmp;
	bmp.LoadBitmap( MAKEINTRESOURCE(IDB_SPLASHBITMAP) );
	CBitmap *pOldBitmap = memDC.SelectObject( &bmp );

	dc.BitBlt( 0, 0, SPLASHBMP_WIDTH, SPLASHBMP_HEIGHT, &memDC, 0, 0, SRCCOPY );
	memDC.SelectObject( pOldBitmap );
	
	bmp.DeleteObject( );
	ReleaseDC( &memDC );
}

BOOL CAboutDlg::OnInitDialog()
{
	CRect rcDesktop;
	CRect rcClient;
	MoveWindow( 0, 0, SPLASHBMP_WIDTH, SPLASHBMP_HEIGHT, FALSE );
	GetClientRect( &rcClient ); 
	HWND hwndDesktop = ::GetDesktopWindow( );
	::GetClientRect( hwndDesktop, &rcDesktop );

	unsigned int center_left = (rcDesktop.Width( ) - rcClient.Width( ) ) >> 1; // half of desktop minus half of the splash width...
	unsigned int center_top = (rcDesktop.Height( ) - rcClient.Height( ) ) >> 1; 

	MoveWindow( center_left, center_top, SPLASHBMP_WIDTH, SPLASHBMP_HEIGHT );
	ShowWindow( SW_SHOW );
	return TRUE;
}
