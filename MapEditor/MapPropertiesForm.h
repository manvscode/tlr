#pragma once

#include "CoolButton.h"
#include "TerrainTexturingDlg.h"
#include "afxwin.h"


#define MAPPROPERTIESFORM_WIDTH		185

// CMapPropertiesForm form view

class CMapPropertiesForm : public CFormView
{
	DECLARE_DYNCREATE(CMapPropertiesForm)

  private:
	float m_fTerrainWidth;
	float m_fTerrainLength;
	float m_fTerrainMinHeight;
	float m_fTerrainMaxHeight;
	UINT m_uiNumberOfXChunks;
	UINT m_uiNumberOfYChunks;
	CString m_MapName;
	
	CSliderCtrl m_WindowSizeSliderCtrl;
	int m_WindowSizeSlider;


	CCoolButton m_Buttons[ 9 ];
	bool m_bControlsSubclassed;



	//unsigned int m_SelectedTextureBrush; // This is a texture id
	CComboBox m_TextureBrushCombo;
	BITMAP m_BrushBitmap;
	HBITMAP m_hBrushBitmap;
	CStatic m_BrushImage;
	bool m_bBitmapDataAllocated;

  protected:
	CMapPropertiesForm();           // protected constructor used by dynamic creation
	virtual ~CMapPropertiesForm();

  public:
		enum { IDD = IDD_MAPPROPERTIESFORM };
	#ifdef _DEBUG
		virtual void AssertValid() const;
	#ifndef _WIN32_WCE
		virtual void Dump(CDumpContext& dc) const;
	#endif
	#endif

  protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	DECLARE_MESSAGE_MAP()


  public:
	afx_msg void OnBnClickedGenrandomterrainButton();
	afx_msg void OnBnClickedGenterrainfromfileButton();
	afx_msg void OnEnChangeTerrainWidthEdit();
	afx_msg void OnEnChangeTerrainLengthEdit();
	afx_msg void OnEnChangeTerrainMinheightEdit();
	afx_msg void OnEnChangeTerrainMaxheightEdit();
	afx_msg void OnEnChangeMapnameEdit();
	afx_msg void OnBnClickedSetbriefingButton();
	afx_msg void OnBnClickedTerraintexturingsettingsButton();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnEnChangeTerrainNumberOfXcellsEdit();
	afx_msg void OnEnChangeTerrainNumberOfYcellsEdit();
	afx_msg void OnNMReleasedcaptureWindowSizeSlider(NMHDR *pNMHDR, LRESULT *pResult);
	virtual void OnInitialUpdate();
	afx_msg void OnCbnSelchangeTexturebrushCombo();
	void updateBrushImage( int brush );

};


