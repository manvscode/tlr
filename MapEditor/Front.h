#pragma once
#include "OrthographicView.h"

// CFront view

class CFront : public COrthographicView
{
	DECLARE_DYNCREATE(CFront)

protected:
	CFront();           // protected constructor used by dynamic creation
	virtual ~CFront();

public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
protected:
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
};


