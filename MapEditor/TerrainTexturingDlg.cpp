// TerrainTexturingDlg.cpp : implementation file
//

#include "stdafx.h"
#include <afxole.h>
#include "MapEditor.h"
#include "MapEditorDoc.h"
#include "TerrainTexturingDlg.h"
#include "resource.h"

#define IDC_TEXTURE0	888880
#define IDC_TEXTURE1	888881
#define IDC_TEXTURE2	888882
#define IDC_TEXTURE3	888883
#define IDC_TEXTURE4	888884
#define IDC_TEXTURE5	888885

// CTerrainTexturingDlg dialog

IMPLEMENT_DYNAMIC(CTerrainTexturingDlg, CDialog)

CTerrainTexturingDlg::CTerrainTexturingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTerrainTexturingDlg::IDD, pParent)/*, m_DragAndDropTarget( )*/, m_bControlsSubclassed(false)
{

}

CTerrainTexturingDlg::~CTerrainTexturingDlg()
{
}

void CTerrainTexturingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTerrainTexturingDlg, CDialog)
	ON_WM_CREATE()

	ON_WM_MOUSEMOVE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_TEXTURE0_BUTTON, &CTerrainTexturingDlg::OnBnClickedTexture0Button)
	ON_BN_CLICKED(IDC_TEXTURE1_BUTTON, &CTerrainTexturingDlg::OnBnClickedTexture1Button)
	ON_BN_CLICKED(IDC_TEXTURE2_BUTTON, &CTerrainTexturingDlg::OnBnClickedTexture2Button)
	ON_BN_CLICKED(IDC_TEXTURE3_BUTTON, &CTerrainTexturingDlg::OnBnClickedTexture3Button)
	ON_BN_CLICKED(IDC_TEXTURE4_BUTTON, &CTerrainTexturingDlg::OnBnClickedTexture4Button)
	ON_BN_CLICKED(IDC_TEXTURE5_BUTTON, &CTerrainTexturingDlg::OnBnClickedTexture5Button)
	ON_BN_CLICKED(IDOK, &CTerrainTexturingDlg::OnBnClickedOk)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CTerrainTexturingDlg message handlers

BOOL CTerrainTexturingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	if( !m_bControlsSubclassed )
	{
		CRect rc;
		m_Buttons[ 0 ].SubclassDlgItem( IDOK, this );
		m_Buttons[ 1 ].SubclassDlgItem( IDCANCEL, this );
		m_Buttons[ 2 ].SubclassDlgItem( IDC_TEXTURE0_BUTTON, this );
		m_Buttons[ 3 ].SubclassDlgItem( IDC_TEXTURE1_BUTTON, this );
		m_Buttons[ 4 ].SubclassDlgItem( IDC_TEXTURE2_BUTTON, this );
		m_Buttons[ 5 ].SubclassDlgItem( IDC_TEXTURE3_BUTTON, this );
		m_Buttons[ 6 ].SubclassDlgItem( IDC_TEXTURE4_BUTTON, this );
		m_Buttons[ 7 ].SubclassDlgItem( IDC_TEXTURE5_BUTTON, this );

		for( int i = 0; i < 6; i++ )
		{
			CString windowName;
			windowName.AppendFormat( _T("Texture_%d"), i );
			rc.left = 20 + 94 * i;
			rc.top = 65;
			rc.right = rc.left + 84;
			rc.bottom = rc.top + 84;
			// IDC_TEXTURE#, starts at a base of IDC_TEXTURE0 + #
			if( !m_Texture[ i ].Create( ((CMapEditorApp*)AfxGetApp())->MapEditorWndClass, windowName, 
				WS_CHILD | WS_VISIBLE | WS_BORDER, rc, this, IDC_TEXTURE0 + i ) )
				TRACE("Error creating trash can window\n");    
		}
	}


	CenterWindow( );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


int CTerrainTexturingDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	return 0;
}

void CTerrainTexturingDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	CRect rc;
	GetClientRect( rc );

	if( rc.PtInRect(point) )
		SetWindowPos( &wndTopMost, 0, 0, 200, 200, SWP_NOSIZE | SWP_NOMOVE );
	

	CDialog::OnMouseMove(nFlags, point);
}

void CTerrainTexturingDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	if( bShow )
		CenterWindow( );
}

void CTerrainTexturingDlg::OnBnClickedTexture0Button()
{
	m_Texture[ 0 ].reset( );
	m_Texture[ 0 ].Invalidate( TRUE );
}

void CTerrainTexturingDlg::OnBnClickedTexture1Button()
{
	m_Texture[ 1 ].reset( );
	m_Texture[ 1 ].Invalidate( TRUE );
}

void CTerrainTexturingDlg::OnBnClickedTexture2Button()
{
	m_Texture[ 2 ].reset( );
	m_Texture[ 2 ].Invalidate( TRUE );
}

void CTerrainTexturingDlg::OnBnClickedTexture3Button()
{
	m_Texture[ 3 ].reset( );
	m_Texture[ 3 ].Invalidate( TRUE );
}

void CTerrainTexturingDlg::OnBnClickedTexture4Button()
{
	m_Texture[ 4 ].reset( );
	m_Texture[ 4 ].Invalidate( TRUE );
}

void CTerrainTexturingDlg::OnBnClickedTexture5Button()
{
	m_Texture[ 5 ].reset( );
	m_Texture[ 5 ].Invalidate( TRUE );
}

void CTerrainTexturingDlg::OnBnClickedOk()
{
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	CArray<unsigned int> textureLayers;

	textureLayers.RemoveAll( );

	// store the selected textures...
	for( int i = 0; i < 6; i++ )
	{
		unsigned int tID = m_Texture[ i ].getTexture( );
		if( tID == 0 ) continue;
		textureLayers.Add( tID );
	}


	pDoc->setTextureLayers( textureLayers );
	pDoc->UpdateAllViews( NULL, UPG_TEXTUREBRUSHES );
	OnOK();
}



void CTerrainTexturingDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}
