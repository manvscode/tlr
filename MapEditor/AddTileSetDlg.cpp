// AddTileSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MapEditor.h"
#include "AddTileSetDlg.h"


// CAddTileSetDlg dialog

IMPLEMENT_DYNAMIC(CAddTileSetDlg, CDialog)

CAddTileSetDlg::CAddTileSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddTileSetDlg::IDD, pParent)
	, m_FolderPath(_T(""))
{

}

CAddTileSetDlg::~CAddTileSetDlg()
{
}

void CAddTileSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TYPE_OF_TILESET_COMBO, m_TypeOfTileSetCombo);
	DDX_Text(pDX, IDC_TILESET_FOLDER_EDIT, m_FolderPath);
}


BEGIN_MESSAGE_MAP(CAddTileSetDlg, CDialog)
//	ON_WM_CREATE()
ON_BN_CLICKED(IDC_BROWSE_BUTTON, &CAddTileSetDlg::OnBnClickedBrowseButton)
ON_CBN_SELCHANGE(IDC_TYPE_OF_TILESET_COMBO, &CAddTileSetDlg::OnCbnSelchangeTypeOfTilesetCombo)
END_MESSAGE_MAP()


// CAddTileSetDlg message handlers

BOOL CAddTileSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_TypeOfTileSetCombo.ModifyStyle( CBS_DROPDOWN, CBS_DROPDOWNLIST | CBS_SORT | CBS_UPPERCASE );
	m_TypeOfTileSetCombo.InsertString( 0, _T("Terrain") );
	m_TypeOfTileSetCombo.InsertString( 1, _T("Entity") );
	m_TypeOfTileSetCombo.SelectString( 0, _T("Terrain") );
	m_TileSetType = TST_TERRAIN;
	

	m_Buttons[ 0 ].SubclassDlgItem( IDOK, this );
	m_Buttons[ 1 ].SubclassDlgItem( IDCANCEL, this );
	m_Buttons[ 2 ].SubclassDlgItem( IDC_BROWSE_BUTTON, this );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CAddTileSetDlg::OnBnClickedBrowseButton()
{
	//TCHAR  path[ MAX_PATH ]; // Allow space for Path AND FileName here.
	//GetModuleFileName( GetModuleHandle(NULL), path, sizeof(path) );
	//
	//for( unsigned int i = wcsnlen( path, MAX_PATH ); i > 0; i-- )
	//	if( path[ i ] != '\\' )
	//		path[ i ] = '\0';
	//	else
	//		break;

	

	BROWSEINFO bi;
	TCHAR folder_path[ MAX_PATH ];
	PTCHAR  displayString = _T("Find a tile set folder");
	LPITEMIDLIST itemlist = NULL;
	//LPITEMIDLIST rootItemList = NULL;
	//IShellFolder *shellFolder = NULL;

	//if( SHGetDesktopFolder( &shellFolder ) != NOERROR )
	//{
	//	AfxMessageBox( _T("An error occured attempting to get a shell interface."), MB_OK | MB_ICONERROR );
	//	return;
	//}

	//if( shellFolder->ParseDisplayName( this->m_hWnd, NULL, path, NULL, &rootItemList, NULL ) != S_OK )
	//{
	//	AfxMessageBox( _T("Error getting root path."), MB_OK | MB_ICONERROR );
	//	return;
	//}

	// zero out bi
	memset( &bi, 0, sizeof(bi) );

	bi.hwndOwner = this->m_hWnd;
	bi.pidlRoot = 0/*rootItemList*/;
	bi.pszDisplayName = folder_path;
	bi.lpszTitle = displayString;
	bi.lpfn = NULL;
	bi.ulFlags = BIF_UAHINT | BIF_NEWDIALOGSTYLE | BIF_NONEWFOLDERBUTTON | BIF_VALIDATE;
	//bi.ulFlags = BIF_USENEWUI;

	if( (itemlist = SHBrowseForFolder( &bi )) != NULL )
	{
		SHGetPathFromIDList( itemlist, folder_path );
		m_FolderPath.SetString( folder_path );
		UpdateData( FALSE );
		LPMALLOC malloc;	
		SHGetMalloc( &malloc );
		
		malloc->Free( itemlist );
	}
	
	//CFolderDialog dlg(this);

	//LPCITEMIDLIST pidl = dlg.BrowseForFolder( _T("Find a tile set folder"),  BIF_UAHINT | BIF_NEWDIALOGSTYLE | BIF_NONEWFOLDERBUTTON | BIF_VALIDATE );
	//CString path = dlg.GetPathName(pidl);

}

CString &CAddTileSetDlg::getTileSetPath( )
{
	return m_FolderPath;
}

TileSetType CAddTileSetDlg::getTileSetType( ) const
{
	return m_TileSetType;
}
void CAddTileSetDlg::OnCbnSelchangeTypeOfTilesetCombo()
{
	// TODO: Add your control notification handler code here
	
	switch( m_TypeOfTileSetCombo.GetCurSel( ) )
	{
		case 0:
			m_TileSetType = TST_TERRAIN;
			break;
		case 1:
			m_TileSetType = TST_ENTITY;
			break;
		default:
			ASSERT(false);
			break;
	}

}
