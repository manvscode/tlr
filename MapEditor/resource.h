//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MapEditor.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_MAPPROPERTIESFORM           103
#define IDD_MAPASSETSFORM               104
#define IDR_MAINFRAME                   128
#define IDR_UntitledMapTYPE             129
#define IDI_TERRAINICON                 131
#define IDI_ENTITYICON                  132
#define IDB_SPLASHBITMAP                133
#define IDD_SPLASHDIALOG                134
#define IDD_ADDTILESET_DIALOG           135
#define IDD_PROGRESS_DIALOG             136
#define IDI_BUILDINGICON                137
#define IDD_MISSIONBRIEFING_DIALOG      138
#define IDD_TERRAINTEXTURING_DIALOG     139
#define IDB_COOLBUTTON1                 141
#define IDB_COOLBUTTON2                 142
#define IDB_COOLBUTTON3                 143
#define IDB_CLEARTILE_BITMAP            144
#define IDB_TILEBOUNDS_BITMAP           145
#define IDB_MENUCHECK_BITMAP            147
#define IDB_MENUUNCHECK_BITMAP          148
#define IDB_MENUSOLID_BITMAP            149
#define IDB_MENUWIREFRAME_BITMAP        150
#define IDB_MENUCENTER_BITMAP           151
#define IDB_TEXTURECTRL_BITMAP          152
#define IDC_TAB1                        1000
#define IDC_ASSET_TAB                   1000
#define IDC_EDIT1                       1001
#define IDC_MAPNAME_EDIT                1001
#define IDC_TILESET_FOLDER_EDIT         1001
#define IDC_MISSION_OBJECTIVE_EDIT      1001
#define IDC_TERRAINTILELIST             1003
#define IDC_BUTTON1                     1004
#define IDC_BROWSE_BUTTON               1004
#define IDC_TERRAINTEXTURINGSETTINGS_BUTTON 1004
#define IDC_TEXTURE0_BUTTON             1004
#define IDC_BUTTON2                     1005
#define IDC_GENTERRAINFROMFILE_BUTTON   1005
#define IDC_TEXTURE1_BUTTON             1005
#define IDC_EDIT2                       1006
#define IDC_ENTITYTILELIST              1006
#define IDC_TERRAIN_WIDTH_EDIT          1006
#define IDC_TEXTURE2_BUTTON             1006
#define IDC_EDIT3                       1007
#define IDC_TERRAIN_LENGTH_EDIT         1007
#define IDC_BUILDINGLIST                1007
#define IDC_TEXTURE3_BUTTON             1007
#define IDC_EDIT4                       1008
#define IDC_TERRAIN_MINHEIGHT_EDIT      1008
#define IDC_TEXTURE4_BUTTON             1008
#define IDC_EDIT5                       1009
#define IDC_TERRAIN_MAXHEIGHT_EDIT      1009
#define IDC_TEXTURE5_BUTTON             1009
#define IDC_EDIT6                       1010
#define IDC_BUTTON3                     1011
#define IDC_BUTTON4                     1012
#define IDC_BUTTON5                     1013
#define IDC_BUTTON6                     1014
#define IDC_RANDOMTERRAIN_BUTTON        1015
#define IDC_GENRANDOMTERRAIN_BUTTON     1015
#define IDC_SETBRIEFING_BUTTON          1016
#define IDC_TYPE_OF_TILESET_COMBO       1017
#define IDC_TERRAINTEXTURINGSETTINGS_BUTTON2 1017
#define IDC_TERRAINTEXTURINGCLEAR_BUTTON 1017
#define IDC_ADDTILESET_BUTTON           1018
#define IDC_PROGRESS                    1019
#define IDC_CHECK1                      1020
#define IDC_TERRAIN_NUMBER_OF_XCELLS_EDIT 1021
#define IDC_TERRAIN_NUMBER_OF_YCELLS_EDIT 1022
#define IDC_WINDOW_SIZE_SLIDER          1023
#define IDC_COMBO1                      1024
#define IDC_TEXTUREBRUSH_COMBO          1024
#define IDC_MISSIONBITMAP_STATIC        1032
#define IDC_TEXTUREBRUSH_STATIC         1035
#define IDC_COMBO2                      1036
#define ID_WIREFRAME_BUTTON             32771
#define ID_SOLID_BUTTON                 32772
#define ID_VIEW_RENDERMODE              32776
#define ID_RENDERMODE_WIREFRAME         32777
#define ID_RENDERMODE_SOLID             32778
#define ID_VIEW_CENTERATORIGIN          32783
#define ID_HELP_MAKEADONATION           32784
#define ID_HELP_OTHERGREATPROGRAMS      32785
#define ID_VIEW_SHOWTERRAINBOUNDARIES   32788
#define ID_TERRAIN_TOGGLEPAINTMODE      32790
#define ID_TERRAIN_TOGGLE_PAINT_MODE    32791
#define ID_Menu                         32792
#define ID_TERRAIN_TERRAIN_TEXTURING_SETTINGS 32793

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        153
#define _APS_NEXT_COMMAND_VALUE         32794
#define _APS_NEXT_CONTROL_VALUE         1037
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
