// Top.cpp : implementation file
//

#include "stdafx.h"
#include "MapEditor.h"
#include "Top.h"
#include <Core.h>
#include <list>
#include <Game/Map/MapSelectable.h>
#include <Game/Map/MapCell.h>
using namespace std;
using namespace Game;

// CTop

IMPLEMENT_DYNCREATE(CTop, COrthographicView)

CTop::CTop()
: COrthographicView( )
{
	m_zoom = 0.025f;
}

CTop::~CTop()
{
}

BEGIN_MESSAGE_MAP(CTop, COrthographicView)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()


// CTop drawing

void CTop::OnDraw(CDC* pDC)
{
	CMapEditorDoc* pDoc = reinterpret_cast<CMapEditorDoc *>( GetDocument( ) );
	COpenGLWnd::SetContext( );
	glLoadIdentity( );

	glRotatef( 90.0f, 1.0f, 0.0f, 0.0f );
	//glDepthMask( GL_FALSE );
	COrthographicView::OnDraw( pDC );
	//glDepthMask( GL_TRUE );
}


// CTop diagnostics

#ifdef _DEBUG
void CTop::AssertValid() const
{
	COrthographicView::AssertValid();
}

#ifndef _WIN32_WCE
void CTop::Dump(CDumpContext& dc) const
{
	COrthographicView::Dump(dc);
}
#endif
#endif //_DEBUG


// CTop message handlers

void CTop::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint )
{
	switch( lHint )
	{
		case UPD_OPENGL:
		{
			CRect cr;
			GetClientRect( &cr );
			InvalidateRect( cr, false );
			break;
		}
		default:
			break;
	}

	COrthographicView::OnUpdate( pSender, lHint, pHint );
}

void CTop::OnLButtonDown(UINT nFlags, CPoint point)
{
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );

	if( Core::selections.isPickingOn( ) )
	{
		// begin picking...
		TRACE0( "-------------- Begin Picking --------------\n" );
		double objX = 0.0,
		  	   objY = 0.0,
			   objZ = 0.0;

		glGetDoublev( GL_MODELVIEW_MATRIX, m_Modelview );
		glGetDoublev( GL_PROJECTION_MATRIX, m_Projection );
		glGetIntegerv( GL_VIEWPORT, m_Viewport );

		gluUnProject( point.x, m_Viewport[ 3 ] - point.y, 0, m_Modelview, m_Projection, m_Viewport, &objX, &objY, &objZ );
		TRACE3( "mouse at (%.2f, %.2f, %.2f)\n", objX, objY, objZ );


		CMapRowContainer *container = NULL;
		if( Core::pMap->getContainerAtZ( static_cast<float>( objZ ), container ) )
		{
			ASSERT( container != NULL );
			TRACE0( "Map Row Found...\n" );
			//float roundFactor = objX / 
			CMapSelectable *sel = container->getObjectAtX( CMapRowContainer::MOT_MAPCELL, static_cast<float>( objX ) );

			if( sel != NULL )
			{
				TRACE0( "Map Cell Found...\n" );
				pDoc->setSelectedObject( sel );
				static_cast<Game::CMapCell *>(sel)->setTextureLayer( pDoc->getCurrentTextureLayer( ), true /*pDoc->getSelectedTextureBrush( ) */ );
			}
		}

		CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
		pDoc->UpdateAllViews( NULL, UPD_OPENGL );
		TRACE0( "--------------- End Picking ---------------\n\n" );
	}

	COrthographicView::OnLButtonDown(nFlags, point);
}

void CTop::OnMouseMove(UINT nFlags, CPoint point)
{
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );

	CRect cr;
	GetClientRect( &cr );
	static CPoint oldPoint;

	if( nFlags & MK_CONTROL )
	{
		if( nFlags & MK_RBUTTON ) // zoom 
		{
			CPoint ptZoom = m_ptLeftDownPos - point;
			m_ptLeftDownPos = point;
			m_zoom += ptZoom.y * 0.005f;
			InvalidateRect(cr, false);
		}
		else if( nFlags & MK_MBUTTON ) // pan
		{
			CPoint ptPan = m_ptLeftDownPos - point;
			m_ptLeftDownPos = point;
			m_xTranslation -= ptPan.x * (1.0f / m_zoom) * 0.001f;
			m_zTranslation -= ptPan.y * (1.0f / m_zoom) * 0.001f;
			InvalidateRect(cr, false);
		}
	}
	else if( !(nFlags & MK_LBUTTON) && Core::selections.isPickingOn( ) )
	{
		// begin picking...
		TRACE0( "-------------- Begin Picking --------------\n" );
		double objX = 0.0;
		double objY = 0.0;
		double objZ = 0.0;

		glGetDoublev( GL_MODELVIEW_MATRIX, m_Modelview );
		glGetDoublev( GL_PROJECTION_MATRIX, m_Projection );
		glGetIntegerv( GL_VIEWPORT, m_Viewport );

		gluUnProject( point.x, m_Viewport[ 3 ] - point.y, 0, m_Modelview, m_Projection, m_Viewport, &objX, &objY, &objZ );
		TRACE3( "mouse at (%.2f, %.2f, %.2f)\n", objX, objY, objZ );


		CMapRowContainer *container = NULL;
		if( Core::pMap->getContainerAtZ( objZ, container ) )
		{
			ASSERT( container != NULL );
			TRACE0( "Map Row Found...\n" );
			CMapSelectable *sel = container->getObjectAtX( CMapRowContainer::MOT_MAPCELL, objX );

			if( sel != NULL )
				pDoc->setSelectedObject( sel );
		}

		InvalidateRect(cr, false);
		TRACE0( "--------------- End Picking ---------------\n\n" );
	}
	else if( (nFlags & MK_LBUTTON) && Core::selections.isPickingOn( ) )
	{
		// begin picking...
		TRACE0( "-------------- Begin Picking --------------\n" );
		double objX = 0.0;
		double objY = 0.0;
		double objZ = 0.0;

		glGetDoublev( GL_MODELVIEW_MATRIX, m_Modelview );
		glGetDoublev( GL_PROJECTION_MATRIX, m_Projection );
		glGetIntegerv( GL_VIEWPORT, m_Viewport );

		gluUnProject( static_cast<double>(point.x), static_cast<double>(m_Viewport[ 3 ] - point.y), 1.0, m_Modelview, m_Projection, m_Viewport, &objX, &objY, &objZ );
		TRACE3( "mouse at (%.2f, %.2f, %.2f)\n", objX, objY, objZ );


		CMapRowContainer *container = NULL;
		if( Core::pMap->getContainerAtZ( objZ, container ) )
		{
			ASSERT( container != NULL );
			TRACE0( "Map Row Found...\n" );
			CMapSelectable *sel = container->getObjectAtX( CMapRowContainer::MOT_MAPCELL, objX );

			if( sel != NULL )
			{
				TRACE0( "Map Cell Found...\n" );
				pDoc->setSelectedObject( sel );
				static_cast<Game::CMapCell *>(sel)->setTextureLayer( pDoc->getCurrentTextureLayer( ), true /*pDoc->getCurrentTextureLayer( )*/ );
			}
		}

		InvalidateRect(cr, false);
		TRACE0( "--------------- End Picking ---------------\n\n" );
	}

	COrthographicView::OnMouseMove(nFlags, point);
}

void CTop::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	switch( nChar )
	{
		case VK_UP:
			//m_yTranslation = m_yTranslation - 0.001f * m_zoom;
			Core::pMap->addWindowPosition( 0, -1 );
			break;
		case VK_DOWN:
			//m_yTranslation = m_yTranslation + 0.001f * m_zoom;
			Core::pMap->addWindowPosition( 0, 1 );
			break;
		case VK_LEFT:
			//m_yTranslation = m_yTranslation - 0.001f * m_zoom;
			Core::pMap->addWindowPosition( -1, 0 );
			break;
		case VK_RIGHT:
			//m_yTranslation = m_yTranslation + 0.001f * m_zoom;
			Core::pMap->addWindowPosition( 1, 0 );
			break;
		default:                  
			return;
	} 

	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	pDoc->UpdateAllViews( NULL, UPD_OPENGL );

	COrthographicView::OnKeyDown(nChar, nRepCnt, nFlags);
}


