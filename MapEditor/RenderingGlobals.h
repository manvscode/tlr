#ifndef _RENDERINGGLOBALS_H_
#define _RENDERINGGLOBALS_H_
#include "Renderer.h"
#include "Afxwin.h"

extern CRenderer *pRenderer;

void render( CDocument *doc );
void cubeTest( );

#endif