// 2DMapEditorDoc.h : interface of the CMapEditorDoc class
//


#pragma once

#include "TerrainTexturingDlg.h"
#include <Game/Map/MapSelectable.h>


class CMapEditorDoc : public CDocument
{
protected: // create from serialization only
	CMapEditorDoc();
	DECLARE_DYNCREATE(CMapEditorDoc)


	
// Attributes
protected:
	static CMapEditorDoc *m_pInstance;
	Game::CMapSelectable *m_pSelectedObject;
	CString m_szMissionName;
	CString m_szMissionObjective;
	BITMAP m_bmpMissionObjective;


	CTerrainTexturingDlg m_TerrainTexturingDlg;
	CArray<unsigned int> m_TextureLayers;


	unsigned int m_TextureLayer;

	bool m_bRenderMapCellBoundaries;

// Operations
public:
	Game::CMapSelectable *getSelectedObject( );
	void setSelectedObject( Game::CMapSelectable *selected );
// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CMapEditorDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	static CMapEditorDoc *getInstance( );

	void setMissionName( CString &name );
	CString &getMissionName( );
	CString getMissionName( ) const;

	void setMissionObjective( CString &objective );
	CString &getMissionObjective( );
	CString getMissionObjective( ) const;

	void setMissionObjectiveBitmap( BITMAP &bmp );
	BITMAP &getMissionObjectiveBitmap( );
	BITMAP getMissionObjectiveBitmap( ) const;

	CTerrainTexturingDlg & getTerrainTexturingDlg( );

	void setTextureLayers( CArray<unsigned int> &textureLayers );
	const CArray<unsigned int> &getTextureLayers( ) const;


	void setCurrentTextureLayer( unsigned int layer );
	unsigned int getCurrentTextureLayer( ) const;


	bool isRenderBoundaryModeSet( );
	void setRenderBoundaryMode( bool bRenderBounderies = true );

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


inline bool CMapEditorDoc::isRenderBoundaryModeSet( )
{ return m_bRenderMapCellBoundaries; }