// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "MapEditor.h"
#include "MapPropertiesForm.h"
#include "MapAssetsForm.h"
#include "OrthographicView.h"
#include "PerspectiveView.h"
#include "MainFrm.h"
#include "Top.h"
#include "Front.h"
#include "Side.h"
#include "TerrainTexturingDlg.h"
#include "GLRCSharer.h"
#include <Core.h>
#include <utility/memleakcheck.h>

//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND(ID_RENDERMODE_SOLID, &CMainFrame::OnRendermodeSolid)
	ON_COMMAND(ID_RENDERMODE_WIREFRAME, &CMainFrame::OnRendermodeWireframe)
	ON_COMMAND(ID_VIEW_CENTERATORIGIN, &CMainFrame::OnViewCenteratorigin)
	ON_COMMAND(ID_HELP_MAKEADONATION, &CMainFrame::OnHelpMakeadonation)
	ON_COMMAND(ID_HELP_OTHERGREATPROGRAMS, &CMainFrame::OnHelpOthergreatprograms)
	ON_COMMAND(ID_VIEW_SHOWTERRAINBOUNDARIES, &CMainFrame::OnViewShowterrainboundaries)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOWTERRAINBOUNDARIES, &CMainFrame::OnUpdateViewShowterrainboundaries)
	ON_COMMAND(ID_TERRAIN_TOGGLE_PAINT_MODE, &CMainFrame::OnTerrainTogglePaintMode)
	ON_UPDATE_COMMAND_UI(ID_TERRAIN_TOGGLE_PAINT_MODE, &CMainFrame::OnUpdateTerrainTogglePaintMode)
	ON_COMMAND(ID_TERRAIN_TERRAIN_TEXTURING_SETTINGS, &CMainFrame::OnTerrainTerrainTexturingSettings)
	ON_WM_DESTROY()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame construction/destruction

CMainFrame::CMainFrame()
	: m_initSplitters(false)
{
	
}

CMainFrame::~CMainFrame()
{
	DestroyWindow( );
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	HBITMAP hbm = (HBITMAP)::LoadImage( AfxGetInstanceHandle( ),
		MAKEINTRESOURCE( IDR_MAINFRAME ),
		IMAGE_BITMAP,
		0, 0, // cx, cy
		LR_CREATEDIBSECTION | LR_LOADMAP3DCOLORS );

	CBitmap bm;
	bm.Attach(hbm);
	m_ToolbarImageList.Create( 24, 24, ILC_COLOR32 | ILC_MASK, 4, 4);
	m_ToolbarImageList.Add( &bm, RGB(255, 0, 255) );
	bm.DeleteObject( );


	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER /*| CBRS_TOOLTIPS*/ | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	m_wndToolBar.GetToolBarCtrl( ).SetImageList( &m_ToolbarImageList );

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);


	m_MenuIcons[ MI_CHECKMARK ].LoadBitmap( IDB_MENUCHECK_BITMAP );
	m_MenuIcons[ MI_UNCHECKMARK ].LoadBitmap( IDB_MENUUNCHECK_BITMAP );
	m_MenuIcons[ MI_CENTER ].LoadBitmap( IDB_MENUCENTER_BITMAP );
	m_MenuIcons[ MI_TILEBOUNDS ].LoadBitmap( IDB_TILEBOUNDS_BITMAP );
	m_MenuIcons[ MI_WIREFRAME ].LoadBitmap( IDB_MENUWIREFRAME_BITMAP );
	m_MenuIcons[ MI_SOLID ].LoadBitmap( IDB_MENUSOLID_BITMAP );


	
	CMenu *pMenu = this->GetMenu( );
	CMenu *pSubMenu = pMenu->GetSubMenu( 2 );

	pSubMenu->SetMenuItemBitmaps( ID_VIEW_TOOLBAR, MF_BYCOMMAND, &m_MenuIcons[ MI_UNCHECKMARK ], &m_MenuIcons[ MI_CHECKMARK ] );
	pSubMenu->SetMenuItemBitmaps( ID_VIEW_STATUS_BAR, MF_BYCOMMAND, &m_MenuIcons[ MI_UNCHECKMARK ], &m_MenuIcons[ MI_CHECKMARK ] );
	pSubMenu->SetMenuItemBitmaps( ID_VIEW_CENTERATORIGIN, MF_BYCOMMAND, &m_MenuIcons[ MI_CENTER ], &m_MenuIcons[ MI_CENTER ] );
	pSubMenu->SetMenuItemBitmaps( ID_VIEW_SHOWTERRAINBOUNDARIES, MF_BYCOMMAND, &m_MenuIcons[ MI_TILEBOUNDS ], &m_MenuIcons[ MI_TILEBOUNDS ] );

	pSubMenu->GetSubMenu( 2 );
	pSubMenu->SetMenuItemBitmaps( ID_RENDERMODE_WIREFRAME, MF_BYCOMMAND, &m_MenuIcons[ MI_WIREFRAME ], &m_MenuIcons[ MI_WIREFRAME ] );
	pSubMenu->SetMenuItemBitmaps( ID_RENDERMODE_SOLID, MF_BYCOMMAND, &m_MenuIcons[ MI_SOLID ], &m_MenuIcons[ MI_SOLID ] );


	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE
		 | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE | WS_SYSMENU;

	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers




BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	CRect cr;
	GetClientRect( &cr );


	if( !m_mainSplitter.CreateStatic( this, 1, 2 ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"), MB_OK | MB_ICONERROR );
		return FALSE;
	}

	if( !m_mainSplitter.CreateView( 0, 1, RUNTIME_CLASS(CMapPropertiesForm), CSize( MAPPROPERTIESFORM_WIDTH, cr.Height() ), pContext ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"), MB_OK | MB_ICONERROR );
		return FALSE;
	}

	if( !m_dividerSplitter.CreateStatic( &m_mainSplitter, 2, 1, WS_CHILD | WS_VISIBLE, m_mainSplitter.IdFromRowCol( 0, 0 ) ) )
	{	
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"), MB_OK | MB_ICONERROR );
		return FALSE;
	}



	if( !m_viewportSplitter.CreateStatic( &m_dividerSplitter, 2, 2, WS_CHILD | WS_VISIBLE, m_dividerSplitter.IdFromRowCol( 0, 0 ) ) )
	{	
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"), MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// Create the views for each pane in the viewport
	// splitter.
	

	// Top viewport
	if( !m_viewportSplitter.CreateView( 0, 0, RUNTIME_CLASS(CTop),
		CSize( ( cr.Width() - MAPPROPERTIESFORM_WIDTH ) / 2, cr.Height() / 2 ), pContext ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"),
			MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// Front viewport
	if( !m_viewportSplitter.CreateView( 0, 1, RUNTIME_CLASS(CFront),
		CSize( ( cr.Width() - MAPPROPERTIESFORM_WIDTH ) / 2, cr.Height() / 2 ), pContext ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"),
			MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// Side viewport
	if( !m_viewportSplitter.CreateView( 1, 0, RUNTIME_CLASS(CSide),
		CSize( ( cr.Width() - MAPPROPERTIESFORM_WIDTH ) / 2, cr.Height() / 2 ), pContext ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"),
			MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// Perspective viewport
	if( !m_viewportSplitter.CreateView( 1, 1, RUNTIME_CLASS(CPerspectiveView),
		CSize( ( cr.Width() - MAPPROPERTIESFORM_WIDTH ) / 2, cr.Height() / 2 ), pContext ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"),
			MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// Has to be after Perspective view because Core::initialize()...
	if( !m_dividerSplitter.CreateView( 1, 0, RUNTIME_CLASS(CMapAssetsForm), CSize( cr.Width( ) - MAPPROPERTIESFORM_WIDTH, MAPASSETSFORM_HEIGHT ), pContext ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"), MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// By this time, this object has instantiated the shared RC's and
	// is no longer needed...
	CGLRCSharer *pRCSharer = CGLRCSharer::getInstance();
	delete pRCSharer;



	m_initSplitters = true;
	return TRUE;//return CFrameWnd::OnCreateClient(lpcs, pContext);
}

void CMainFrame::OnSize(UINT nType, int cx, int cy)
{
	CFrameWnd::OnSize(nType, cx, cy);
	CRect cr;

	if(  m_initSplitters && nType != SIZE_MINIMIZED )
	{
		m_mainSplitter.SetRowInfo( 0, cy, 0 );
		m_mainSplitter.SetColumnInfo( 0, cx - MAPPROPERTIESFORM_WIDTH, 0 );
		m_mainSplitter.SetColumnInfo( 1, MAPPROPERTIESFORM_WIDTH, 0 );

		m_dividerSplitter.SetRowInfo( 0, cy - MAPASSETSFORM_HEIGHT, 0 );
		m_dividerSplitter.SetRowInfo( 1, MAPASSETSFORM_HEIGHT, 0 );
		m_dividerSplitter.SetColumnInfo( 0, cx - MAPPROPERTIESFORM_WIDTH, 0 );

		m_viewportSplitter.GetClientRect( &cr );
		m_viewportSplitter.SetRowInfo( 0, (cy - MAPASSETSFORM_HEIGHT) / 2, 0 );
		m_viewportSplitter.SetRowInfo( 1, (cy - MAPASSETSFORM_HEIGHT) / 2, 0 );
		m_viewportSplitter.SetColumnInfo( 0, (cx - MAPPROPERTIESFORM_WIDTH) / 2, 0 );
		m_viewportSplitter.SetColumnInfo( 1, (cx - MAPPROPERTIESFORM_WIDTH) / 2, 0 );
		
		
		m_viewportSplitter.RecalcLayout( );
		m_mainSplitter.RecalcLayout( );
		m_dividerSplitter.RecalcLayout( );
		

		//m_viewportSplitter.GetPane()
	}
}

void CMainFrame::OnRendermodeSolid()
{
	COpenGLWnd *wnd = reinterpret_cast<COpenGLWnd *>( GetActiveView( ) );
	wnd->setWireFrameRenderMode( false );
	wnd->Invalidate( );
}

void CMainFrame::OnRendermodeWireframe()
{
	COpenGLWnd *wnd = reinterpret_cast<COpenGLWnd *>( GetActiveView( ) );
	wnd->setWireFrameRenderMode( true );
	wnd->Invalidate( );
}

void CMainFrame::OnViewCenteratorigin()
{
	COpenGLWnd *wnd = reinterpret_cast<COpenGLWnd *>( GetActiveView( ) );
	if( wnd->IsKindOf( RUNTIME_CLASS(COrthographicView) ) )
		wnd->setTranslationVector( 0.0f, 0.0f, 0.0f );
	else
		wnd->setTranslationVector( 0.0f, -5.0f, 0.0f );
	wnd->Invalidate( );
}

void CMainFrame::OnHelpMakeadonation()
{
	// Go to the make a donation web page...
	::ShellExecute( 0, _T("open"), _T("http://www.l33tprogrammer.com/index.php?page=donation"), NULL, NULL, SW_SHOWNORMAL );
}

void CMainFrame::OnHelpOthergreatprograms()
{
	// Go to homepage...
	::ShellExecute( 0, _T("open"), _T("http://www.l33tprogrammer.com/"), NULL, NULL, SW_SHOWNORMAL );
}

void CMainFrame::OnViewShowterrainboundaries()
{
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	bool bMode = pDoc->isRenderBoundaryModeSet( );

	pDoc->setRenderBoundaryMode( !bMode );
	UpdateType update = UPD_OPENGL;
	pDoc->UpdateAllViews( NULL, update );
}

void CMainFrame::OnUpdateViewShowterrainboundaries(CCmdUI *pCmdUI)
{	
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	pCmdUI->SetCheck( pDoc->isRenderBoundaryModeSet( ) );
}

void CMainFrame::OnTerrainTogglePaintMode()
{
	bool bMode = Core::selections.isPickingOn( );
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );

	Core::selections.setPicking( !bMode );
	pDoc->UpdateAllViews( NULL, UPD_OPENGL );
}

void CMainFrame::OnUpdateTerrainTogglePaintMode(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck( Core::selections.isPickingOn( ) );
}

void CMainFrame::setTerrainPaintMode( bool mode )
{
	Core::selections.setPicking( false );
}

void CMainFrame::OnTerrainTerrainTexturingSettings()
{
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	CTerrainTexturingDlg &dlg = pDoc->getTerrainTexturingDlg( );
	dlg.ShowWindow( SW_SHOW );
}

void CMainFrame::OnDestroy()
{
	CFrameWnd::OnDestroy();

	for( int i = 0; i < 7; i++ )
		if( m_MenuIcons[ i ].m_hObject ) m_MenuIcons[ i ].DeleteObject( );

	m_ToolbarImageList.DeleteImageList( );



	//m_viewportSplitter.DeleteView( 0, 0 );
	//m_viewportSplitter.DeleteView( 0, 1 );
	////m_viewportSplitter.DeleteView( 1, 1 );
	//m_viewportSplitter.DeleteView( 1, 0 );
	//m_viewportSplitter.DestroyWindow( );

	//m_dividerSplitter.DeleteView( 1, 0 );
	//m_dividerSplitter.DestroyWindow( );

	//m_mainSplitter.DeleteView( 0, 1 );
	//m_mainSplitter.DestroyWindow( );

}
