// OrthogonalView.cpp : implementation file
//

#include "stdafx.h"
#include "MapEditor.h"
#include "OrthographicView.h"
#include <Core.h>

// COrthographicView

IMPLEMENT_DYNCREATE(COrthographicView, COpenGLWnd)

COrthographicView::COrthographicView()
: COpenGLWnd( )
{
	m_zoom = 0.05f;
	m_xTranslation = 0.0;
	m_yTranslation = 0.0;
	m_zTranslation = -5.0;
	m_xRotation = 0.0;
	m_yRotation = 0.0;
	m_zRotation = 0.0;
	m_dAspect = 1.0;
	m_dNearPlane = 1.0;
	m_dFarPlane = 100.0;
}

COrthographicView::~COrthographicView()
{
}

BEGIN_MESSAGE_MAP(COrthographicView, COpenGLWnd)
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_MBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()


// COrthographicView drawing

void COrthographicView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	// move around
	glScalef( m_zoom, m_zoom, m_zoom );
	glTranslatef( m_xTranslation, m_yTranslation, m_zTranslation );
	
	//DrawGrid( 24 );
	COpenGLWnd::RenderScene( );

	SwapGLBuffers( );
}


void COrthographicView::SetFrustum( )
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho( -1.0 * m_dAspect, 1.0 * m_dAspect, -1.0, 1.0, -1000.0, 1000.0 );
	glMatrixMode(GL_MODELVIEW);
}

bool COrthographicView::InitOpenGL( )
{
	COpenGLWnd::InitOpenGL( );
	return true;
}

// COrthographicView diagnostics

#ifdef _DEBUG
void COrthographicView::AssertValid() const
{
	COpenGLWnd::AssertValid();
}

#ifndef _WIN32_WCE
void COrthographicView::Dump(CDumpContext& dc) const
{
	COpenGLWnd::Dump(dc);
}
#endif
#endif //_DEBUG

void COrthographicView::OnSize(UINT nType, int cx, int cy)
{
	SetContext( );
	COpenGLWnd::OnSize(nType, cx, cy);
}

void COrthographicView::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_ptLeftDownPos = point;
	COpenGLWnd::OnLButtonDown(nFlags, point);
}

void COrthographicView::OnMButtonDown(UINT nFlags, CPoint point)
{
	m_ptLeftDownPos = point;
	COpenGLWnd::OnMButtonDown(nFlags, point);
}

void COrthographicView::OnRButtonDown(UINT nFlags, CPoint point)
{
	m_ptLeftDownPos = point;
	COpenGLWnd::OnRButtonDown(nFlags, point);
}

void COrthographicView::OnInitialUpdate()
{
	COpenGLWnd::OnInitialUpdate();

}

