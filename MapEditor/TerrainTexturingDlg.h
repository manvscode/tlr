#pragma once

#include <afxole.h>
#include <afxtempl.h>
#include "CoolButton.h"
#include "afxwin.h"
#include "TextureCtrl.h"
#include "resource.h"

// CTerrainTexturingDlg dialog

class CTerrainTexturingDlg : public CDialog
{
	DECLARE_DYNAMIC(CTerrainTexturingDlg)

public:
	CTerrainTexturingDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTerrainTexturingDlg();

// Dialog Data
	enum { IDD = IDD_TERRAINTEXTURING_DIALOG };

protected:
	CCoolButton m_Buttons[ 8 ];
	//COleDropTarget m_DragAndDropTarget;
	CTextureCtrl m_Texture[ 6 ];
	bool m_bControlsSubclassed;
	CArray<unsigned int> m_TextureLayers;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedTexture0Button();
	afx_msg void OnBnClickedTexture1Button();
	afx_msg void OnBnClickedTexture2Button();
	afx_msg void OnBnClickedTexture3Button();
	afx_msg void OnBnClickedTexture4Button();
	afx_msg void OnBnClickedTexture5Button();
	afx_msg void OnBnClickedOk();

	afx_msg void OnDestroy();
};
