// MapAssetsForm.cpp : implementation file
//

#include "stdafx.h"
#include "MapEditor.h"
#include "MapAssetsForm.h"
#include "AddTileSetDlg.h"
#include "MapEditorDoc.h"
#include "TerrainTexturingDlg.h"
#include "afx.h"
#include "mfc_util.h"
#include <sstream>
#include <Core.h>
#include <ImageIO.h>
#include <utility/memleakcheck.h>




// CMapAssetsForm

IMPLEMENT_DYNCREATE(CMapAssetsForm, CFormView)

CMapAssetsForm::CMapAssetsForm()
	: CFormView(CMapAssetsForm::IDD), m_bCreated(false), m_bAfterInitialUpdate(false), m_DragAndDropSource( )
{
}

CMapAssetsForm::~CMapAssetsForm()
{
}

void CMapAssetsForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TERRAINTILELIST, m_TerrainTileListCtrl);
	DDX_Control(pDX, IDC_ADDTILESET_BUTTON, m_AddTileSetBtn);
	DDX_Control(pDX, IDC_ENTITYTILELIST, m_EntityTileListCtrl);
	DDX_Control(pDX, IDC_BUILDINGLIST, m_BuildingListCtrl);
	DDX_Control(pDX, IDC_ASSET_TAB, m_AssetTabCtrl);
	
}

BEGIN_MESSAGE_MAP(CMapAssetsForm, CFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_ADDTILESET_BUTTON, &CMapAssetsForm::OnBnClickedAddtilesetButton)
	ON_NOTIFY(TCN_SELCHANGE, IDC_ASSET_TAB, &CMapAssetsForm::OnTcnSelchangeAssetTab)
	ON_NOTIFY(LVN_BEGINDRAG, IDC_TERRAINTILELIST, &CMapAssetsForm::OnLvnBegindragTerraintilelist)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CMapAssetsForm diagnostics

#ifdef _DEBUG
void CMapAssetsForm::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CMapAssetsForm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CMapAssetsForm message handlers

int CMapAssetsForm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	CRect rc;
	GetClientRect( &rc );

	// Create the image list for the tab control...
	m_TabImageList.Create( 16, 16, ILC_COLOR32, 0, 0 );
	m_TabImageList.Add( AfxGetApp( )->LoadIcon( MAKEINTRESOURCE(IDI_TERRAINICON) ) );
	m_TabImageList.Add( AfxGetApp( )->LoadIcon( MAKEINTRESOURCE(IDI_ENTITYICON) ) );
	m_TabImageList.Add( AfxGetApp( )->LoadIcon( MAKEINTRESOURCE(IDI_BUILDINGICON) ) );

	//Create the Tab control
	m_AssetTabCtrl.Create( TCS_TABS | TCS_FIXEDWIDTH | TCS_FLATBUTTONS | TCS_FORCELABELLEFT | WS_CHILD | WS_VISIBLE, rc, this, IDC_ASSET_TAB );
	m_AssetTabCtrl.SetImageList( &m_TabImageList );
	m_AssetTabCtrl.InsertItem( 0, _T("Terrain Tiles"), 0 );
	m_AssetTabCtrl.InsertItem( 1, _T("Entity Objects"), 1 );
	m_AssetTabCtrl.InsertItem( 2, _T("Buildings"), 2 );
	m_AssetTabCtrl.SetPadding( CSize( 3, 5 ) );
	m_AssetTabCtrl.SetItemSize( CSize( 150, 20 ) );

	// Create the image lists for the terrain & entity tile sets...
	m_TerrainImageList.Create( IMAGELIST_TILE_X_DIMENSION, IMAGELIST_TILE_Y_DIMENSION, ILC_COLOR32, 0, 10 );
	m_TerrainImageList.SetBkColor( RGB(255,255,255) );
	
	m_EntityImageList.Create( IMAGELIST_TILE_X_DIMENSION, IMAGELIST_TILE_Y_DIMENSION, ILC_COLOR32, 0, 10 );
	m_EntityImageList.SetBkColor( RGB(255,255,255) );

	m_BuildingImageList.Create( IMAGELIST_TILE_X_DIMENSION, IMAGELIST_TILE_Y_DIMENSION, ILC_COLOR32, 0, 10 );
	m_BuildingImageList.SetBkColor( RGB(255,255,255) );

	// Terrain tile list controls...
	m_TerrainTileListCtrl.Create( WS_CHILD | WS_VISIBLE | WS_BORDER | LVS_ICON /*| LVS_NOSCROLL*/ |
								LVS_NOLABELWRAP | LVS_SINGLESEL | LVS_SHOWSELALWAYS | LVS_ALIGNLEFT |
								LVS_TYPEMASK, rc, this, IDC_TERRAINTILELIST );
	m_TerrainTileListCtrl.SetImageList( &m_TerrainImageList, LVSIL_NORMAL );
	m_TerrainTileListCtrl.SetIconSpacing( IMAGELIST_TILE_X_DIMENSION + 2, IMAGELIST_TILE_Y_DIMENSION + 2 );
	m_TerrainTileListCtrl.SetExtendedStyle( LVS_EX_BORDERSELECT | LVS_EX_FLATSB );
	m_TerrainTileListCtrl.SetView( LV_VIEW_ICON );
	m_TerrainTileListCtrl.SetBkColor( RGB(40,40,40) );
	m_TerrainTileListCtrl.SetTextColor( RGB(255,255,255) );
	m_TerrainTileListCtrl.SetOutlineColor( RGB(200,0,0) );
	m_TerrainTileListCtrl.SetInsertMarkColor( RGB(255,0,0) );

	// Entity tile list control
	m_EntityTileListCtrl.Create( WS_CHILD | WS_VISIBLE | WS_BORDER | LVS_ICON /*| LVS_NOSCROLL*/ |
								LVS_NOLABELWRAP | LVS_SINGLESEL | LVS_SHOWSELALWAYS | LVS_ALIGNLEFT |
								LVS_TYPEMASK, rc, this, IDC_ENTITYTILELIST );
	m_EntityTileListCtrl.SetImageList( &m_EntityImageList, LVSIL_NORMAL );
	m_EntityTileListCtrl.SetIconSpacing( IMAGELIST_TILE_X_DIMENSION + 2, IMAGELIST_TILE_Y_DIMENSION + 2 );
	m_EntityTileListCtrl.SetExtendedStyle( LVS_EX_BORDERSELECT | LVS_EX_FLATSB );
	m_EntityTileListCtrl.SetView( LV_VIEW_ICON );
	m_EntityTileListCtrl.SetBkColor( RGB(40,40,40) );
	m_EntityTileListCtrl.SetTextColor( RGB(255,255,255) );
	m_EntityTileListCtrl.SetOutlineColor( RGB(200,0,0) );
	m_EntityTileListCtrl.SetInsertMarkColor( RGB(255,0,0) );

	// Set up Building list control...
	m_BuildingListCtrl.Create( WS_CHILD | WS_VISIBLE | WS_BORDER | LVS_ICON /*| LVS_NOSCROLL*/ |
								LVS_NOLABELWRAP | LVS_SINGLESEL | LVS_SHOWSELALWAYS | LVS_ALIGNLEFT |
								LVS_TYPEMASK, rc, this, IDC_ENTITYTILELIST );
	m_BuildingListCtrl.SetImageList( &m_BuildingImageList, LVSIL_NORMAL );
	m_BuildingListCtrl.SetIconSpacing( IMAGELIST_TILE_X_DIMENSION + 2, IMAGELIST_TILE_Y_DIMENSION + 2 );
	m_BuildingListCtrl.SetExtendedStyle( LVS_EX_BORDERSELECT | LVS_EX_FLATSB );
	m_BuildingListCtrl.SetView( LV_VIEW_ICON );
	m_BuildingListCtrl.SetBkColor( RGB(40,40,40) );
	m_BuildingListCtrl.SetTextColor( RGB(255,255,255) );
	m_BuildingListCtrl.SetOutlineColor( RGB(200,0,0) );
	m_BuildingListCtrl.SetInsertMarkColor( RGB(255,0,0) );


	// set up who gets initial visibility
	m_TerrainTileListCtrl.EnableWindow( TRUE );
	m_TerrainTileListCtrl.ShowWindow( SW_SHOW );
	m_EntityTileListCtrl.EnableWindow( FALSE );
	m_EntityTileListCtrl.ShowWindow( SW_HIDE );
	m_BuildingListCtrl.EnableWindow( FALSE );
	m_BuildingListCtrl.ShowWindow( SW_HIDE );


	// Add default tile sets...
	addDefaultTileSets( );
	
	// Disable vertical scrollbars...
	//m_TerrainTileListCtrl.ShowScrollBar( SB_VERT, FALSE );
	//m_EntityTileListCtrl.ShowScrollBar( SB_VERT, FALSE );

	m_bCreated = true;
	return 0;
}

void CMapAssetsForm::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if( m_bCreated )
	{
		CRect rc;
		GetClientRect( &rc );

		rc.right += 2;
		rc.bottom += 2;

		m_AssetTabCtrl.SetWindowPos( &wndBottom, rc.left, rc.top, rc.right, rc.bottom, SWP_SHOWWINDOW );


		// move/resize list boxes...
		m_TerrainTileListCtrl.MoveWindow( 5, rc.top + 30, rc.right - 12, rc.Height() - 35, FALSE );
		m_EntityTileListCtrl.MoveWindow( 5, rc.top + 30, rc.right - 12, rc.Height() - 35, FALSE );
		m_BuildingListCtrl.MoveWindow( 5, rc.top + 30, rc.right - 12, rc.Height() - 35, FALSE );
		if( m_TerrainTileListCtrl.IsWindowVisible( ) ) m_TerrainTileListCtrl.InvalidateRect( NULL, false );
		if( m_EntityTileListCtrl.IsWindowVisible( ) ) m_EntityTileListCtrl.InvalidateRect( NULL, false );
		if( m_BuildingListCtrl.IsWindowVisible( ) ) m_BuildingListCtrl.InvalidateRect( NULL, false );

		// Disable vertical scrollbars...
		//m_TerrainTileListCtrl.ShowScrollBar( SB_VERT, FALSE );
		//m_EntityTileListCtrl.ShowScrollBar( SB_VERT, FALSE );
		//m_BuildingListCtrl.ShowScrollBar( SB_VERT, FALSE );

		// move the Add Tile Set Button...
		if( m_bAfterInitialUpdate )
		{
			CRect rcAddTilesSetBtn;
			m_AddTileSetBtn.GetClientRect( rcAddTilesSetBtn );
			m_AddTileSetBtn.SetWindowPos( &wndTop, cx - rcAddTilesSetBtn.Width(), 1, rcAddTilesSetBtn.Width(), rcAddTilesSetBtn.Height( ), SWP_NOOWNERZORDER | SWP_SHOWWINDOW );
		}
	
		////m_TerrainTileListCtrl.RedrawWindow( );
		////m_EntityTileListCtrl.RedrawWindow( );
		////m_BuildingListCtrl.RedrawWindow( );
	}
}

void CMapAssetsForm::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	switch( lHint )
	{
		case UPD_OPENGL:
			//InvalidateRect( NULL, false );
			break;
		case UPG_MAPASSETSFORM:
		default:
			if( m_TerrainTileListCtrl.IsWindowVisible( ) )
				m_TerrainTileListCtrl.RedrawWindow( );
			if( m_EntityTileListCtrl.IsWindowVisible( ) )
				m_EntityTileListCtrl.RedrawWindow( );
			if( m_BuildingListCtrl.IsWindowVisible( ) )
				m_BuildingListCtrl.RedrawWindow( );
				
				// m_BuildingListCtrl.RedrawWindow( );
			break;
	}

}

//BOOL CMapAssetsForm::OnEraseBkgnd(CDC* pDC)
//{
//	//CRect rc;
//	//GetClientRect( &rc );
//
//	//pDC->FillSolidRect( &rc, RGB( 0, 0, 0 ) );
//	//return TRUE;
//	return CFormView::OnEraseBkgnd(pDC);
//}

void CMapAssetsForm::OnBnClickedAddtilesetButton()
{
	CAddTileSetDlg dlg;

	if( dlg.DoModal( ) == IDOK )
	{
		addTileSet( dlg.getTileSetType( ), dlg.getTileSetPath( ) );

		// deselect all textures
		glBindTexture( GL_TEXTURE_2D, 0 );
		
		// Disable vertical scrollbars...
		//m_TerrainTileListCtrl.ShowScrollBar( SB_VERT, FALSE );
		//m_EntityTileListCtrl.ShowScrollBar( SB_VERT, FALSE );
		AfxMessageBox( _T("Adding Tiles complete.") );
	}
}

void CMapAssetsForm::addTileSet( TileSetType type, CString &path, bool isVerbose )
{
	CFileFind fileFind;

	path.Append( _T("\\*.*") );
	BOOL bWorking = fileFind.FindFile( path );
	
	while( bWorking )
	{
		bWorking = fileFind.FindNextFile( );
		CString &tilePath = fileFind.GetFilePath( );

		if( fileFind.IsDots( ) )
			continue;
		
		if( fileFind.IsDirectory( ) )
			addTileSet( type, tilePath );

		int i = tilePath.Find( '.' );
		CString extension = tilePath.Right( tilePath.GetLength( ) - i - 1 );
		
		if( extension.Compare( _T("tga") ) == 0 )
			addTile( type, tilePath, ResourceManagement::TX_TARGA, isVerbose );
		else if( extension.Compare( _T("bmp") ) == 0 )
			addTile( type, tilePath, ResourceManagement::TX_BITMAP, isVerbose );
		else {
			CString errorMsg;
			errorMsg.Format( _T("An unknown extension of '%s' was encountered will skip the file '%s'."),
							 extension, tilePath );
			
			if( isVerbose )
				AfxMessageBox( errorMsg );
		}
	}

	UpdateWindow( );
	fileFind.Close( );
}


void CMapAssetsForm::addTile( TileSetType type, CString &tilePath, ResourceManagement::TextureFormat format, bool isVerbose )
{
	CListCtrl *pListCtrl = NULL;
	unsigned int currentTexture = 0;

	switch( type )
	{
		case TST_TERRAIN:
			pListCtrl = &m_TerrainTileListCtrl;
			break;
		case TST_ENTITY:
			pListCtrl = &m_EntityTileListCtrl;
			break;
		case TST_BUILDINGS:
			pListCtrl = &m_BuildingListCtrl;
			break;
		default:
			if( isVerbose )
				AfxMessageBox( _T("Unrecognizable tile set type. Will proceed to ignore."), MB_OK | MB_ICONERROR );
			return;
	}

	char cFilePath[ _MAX_PATH ];
	WideCharToMultiByte( CP_ACP, WC_NO_BEST_FIT_CHARS, tilePath.GetString( ), -1, cFilePath, _MAX_PATH, NULL, NULL );
			
	// Add texture to Texture Manager...
	currentTexture = Core::pTextureManager->addTexture( format, cFilePath );

	if( type == TST_TERRAIN )
		Core::pTextureManager->setupTexture( currentTexture, false, GL_LINEAR, GL_LINEAR/*GL_LINEAR_MIPMAP_LINEAR*/ );

	// Add to terrain image list...
	int img = addTileToImageList( type, currentTexture );
	ASSERT( img >= 0 );

			// Add texture tile to list ctrl...
	ASSERT( pListCtrl != NULL );

	LVITEM item;
	memset( &item, 0, sizeof(LVITEM) );
	item.mask =  LVIF_IMAGE /*| LVIF_TEXT*/ | LVIF_PARAM;
	item.pszText = (PTCHAR) tilePath.GetString( );
	item.iItem = pListCtrl->GetItemCount( ); 
	item.iSubItem = 0;
	item.iImage = img;
	item.lParam = (LPARAM) currentTexture; /* Texture ID stored in lParam */

	pListCtrl->InsertItem( &item );
}



int CMapAssetsForm::addTileToImageList( TileSetType type, int texture )
{
	ResourceManagement::CTexture2D *tileTexture = Core::pTextureManager->find( texture );
	ASSERT( tileTexture != NULL );
	CBitmap bmp;
	
	int img = -1;

	// allocate memory for the copy of the texture...
	BYTE *tileBitmap = new BYTE[ IMAGELIST_TILE_X_DIMENSION * IMAGELIST_TILE_Y_DIMENSION * tileTexture->byteCount( ) ];
	
	#ifdef _DEBUG
		memset( tileBitmap, 0, sizeof( BYTE ) * IMAGELIST_TILE_X_DIMENSION * IMAGELIST_TILE_Y_DIMENSION * tileTexture->byteCount( ) );
	#endif

	// Resize the image and place into the tileBitmap...
	ImageIO::resizeImage( tileTexture->width(), tileTexture->height(), tileTexture->bitmap(),
						  IMAGELIST_TILE_X_DIMENSION, IMAGELIST_TILE_Y_DIMENSION, tileBitmap,
						  tileTexture->bitCount( ), ImageIO::ALG_NEARESTNEIGHBOR );

	// convert RGB to BGR...
	ImageIO::swapRBinRGB( IMAGELIST_TILE_X_DIMENSION, IMAGELIST_TILE_Y_DIMENSION, tileTexture->byteCount(), tileBitmap );

	// flip vertically
	ImageIO::flipImageVertically( IMAGELIST_TILE_X_DIMENSION, IMAGELIST_TILE_Y_DIMENSION, tileTexture->byteCount(), tileBitmap );

	HBITMAP hBmp = CreateBitmap( IMAGELIST_TILE_X_DIMENSION, IMAGELIST_TILE_Y_DIMENSION, 1, tileTexture->bitCount( ), tileBitmap );

	bmp.Attach( hBmp );
	


	switch( type )
	{
	case TST_TERRAIN:
		img = m_TerrainImageList.Add( &bmp, (CBitmap *) NULL );
		break;
	case TST_ENTITY:
		img = m_EntityImageList.Add( &bmp, (CBitmap *) NULL );
		break;
	case TST_BUILDINGS:
		img = m_BuildingImageList.Add( &bmp, (CBitmap *) NULL );
		break;
	default:
		ASSERT( false ); // This is a bug; How did we get a bad type?
		break;
	}

	bmp.DeleteObject( );
	DeleteObject( hBmp );
	delete [] tileBitmap;
	return img; // Same as CImageList::Add( ... );
}

//void CMapAssetsForm::OnTcnSelchangingAssetTab(NMHDR *pNMHDR, LRESULT *pResult)
//{
//	// TODO: Add your control notification handler code here
//
//
//
//
//	*pResult = 0;
//}


void CMapAssetsForm::addDefaultTileSets( )
{
	TCHAR  path[ MAX_PATH ]; // Allow space for Path AND FileName here.
	GetModuleFileName( GetModuleHandle(NULL), path, sizeof(path) );
	
	for( unsigned int i = wcsnlen( path, MAX_PATH ); i > 0; i-- )
		if( path[ i ] != '\\' )
			path[ i ] = '\0';
		else
			break;

	CString thePath = path;

	addTileSet( TST_TERRAIN, thePath + _T("assets\\Terrain"), false );
	addTileSet( TST_ENTITY, thePath + _T("assets\\Entity"), false );
	addTileSet( TST_BUILDINGS, thePath + _T("assets\\Buildings"), false );
	
	// deselect all textures
	glBindTexture( GL_TEXTURE_2D, 0 );
}

void CMapAssetsForm::OnTcnSelchangeAssetTab(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	int selected = m_AssetTabCtrl.GetCurSel( );

	switch( selected )
	{
		case 0:
			m_TerrainTileListCtrl.EnableWindow( TRUE );
			m_TerrainTileListCtrl.ShowWindow( SW_SHOW );
			
			m_EntityTileListCtrl.EnableWindow( FALSE );
			m_EntityTileListCtrl.ShowWindow( SW_HIDE );
			m_BuildingListCtrl.EnableWindow( FALSE );
			m_BuildingListCtrl.ShowWindow( SW_HIDE );
			break;	

		case 1:
			m_EntityTileListCtrl.EnableWindow( TRUE );
			m_EntityTileListCtrl.ShowWindow( SW_SHOW );
			
			m_TerrainTileListCtrl.EnableWindow( FALSE );
			m_TerrainTileListCtrl.ShowWindow( SW_HIDE );
			m_BuildingListCtrl.EnableWindow( FALSE );
			m_BuildingListCtrl.ShowWindow( SW_HIDE );
			break;
		case 2:
			m_BuildingListCtrl.EnableWindow( TRUE );
			m_BuildingListCtrl.ShowWindow( SW_SHOW );

			m_EntityTileListCtrl.EnableWindow( FALSE );
			m_EntityTileListCtrl.ShowWindow( SW_HIDE );
			m_TerrainTileListCtrl.EnableWindow( FALSE );
			m_TerrainTileListCtrl.ShowWindow( SW_HIDE );
			
			break;
		default:
			break;
	}
	*pResult = 0;
}

void CMapAssetsForm::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO: Add your specialized code here and/or call the base class
	m_bAfterInitialUpdate = true;
}

void CMapAssetsForm::OnLvnBegindragTerraintilelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	// Create global memory for sharing dragdrop text
	CMapEditorDoc *pDoc = CMapEditorDoc::getInstance( );
	CTerrainTexturingDlg &dlg = pDoc->getTerrainTexturingDlg( );
	::SetWindowPos( dlg.m_hWnd, HWND_TOPMOST, 0, 0, 200, 200, SWP_NOSIZE | SWP_NOMOVE );


	HGLOBAL hgData = GlobalAlloc( GPTR, sizeof(unsigned int) );   
	ASSERT( hgData != NULL );

	// Lock global data (get pointer)
	unsigned int *lpData = (unsigned int *) GlobalLock( hgData );
	ASSERT( lpData != NULL );
	
	unsigned int textureId = m_TerrainTileListCtrl.GetItemData( pNMLV->iItem );

	memcpy( (LPVOID) lpData, &textureId/* Texture ID */, sizeof(unsigned int) );


	// Cache the data, and initiate DragDrop
	m_OleDataSource.CacheGlobalData( CF_TEXT, hgData );   

	CRect rClient;
	GetClientRect( rClient );
	DROPEFFECT dropEffect = m_OleDataSource.DoDragDrop( DROPEFFECT_MOVE | DROPEFFECT_COPY | DROPEFFECT_LINK, (LPCRECT) &rClient );  

	// Clear the Data Source's cache
	m_OleDataSource.Empty( );


	*pResult = 0;
}

void CMapAssetsForm::OnDestroy()
{
	CFormView::OnDestroy();

	m_TabImageList.DeleteImageList( );
	m_TerrainImageList.DeleteImageList( );
	m_EntityImageList.DeleteImageList( );
	m_BuildingImageList.DeleteImageList( );

	m_AssetTabCtrl.DestroyWindow( );
	m_TerrainTileListCtrl.DestroyWindow( );
	m_EntityTileListCtrl.DestroyWindow( );
	m_BuildingListCtrl.DestroyWindow( );

	//pListCtrl->
}
