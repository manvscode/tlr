#pragma once
#include "OrthographicView.h"

// CTop view

class CTop : public COrthographicView
{
	DECLARE_DYNCREATE(CTop)

protected:
	CTop();           // protected constructor used by dynamic creation
	virtual ~CTop();


	double m_Modelview[ 16 ];
	double m_Projection[ 16 ];
	int m_Viewport[ 4 ];

public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};


