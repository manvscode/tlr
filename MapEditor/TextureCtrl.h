#pragma once

#include "OleDropWndTarget.h"

// CTextureCtrl

class CTextureCtrl : public CWnd
{
	DECLARE_DYNAMIC(CTextureCtrl)
protected:
	bool m_bBitmapDataAllocated;
	BITMAP m_BitmapData;
	HBITMAP m_hBitmap;
	COleDropWndTarget m_DropWndTarget;
	unsigned int m_uiTextureID;



public:
	CTextureCtrl();
	virtual ~CTextureCtrl();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	void initialize( );
	void setTexture( const BITMAP &bmp,  unsigned int textureID = 0 );
	void setTexture( unsigned int textureID );
	unsigned int getTexture( ) const;
	void reset( );
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
};


inline unsigned int CTextureCtrl::getTexture( ) const
{ return m_uiTextureID; }