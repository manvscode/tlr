#pragma once
#pragma comment(lib, "opengl32.lib")				// Link OpenGL32.lib
#pragma comment(lib, "glu32.lib")				// Link Glu32.lib
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math/Vertex.h>
#include <math/Vector4.h>
#include "MapEditorDoc.h"

#include <Game/Map/MapSelectable.h>
// COpenGLWnd view


// light0
static GLfloat light_position[4] = { 0.0f, 0.0f, 0.0f, 1.0f }; // light source is infinitely away
//static GLfloat light_vector[4] = { 2.0f, -1.0f, 2.0f, 0.0f }; //Must relate to grid, light point down at a slight angle
//static GLfloat ambient_light[4] = {0.2f, 0.2f, 0.2f, 0.0f }; //not to bright white light

static Math::CVector4<float> light_vector( -3.0f, -3.0f, -2.0f, 0.0f );
static Math::CVector4<float> diffuse_light( 0.7f, 0.7f, 0.7f, 0.0f );
static Math::CVector4<float> ambient_light( 0.05f, 0.05f, 0.05f, 0.0f );


//Generic Material
static GLfloat ambientMaterial[4] = {0.5f, 0.5f, 0.5f, 0.0f};

inline void DrawCube( CMapEditorDoc* doc );


class COpenGLWnd : public CView
{
	DECLARE_DYNCREATE(COpenGLWnd)

protected:
	COpenGLWnd();           // protected constructor used by dynamic creation
	virtual ~COpenGLWnd();

public:
	bool SetDCPixelFormat( HDC hDC, DWORD dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER );
	virtual bool InitOpenGL( );
	virtual void SetGLConfig( );
	virtual void SetGLPickingConfig( );
	void SetContext( ) { wglMakeCurrent( m_hDC->GetSafeHdc( ), m_hRC ); }
	void SwapGLBuffers( ) { 	SwapBuffers( m_hDC->GetSafeHdc( ) ); }
	void DrawLine( VERTEX *v1, VERTEX *v2 );
	void DrawGrid( const float grid_size = 25.0 );
	void DrawOriginAxes( const float axis_size = 1.5 );
	virtual void RenderScene( );
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void SetFrustum( );
	void SetRenderFunc( void (*func) ( CDocument* ) ) { m_RenderScene = func; }

	void setTranslationVector( const float x, const float y, const float z );
	static void resetSelectedTile( );

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	void setWireFrameRenderMode( const bool bWireframe = true );
	bool isRenderModeInWireframe( ) const;
	HGLRC getGLRC( ) const;

protected:
	void (*m_RenderScene)( CDocument *doc );	// void function pointer to the rendering

	HWND m_hWnd;
	CDC* m_hDC;
	HGLRC	m_hRC;
	double		m_dNearPlane; // always positive 
	double		m_dFarPlane;  // always positive
	double		m_dAspect;
	bool m_bRenderWireframe;
//private:
//	GLuint *m_SelectionBuffer;
//	GLsizei m_SelectionBufferSize;
protected:
// Attributes
	float m_zoom,
		  m_xTranslation,
		  m_yTranslation,
		  m_zTranslation,
		  m_xRotation,
		  m_yRotation,
		  m_zRotation;
	float m_fRadius;
	double m_dMaxObjSize;
	


	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
};

inline HGLRC COpenGLWnd::getGLRC( ) const
{ return m_hRC; }

inline void DrawCube( CDocument* doc )
{
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambientMaterial);

	glPushMatrix( );
		glTranslatef(0.0f, 1.0f, 1.0f);
		glBegin( GL_TRIANGLE_STRIP );
		glColor4f( 1.0f, 0.0f, 0.0f, 0.25f );
			glVertex3f( -1.0f, -1.0f, 0.0f );
			glVertex3f( 1.0f, -1.0f, 0.0f );
			glVertex3f( -1.0f, 1.0f, 0.0f );
			glVertex3f( 1.0f, 1.0f, 0.0f );
		glEnd();

		glBegin( GL_TRIANGLE_STRIP );
		glColor4f( 1.0f, 0.0f, 0.0f, 0.25f );
			glVertex3f( -1.0f, -1.0f, -2.0f );
			glVertex3f( 1.0f, -1.0f, -2.0f );
			glVertex3f( -1.0f, 1.0f, -2.0f );
			glVertex3f( 1.0f, 1.0f, -2.0f );
		glEnd();

		glBegin( GL_TRIANGLE_STRIP );
		glColor4f( 0.0f, 1.0f, 0.0f, 0.25f );
			glVertex3f( -1.0f, -1.0f, 0.0f );
			glVertex3f( -1.0f, -1.0f, -2.0f );
			glVertex3f( -1.0f, 1.0f, 0.0f );
			glVertex3f( -1.0f, 1.0f, -2.0f );
		glEnd();

		glBegin( GL_TRIANGLE_STRIP );
		glColor4f( 0.0f, 1.0f, 0.0f, 0.25f );
			glVertex3f( 1.0f, -1.0f, 0.0f );
			glVertex3f( 1.0f, -1.0f, -2.0f );
			glVertex3f( 1.0f, 1.0f, 0.0f );
			glVertex3f( 1.0f, 1.0f, -2.0f );
		glEnd();

		glBegin( GL_TRIANGLE_STRIP );
		glColor4f( 0.0f, 0.0f, 1.0f, 0.25f );
			glVertex3f( -1.0f, 1.0f, 0.0f );
			glVertex3f( 1.0f, 1.0f, 0.0f );
			glVertex3f( -1.0f, 1.0f, -2.0f );
			glVertex3f( 1.0f, 1.0f, -2.0f );
		glEnd();

		glBegin( GL_TRIANGLE_STRIP );
		glColor4f( 0.0f, 0.0f, 1.0f, 0.25f );
			glVertex3f( -1.0f, -1.0f, 0.0f );
			glVertex3f( 1.0f, -1.0f, 0.0f );
			glVertex3f( -1.0f, -1.0f, -2.0f );
			glVertex3f( 1.0f, -1.0f, -2.0f );
		glEnd();
	glPopMatrix( );
}
