#pragma once
#include "afxcmn.h"
#include "AddTileSetDlg.h"
#include "afxwin.h"
#include <afxole.h>
#include <ResourceManagement/TextureManager/TextureManager.h>
#include "CoolButton.h"

#define IMAGELIST_TILE_X_DIMENSION		100
#define IMAGELIST_TILE_Y_DIMENSION		100
#define MAPASSETSFORM_HEIGHT			(IMAGELIST_TILE_Y_DIMENSION + 120)


// CMapAssetsForm form view

class CMapAssetsForm : public CFormView
{
	DECLARE_DYNCREATE(CMapAssetsForm)

protected:
	CMapAssetsForm();           // protected constructor used by dynamic creation
	virtual ~CMapAssetsForm();

	void addDefaultTileSets( );
	void addTileSet( TileSetType type, CString &path, bool isVerbose = true );
	void addTile( TileSetType type, CString &tilePath, ResourceManagement::TextureFormat format, bool isVerbose = true );
	int addTileToImageList( TileSetType type, int texture );

	CImageList m_TabImageList;
	CImageList m_TerrainImageList;
	CImageList m_EntityImageList;
	CImageList m_BuildingImageList;

	CTabCtrl m_AssetTabCtrl;
	CListCtrl m_TerrainTileListCtrl;
	CListCtrl m_EntityTileListCtrl;
	CListCtrl m_BuildingListCtrl;
	bool m_bCreated, m_bAfterInitialUpdate;
	CCoolButton m_AddTileSetBtn;
	COleDropSource m_DragAndDropSource;
    COleDataSource m_OleDataSource;

public:
	enum { IDD = IDD_MAPASSETSFORM };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()


public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	afx_msg void OnTcnSelchangeAssettab(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedAddtilesetButton();
	afx_msg void OnTcnSelchangeAssetTab(NMHDR *pNMHDR, LRESULT *pResult);

	virtual void OnInitialUpdate();
//	afx_msg void OnLvnItemActivateTerraintilelist(NMHDR *pNMHDR, LRESULT *pResult);
//	afx_msg void OnLvnBegindragEntitytilelist(NMHDR *pNMHDR, LRESULT *pResult);
//	afx_msg void OnLvnBegindragTerraintilelist(NMHDR *pNMHDR, LRESULT *pResult);
//	afx_msg void OnLvnBegindragBuildinglist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnBegindragTerraintilelist(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnDestroy();
};


