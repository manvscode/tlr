#pragma once
#ifndef _RENDERER_H_
#define _RENDERER_H_

#include <afxwin.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

class CRenderer
{
  public:
	static CRenderer *getInstance( );
	virtual ~CRenderer( );
	
	void render( );
	
  protected:
	CRenderer( );

		GLUquadric *pQuadric;
	static CRenderer *m_pInstance;

};

#endif