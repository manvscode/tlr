/*
*	StringConverter.h
*
*	A function object to convert wchar to char and char to wchar.
*/
#include "stdafx.h"
#include <cassert>
#include "StringConverter.h"

CStringConverter::CStringConverter(  bool toWideChar )
  : m_bConvertToWideChars(toWideChar), m_bMemoryFreed(true)
{
}

CStringConverter::~CStringConverter( )
{
	assert( m_bMemoryFreed ); // You didn't free your memory! Make sure you call freeMemory()
}


CHAR *CStringConverter::operator()( LPCWSTR wcString )
{
	m_bMemoryFreed = false;
	int theString_size = WideCharToMultiByte( CP_ACP, WC_NO_BEST_FIT_CHARS, wcString, -1, NULL, 0, NULL, NULL );
	PCHAR theString = new CHAR[ theString_size ];
	WideCharToMultiByte( CP_ACP, WC_NO_BEST_FIT_CHARS, wcString, -1, theString, theString_size, NULL, NULL );
	return theString;
}

WCHAR *CStringConverter::operator()( LPCSTR string )
{
	m_bMemoryFreed = false;
	int theString_size = MultiByteToWideChar( CP_ACP, MB_PRECOMPOSED, string, -1, NULL, NULL );
	PWCHAR theString = new WCHAR[ theString_size ];
	MultiByteToWideChar( CP_ACP, MB_PRECOMPOSED, string, -1, theString, theString_size );
	return theString;
}
