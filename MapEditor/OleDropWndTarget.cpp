// OleDropWndTarget.cpp : implementation file
//

#include "stdafx.h"
#include "MapEditor.h"
#include "OleDropWndTarget.h"
#include "TextureCtrl.h"

// COleDropWndTarget

IMPLEMENT_DYNAMIC(COleDropWndTarget, COleDropTarget)


COleDropWndTarget::COleDropWndTarget()
 : COleDropTarget( )
{
}

COleDropWndTarget::~COleDropWndTarget()
{
	Revoke( );
}


BEGIN_MESSAGE_MAP(COleDropWndTarget, COleDropTarget)
END_MESSAGE_MAP()



// COleDropWndTarget message handlers

DROPEFFECT COleDropWndTarget::OnDragEnter(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
	//return COleDropTarget::OnDragEnter(pWnd, pDataObject, dwKeyState, point);
	return DROPEFFECT_LINK;
}

void COleDropWndTarget::OnDragLeave(CWnd* pWnd)
{
	COleDropTarget::OnDragLeave(pWnd);
}

DROPEFFECT COleDropWndTarget::OnDragOver(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
	CRect rc;
	pWnd->GetClientRect( rc );

	if( !rc.PtInRect( point ) )
		return DROPEFFECT_NONE;


	return DROPEFFECT_LINK;  // move source
	//return COleDropTarget::OnDragOver(pWnd, pDataObject, dwKeyState, point);
}

BOOL COleDropWndTarget::OnDrop(CWnd* pWnd, COleDataObject* pDataObject,	DROPEFFECT dropEffect, CPoint point)
{
	HGLOBAL  hGlobal;
	unsigned int *pTextureId = NULL;                     


	if( (dropEffect & DROPEFFECT_LINK) != DROPEFFECT_LINK )
		return FALSE;

	// Get text data from COleDataObject
	hGlobal = pDataObject->GetGlobalData( CF_TEXT );

	// Get pointer to data
	pTextureId = (unsigned int *) GlobalLock( hGlobal );    
	ASSERT( pTextureId != NULL ); 

	
	//textureID += 2;

	static_cast<CTextureCtrl *>(pWnd)->setTexture( *pTextureId );

	// Unlock memory - Send dropped text into the "bit-bucket"
	GlobalUnlock( hGlobal );

	return TRUE;
	//return COleDropTarget::OnDrop(pWnd, pDataObject, dropEffect, point);
}
