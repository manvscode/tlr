#pragma once
#include "CoolButton.h"
#include "afxwin.h"

// CMissionObjectiveDlg dialog

class CMissionObjectiveDlg : public CDialog
{
	DECLARE_DYNAMIC(CMissionObjectiveDlg)

public:
	CMissionObjectiveDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMissionObjectiveDlg();

// Dialog Data
	enum { IDD = IDD_MISSIONBRIEFING_DIALOG };

protected:
	CCoolButton m_Buttons[ 3 ];
	CEdit m_MissionObjective;
	BITMAP m_bmpImage;
	BITMAP m_bmpSmallImage;
	HBITMAP m_hbmpMissionObjectiveImage;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CString getMissionObjective( );
	void setMissionObjective( CString &objective );
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBrowseButton();

	void loadBitmap( CString &file );
	void loadTarga( CString &file );
	void refreshMissionObjectiveImage( );

	CStatic m_MissionObjectiveImageCtrl;
	afx_msg void OnBnClickedCancel();
};
