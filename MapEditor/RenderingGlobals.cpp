#include "RenderingGlobals.h"
#include <GL/glew.h>
#include <GL/gl.h>

CRenderer *pRenderer = NULL;


void render( CDocument *doc )
{
	ASSERT( pRenderer != NULL );
	//cubeTest( );
	pRenderer->render( );
}

void cubeTest( )
{
	glPushAttrib( GL_CURRENT_BIT | GL_LIGHTING_BIT | GL_TEXTURE_BIT );
		//glDisable( GL_LIGHTING );
		//glDisable( GL_TEXTURE_2D );
	//glMaterialfv(GL_FRONT, GL_AMBIENT, ambientMaterial);

		glPushMatrix( );
			glTranslatef(0.0f, 1.0f, 1.0f);
			glBegin( GL_TRIANGLE_STRIP );
			glColor4f( 1.0f, 0.0f, 0.0f, 1.0f );
				glVertex3f( -1.0f, -1.0f, 0.0f );
				glVertex3f( 1.0f, -1.0f, 0.0f );
				glVertex3f( -1.0f, 1.0f, 0.0f );
				glVertex3f( 1.0f, 1.0f, 0.0f );
			glEnd();

			glBegin( GL_TRIANGLE_STRIP );
			glColor4f( 1.0f, 0.0f, 0.0f, 1.0f );
				glVertex3f( -1.0f, -1.0f, -2.0f );
				glVertex3f( 1.0f, -1.0f, -2.0f );
				glVertex3f( -1.0f, 1.0f, -2.0f );
				glVertex3f( 1.0f, 1.0f, -2.0f );
			glEnd();

			glBegin( GL_TRIANGLE_STRIP );
			glColor4f( 0.0f, 1.0f, 0.0f, 1.0f );
				glVertex3f( -1.0f, -1.0f, 0.0f );
				glVertex3f( -1.0f, -1.0f, -2.0f );
				glVertex3f( -1.0f, 1.0f, 0.0f );
				glVertex3f( -1.0f, 1.0f, -2.0f );
			glEnd();

			glBegin( GL_TRIANGLE_STRIP );
			glColor4f( 0.0f, 1.0f, 0.0f, 1.0f );
				glVertex3f( 1.0f, -1.0f, 0.0f );
				glVertex3f( 1.0f, -1.0f, -2.0f );
				glVertex3f( 1.0f, 1.0f, 0.0f );
				glVertex3f( 1.0f, 1.0f, -2.0f );
			glEnd();

			glBegin( GL_TRIANGLE_STRIP );
			glColor4f( 0.0f, 0.0f, 1.0f, 1.0f );
				glVertex3f( -1.0f, 1.0f, 0.0f );
				glVertex3f( 1.0f, 1.0f, 0.0f );
				glVertex3f( -1.0f, 1.0f, -2.0f );
				glVertex3f( 1.0f, 1.0f, -2.0f );
			glEnd();

			glBegin( GL_TRIANGLE_STRIP );
			glColor4f( 0.0f, 0.0f, 1.0f, 1.0f );
				glVertex3f( -1.0f, -1.0f, 0.0f );
				glVertex3f( 1.0f, -1.0f, 0.0f );
				glVertex3f( -1.0f, -1.0f, -2.0f );
				glVertex3f( 1.0f, -1.0f, -2.0f );
			glEnd();
		glPopMatrix( );
	glPopAttrib( );
}