// 2DMapEditor.h : main header file for the 2DMapEditor application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CMapEditorApp:
// See 2DMapEditor.cpp for the implementation of this class
//

class CMapEditorApp : public CWinApp
{
public:
	CMapEditorApp();
	CString MapEditorWndClass; 

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
public:
	virtual int ExitInstance();
};


typedef enum tagUpdateType {
	UPD_DEFAULT = 0,
	UPD_OPENGL = 1,
	UPG_MAPPROPERTIESFORM = 2,
	UPG_MAPASSETSFORM = 3,
	UPG_TEXTUREBRUSHES = 4
} UpdateType;

extern CMapEditorApp theApp;