// 2DMapEditorDoc.cpp : implementation of the CMapEditorDoc class
//

#include "stdafx.h"
#include "MapEditor.h"

#include "MapEditorDoc.h"
#include <Core.h>

//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif


// CMapEditorDoc

IMPLEMENT_DYNCREATE(CMapEditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CMapEditorDoc, CDocument)
END_MESSAGE_MAP()


// CMapEditorDoc construction/destruction


CMapEditorDoc::CMapEditorDoc()
{
	// TODO: add one-time construction code here
	m_pInstance = this;
	m_pSelectedObject = NULL;
	m_szMissionName = _T("");
	m_szMissionObjective = _T("");
	memset( &m_bmpMissionObjective, 0, sizeof(BITMAP) );

	m_TerrainTexturingDlg.Create( IDD_TERRAINTEXTURING_DIALOG, AfxGetMainWnd( ) );
	m_TextureLayer = 0;
	m_bRenderMapCellBoundaries = false;
}

CMapEditorDoc::~CMapEditorDoc()
{
	if( m_bmpMissionObjective.bmBits != NULL )
		delete [] m_bmpMissionObjective.bmBits;
}

BOOL CMapEditorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CMapEditorDoc serialization

void CMapEditorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CMapEditorDoc diagnostics

#ifdef _DEBUG
void CMapEditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMapEditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


CMapEditorDoc *CMapEditorDoc::getInstance( )
{
	return m_pInstance;
}

CMapEditorDoc *CMapEditorDoc::m_pInstance = NULL;
// CMapEditorDoc commands


Game::CMapSelectable *CMapEditorDoc::getSelectedObject( )
{ return m_pSelectedObject; }

void CMapEditorDoc::setSelectedObject( Game::CMapSelectable *selected )
{ m_pSelectedObject = selected; }

void CMapEditorDoc::setMissionName( CString &name )
{ m_szMissionName = name; }

CString &CMapEditorDoc::getMissionName( )
{ return m_szMissionName; }

CString CMapEditorDoc::getMissionName( ) const
{ return m_szMissionName; }

void CMapEditorDoc::setMissionObjective( CString &objective )
{ m_szMissionObjective = objective; }

CString &CMapEditorDoc::getMissionObjective( )
{ return m_szMissionObjective; }

CString CMapEditorDoc::getMissionObjective( ) const
{ return m_szMissionObjective; }

void CMapEditorDoc::setMissionObjectiveBitmap( BITMAP &bmp )
{ m_bmpMissionObjective = bmp; }

BITMAP &CMapEditorDoc::getMissionObjectiveBitmap( )
{ return m_bmpMissionObjective; }

BITMAP CMapEditorDoc::getMissionObjectiveBitmap( ) const
{ return m_bmpMissionObjective; }

CTerrainTexturingDlg & CMapEditorDoc::getTerrainTexturingDlg( )
{ return m_TerrainTexturingDlg; }

void CMapEditorDoc::setTextureLayers( CArray<unsigned int> &textureLayers )
{
	m_TextureLayers.RemoveAll( );
	m_TextureLayers.Append( textureLayers );

	// set these textures in TLR Core...
	for( unsigned int layer = 0; layer < Game::MAX_TEXTURE_LAYERS && layer < m_TextureLayers.GetCount( ); layer++ )
		Core::pMap->setTexture( layer, m_TextureLayers[ layer ] );
}

const CArray<unsigned int> &CMapEditorDoc::getTextureLayers( ) const
{ return m_TextureLayers; }




void CMapEditorDoc::setCurrentTextureLayer( unsigned int layer )
{ m_TextureLayer = layer; }

unsigned int CMapEditorDoc::getCurrentTextureLayer( ) const
{ return m_TextureLayer; }




void CMapEditorDoc::setRenderBoundaryMode( bool bRenderBounderies )
{ m_bRenderMapCellBoundaries = bRenderBounderies; }