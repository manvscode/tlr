#include <windows.h>
#include <tchar.h>
#include "mfc_util.h"

void ShowLastError( )
{
	LPVOID lpMsgBuf; // Default language:
	DWORD err = GetLastError();
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, err, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
	MessageBox(NULL, (LPTSTR)lpMsgBuf, _T("Error Information"), MB_OK|MB_ICONINFORMATION);
	LocalFree(lpMsgBuf);
}